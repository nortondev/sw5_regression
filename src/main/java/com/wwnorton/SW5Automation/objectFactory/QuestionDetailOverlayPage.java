package com.wwnorton.SW5Automation.objectFactory;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class QuestionDetailOverlayPage {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@id='question-detail-pdf-container']//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Average score')]/following-sibling::*")
	public WebElement OverallScore;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info btn-title-reset ml15 lato-regular-14']/span[contains(text(),'EDIT')]")
	public WebElement EditButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info lato-regular-14']/span[contains(text(),'RESET ALL RESULTS')]")
	public WebElement ResetAllResultButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s lato-regular-14 fl btn-info ml15']/sapn[contains(text(),'RESET')]")
	public WebElement ResetButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary fr lato-regular-14']/span[contains(text(),'RESET')]")
	public WebElement ResetButton_PopUp;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET ALL RESULTS')]")
	public WebElement PopUpRESETALLRESULTSButton;

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Average score')]/following-sibling::td")
	public WebElement QuestionDetailAverageScore;

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Average time spent')]/following-sibling::td")
	public WebElement QuestionDetailAverageTimeSpent;

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Student submissions')]/following-sibling::td")
	public WebElement QuestionDetailStudentSubmission;

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Item ID')]/following-sibling::td")
	public WebElement QuestionDetailItemID;

	@FindBy(how = How.XPATH, using = "//span[@class='qd-full-credit cantSelect' and contains(text(),'Give credit to all Students')]")
	public WebElement QuestionDetailGiveCredit;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[@class='btn-submit-s btn-primary fr lato-regular-14']/span[contains(text(),'GIVE FULL CREDIT')]")
	public WebElement QuestionDetailGiveFullCreditButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-act lato-regular-14 btn-primary btn-yellow'][span[contains(text(),'SAVE')]]")
	public WebElement QuestionDetailSaveButton;

	@FindBy(how = How.XPATH, using = "//div[@id='question-detail-pdf-container']/div/button[@class='close']")
	public WebElement CloseQuestionDetailOverlay;

	// Initializing Web Driver and PageFactory.
	public QuestionDetailOverlayPage() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

		waitForLoadPageElements();
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			wait.until(ExpectedConditions.elementToBeClickable(CloseQuestionDetailOverlay));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Reset All results Button,  Method: {method} ")
	public void clickResetAllResultsButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(ResetAllResultButton));
		ResetAllResultButton.click();
	}

	@Step("Click on Reset Button,  Method: {method} ")
	public void clickResetButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//button[@class='btn-submit-s lato-regular-14 fl btn-info ml15']/sapn[contains(text(),'RESET')]")));
		ResetButton.click();
	}
	
	@Step("Click on Reset Button,  Method: {method} ")
	public void clickResetButton_PopUp() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//button[@class='btn-submit-s btn-primary fr lato-regular-14']/span[contains(text(),'RESET')]")));
		ResetButton_PopUp.click();
	}

	@Step("Click on ResetAll Button on Pop Up,  Method: {method} ")
	public void clickResetAllResultsButtonOnPopUp() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET ALL RESULTS')]")));
		PopUpRESETALLRESULTSButton.click();
	}

	@Step("Click on Give Credit All Student link,  Method: {method} ")
	public void clickGiveCreditAllStudentLink() throws Exception {
		Actions builder = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(QuestionDetailGiveCredit));
		builder.moveToElement(QuestionDetailGiveCredit).build().perform();
		//ReusableMethods.moveToElement(driver, QuestionDetailGiveCredit);
		QuestionDetailGiveCredit.click();
	}

	@Step("Click on Give Credit Full Button on Pop Up k,  Method: {method} ")
	public void clickGiveCreditButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(QuestionDetailGiveFullCreditButton));
		QuestionDetailGiveFullCreditButton.click();
	}
	

	@Step("Get Overall Score for student,  Method: {method} ")
	public String getOverallScore() {
		return OverallScore.getText();
	}

	@Step("Total of time spent for each question in Time Spent column,  Method: {method}")
	public List<String> getdataTimeSpentColumn() {
		// No of columns
		List<String> retVal = new ArrayList<String>();
		WebElement table = driver.findElement(By.xpath("//tbody[@class='stuDetailTabShow']"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		for (WebElement allrows : rows) {
			List<WebElement> columns = allrows.findElements(By.xpath("//td[@class='text-center textIndentLeft']"));
			for (int i = 0; i < columns.size(); i++) {
				if (i == 4) {
					String timeSpentCol = columns.get(i).getText();

					retVal.add(timeSpentCol);

				}

			}
		}
		return retVal;

	}

	@Step("Get Student Detail Overlay Time Spent Data,  Method: {method} ")
	public List<String> getStudentDetailTimeSpentData() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentTimeSpentList = driver.findElements(
				By.xpath("//tbody[@class='stuDetailTabShow']/tr/td[5][@class='text-center textIndentLeft']"));
		int timelength = studentTimeSpentList.size();
		if (timelength > 0) {
			for (WebElement studentTimeSpent : studentTimeSpentList) {
				retVal.add(studentTimeSpent.getText());
			}
		}
		return retVal;
	}

	@Step("Click the Circle Arrow on Student Detail,  Method: {method} ")
	public void getStudentDetailCircleArrow(int position) throws Exception {

		List<WebElement> studentCircleArrowList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td/span[@class='glyphicons glyphicons-circle-arrow-right cursor-pointer fr']"));
		int timelength = studentCircleArrowList.size();
		for (int i = 0; i < timelength; i++) {
			WebElement clickCursor = studentCircleArrowList.get(i);
			if ((i == position) && (clickCursor.isEnabled())) {
				clickCursor.click();
			} else {
				boolean arrowDisabled = driver.findElement(By.xpath(
						"//tbody[@class='stuDetailTabShow']/tr/td/span[@class='glyphicons glyphicons-circle-arrow-right cursor-pointer fr']"))
						.isEnabled();
				Assert.assertTrue(arrowDisabled, "The Arrow is Disabled");
			}

		}

	}

	@Step("Instructor Click the Edit Button on Student Detail Page,  Method: {method} ")
	public void clickEditButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//button[@class='btn-submit-s btn-info btn-title-reset ml15 lato-regular-14']")));
		if (EditButton.isEnabled()) {
			EditButton.click();

		}
	}

	@Step("Check the Reset Button on Edit Mode and Reset the question,  Method: {method} ")
	public void getResetQuestiononEditMode(int questionNumber) throws Exception {

		List<WebElement> studentQuestionresetList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[@class='text-center textIndentLeft']/button[@class='btn-submit-s btn-info reset']"));
		int resetButtons = studentQuestionresetList.size();
		for (int i = 0; i < resetButtons; i++) {
			WebElement clickResetButton = studentQuestionresetList.get(i);
			if ((i == questionNumber) && (clickResetButton.isEnabled())) {
				clickResetButton.click();
				break;
			}
		}
	}

	@Step("Updated the Student Score ob Edit Mode ,  Method: {method} ")
	public void updateStudentScoreOnEditMode(int updateQuestionNumber, String score) throws Exception {

		List<WebElement> studentScoreList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[2][@class='text-center textIndentSec']/div[@type='text']/input"));
		int scoreSize = studentScoreList.size();

		for (int i = 0; i < scoreSize; i++) {
			WebElement updateQuestionScore = studentScoreList.get(i);
			if (i == updateQuestionNumber) {
				updateQuestionScore.clear();
				updateQuestionScore.sendKeys(score);
				break;
			}

		}
	}

	@Step("Get Average score on Question details,  Method: {method} ")
	public String getQuestiondetailAverageScore() throws Exception {
		return QuestionDetailAverageScore.getText();
	}

	@Step("Get Average time Spent on Question details,  Method: {method} ")
	public String getQuestiondetailAverageTimeSpent() throws Exception {
		return QuestionDetailAverageTimeSpent.getText();
	}

	@Step("Get Student Submission on Question details,  Method: {method} ")
	public String getQuestiondetailStudentSubmission() throws Exception {
		return QuestionDetailStudentSubmission.getText();
	}

	@Step("Get Item ID on Question details,  Method: {method} ")
	public String getQuestiondetailItemID() throws Exception {
		return QuestionDetailItemID.getText();
	}

	@Step("Get Question Detail Overlay List of Student,  Method: {method} ")
	public List<String> getQuestiondetailListOfStudent() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> listOfStudents = driver.findElements(By.xpath(
				"//table[@class='question-detail-table table table-striped table-condensed table-hover']/tbody[@class='stuDetailTabShow']/tr[@class='question-detail-type']/td[1][@class='textIndentLeft']"));
		int studentList = listOfStudents.size();
		if (studentList > 0) {
			for (WebElement studentlist : listOfStudents) {
				retVal.add(studentlist.getText());
			}
		}
		return retVal;
	}

	@Step("Enter value in score texbox of QuestionDetailOverlayPopup,  Method: {method} ")
	public void enterScoreQuestionDetailOverlayPopup(int questionNumber, String score) throws Exception {
		if (questionNumber > 0) {
			List<WebElement> scoreInputs = driver.findElements(By.xpath(
					"//div[@id='question-detail-pdf-container']//tbody[@class='stuDetailTabShow']/tr/td[2]//input[@type='text']"));
			if (scoreInputs.size() > 0) {
				scoreInputs.get(questionNumber - 1).sendKeys(score);
			}
		}
	}

	@Step("Click on QuestionDetail Save Button,  Method: {method} ")
	public void clickQuestionDetailSaveButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(QuestionDetailSaveButton));
		QuestionDetailSaveButton.click();
		Thread.sleep(10000);
	}

	@Step("Click on close QuestionDetailOverlay button,  Method: {method} ")
	public void clickCloseQuestionDetailOverlay() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(CloseQuestionDetailOverlay));
			CloseQuestionDetailOverlay.click();
			
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
}
