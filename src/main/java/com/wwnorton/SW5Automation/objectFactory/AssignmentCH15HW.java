package com.wwnorton.SW5Automation.objectFactory;



import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH15HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH15HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	
	@Step("Drag and drop Correct image icons for question #1,  Method: {method} ")
	public void question1DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
		WebElement dragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
		

		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_")));
	
		
		sqp = new StudentQuestionPlayer();
		if(sqp.isAttemptViewDisplayed("Practice Attempt")) {
			
			ReusableMethods.scrollToElement(driver, By.id("item-answer-1-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_"));
		
			// Enter Correct Answer
				// Dragged and dropped.
				ReusableMethods.moveToElement(driver, dragItem1Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element, dropContainer1Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem4Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element, dropContainer1Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem2Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element, dropContainer2Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem5Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem5Element, dropContainer2Element).build().perform();	
				
		}
		
		else {
			
				ReusableMethods.scrollToElement(driver, By.id("categorySubContainer-1-7740780020bdeaa6bb7f086003f078f9de2221d7c106478_"));
			
				// Enter Correct Answer
				// Dragged and dropped.
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element, dropContainer1Element).build().perform();
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element, dropContainer1Element).build().perform();
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element, dropContainer2Element).build().perform();
				Thread.sleep(2000);
				act.dragAndDrop(dragItem5Element, dropContainer2Element).build().perform();	
		}

		
	}
	
	
	@Step("Select Correct Answers for question #1,  Method: {method} ")
	public void question1MultipleParts_CorrectAnswer() throws Exception {
		
		//Part 1
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_checkbox_div4034994804bdeaa6bb7f086003f078f9de2221d7c107540_"));
		               
		List<WebElement> Elements = driver.findElements(By.xpath(
				"//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='1']"));

		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", Elements.get(0));
		
		//Part 2
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_checkbox_div4047356000bdeaa6bb7f086003f078f9de2221d7c100947_"));
		
	
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", Elements.get(1));
		
		//Part 3
		ReusableMethods.scrollToElement(driver, 
						By.id("wafer_ps_radio_div4047894612bdeaa6bb7f086003f078f9de2221d7c104826_"));
				
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", Elements.get(2));
		
	
		//Part 3
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_radio_div4048430613bdeaa6bb7f086003f078f9de2221d7c107813_"));
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", Elements.get(3));
				
	}


	@Step("Drag and drop Incorrect image icons for question #2,  Method: {method} ")
	public void question2DragAndDrop_IncorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
				
				WebElement dragItem2Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-2-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));				
				WebElement dragItem5Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-5-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dragItem3Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-3-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dragItem4Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-4-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dragItem1Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-1-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				

				// Element on which need to drop.
				WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("itemContent-sorted-2-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("itemContent-sorted-5-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("itemContent-sorted-3-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("itemContent-sorted-4-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				WebElement dropContainer5Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("itemContent-sorted-1-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_")));
				
				// Enter Correct Answer
				// Dragged and dropped.
				
				ReusableMethods.scrollToElement(driver, By.id("itemContent-sorted-3-07004362630cd9d0f1ccb4b38dfecf49186b4546db08897_"));
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem3Element, dropContainer1Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem5Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem5Element, dropContainer2Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem2Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element, dropContainer3Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem4Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element, dropContainer5Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem1Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element, dropContainer4Element).build().perform();
		
	}
	
	@Step("Drag and drop Incorrect image icons for question #2,  Method: {method} ")
	public void question6DragAndDrop_IncorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
				
				WebElement dragItem1Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-1-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));				
				WebElement dragItem2Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-2-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				WebElement dragItem3Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-3-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				WebElement dragItem4Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-4-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				                                           
				//WebElement dragItem5Element = wait.until(ExpectedConditions
				//		.visibilityOfElementLocated(By.id("item-answer-5-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				

				// Element on which need to drop.
				WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-1-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-2-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				WebElement dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-3-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
				WebElement dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-4-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_")));
							   
				
				// Enter Correct Answer
				// Dragged and dropped.
				
				ReusableMethods.scrollToElement(driver, By.id("sortingPreviewAnswerItemList-3902087035bdeaa6bb7f086003f078f9de2221d7c100469_"));
				                                               
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element, dropContainer2Element).build().perform();
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element, dropContainer3Element).build().perform();
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem3Element, dropContainer1Element).build().perform();
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element, dropContainer4Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem1Element);
		
	}
	
	
	@Step("Drag and drop Incorrect image icons for question #2,  Method: {method} ")
	public void question3DragAndDrop_IncorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Part 1 - Element which needs to drag.
				
				WebElement part1_dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				WebElement part1_dragItem2Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-2-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));				
				WebElement part1_dragItem3Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-3-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				WebElement part1_dragItem4Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-4-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				

				// Part 1 - Element on which need to drop.
				WebElement part1_dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-1-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				WebElement part1_dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-2-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				WebElement part1_dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-3-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				WebElement part1_dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-4-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_")));
				
				
				// Part 1 - Enter Correct Answer
				// Dragged and dropped.
				
				ReusableMethods.scrollToElement(driver, By.id("categorySubContainer-1-9111231119bdeaa6bb7f086003f078f9de2221d7c101009_"));
				
				Thread.sleep(2000);
				act.dragAndDrop(part1_dragItem1Element, part1_dropContainer2Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part1_dragItem2Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part1_dragItem2Element, part1_dropContainer1Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part1_dragItem3Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part1_dragItem3Element, part1_dropContainer3Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part1_dragItem4Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part1_dragItem4Element, part1_dropContainer4Element).build().perform();
				
				
				// Part 2 - Element which needs to drag.
				
				WebElement part2_dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));				
				WebElement part2_dragItem3Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-3-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				WebElement part2_dragItem4Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-4-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				WebElement part2_dragItem5Element = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-5-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				

				// Part 1 - Element on which need to drop.
				WebElement part2_dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-1-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				WebElement part2_dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-2-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				WebElement part2_dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-3-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				WebElement part2_dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("subAnswerCategoryList-4-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_")));
				
				
				// Part 1 - Enter Correct Answer
				// Dragged and dropped.
				
				ReusableMethods.scrollToElement(driver, By.id("categorySubContainer-1-9113330289bdeaa6bb7f086003f078f9de2221d7c101355_"));
				
				Thread.sleep(2000);
				act.dragAndDrop(part2_dragItem1Element, part2_dropContainer2Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part2_dragItem3Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part2_dragItem3Element, part2_dropContainer3Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part2_dragItem4Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part2_dragItem4Element, part2_dropContainer1Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", part2_dragItem5Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(part2_dragItem5Element, part2_dropContainer4Element).build().perform();
		
	}
	
	@Step("Enter Incorrect answer for question #2,  Method: {method} ")
	public void question11_IncorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		WebElement answerElement = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number12937024090cd9d0f1ccb4b38dfecf49186b4546db04068_")));
		                                          
		Actions act = new Actions(driver);
		Thread.sleep(2000);
		act.moveToElement(answerElement);
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answerElement);
		((JavascriptExecutor) driver).executeScript("arguments[0].value='4';", answerElement);
		
	}


}