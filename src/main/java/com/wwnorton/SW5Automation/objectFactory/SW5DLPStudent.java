package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.DateTimeHelper;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class SW5DLPStudent {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']//img")
	WebElement gearMenuIcon;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Add Yourself to a Student Set')]")
	WebElement addToStudentSet_MenuItem;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement StudentSet;

	@FindBy(how = How.ID, using = "student_active_class_menu")
	public WebElement StudentSetName;

	@FindBy(how = How.XPATH, using = "//div[@id=\"activity_list_table_wrapper\"]/table/tbody")
	public WebElement SelectAssignment;

	@FindBy(how = How.XPATH, using = "//button[@id='swfb_no_student_set_btn']")
	public WebElement JoinStudentSetButton;

	@FindBy(how = How.XPATH, using = "//input[@id='class_registration_id']")
	public WebElement StudentSetIDTextBox;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button[span[contains(text(),'OK')]]")
	public WebElement JoinStudentSetOkButton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='login_button']")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	WebElement Signin_or_Register;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button'][//span[@id='gear_button_username']]")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	// Initializing Web Driver and PageFactory.

	public SW5DLPStudent() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Select StudentSet by Title on SW5 DLP page,  Method: {method} ")
	public void selectSSByTitle(String SSTitle) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByVisibleText(SSTitle);

	}

	@Step("Select StudentSet by Id on SW5 DLP page,  Method: {method} ")
	public void selectSSByValue(String SSId) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByValue(SSId);

	}

	@Step("Select Student Set by option Value on SW5 DLP page,  Method: {method} ")
	public Boolean selectByPartOfVisibleText(String value) {
		Boolean isFound = false;
		try {
			if (StudentSetName.isDisplayed()) {
				List<WebElement> optionElements = StudentSetName.findElements(By.tagName("option"));

				for (WebElement optionElement : optionElements) {
					if (optionElement.getText().contains(value)) {

						System.out.println(optionElement.getText());
						optionElement.click();
						isFound = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return isFound;
	}

	@Step("Join Student by given Student Set Id on SW5 DLP page,  Method: {method} ")
	public void joinStudentSet(String studentSetID) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			if (JoinStudentSetButton.isDisplayed()) {
				JoinStudentSetButton.click();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(JoinStudentSetOkButton));

				StudentSetIDTextBox.clear();
				StudentSetIDTextBox.sendKeys(studentSetID);

				JoinStudentSetOkButton.click();
				Thread.sleep(2000);
				JoinStudentSetOkButton.click();// Ok Button on alert popup
				Thread.sleep(2000);
				// SelectAssignment
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody")));
			}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Add Student to Student Set on SW5 DLP page,  Method: {method} ")
	public void addStudentToStudentSet(String studentSetID) {

		try {
				WebDriverWait wait = new WebDriverWait(driver, 50);
				Thread.sleep(3000);
				gearMenuIcon.click();
				wait.until(ExpectedConditions.visibilityOf(addToStudentSet_MenuItem));
				addToStudentSet_MenuItem.click();
				StudentSetIDTextBox.clear();
				StudentSetIDTextBox.sendKeys(studentSetID);
				JoinStudentSetOkButton.click();
				
				Thread.sleep(2000);
				JoinStudentSetOkButton.click();// Ok Button on alert popup
	
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	

	@Step("Select assignment by name on SW5 DLP page,  Method: {method} ")
	public void SelectAssignment(String assignmentName) {
		// List<WebElement> links = SelectAssignment.findElements(By.tagName("a"));

		// for (int i = 0; i < links.size(); i++) {

		// List<WebElement> linksText =
		// links.get(i).findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		List<WebElement> linksText = SelectAssignment.findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		for (int j = 0; j < linksText.size(); j++) {

			String AssignmentText = linksText.get(j).getText();
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				ReusableMethods.scrollIntoView(driver, linksText.get(j));
				//linksText.get(j).click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", linksText.get(j));
			}
		}
		// }
	}
	
	@Step("Scroll To AssignmentTitle,  Method: {method} ")
	public void scrollToAssignmentTitle(String assignmentTitle) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]")));
		
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]"));
		
	}

	@Step("Check if assignment is visible on SW5 DLP page,  Method: {method} ")
	public boolean IsAssignmentVisible(String assignmentName) {
		// List<WebElement> links = SelectAssignment.findElements(By.tagName("a"));

		// for (int i = 0; i < links.size(); i++) {

		// List<WebElement> linksText =
		// links.get(i).findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		List<WebElement> linksText = SelectAssignment.findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		for (int j = 0; j < linksText.size(); j++) {

			String AssignmentText = linksText.get(j).getText();
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				return true;
			}
		}
		// }
		return false;
	}

	@Step("Select assignment by name on SW5 DLP page,  Method: {method} ")
	public boolean IsAssignmentHaveGAU(String assignmentName) {
		// List<WebElement> links = SelectAssignment.findElements(By.tagName("a"));

		// for (int i = 0; i < links.size(); i++) {

		// List<WebElement> linksText =
		// links.get(i).findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		List<WebElement> linksText = SelectAssignment.findElements(By.xpath("//tr/td[@class=' title_td']/a"));
		List<WebElement> linksGAU = SelectAssignment.findElements(By.xpath("//tr/td[@class=' gau_td']/span"));
		for (int j = 0; j < linksText.size(); j++) {

			String AssignmentText = linksText.get(j).getText();
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {

				String AssignmentGAU = linksGAU.get(j).getText();

				if (DateTimeHelper.IsValidDateTime(AssignmentGAU, "MM/dd/yy HH:mm a"))
					return true;
			}
		}
		// }
		return false;
	}
	
	@Step("Get Student Grade  by Assignment name on SW5 Student DLP page,  Method: {method} ")
	public String getAssignmentGradeOnStudentDLP(String assignmentName) {
		
		String AssignmentGrade = null;
		
		List<WebElement> linksText = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' title_td']/a"));
		
		List<WebElement> linksGrade = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' grade_td']"));
		
		for (int j = 0; j < linksText.size(); j++) {

			String AssignmentText = linksText.get(j).getText();
			
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				
				AssignmentGrade = linksGrade.get(j).getText();
			}
		}
		
		return AssignmentGrade;
	
	}
	
	@Step("Get Extra Credit by Assignment name on SW5 Student DLP page,  Method: {method} ")
	public String getExtraCreditOnStudentDLP(String assignmentName) {
		
		String extraCreditText = null;
		//String updatedAssignmentName = assignmentName + " " + "(Extra Credit)";
		
		List<WebElement> linksText = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' title_td']/a"));
		
		List<WebElement> extraCreditTitles = SelectAssignment.findElements
				(By.xpath("//span[@class='extra_credit_assignment_title' and contains(text(),' (Extra Credit)')]"));
		
		for (int i = 0; i < linksText.size(); i++) {

			String AssignmentText = linksText.get(i).getText();
			
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				
				for (int j = 0; j < extraCreditTitles.size(); j++) {
				
					extraCreditText = extraCreditTitles.get(j).getText();
					
				}
			}
		}
		
		return extraCreditText;
	
	}


	@Step("Logout Smartwork5,  Method: {method} ")
	public void logoutSmartwork5() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(gear_button_username));
		gear_button_username.click();
		wait.until(ExpectedConditions.elementToBeClickable(SignOut_link));
		SignOut_link.click();
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(loginButton),
				ExpectedConditions.elementToBeClickable(Signin_or_Register)));
	}
}
