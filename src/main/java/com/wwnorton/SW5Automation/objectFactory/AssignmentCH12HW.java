package com.wwnorton.SW5Automation.objectFactory;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH12HW {

	WebDriver driver;
	Actions act;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH12HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	@Step("Drag And Drop InCorrect Answer for question #1,  Method: {method} ")
	public void question1CorrectAnswer() throws Exception {
		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_"));

		// Element which needs to drag.
		WebElement DragItem1Element_Red = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement DragItem2Element_Green = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement DragItem3Element_Brown = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement DragItem4Element_GreenBlue = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement DragItem5Element_GreenRed = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement DragItem6Element_White = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));

		// Element on which need to drop.
		WebElement iconic = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement molecular = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));
		WebElement metallic = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-3-0820266384bdeaa6bb7f086003f078f9de2221d7c100770_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem1Element_Red);
		act.dragAndDrop(DragItem1Element_Red, molecular).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem2Element_Green);
		act.dragAndDrop(DragItem2Element_Green, molecular).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem3Element_Brown);
		act.dragAndDrop(DragItem3Element_Brown, metallic).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem4Element_GreenBlue);
		act.dragAndDrop(DragItem4Element_GreenBlue, iconic).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem5Element_GreenRed);
		act.dragAndDrop(DragItem5Element_GreenRed, iconic).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem6Element_White);
		act.dragAndDrop(DragItem6Element_White, metallic).build().perform();

	}

	@Step("Enter Correct Answer for question #2,  Method: {method} ")
	public void enterQuestion2_Correct() throws Exception {

		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		//JavascriptExecutor js = (JavascriptExecutor) driver;

		// # Part - 1
		
		//js.executeScript("window.scrollBy(0,500)");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		
		// In Correct answer
		WebElement IncorrectAnswer1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'K')]]//input[@value='3' and not(@disabled)]")));
		WebElement IncorrectAnswer2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'I')]]//input[@value='5' and not(@disabled)]")));


		// Enter Correct Answer
		act.moveToElement(IncorrectAnswer1);
		//IncorrectAnswer1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", IncorrectAnswer1);
		
		act.moveToElement(IncorrectAnswer2);
		//IncorrectAnswer2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", IncorrectAnswer2);

		// Correct answer
		WebElement correctAnswer1 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'K')]]//input[@value='1' and not(@disabled)]")));
		WebElement correctAnswer2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'I')]]//input[@value='2' and not(@disabled)]")));

		
		// Enter Correct Answer
		act.moveToElement(correctAnswer1);
		//correctAnswer1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswer1);
		
		act.moveToElement(correctAnswer2);
		//correctAnswer2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswer2);

	}
	
	@Step("Enter Correct Answer for question #2,  Method: {method} ")
	public void enterQuestion2_InCorrect() throws Exception {

		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		JavascriptExecutor js = (JavascriptExecutor) driver;

		// # Part - 1
		js.executeScript("window.scrollBy(0,500)");

		// In Correct answer
		WebElement correctAnswer1 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'K')]]//input[@value='3' and not(@disabled)]")));
		WebElement correctAnswer2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'I')]]//input[@value='5' and not(@disabled)]")));

		act.moveToElement(correctAnswer1);
		act.moveToElement(correctAnswer1);

		// Enter Correct Answer
		correctAnswer1.click();
		correctAnswer2.click();

		// # Part - 2
		js.executeScript("window.scrollBy(0,1500)");

		// Correct answer
		WebElement correctAnswerA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'a “sea” of electrons')]]//input[@value='3' and not(@disabled)]")));
		WebElement correctAnswerB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'the positive Pt nucleus')]]//input[@value='4' and not(@disabled)]")));

		act.moveToElement(correctAnswerA);
		act.moveToElement(correctAnswerB);

		// Enter Correct Answer
		correctAnswerA.click();
		correctAnswerB.click();

	}

	@Step("Enter Answer for question #2,  Method: {method} ")
	public void enterQuestion2(boolean submitCorrect) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Terms
		WebElement valueA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-1']")));
		WebElement valueB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-2']")));
		WebElement valueD = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-4']")));
		WebElement valueE = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-5']")));

		// Terms container.
		WebElement containerA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_-1")));
		WebElement containerB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_-2")));
		WebElement containerC = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_-3")));
		WebElement containerD = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_-4")));

		// Reset answer
		WebElement removeAnswer = wait
				.until(ExpectedConditions.elementToBeClickable(By
						.id("removeAnswer-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_")));
		ReusableMethods
				.scrollToElement(
						driver,
						By.id("removeAnswer-7322144901bb7f8a1da6ba3ef0e4be867f7f09ac2308737_"));

		// Drag and drop terms
		if (submitCorrect) {
			Thread.sleep(2000);
			// Remove/Reset current selection
			ReusableMethods.scrollIntoView(driver, removeAnswer);
			removeAnswer.click();
			Thread.sleep(2000);
			act.dragAndDrop(valueA, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueD, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueE, containerC).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerD).build().perform();
		} else {
			act.dragAndDrop(valueA, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueD, containerC).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueE, containerD).build().perform();
		}
	}
	
	
	@Step("Select Correct Answers for question #3,  Method: {method} ")
	public void question3SelectCorrectAnswer() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		//Part 1
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_select_div576343115246cdeba34ad9dd3895e73d3972b368c508096_"));
	    Thread.sleep(2000);

		WebElement dropDown_Allotrope_A = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576343115246cdeba34ad9dd3895e73d3972b368c508096_")));
		
		//dropDown_Allotrope_A.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_Allotrope_A);
		
		WebElement dropdownSelectOption1 = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576343115246cdeba34ad9dd3895e73d3972b368c508096_']/li[contains(text(),'tetrahedral')]"));
		
		//dropdownSelectOption1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOption1);
		
		WebElement dropDown_Allotrope_B = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576345813446cdeba34ad9dd3895e73d3972b368c500562_")));
		
		//dropDown_Allotrope_B.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_Allotrope_B);
		
		WebElement dropdownSelectOption2 = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576345813446cdeba34ad9dd3895e73d3972b368c500562_']/li[contains(text(),'trigonal planar')]"));
		
		//dropdownSelectOption2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOption2);
		
		//Part 2
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_select_div576374528246cdeba34ad9dd3895e73d3972b368c507840_"));
	    Thread.sleep(2000);
	    
	    WebElement dropDown_1 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576374528246cdeba34ad9dd3895e73d3972b368c507840_")));
	    
	    //dropDown_1.click();
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_1);
		
	    WebElement dropdownSelectOptionA = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576374528246cdeba34ad9dd3895e73d3972b368c507840_']/li[contains(text(),'Allotrope B')]"));
		
		//dropdownSelectOptionA.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionA);
		
		WebElement dropDown_2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576376295346cdeba34ad9dd3895e73d3972b368c507512_")));
		
		//dropDown_2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_2);
		
		WebElement dropdownSelectOptionB = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576376295346cdeba34ad9dd3895e73d3972b368c507512_']/li[contains(text(),'Allotrope A')]"));
		
		//dropdownSelectOptionB.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionB);
		
		WebElement dropDown_3 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576377989246cdeba34ad9dd3895e73d3972b368c505555_")));
		
		//dropDown_3.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_3);
		
		WebElement dropdownSelectOptionC = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576377989246cdeba34ad9dd3895e73d3972b368c505555_']/li[contains(text(),'Allotrope B')]"));
		
		//dropdownSelectOptionC.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionC);
		
	}
	
	@Step("Select InCorrect Answers for question #3,  Method: {method} ")
	public void question3SelectInCorrectAnswer() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		//Part 1
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_select_div576343115246cdeba34ad9dd3895e73d3972b368c508096_"));
	    Thread.sleep(2000);

		WebElement dropDown_Allotrope_A = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576343115246cdeba34ad9dd3895e73d3972b368c508096_")));
		
		//dropDown_Allotrope_A.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_Allotrope_A);
		
		WebElement dropdownSelectOption1 = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576343115246cdeba34ad9dd3895e73d3972b368c508096_']/li[contains(text(),'linear')]"));
		
		//dropdownSelectOption1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOption1);
		
		WebElement dropDown_Allotrope_B = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576345813446cdeba34ad9dd3895e73d3972b368c500562_")));
		
		//dropDown_Allotrope_B.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_Allotrope_B);
		
		WebElement dropdownSelectOption2 = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576345813446cdeba34ad9dd3895e73d3972b368c500562_']/li[contains(text(),'tetrahedral')]"));
		
		//dropdownSelectOption2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOption2);
		
		//Part 2
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_select_div576374528246cdeba34ad9dd3895e73d3972b368c507840_"));
	    Thread.sleep(2000);
	    
	    WebElement dropDown_1 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576374528246cdeba34ad9dd3895e73d3972b368c507840_")));
	    
	   // dropDown_1.click();
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_1);
		
		WebElement dropdownSelectOptionA = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576374528246cdeba34ad9dd3895e73d3972b368c507840_']/li[contains(text(),'Allotrope B')]"));
		
		//dropdownSelectOptionA.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionA);
		
		WebElement dropDown_2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576376295346cdeba34ad9dd3895e73d3972b368c507512_")));
		
		//dropDown_2.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_2);
		
		WebElement dropdownSelectOptionB = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576376295346cdeba34ad9dd3895e73d3972b368c507512_']/li[contains(text(),'Allotrope A')]"));
		
		//dropdownSelectOptionB.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionB);
		
		WebElement dropDown_3 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_select576377989246cdeba34ad9dd3895e73d3972b368c505555_")));
		
		//dropDown_3.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDown_3);
		
		WebElement dropdownSelectOptionC = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option576377989246cdeba34ad9dd3895e73d3972b368c505555_']/li[contains(text(),'Allotrope B')]"));
		
		//dropdownSelectOptionC.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelectOptionC);
		
	}
	
	@Step("Enter Answer for question #4,  Method: {method} ")
	public void Question4_InCorrectAnswer() throws Exception {
		
		Actions builder = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Part 1
		ReusableMethods.scrollToElement(driver,
				By.id("answerItemList-39676469663c4a0055bb2da4257d6df04c95ce0b5902102_"));
		
		Thread.sleep(2000);
		
		WebDriverWait t = new WebDriverWait(driver, 10); 
		WebElement element1 = driver.findElement(By.xpath("//ul/li[contains(@id,'item-answer-3')]/div[contains(@class,'item-content')]"));
		ReusableMethods.scrollIntoView(driver, element1);
		WebElement container1 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//li/div[contains(@id,'itemContent-sorted') and text()='1']")));
		 
		t.until(ExpectedConditions.elementToBeClickable(element1));
		
		builder.moveToElement(element1).build().perform();
		Action dragAndDrop1 = builder.clickAndHold(element1)
				.moveToElement(container1)
				.release(container1)
				.build();
		dragAndDrop1.perform();

		Thread.sleep(2000);
		WebElement element2 = driver.findElement(By.xpath("//ul/li[contains(@id,'item-answer-4')]/div[contains(@class,'item-content')]"));

		ReusableMethods.scrollIntoView(driver, element2);
		WebElement container2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//li/div[contains(@id,'itemContent-sorted') and text()='2']")));
		
		builder.moveToElement(element2).build().perform();
		Action dragAndDrop2 = builder.clickAndHold(element2)
				.moveToElement(container2)
				.release(container2)
				.build();
		dragAndDrop2.perform();
				
		
		ReusableMethods
				.scrollToElement(
						driver,
						By.id("wafer_ps_radio_div39680292063c4a0055bb2da4257d6df04c95ce0b5903559_"));
		
		WebElement radioOption = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='4']")));
		
		//radioOption.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioOption);
		
	}

	@Step("Enter Answer for question #4,  Method: {method} ")
	public void enterQuestion4(boolean submitCorrect) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Terms
		WebElement valueA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-1']")));
		WebElement valueB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-2']")));
		WebElement valueC = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-3']")));
		WebElement valueD = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-4']")));

		// Terms container.
		WebElement containerA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_-1")));
		WebElement containerB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_-2")));
		WebElement containerC = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_-3")));
		WebElement containerD = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("targGroup-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_-4")));

		// Reset answer
		WebElement removeAnswer = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("removeAnswer-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_")));
		ReusableMethods
				.scrollToElement(
						driver,
						By.id("removeAnswer-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_"));
		// Drag and drop terms
		if (submitCorrect) {
			Thread.sleep(2000);
			ReusableMethods.scrollIntoView(driver, removeAnswer);
			// Remove/Reset current selection
			removeAnswer.click();
			Thread.sleep(2000);

			act.dragAndDrop(valueC, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueD, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerC).build().perform();
			Thread.sleep(2000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,-100)", "");
			Thread.sleep(2000);
			act.dragAndDrop(valueA, containerD).build().perform();
			Thread.sleep(2000);
		} else {
			ReusableMethods
					.scrollToElement(
							driver,
							By.id("targGroup-54048351163c4a0055bb2da4257d6df04c95ce0b5908938_-1"));
			act.dragAndDrop(valueA, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueD, containerD).build().perform();
			Thread.sleep(3000);
			act.dragAndDrop(valueC, containerC).build().perform();
		}
	}

	@Step("Enter Answer for question #5,  Method: {method} ")
	public void enterQuestion5(boolean submitCorrect) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Terms
		WebElement valueA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-1']")));
		WebElement valueB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-2']")));
		WebElement valueC = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-3']")));

		// Terms container.
		WebElement containerA = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='1']")));
		WebElement containerB = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='2']")));
		WebElement containerC = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='3']")));
		ReusableMethods
				.scrollToElement(
						driver,
						By.id("itemContent-sorted-1-48008230738e670493181b0fa75bdfa87d0806e23607140_"));
		// Drag and drop terms
		if (submitCorrect) {

			act.dragAndDrop(valueA, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueC, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerC).build().perform();
		} else {
			act.dragAndDrop(valueB, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueA, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueC, containerC).build().perform();
		}
	}

	@Step("Enter Answer for question #19,  Method: {method} ")
	public void enterQuestion21(int correctAttemptPart) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// # Part - 1
		WebElement correctAnswerPart1 = wait
				.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'11, 11, 13')]]//input[@value='1' and not(@disabled)]")));

		act.moveToElement(correctAnswerPart1).perform();

		if (correctAttemptPart == 1) {
			//correctAnswerPart1.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswerPart1);
		}
		// # Part - 2
		ReusableMethods.scrollToBottom(driver);

		WebElement correctAnswerPart2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'5, 6, 6')]]//input[@value='1' and not(@disabled)]")));

		WebElement inCorrectAnswerPart2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'11, 11, 13')]]//input[@value='2' and not(@disabled)]")));

		act.moveToElement(correctAnswerPart2).perform();

		if (correctAttemptPart == 2) {
			/*
			 * JavascriptExecutor js = (JavascriptExecutor) driver;
			 * js.executeScript("window.scrollBy(0,-50)", "");
			 */
			Thread.sleep(2000);
			//correctAnswerPart2.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswerPart2);
		} else {
			//inCorrectAnswerPart2.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", inCorrectAnswerPart2);
		}
		// # Part - 3
		WebElement correctAnswerPart3 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'(b) and (c)')]]//input[@value='1' and not(@disabled)]")));

		WebElement inCorrectAnswerPart3 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'(a) and (b)')]]//input[@value='2' and not(@disabled)]")));

		act.moveToElement(correctAnswerPart3).perform();

		if (correctAttemptPart == 3) {
			Thread.sleep(2000);
			//correctAnswerPart3.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswerPart3);
			
		} else {
			//inCorrectAnswerPart3.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", inCorrectAnswerPart3);
		}
	}
	
	public void enterQuestion21() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		ReusableMethods.scrollToBottom(driver);
		WebElement correctAnswerPart3 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'(b) and (c)')]]//input[@value='1' and not(@disabled)]")));
		//correctAnswerPart3.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswerPart3);
	}
	
	@Step("Enter Incorrect Answer for question #10,  Method: {method} ")
	public void Question10_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement correctAnswer = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("wafer_ps_number7495413142bb7f8a1da6ba3ef0e4be867f7f09ac2303894_")));
		correctAnswer.sendKeys("120");
	}


}