package com.wwnorton.SW5Automation.objectFactory;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TimeZone;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentPage {
	com.wwnorton.SW5Automation.utilities.GetDate getsystemdate = new com.wwnorton.SW5Automation.utilities.GetDate();
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	

	@FindBy(how = How.XPATH, using = "//div[@class='container-box one-rower-box bgFFF']/div/button/span[contains(text(),'EDIT ASSIGNMENT')]")
	public WebElement EditAssignmentbutton;

	@FindBy(how = How.XPATH, using = "//div[@type='text']/input[@placeholder ='Your assignment name']")
	public WebElement AssignmentName;

	@FindBy(how = How.XPATH, using = "//input[@class='ant-calendar-picker-input ant-input']")
	public WebElement GAUDate;

	@FindBy(how = How.XPATH, using = "//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td/span[@class='ant-calendar-date']")
	public WebElement calendarGAUDate;
	
	@FindBy(how = How.XPATH, using = "//div[@class='form-item-icon-title']/span/span[@class='ant-switch-inner' and text()= 'OFF']")
	public WebElement ExamModeOff;
	
	@FindBy(how = How.XPATH, using = "//div[@class='form-item-icon-title']/span/span[@class='ant-switch-inner' and text()= 'ON']")
	public WebElement ExamModeOn;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[span[@class='ant-switch-inner']]")
	public WebElement AdaptiveSwitch;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span/span")
	public WebElement AdaptiveSwitchState;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-switch ant-switch-disabled']")
	public WebElement AdaptiveStateDisabled;
	
	@FindBy(how = How.XPATH, using = "//select[@type='select' and @disabled]")
	public List <WebElement> disabledAdditionalSettings;
	
	@FindBy(how = How.XPATH, using = "//div[@class='examMode-title-info']")
	public List <WebElement> disabledAdditionalSettings_Info;
	
	@FindBy(how = How.XPATH, using = "//input[@data-passref='totalPoints']")
	public List <WebElement> ExamMode_Points;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-select-selection__rendered']/span[text()='1']")
	public List <WebElement> ExamMode_Attempts;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-select-selection__rendered']/span[text()='0%']")
	public List <WebElement> ExamMode_GradePenalties;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-switch']/span[text()='OFF']")
	public List <WebElement> ExamMode_Hint;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title']/span[text()='Unable to Save']")
	public WebElement ExamMode_UnableToSaveModal;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[contains(text(), 'Grades Accepted Until (GAU)')]")
	public WebElement ExamMode_UnableSaveValidationMsg;
	
	
	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-stopwatch']]/span[@class='ant-switch']")
	public WebElement TimeLimitOff;

	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-stopwatch']]/span[@class='ant-switch ant-switch-checked']")
	public WebElement TimeLimitON;

	@FindBy(how = How.XPATH, using = "//div[@class='timeLimitWidth form-group']/input[@class='timeLimitWidth form-control']")
	public WebElement TimeLimitText;
	
	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons iconfont-ebook']]/span[@class='ant-switch']")
	public WebElement ShowEbookLinkOff;

	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons iconfont-ebook']]/span[@class='ant-switch ant-switch-checked']")
	public WebElement ShowEbookLinkON;
	
	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-random']]/span[@class='ant-switch']")
	public WebElement RandomizeQuestionOff;

	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-random']]/span[@class='ant-switch ant-switch-checked']")
	public WebElement RandomizeQuestionON;
	
	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-equalizer']]/span[@class='ant-switch']")
	public WebElement ShowPeriodicTableOff;

	@FindBy(how = How.XPATH, using = "//div[span[@class='glyphicons glyphicons-equalizer']]/span[@class='ant-switch ant-switch-checked']")
	public WebElement ShowPeriodicTableON;

	@FindBy(how = How.ID, using = "warm-up")
	public WebElement Warmup;

	@FindBy(how = How.NAME, using = "follow-up")
	public WebElement followup;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'ADD OBJECTIVES')]")
	public WebElement addOBJECTIVES;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info lato-regular-14']/span[contains(text(),'SELECT OBJECTIVES')]")
	public WebElement SELECTOBJECTIVES;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[@class='regular-modal-body']")
	public WebElement LearningObjectives_popup;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[@class='add-objectives-overlay']")
	public WebElement allLearningObjectives_modal;
	
	@FindBy(how = How.XPATH, using = "//div[@class='filter-sel-div meta-content-bold  ']/span[contains(text(),'CHAPTER')]")
	public WebElement filterBy_Chapter;
	
	@FindBy(how = How.XPATH, using = "//ul[@class='ant-tree']")
	public WebElement chapterList;
	
	@FindBy(how = How.XPATH, using = "//ul[@class='ant-tree']/li[1]/span[2]")
	public WebElement firstChapter_checkbox;

	@FindBy(how = How.XPATH, using = "//input[@class='ant-checkbox-input']")
	public WebElement LearningObjectives_checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='knewton-dialog-title']")
	public WebElement maxLearningObjectives_PopUp;
	
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary fr lato-regular-14'][contains(.,'ADD')]")
	public WebElement LearningObjectives_AddButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary fr lato-regular-14'][contains(.,'GOT IT')]")
	public WebElement LearningObjectives_GotItButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr btn-new-color-2020'][contains(.,'DONE')]")
	public WebElement LearningObjectives_DoneButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr'][contains(.,'CANCEL')]")
	public WebElement LearningObjectives_CancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='form-item-icon-title']/b[contains(.,'Learning Objectives:')]/following-sibling::span")
	public WebElement objectiveselected_info;

	@FindBy(how = How.XPATH, using = "//button[@id='save']")
	public WebElement SaveButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'PUBLISH')]")
	public WebElement PublishButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'PUBLISH TO ALL')]")
	public WebElement PublishToAllButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'SAVE TO ALL STUDENT SETS')]")
	public WebElement SaveToAllStudentSetsButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'PUBLISH TO ALL STUDENT SETS')]")
	public WebElement publishToAllStudentSetsButton;

	@FindBy(how = How.CSS, using = ".ant-calendar-picker-input")
	public WebElement DatePicker;

	@FindBy(how = How.XPATH, using = "//div[@class='assignment-switch']/span[contains(text(),'SHOW ADDITIONAL SETTINGS')]")
	public WebElement ShowAdditionalSettingsButton;

	@FindBy(how = How.XPATH, using = "//div[@type='select']/select[@class='body-content form-control']")
	public WebElement SelectTime;
	
	@FindBy(how = How.XPATH, using = "//select[@class = 'knewton-regular-select body-content']")
	public WebElement SelectPoints;

	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.1.6.0.0.1.0.1.0.1.1:$input']")
	public WebElement SelectUngradedPractice;
	
	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.1.6.0.0.1.0.1.1.1.1:$input']")
	public WebElement SelectShowFeedback;

	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.1.1.6.0.0.1.0.0.1.1.1:$input']")
	public WebElement SelectShowsolution;

	@FindBy(how = How.XPATH, using = "//span[@class='ant-select-selection__rendered']")
	public WebElement SelectAttempts;

	@FindBy(how = How.XPATH, using = "//tr[@class='allQuestionBack']/td[6]//span[@class='select-option-ant ant-select ant-select-enabled']")
	public WebElement SelectGradePanelty;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-xs btn-info clear-margin']/span[contains(text(),'APPLY TO ALL')]")
	public WebElement ApplyToAllButton;

	@FindBy(how = How.XPATH, using = "//div[@class='form-item'][.//*[contains(text(),'Show Student Score In:')]]//select")
	public WebElement SelectShowstudentscore;

	@FindBy(how = How.XPATH, using = "//select[@class='body-content timezone-gmt-style form-control']")
	public WebElement SelecttimeZone;
	
	@FindBy(how =How.XPATH, using ="//div[@type='select']/select[@type='select'][@class='body-content timezone-gmt-style form-control']")
	public WebElement getselectedtimeZone;

	@FindBy(how = How.XPATH, using = "//span[@class='check-late-input']/input[@type='checkbox']")
	public WebElement lateworkacceptedcheckbox;

	@FindBy(how = How.ID, using = "late-work-days")
	public WebElement lateworkdaystextbox;

	@FindBy(how = How.ID, using = "late-work-pr")
	public WebElement penaltyperday;
	
	@FindBy(how = How.XPATH, using = "//div[@id=\'late-work-pr\']/input")
	public WebElement penaltyPercentage;
	

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'UNPUBLISH')]")
	public WebElement unpublishbutton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14']/span[contains(text(),'UNPUBLISH TO ALL')]")
	public WebElement unpublishToAllbutton;

	@FindBy(how = How.XPATH, using = "//div[@class='head-score']/div[@class='head-big-txt']")
	public WebElement HeaderScore;

	//@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	//public WebElement EditAssignmentHeaderTitle;
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons glyphicons-chevron-left']")
	public WebElement EditAssignmentHeaderTitle;

	@FindBy(how = How.XPATH, using = "//tr[@class='allQuestionBack']/td[4]/div/input[@type='text']")
	public WebElement PointsPerQuestionAllQuestionBank;

	@FindBy(how = How.XPATH, using = "//table[@class='table table-bordered table-condensed table-hover']/tbody")
	public WebElement QuestionList;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer']/button[@class='btn-submit-s btn-primary lato-regular-14 fr']")
	public WebElement RemoveButton;

	@FindBy(how = How.XPATH, using = "//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']")
	public WebElement calendarDate;

	@FindBy(how = How.XPATH, using = "//input[@class='ant-calendar-input  ']")
	public WebElement calendarInput;

	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[@class='main-heading']")
	public WebElement settingsLabel;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ant-calendar-input-wrap']/a[@class='ant-calendar-clear-btn']")
	public WebElement calendarInput_ClearButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-footer-left']/button[@class='btn-submit-s btn-info lato-regular-14']/span[contains(text(),'PREVIEW')]")
	public WebElement previewButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='instructor-intro-assignment-title']")
	public WebElement assignmentName_Ins;
	
	@FindBy(how = How.XPATH, using = "//span[@class='meta-content-regular' and contains(text(),'Warm-up')]")
	public WebElement warmUpTitle;
	
	@FindBy(how = How.XPATH, using = "//span[@class='meta-content-regular' and contains(text(),'Follow-up')]")
	public WebElement followUpTitle;
	
	@FindBy(how = How.XPATH, using = "//td[@class='textIndentSec meta-content-regular' and contains(text(),'Practice')]")
	public WebElement adaptivePracticeAttempt;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer']/button[@class='btn-submit-s btn-primary fr lato-regular-14']/span[contains(text(),'GOT IT')]")
	public WebElement modalGotItButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/h4[@class='modal-title']")
	public WebElement modalActivityName;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[@class='body-content']")
	public WebElement msgActivityName;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/button[@class='close']")
	public WebElement btnActivityNameClose;
	
	@FindBy(how = How.XPATH, using = "//div[@class='knewton-dialog-title' and contains(text(),'Time limit has been disabled')]")
	public WebElement timeLimitModalTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']/div[@class='knewton-help-body']")
	public WebElement timeLimitDisabledMessageText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='knewton-dialog-title' and contains(text(),'Warning')]")
	public WebElement warningLOModalTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='knewton-help-body']/span[@class='knewton-help-span']")
	public WebElement warningMessageText_LearningObjectives;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='head-bar modal-header']")
	public WebElement LOModal;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 disabled']/span[contains(text(),'ADD QUESTIONS')]")
	public WebElement btnAddQuestion_Disabled;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[4]/div/input[@type='text']")
	public WebElement pointsPerQuestion;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[5]/div/span[@class='select-option-ant ant-select ant-select-disabled']")
	public WebElement attempts_Disabled; 
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]/div/span[@class='select-option-ant ant-select ant-select-disabled']")
	public WebElement gradePenalties_Disabled;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[7]/span[@class='select-option-ant ant-select ant-select-disabled']")
	public WebElement hints_Disabled; 
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons glyphicons-chevron-left']")
	public WebElement returnToAssignmentList;
	
	@FindBy(how = How.XPATH, using = "//input[@class='applyToAll-checkbox']")
	public WebElement applyToAllCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/button[@class='close']")
	public WebElement alertClose;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary fr lato-regular-14' and text()='CLOSE']")
	public WebElement closeButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[text()='Continue']")
	public WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/button[@class='close']")
	public WebElement modal_Close;
	
	
	
	List<WebElement> AllQuestions;

	public AssignmentPage() throws Exception {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		waitForLoadPageElements();
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 300);
			wait.until(ExpectedConditions
					.elementToBeClickable(EditAssignmentbutton));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("User Click the Edit Assignment button,  Method: {method} ")
	public void editAssignmentbutton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn-normal btn-submit-s btn-info lato-regular-14']")));
		EditAssignmentbutton.click();

	}
	
	
	@Step("Check Exam Mode Button is On,  Method: {method} ")
	public boolean examModeButton_Off_Enabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(ExamModeOff.isDisplayed() == true) {
					
					return ExamModeOff.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	
	@Step("Click the Exam Settings Off button,  Method: {method} ")
	public void examSettingsButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ExamModeOff));
		ExamModeOff.click();
		
	}
	
	
	@Step("Check Adaptive State is On,  Method: {method} ")
	public boolean adaptiveState_Disabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(AdaptiveStateDisabled.isDisplayed() == true) {
					
					return AdaptiveStateDisabled.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
		
	}
	
	
	@Step("Get Exam Mode Adaptive Info,  Method: {method} ")
	public String examMode_AdaptiveInfo() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement adaptiveInfo  = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='examMode-adaptive-info']/span")));
		
		return adaptiveInfo.getText();
		
	}
	
	@Step("Get Exam Mode Instructor Assignment Name,  Method: {method} ")
	public String examMode_AssignmentName() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement assignmentName  = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//span[@class='instructor-intro-assignment-title']")));
		
		return assignmentName.getText();
		
	}
	
	
	@Step("Get Exam Mode Question Table Msg,  Method: {method} ")
	public String examMode_QuestionTableMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement QuestionTableMsg  = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='exam-question-table-msg']/span")));
		
		return QuestionTableMsg.getText();
		
	}
	
	
	@Step("Get Additional Settings Disabled Options,  Method: {method} ")
	public String[] additonalSettings_Disabled() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfAllElements(disabledAdditionalSettings));
		
		String[] arrString = new String[3];
		arrString[0] =  disabledAdditionalSettings.get(0).getText();
		arrString[1] =  disabledAdditionalSettings.get(1).getText();
		arrString[2] =  disabledAdditionalSettings.get(2).getText();
		
		return arrString;
		
	}
	
	
	@Step("Get Additional Settings Disabled Info,  Method: {method} ")
	public String[] additonalSettings_DisabledInfo() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfAllElements(disabledAdditionalSettings_Info));
		
		String[] arrString = new String[3];
		arrString[0] =  disabledAdditionalSettings_Info.get(0).getText();
		arrString[1] =  disabledAdditionalSettings_Info.get(1).getText();
		arrString[2] =  disabledAdditionalSettings_Info.get(2).getText();
		
		return arrString;
		
	}
	
	
	@Step("Get Exam Mode Points,  Method: {method} ")
	public List<String> examMode_PointsPerQuestion() throws InterruptedException {
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		List<String> questionPointsList = new ArrayList<String>();
		
		for (int i = 0; i < 5; i++) {
		
			questionPointsList.add(i, ExamMode_Points.get(i).getAttribute("value"));
		
		}
	
		return questionPointsList;
		
	}
	
	@Step("Get Exam Mode Attempts,  Method: {method} ")
	public String examMode_Attempts() throws InterruptedException {
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		
		return ExamMode_Attempts.get(0).getText();
		
	}
	

	@Step("Get Hint Info,  Method: {method} ")
	public boolean examMode_QuestionsHintDefaultInfo(int count) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfAllElements(ExamMode_Hint));
		
		boolean[] arrString = new boolean[count];
		
		try {
				
				arrString[0] =  ExamMode_Hint.get(0).isDisplayed();
				arrString[1] =  ExamMode_Hint.get(1).isDisplayed();
				arrString[2] =  ExamMode_Hint.get(2).isDisplayed();
				arrString[3] =  ExamMode_Hint.get(2).isDisplayed();
				arrString[4] =  ExamMode_Hint.get(2).isDisplayed();
		
			}
		
		catch(Exception e) { return false;}
		
		return true;
	}
	
	
	@Step("Check Unable To Save Modal is displayed,  Method: {method} ")
	public String examMode_UnableToSaveValidation() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfAllElements(ExamMode_UnableToSaveModal));
		
		String validationMsg = null; 
		
		try { 
				if (ExamMode_UnableToSaveModal.isDisplayed() == true) {
			
					validationMsg = ExamMode_UnableSaveValidationMsg.getText();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", modal_Close);
					
				}
		
			}
		
		catch(Exception e) { }
		
		return validationMsg;
	}	
	
	
	@Step("Get Exam Mode Grade Penalties,  Method: {method} ")
	public String examMode_GradePenalties() throws InterruptedException {
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		
		return ExamMode_GradePenalties.get(0).getText();
		
	}
	
	@Step("Check the Apply To All Student Sets chechbox,  Method: {method} ")
	public void checkApplyToAllStudentSets() {
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//input[@class='applyToAll-checkbox']"));
		applyToAllCheckbox.click();

	}
	
	
	@Step("Check if Apply to All Student set checkbox is Enabled,  Method: {method} ")
	public boolean isApplyToAllStudentSet_CheckboxEnabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(applyToAllCheckbox.isEnabled() == true) {
					System.out.println("Apply to All Student Set checkbix is Enabled");
				}

			} catch(Exception e) { e.getMessage(); }
		
		return applyToAllCheckbox.isEnabled();
	
	}
	
	
	@Step("Check Time Limit Settings,  Method: {method} ")
	public boolean checkTimeLimitSettings() {
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(TimeLimitOff));

				if(TimeLimitOff.isDisplayed() == true) {
					TimeLimitOff.click();
					wait.until(ExpectedConditions.elementToBeClickable(TimeLimitON));
					TimeLimitON.isDisplayed();
					TimeLimitON.click();
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Show Ebook Link Button is On,  Method: {method} ")
	public boolean showEbookLinkButton_On_Enabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(ShowEbookLinkON.isDisplayed() == true) {
					
					return ShowEbookLinkON.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	
	@Step("Check Show Periodic Table Button is On,  Method: {method} ")
	public boolean showPeriodicTableButton_On_Enabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(ShowPeriodicTableON.isDisplayed() == true) {
					
					return ShowPeriodicTableON.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Randomize Question Button is On,  Method: {method} ")
	public boolean randomizeQuestionButton_On_Enabled() {
		
		try {
				wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);

				if(RandomizeQuestionON.isDisplayed() == true) {
					
					return RandomizeQuestionON.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Show Ebook Link settings,  Method: {method} ")
	public boolean checkShowEbookLinkSettings() {
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(ShowEbookLinkON));

				if(ShowEbookLinkON.isDisplayed() == true) {
					ReusableMethods.scrollToElement(driver, 
							By.xpath("//div[span[@class='glyphicons iconfont-ebook']]"
									+ "/span[@class='ant-switch ant-switch-checked']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", ShowEbookLinkON);
					//ShowEbookLinkON.click();
					wait.until(ExpectedConditions.elementToBeClickable(ShowEbookLinkOff));
					ShowEbookLinkOff.isDisplayed();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", ShowEbookLinkOff);
					//ShowEbookLinkOff.click();
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Randomize Question settings,  Method: {method} ")
	public boolean checkRandomizeQuestionSettings() {
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(RandomizeQuestionOff));
				
				if(RandomizeQuestionOff.isDisplayed() == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", RandomizeQuestionOff);
					//RandomizeQuestionOff.click();
					wait.until(ExpectedConditions.elementToBeClickable(RandomizeQuestionON));
					RandomizeQuestionON.isDisplayed();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", RandomizeQuestionON);
					//RandomizeQuestionON.click();
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Show Periodic Table settings,  Method: {method} ")
	public boolean checkShowPeriodicTableSettings() {
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(ShowPeriodicTableON));

				if(ShowPeriodicTableON.isDisplayed() == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", ShowPeriodicTableON);
					//ShowPeriodicTableON.click();
					wait.until(ExpectedConditions.elementToBeClickable(ShowPeriodicTableOff));
					ShowPeriodicTableOff.isDisplayed();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", ShowPeriodicTableOff);
					//ShowPeriodicTableOff.click();
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	
	@Step("Check Post Student submission Question Settings are disabled,  Method: {method} ")
	public boolean isQuestionSettingsDisabled() {
		
		try {
				if (pointsPerQuestion.isEnabled() == false) {
					btnAddQuestion_Disabled.isDisplayed();
					attempts_Disabled.isDisplayed();
					gradePenalties_Disabled.isDisplayed();
					hints_Disabled.isDisplayed();
				}
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}
	
	@Step("Check Question Settings are enabled for Expired Assignment,  Method: {method} ")
	public boolean isQuestionSettingsEnabled() {
		
		try {
				if (pointsPerQuestion.isEnabled() == true) {
					btnAddQuestion_Disabled.isDisplayed();
					attempts_Disabled.isDisplayed();
					gradePenalties_Disabled.isDisplayed();
					hints_Disabled.isDisplayed();
				}
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}

	@Step("User enters the Assignment Name,  Method: {method} ")
	public String enterAssignmentName(String assignmentName) {
		AssignmentName.sendKeys(assignmentName);
		return assignmentName;
	}

	@Step("Enter GAU date,  Method: {method} ")
	public void enterGAUDate(String gauDate) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.elementToBeClickable(calendarDate));
		calendarDate.click();
		WebDriverWait wait1 = new WebDriverWait(driver, 20);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='ant-calendar-input  ']")));
		calendarInput.clear();
		calendarInput.sendKeys(gauDate);
		settingsLabel.click();
	}

	


	@Step("User selects the GAUDate,  Method: {method} ")
	public String gauCurrentDate() throws InterruptedException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String CurrentDate = dateFormat.format(new Date());

		return CurrentDate;
	}

	@Step("User select the ON for Time Limit Option,  Method: {method} ")
	public void timeLimitOn() {
		try {
			TimeLimitOff.click();
		} catch (Exception e) {

		}
	}

	@Step("User select the OFF for Time Limit Option,  Method: {method} ")
	public void timeLimitOFF() {
		try {
			TimeLimitON.click();
		} catch (Exception e) {

		}
	}
	
	@Step("User select the OFF for Time Limit Option,  Method: {method} ")
	public boolean timeLimitModal() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(modalGotItButton));
			
			return timeLimitModalTitle.isDisplayed();

		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check If Activity Name modal is displayed,  Method: {method} ")
	public String activityNameModalText() {
		
		String activityNameText = null;
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOf(modalActivityName));
				
				if (modalActivityName.isDisplayed()) {
					
					activityNameText = msgActivityName.getText();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnActivityNameClose);
				}
					

			} catch (Exception e) {}
		
		return activityNameText;
	}
	
	public String getTimeLimitDisabledMessageText() throws Exception {
		return timeLimitDisabledMessageText.getText();
	}

	@Step("User select the OFF for Time Limit Option,  Method: {method} ")
	public void setTimeLimit(String timeLimitInMinutes) throws Exception {
		TimeLimitText.clear();

		TimeLimitText.sendKeys(timeLimitInMinutes);
	}

	@Step("User select the ON option for Adaptive Option,  Method: {method} ")
	public void adaptiveOn() {

		if (AdaptiveSwitchState.getText().equalsIgnoreCase("OFF"))
			AdaptiveSwitch.click();
	}

	@Step("User select the OFF option for Adaptive Option,  Method: {method} ")
	public void adaptiveOff() {

		if (AdaptiveSwitchState.getText().equalsIgnoreCase("ON"))
			AdaptiveSwitch.click();

	}

	@Step("User select the Warm-Up option ,  Method: {method} ")
	public void warmUp() {
		/*
		 * boolean warmUp = true; if(warmUp==false)
		 */
		Warmup.click();

	}

	@Step("Click Learnings Objective Button and select the Learnings Objectives ,  Method: {method} ")
	public void selectObejctive() throws InterruptedException {
		//SELECTOBJECTIVES.click();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SELECTOBJECTIVES);
		wait.until(ExpectedConditions.visibilityOf(LearningObjectives_popup));
		
		WebElement eleLearningObj;
		List<WebElement> LOList = driver.findElements(By.xpath("//table/tbody/ul"));

		
		for(int i=1; i<=LOList.size(); i++) {
				
				ReusableMethods.scrollToElement(driver, 
						By.xpath("//tbody[@class='stuDetailTabShow']/ul[" + i + 
						"]/li/span[1]/input[@type='checkbox']"));
				
				eleLearningObj = driver.findElement(By.xpath("//tbody[@class='stuDetailTabShow']/ul[" + i + 
						"]/li/span[1]/input[@type='checkbox']"));
				
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", eleLearningObj);
					
				Thread.sleep(1000);
				if(LOModalExist() == true) {
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", modalGotItButton);	
						//modalGotItButton.click();
						break;
					}
		}
		
		//LearningObjectives_DoneButton.click();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", LearningObjectives_DoneButton);
		
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(objectiveselected_info));
		String text = objectiveselected_info.getText();
		System.out.println(text);
	}
	
	@Step("Check if Leanring Objectives modal is displayed ,  Method: {method} ")
	public boolean LOModalExist() throws InterruptedException {
		Thread.sleep(2000);
		boolean LOModalDisplayed = false;
		try {
				LOModalDisplayed = LOModal.isDisplayed();
				return LOModalDisplayed;
	} catch (Exception e) {
			return LOModalDisplayed;
		}

	}
	
	@Step("Click Add Objective Button and select the Learnings Objectives ,  Method: {method} ")
	public void addObejctives() throws InterruptedException {
		
		ReusableMethods.moveToElement(driver, addOBJECTIVES);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", addOBJECTIVES);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(allLearningObjectives_modal));
		
		filterBy_Chapter.click();
		wait.until(ExpectedConditions.visibilityOf(chapterList));
		
		firstChapter_checkbox.click();
		ReusableMethods.moveToElement(driver, allLearningObjectives_modal);
		
		//List<WebElement> LOList = driver.findElements(By.xpath("//*[@id='tableBody']/tr"));

		for(int i=1; i<=6; i++) {
			
					driver.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + i + "]/td[1]/span/label/span[1]/input")).click();
					
				}

		LearningObjectives_AddButton.click();

	}

	@Step("User select the Follow-Up option ,  Method: {method} ")
	public void followUp() {
		boolean followUp = false;
		if (followUp == true)
			followup.click();

	}

	@Step("Click Save Button,  Method: {method} ")
	 public void saveButton() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 50);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='save']")));
	 SaveButton.click();
	}

	@Step("Click Save To All Student Sets Button Button,  Method: {method} ")
	public void saveToAllStudentSetsButton() {
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(SaveToAllStudentSetsButton));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SaveToAllStudentSetsButton);
		//SaveToAllStudentSetsButton.click();
	}
	
	@Step("Click Publish To All Student Sets Button Button,  Method: {method} ")
	public void publishToAllStudentSetsButton() {
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(publishToAllStudentSetsButton));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", publishToAllStudentSetsButton);
		//publishToAllStudentSetsButton.click();
	}
	
	@Step("Click Publish Button,  Method: {method} ")
	public void publishButton() {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(PublishButton));
		PublishButton.click();
		wait.until(ExpectedConditions.elementToBeClickable(unpublishbutton));
	}
	
	@Step("Click Publish To All Button,  Method: {method} ")
	public void publishToAllButton() {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(PublishToAllButton));
		PublishToAllButton.click();
	}

	@Step("Click Show Additional Settings Button,  Method: {method} ")
	public void ShowAdditionalSettingsButton() {
		ReusableMethods.waitElementClickable(driver, By.xpath("//div[@class='assignment-switch']/span[contains(text(),'SHOW ADDITIONAL SETTINGS')]"));
		//ShowAdditionalSettingsButton.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ShowAdditionalSettingsButton);
	}
	
	
	@Step("Check Show Additional Settings Button,  Method: {method} ")
	public boolean showAdditionalSettingsButton_Dispalyed() {
		
		try {
				wait = new WebDriverWait(driver, 25);
				TimeUnit.SECONDS.sleep(10);

				if(ShowAdditionalSettingsButton.isDisplayed() == true) {
					
					return ShowAdditionalSettingsButton.isDisplayed();
					
				}
				
			} 
		
			catch(Exception e) { return false;}
		
		return true;
	
	}


	@Step("Verify Warm-Up or Follow-Up Type is displayed as expected,  Method: {method} ")
	public void adaptive_Questions(String adaptiveType) {
		
		String adaptiveTitle = null;
		wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr")));
		
		List<WebElement> QuestionList = driver
				.findElements(By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr"));
		
		FirstLoop: for (int i = 0; i < QuestionList.size(); i++) {
			List<WebElement> questionTitle = QuestionList.get(i).findElements(
					By.xpath("//td[@class='textIndentLeft']"));
			
			for (int j = 0; j < questionTitle.size();) {
				adaptiveTitle = questionTitle.get(j).getText();
					while(adaptiveTitle.equalsIgnoreCase(adaptiveType)) {
						break FirstLoop;
					} j++;
			}
		}
	}

	
	@Step("Verify Created Assignment is displayed in Assignment Title Section,  Method: {method} ")
	public void Assignmentlinks(String AssignmentName) {
		
		ReusableMethods.waitVisibilityOfElement(driver, By
				.xpath("//div[@id='activity_list_table_wrapper']/table/tbody"));
		
		List<WebElement> links = driver.findElements(By
				.xpath("//div[@id='activity_list_table_wrapper']/table/tbody"));
		
		firstLoop: for (int i = 0; i < links.size(); i++) {
			
			List<WebElement> linkstext = links.get(i).findElements(
					By.xpath("//tr/td[@class=' title_td']/a"));
			secondLoop: for (int j = 0; j < linkstext.size(); j++) {
				String AssignmentText = linkstext.get(j).getText();
				if (AssignmentText.equalsIgnoreCase(AssignmentName)) {
					ReusableMethods.scrollIntoView(driver, linkstext.get(j));
					//linkstext.get(j).click();
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkstext.get(j));
					break firstLoop;
				}
			}

		}

	}

	@Step("User select the GAU Time,  Method: {method} ")
	public void selectGAUTime(String time) {
		time = TimeZone.getTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);
	}
	
	@Step("User select the GAU Time,  Method: {method} ")
	public void setGAUTime(String time) {
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);
	}

	@Step("User Set the GAU to +30 mins of current date & time,  Method: {method} ")
	public String getSetGAUTime() {
		String time = TimeZone.getthirtyMinutesTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1"))
			time = "0" + time;

		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}

	@Step("User select the Ungraded Practice,  Method: {method} ")
	public void selectUngradedPractice(String practiceName) throws InterruptedException {
		Thread.sleep(3000);
		Select oselect1 = new Select(SelectUngradedPractice);
		oselect1.selectByVisibleText(practiceName);
		LogUtil.log("The Ungraded Practice Name " + practiceName);
	}
	
	@Step("User select the Show feedback,  Method: {method} ")
	public void selectShowFeedback(String feedbackName) throws InterruptedException {
		Thread.sleep(3000);
		Select oselect1 = new Select(SelectShowFeedback);
		oselect1.selectByVisibleText(feedbackName);
		LogUtil.log("The Show Feedback Name " + feedbackName);
	}

	@Step("User select the Show Solution,  Method: {method} ")
	public void selectshowsolution(String solutionName) {
		Select oselectsolName = new Select(SelectShowsolution);
		oselectsolName.selectByVisibleText(solutionName);
		LogUtil.log("The Show Solution Name " + solutionName);
	}

	@Step("User select the Attempts from Question Level Settings,  Method: {method} ")
	public void selectAttempts(String attemptsCount) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(SelectAttempts));
		ReusableMethods.scrollToElement(driver, By.xpath("//span[@class='ant-select-selection__rendered']"));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SelectAttempts);
		//SelectAttempts.click();
		
		Thread.sleep(2000);
		
		List<WebElement> attemptMenuItem = driver.findElements
				(By.xpath("//ul[@class='ant-select-dropdown-menu ant-select-dropdown-menu-vertical  ant-select-dropdown-menu-root']/li"));
		
		for (WebElement liopt : attemptMenuItem) {

			if (attemptsCount.equals(liopt.getText())) {
				liopt.click();
				return;
			}
			LogUtil.log("The Attempts from Question Level Settings "
					+ attemptsCount);

		}
		
		throw new NoSuchElementException("Can't find " + attemptMenuItem
				+ " in dropdown");

	}
	
	@Step("User select the Attempts for any questions,  Method: {method} ")
	public void selectAttemptsforQuestion(String attemptsCount) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@data-reactid='.0.1.1.7.0.0.1.0:0.4.0.0.$selection.$arrow']")));
		WebElement selectAttemptforQuestion =driver.findElement(By.xpath("//span[@data-reactid='.0.1.1.7.0.0.1.0:0.4.0.0.$selection.$arrow']"));
		//selectAttemptforQuestion.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", selectAttemptforQuestion);
		
		Thread.sleep(2000);
		List<WebElement> attemptMenuItem = driver
				.findElements(By
						.xpath("//ul[@class='ant-select-dropdown-menu ant-select-dropdown-menu-vertical  ant-select-dropdown-menu-root']/li"));
		for (WebElement liopt : attemptMenuItem) {

			if (attemptsCount.equals(liopt.getText())) {
				liopt.click();
				return;
			}
			LogUtil.log("The Attempts from Question Level Settings "
					+ attemptsCount);

		}
		throw new NoSuchElementException("Can't find " + attemptMenuItem
				+ " in dropdown");
	}

	@Step("User select the Points from Question Level Settings,  Method: {method} ")
	public void selectPoints(String points) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("publish")));
		PointsPerQuestionAllQuestionBank.clear();
		PointsPerQuestionAllQuestionBank.sendKeys(points);
	}

	@Step("User select the Grade Panelties from Question Level Settings,  Method: {method} ")
	public void selectGradePanelties(String gradePanelty) {

		SelectGradePanelty.click();
		List<WebElement> attemptMenuItem = driver
				.findElements(By
						.xpath("//ul[@class='ant-select-dropdown-menu ant-select-dropdown-menu-vertical  ant-select-dropdown-menu-root']/li"));
		for (WebElement liopt : attemptMenuItem) {

			if (gradePanelty.equals(liopt.getText())) {
				liopt.click();
				return;
			}
			LogUtil.log("The GradePanelty from Question Level Settings "
					+ gradePanelty);

		}
		throw new NoSuchElementException("Can't find " + attemptMenuItem
				+ " in dropdown");

	}

	/**
	 * @param showStudentScore
	 */
	@Step("User select the Show Student Score in,  Method: {method} ")
	public void selectshowstudentscore(String showStudentScore)
			throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='form-item'][.//*[contains(text(),'Show Student Score In:')]]//select")));
		ReusableMethods.scrollIntoView(driver, SelectTime);
		Select oselectstudentscore = new Select(SelectShowstudentscore);
		oselectstudentscore.selectByVisibleText(showStudentScore);
		Thread.sleep(3000);
		LogUtil.log("The Show Student Score In: " + getShowStudentScoreValue());
	}
	
	@Step("User select the Adaptive Points,  Method: {method} ")
	public void selectAdaptivePoints(String adaptivePoints)
			throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@class = 'knewton-regular-select body-content']")));
		ReusableMethods.scrollIntoView(driver, SelectPoints);
		Select oselectAdaptivePoints = new Select(SelectPoints);
		oselectAdaptivePoints.selectByVisibleText(adaptivePoints);
	}

	@Step("Get  select the Show Student Score in,  Method: {method} ")
	public String getShowStudentScoreValue() {
		Select oselectstudentscore = new Select(SelectShowstudentscore);
		return oselectstudentscore.getFirstSelectedOption().getText();
	}

	

	@Step("User select the Date,  Method: {method} ")
	public void selectDate(String day) {
		List<WebElement> datepicker = driver
				.findElements(By
						.xpath("//table[@class='ant-calendar-table']/tbody[@class='ant-calendartbody']/tr/td/span"));
		for (WebElement ele : datepicker) {
			String date = ele.getText();
			if (date.equalsIgnoreCase(day)) {
				ele.click();
				break;
			}
		}

	}

	@Step("User select the  GAU Date,  Method: {method} ")
	public void selectGAUDate() throws ParseException {

		String dot = "11/15/2019";

		System.out.println(dot);
		String date, month, year;
		String caldt, calmonth, calyear;
		String dateArray[] = dot.split("/");
		month = dateArray[0];
		date = dateArray[1];
		year = dateArray[2];
		driver.findElement(By.xpath("//input[@placeholder='mm/dd/yyyy']"))
				.click();
		WebElement cal;
		cal = driver.findElement(By.className("ant-calendar"));
		calyear = driver.findElement(By.className("ant-calendar-year-select"))
				.getText();
		System.out.println(calyear);
		/**
		 * Select the year
		 */
		while (!calyear.equals(year)) {
			driver.findElement(By.className("ant-calendar-year-panel-year"))
					.click();
			calyear = driver.findElement(
					By.className("ant-calendar-year-panel-year")).getText();
			System.out.println("Displayed Year::" + calyear);
		}

		calmonth = driver
				.findElement(By.className("ant-calendar-month-select"))
				.getText();
		System.out.println(calmonth);
		SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
		Calendar cal1 = Calendar.getInstance();

		cal1.setTime(inputFormat.parse(calmonth));
		SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
		String calMonthNum = (outputFormat.format(cal1.getTime()));

		if (!calMonthNum.equalsIgnoreCase(month)) {
			driver.findElement(By.className("ant-calendar-next-month-btn"))
					.click();
			calmonth = driver.findElement(
					By.className("ant-calendar-month-select")).getText();
			System.out.println("Displayed Month::" + calmonth);
		} else {
			System.out.println("Displayed Month::" + calmonth);
		}

		cal = driver.findElement(By.className("ant-calendar"));
		/**
		 * Select the Date
		 */
		List<WebElement> rows, cols;
		rows = cal.findElements(By.tagName("tr"));

		for (int i = 1; i < rows.size(); i++) {
			cols = rows.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < cols.size(); j++) {

				caldt = cols.get(j).getText();
				if (caldt.equals(date)) {
					cols.get(j).click();
					break;
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	@Step("Instructor Clears GAU date,  Method: {method} ") 
	 public void resetSW5GAUDate(String SW5GAUDate) throws Exception {

			String date, month, year;
			String caldt;
			
			String dateArray[] = SW5GAUDate.split("/");
			month = dateArray[0];
			date = dateArray[1];
			year = dateArray[2];
	
			Date newDate = new SimpleDateFormat("dd").parse(date);
			String strDate = new SimpleDateFormat("d").format(newDate);
			
			driver.findElement(By.xpath("//input[@placeholder='mm/dd/yyyy']")).click();
			
			WebElement cal;
			cal = driver.findElement(By.className("ant-calendar"));

			//Select the date
			
			List<WebElement> rows, cols;
			//rows = cal.findElements(By.tagName("tr"));
			rows = driver.findElements(By.xpath("//tbody[@class='ant-calendartbody']//tr"));

			firstLoop: for (int i = 0; i <rows.size(); i++) {
				//cols = rows.get(i).findElements(By.tagName("td"));
				cols = driver.findElements(By.xpath("//tbody[@class='ant-calendartbody']//tr")).get(i).findElements(By.tagName("td"));

				secondLoop: for (int j = 0; j < cols.size(); j++) {

					caldt = cols.get(j).getText();
					if (caldt.equals(strDate)) {
						cols.get(j).click();
						break firstLoop;
					}
				}
				
			}
		}
	

	@Step("User select the time zone,  Method: {method} ")
	public void selecttimeZone(String timezoneName) {
		Select oselecttimezoneName = new Select(SelecttimeZone);
		oselecttimezoneName.selectByVisibleText(timezoneName);
	
		LogUtil.log("The time zone Name " + timezoneName);
		//System.out.println("The time zone Name " + timezoneName);
	}
	
	
	
	
	@Step("User get the time Zone,  Method: {method} ")
	public String gettimeZone() {
		Select ogetimezoneName = new Select(getselectedtimeZone);
		WebElement option = ogetimezoneName.getFirstSelectedOption();
		String defaultTimeZone = option.getText();
		LogUtil.log("The time zone Name " + defaultTimeZone);
		System.out.println("The time zone Name " + defaultTimeZone);
		return defaultTimeZone;
	}

	@Step("User click the Late work accepted check box and Add the days,  Method: {method} ")
	public void LateworkCheckbox(String days, String penalty) throws InterruptedException, Exception {
		WebDriverWait waite = new WebDriverWait(driver, 25);
		waite.until(ExpectedConditions
				.elementToBeClickable(lateworkacceptedcheckbox));
		WebElement element = lateworkacceptedcheckbox;
		Actions builder2 = new Actions(driver);
		builder2.moveToElement(element).release().perform();

		WebElement latedays = driver.findElement(By
				.cssSelector(".check-late-input"));
		Actions builder = new Actions(driver);
		builder.moveToElement(latedays).release().perform();
		
		WebElement elementInput = driver.findElement(By.cssSelector(".check-late-input > input"));
		if(elementInput.isSelected() == false) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", elementInput);
		}
		
		WebDriverWait waitq = new WebDriverWait(driver, 25);
		waitq.until(ExpectedConditions.visibilityOfElementLocated(By
				.cssSelector("#late-work-days > #late-work-days")));
		driver.findElement(By.cssSelector("#late-work-days > #late-work-days"))
				.click();
		
		driver.findElement(By.xpath("//div[@id=\'late-work-days\']/input"))
				.clear();
		driver.findElement(By.xpath("//div[@id=\'late-work-days\']/input"))
				.sendKeys(days);
		LogUtil.log("The Late work days are entered " + days);
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.cssSelector("#late-work-pr > #late-work-pr")));
		
		WebElement penaltyelement = driver.findElement(
				By.xpath("//input[@id='late-work-pr']"));
		 
		wait = new WebDriverWait(driver, 2);
		TimeUnit.SECONDS.sleep(2);
		Actions builder1 = new Actions(driver);
		String fieldLen = penaltyelement.getAttribute("value");
		int lengthText = fieldLen.length();
		
		if (fieldLen != null) {
			String os = System.getProperty("os.name");
			for(int i = 0; i < lengthText; i++){	
				if (os.equals("Mac OS X")){

					Action select= builder1.click(penaltyelement).keyDown(Keys.COMMAND)
				            .sendKeys("a")
				            .keyUp(Keys.COMMAND)
				            .build();
							select.perform();
					
					}else {
							Action select= builder1.click(penaltyelement).keyDown(Keys.CONTROL)
				            .sendKeys("a")
				            .keyUp(Keys.CONTROL)
				            .build();
							select.perform();
					}
				//penaltyelement.sendKeys(Keys.ARROW_LEFT);
				penaltyelement.sendKeys(Keys.DELETE);
			}
		}

		Action clk= builder1.click(penaltyelement)
				.build();
		clk.perform();
		TimeUnit.SECONDS.sleep(2);
		/*JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].value='"+penalty+"';", penaltyelement);
		Action tab= builder1.click(penaltyelement).sendKeys(Keys.TAB)
				.build();
				tab.perform(); */
		Robot robot= new Robot();
		    for (char c : penalty.toCharArray()) {
		        int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
		        if (KeyEvent.CHAR_UNDEFINED == keyCode) {
		            throw new RuntimeException(
		                "Key code not found for character '" + c + "'");
		        }
		        robot.keyPress(keyCode);
		        robot.delay(100);
		        robot.keyRelease(keyCode);
		        robot.delay(100);
		    }
		}
	
	@Step("Penalty Alrert is displayed,  Method: {method} ")
	public boolean penaltyAlert() {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement penaltyAltert = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class=\'body-content\' and contains(text(),\'Penalty per day is a required entry.\')]")));
		
		return penaltyAltert.isDisplayed();
	}
	

	@Step("get the Assignment is due information from Assignment page,  Method: {method}")
	public void Assignmentinfo() {
		WebElement assignmentTextinfo = driver.findElement(By
				.className("container-box assignment-info-box"));
		String assignmentText = assignmentTextinfo.getText();
		LogUtil.log("The Assignment due information is  " + assignmentText);
	}

	@Step("Verify Activity details of a Student on Assignment page,  Method: {method } ")
	public void AssignmentActivityDetails() {
		// Grab the table
		WebElement table = driver.findElement(By
				.xpath("//table[@id='activity_list_table']/tbody"));

		// Now get all the TR elements from the table
		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		// And iterate over them, getting the cells
		for (WebElement row : allRows) {
			List<WebElement> cells = row.findElements(By.tagName("td"));
			for (WebElement cell : cells) {
				System.out.println("content >>   " + cell.getText());
			}
		}
	}

	@Step("Check UNPUBLISH button displayed,  Method: {method} ")
	public Boolean isUnpublishButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(unpublishbutton));
			return unpublishbutton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check UNPUBLISH To All button displayed,  Method: {method} ")
	public Boolean isUnpublishToAllButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(unpublishToAllbutton));
			return unpublishToAllbutton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Remove the Specified Question,  Method: {method} ")
	public void removeQuestion(int questionNumber) throws Exception {
		ReusableMethods.waitElementClickable(driver, By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody"));
		ReusableMethods.scrollToBottom(driver);
		List<WebElement> questionstoDelete = driver
				.findElements(By
						.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr[" + questionNumber + "]/td[@class='text-center toolbar-delete-non-edit']/div"));
		
		questionstoDelete.get(0).click();

	}

	@Step("click the Remove Button,  Method: {method} ")
	public void clickRemovebutton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(RemoveButton));
		ReusableMethods.scrollIntoView(driver, RemoveButton);
		RemoveButton.click();

	}

	public String getHeaderScore() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(HeaderScore));
		return HeaderScore.getText().replace('\n', ' ');
	}

	public void clickHeaderTitle(AssignmentPage ap) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(EditAssignmentHeaderTitle));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditAssignmentHeaderTitle);
		//EditAssignmentHeaderTitle.click();
		if (ap != null) {
			ap.waitForLoadPageElements();
		}
	}
	
	public boolean isPreviewButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(previewButton));
			return previewButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Click the Preview button,  Method: {method} ")
	public void clickPreviewButton() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(previewButton));
		previewButton.click();
	}
	
	@Step("User select the OFF for Time Limit Option,  Method: {method} ")
	public boolean warningLearningObjectivesModal() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(modalGotItButton));
			
			return warningLOModalTitle.isDisplayed();

		} catch (Exception e) {
			return false;
		}
	}
	
	public String getWarningMsg_LearningObjectives() throws Exception {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(modalGotItButton));
				
				return warningMessageText_LearningObjectives.getText();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return null;
		}
		
	}
	
	@Step("Follow Up is displayed as the end on Instructor Assignment Page ?,  Method: {method} ")
	public boolean followUpDisplayedAtEnd_AssignmentPage() throws Exception {
		
		boolean  FollowUpExist;
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#main-content table tbody tr').length");

		int questionsCount =  Integer.valueOf(rowCountObj.toString());
	
		
	try {
			WebElement	questionTitle = driver
						.findElement(By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr[" 
				+ questionsCount + "]/td[@class='textIndentLeft']/button[@class='btn-cell']/span[@class='meta-content-regular' and contains(text(),'Follow-up')]"));
			
			ReusableMethods.scrollIntoView(driver, questionTitle);
			FollowUpExist = questionTitle.isDisplayed();
			
			return FollowUpExist;

		} catch (Exception e) {
			e.getMessage(); 
			return false;
		}
	}
	
	
	@Step("Check Maximum Exception is displayed,  Method: {method} ")
	public boolean isMaximumExceptionDisplayed() throws Exception {
		
		boolean maximumExceptionExist = false;
		
		try {	
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//div[@class='head-bar modal-header']/h4")));
				
				WebElement	maximumException = driver.findElement(By.xpath
						("//div[@aria-live='assertive']/div/div/span[contains(text(), "
								+ "'An error has occured. Please try saving again or contact')]"));
				
				maximumExceptionExist = maximumException.isDisplayed();
				closeButton.click();
				
				return maximumExceptionExist;
			
		} catch (Exception e) {
				return maximumExceptionExist;
			}
	}
	
	
	@Step("Check Exit Without Saving Exception is displayed,  Method: {method} ")
	public boolean isExitWithoutSavingExceptionDisplayed() throws Exception {
		
		boolean exitWithoutSavingExceptionExist = false;
		
		try {	
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//h4[@class='modal-title' and contains(text(), "
								+ "'Are you sure you want to exit without saving?')]")));
				
				WebElement	exitWithoutSavingException = driver.findElement(By.xpath
						("//div[@class='modal-body']/p[contains(text(), "
								+ "'If you continue without saving, your modifications will not be applied.')]"));
				
				exitWithoutSavingExceptionExist = exitWithoutSavingException.isDisplayed();
				continueButton.click();
				
				return exitWithoutSavingExceptionExist;
			
		} catch (Exception e) {
				return exitWithoutSavingExceptionExist;
			}
	}


}
