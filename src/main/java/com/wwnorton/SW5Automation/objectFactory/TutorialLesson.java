package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class TutorialLesson {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//button[@id='startTutorial']")
	public WebElement StartTutorialButton;

	@FindBy(how = How.XPATH, using = "//button[@id='closeTutorial']")
	public WebElement CloseTutorialButton;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswerButton;

	@FindBy(how = How.XPATH, using = "//div[@role='dialog']/div[@class='fade in modal']")
	public WebElement Feedback;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div/button[@class='close']")
	public WebElement CloseFeedback;

	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'tutorial-step-num')][span[contains(text(),1)]]")
	public WebElement TutorialQuestion1;

	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'tutorial-step-num')][span[contains(text(),2)]]")
	public WebElement TutorialQuestion2;

	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'tutorial-step-num')][span[contains(text(),3)]]")
	public WebElement TutorialQuestion3;

	public TutorialLesson(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	public void waitForTutorialQPElementsToLoad() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			// TutorialTitle Step
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='tutorial-title-step']")));

			// CloseTutorialButton OR Tutorial Questions
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(CloseTutorialButton),
					ExpectedConditions.elementToBeClickable(TutorialQuestion1),
					ExpectedConditions.elementToBeClickable(TutorialQuestion2),
					ExpectedConditions.elementToBeClickable(TutorialQuestion3)));

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on 'START TUTORIAL' button,  Method: {method} ")
	public void clickStartTutorialButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(StartTutorialButton));
		StartTutorialButton.click();

	}

	@Step("Click on 'CLOSE TUTORIAL' button,  Method: {method} ")
	public void clickCloseTutorialButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(CloseTutorialButton));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", CloseTutorialButton);
		//CloseTutorialButton.click();

	}

	@Step("Click on 'Submit Answer' button,  Method: {method} ")
	public void clickSubmitAnswerButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(SubmitAnswerButton));
		SubmitAnswerButton.click();

	}

	@Step("Click on question #2,  Method: {method} ")
	public void clickTutorialQuestion2() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(TutorialQuestion2));
		TutorialQuestion2.click();

	}

	@Step("Click on question #3,  Method: {method} ")
	public void clickTutorialQuestion3() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(TutorialQuestion3));
		TutorialQuestion3.click();

	}

	@Step("Check Submit Answer button displayed,  Method: {method} ")
	public boolean isSubmitAnswerButtonDisplayed() throws Exception {
		try {
			return SubmitAnswerButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isFeedbackOverlayDisplayed() {
		try {
			return Feedback.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	@Step("Click on Close Feedback popup,  Method: {method} ")
	public void clickFeedbackClose() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(CloseFeedback));

		CloseFeedback.click();

		// Unable to close Feedback sometimes so trying again in-case still its open
		if (isFeedbackOverlayDisplayed())
			CloseFeedback.click();

		Thread.sleep(5000);
		waitForTutorialQPElementsToLoad();
	}

}
