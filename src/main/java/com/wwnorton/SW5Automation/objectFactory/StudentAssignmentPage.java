package com.wwnorton.SW5Automation.objectFactory;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class StudentAssignmentPage {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Keep Using Trial Access'")
	public WebElement KeepUsingTrialAccess;

	@FindBy(how = How.XPATH, using = "//div[@class='head-score']/div[@class='head-big-txt']")
	public WebElement HeaderScore;
	
	@FindBy(how = How.XPATH, using = "//span[@class='exam-mode-text']")
	public WebElement examText;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash']//div[@class='head-timeleft head-dash-list']/span[@class='glyphicons glyphicons-stopwatch']")
	public WebElement TimerIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash']//div[@class='head-timeleft head-dash-list']/div[@class='head-big-txt']")
	public WebElement TimerText;

	@FindBy(how = How.XPATH, using = "//div[@class='container-box assignment-info-box']/div[@class='assignment-info-box-r']")
	public WebElement AssignmentInfoBox;
	
	@FindBy(how = How.XPATH, using = "//span[@class='student-intro-assignment-title']")
	public WebElement assignmentName_Std;
	
	@FindBy(how = How.XPATH, using = "//td[@class='textIndentLeft']/button[@class='btn-cell']/span[contains(text(),'Warm Up')]")
	public WebElement warmUpTitle;
	
	@FindBy(how = How.XPATH, using = "//td[@class='textIndentLeft']/button[@class='btn-cell']/span[contains(text(),'Follow Up')]")
	public WebElement followUpTitle;
	
	@FindBy(how = How.XPATH, using = "//td[@class='textIndentSec' and contains(text(),'Practice')]")
	public WebElement adaptivePracticeAttempt;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'BEGIN ASSIGNMENT')]")
	public WebElement BeginAssignment;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'RESUME ASSIGNMENT')]")
	public WebElement ResumeAssignment;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'REVIEW ASSIGNMENT')]")
	public WebElement ReviewAssignment;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'BEGIN EXAM')]")
	public WebElement BeginExam;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'RESUME EXAM')]")
	public WebElement ResumeExam;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'REVIEW EXAM')]")
	public WebElement ReviewExam;

	@FindBy(how = How.XPATH, using = "//table[@id='realQuestionTable']/tbody")
	public WebElement QuestionTable;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'Yes, I am ready to begin')]]")
	public WebElement TimeLimitConfirmYesButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'No, I am not ready to begin')]]")
	public WebElement TimeLimitConfirmNoButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content'][div/div[contains(text(),'You have reached the deadline of this activity!')]]/div/button[@class='close']")
	public WebElement CloseDeadlineReached;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.1']")
	public WebElement latePenaltyText_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']//span[@data-reactid='.0.2.2.1.2']/span[@data-reactid='.0.2.2.1.2.0']")
	public WebElement latePenaltyText_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.0']")
	public WebElement latePenaltyText_part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.1']")
	public WebElement latePenaltyText_part4;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.2']")
	public WebElement latePenaltyText_part5;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.3']")
	public WebElement latePenaltyText_part6;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.4']")
	public WebElement latePenaltyText_part7;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.5']")
	public WebElement latePenaltyText_part8;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.6']")
	public WebElement latePenaltyText_part9;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[@data-reactid='.0.1.6.1.1.0']")
	public WebElement gauInfo_LatePenaltyText_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/strong[1]/span[@data-reactid='.0.1.6.1.1.1.1']")
	public WebElement gauInfo_LatePenaltyText_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[@data-reactid='.0.1.6.1.1.2']")
	public WebElement gauInfo_LatePenaltyText_part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/span[@data-reactid='.0.1.6.1.1.3.0']")
	public WebElement gauInfo_LatePenaltyText_part4;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/span[@data-reactid='.0.1.6.1.1.3.1']")
	public WebElement gauInfo_LatePenaltyText_part5;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/span[@data-reactid='.0.1.6.1.1.3.2']")
	public WebElement gauInfo_LatePenaltyText_part6;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.0']")
	public WebElement gauInfo_LatePenaltyText_part7;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.1']")
	public WebElement gauInfo_LatePenaltyText_part8;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.2']")
	public WebElement gauInfo_LatePenaltyText_part9;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.3']")
	public WebElement gauInfo_LatePenaltyText_part10;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.4']")
	public WebElement gauInfo_LatePenaltyText_part11;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@data-reactid='.0.1.6.1.1']/span[3]/strong[1]/span[@data-reactid='.0.1.6.1.1.3.3.5']")
	public WebElement gauInfo_LatePenaltyText_part12;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.1']/span[@data-reactid='.0.2.2.1.1.0']")
	public WebElement gauInfo_std_LatePenaltyText_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.1']/strong[1]/span[@data-reactid='.0.2.2.1.1.1.1']")
	public WebElement gauInfo_std_LatePenaltyText_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.1']/span[@data-reactid='.0.2.2.1.1.2']")
	public WebElement gauInfo_std_LatePenaltyText_part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/span[@data-reactid='.0.2.2.1.2.0']")
	public WebElement gauInfo_std_LatePenaltyText_part4;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.0']")
	public WebElement gauInfo_std_LatePenaltyText_part5;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.2']")
	public WebElement gauInfo_std_LatePenaltyText_part6;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.3']")
	public WebElement gauInfo_std_LatePenaltyText_part7;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.4']")
	public WebElement gauInfo_std_LatePenaltyText_part8;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@data-reactid='.0.2.2.1.2']/strong[1]/span[@data-reactid='.0.2.2.1.2.1.5']")
	public WebElement gauInfo_std_LatePenaltyText_part9;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part1']/span[@data-reactid='.0.2.2.1.0.0']")
	public WebElement extraCreditText_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part1']/strong[1]/span[@data-reactid='.0.2.2.1.0.1.0']")
	public WebElement extraCreditText_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part1']/strong[1]/span[@data-reactid='.0.2.2.1.0.1.1']")
	public WebElement extraCreditText_part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part1']/strong[1]/span[@data-reactid='.0.2.2.1.0.1.2']")
	public WebElement extraCreditText_part4;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ExtraCreditBesideTitle']")
	public WebElement extraCreditTitle_SAP;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@class='part1']/span[@data-reactid='.0.1.6.1.0.0']")
	public WebElement extraCreditText_Instructor_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@class='part1']/strong[1]/span[@data-reactid='.0.1.6.1.0.1.0']")
	public WebElement extraCreditText_Instructor_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@class='part1']/strong[1]/span[@data-reactid='.0.1.6.1.0.1.1']")
	public WebElement extraCreditText_Instructor_part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r body-content']/span[@class='part1']/strong[1]/span[@data-reactid='.0.1.6.1.0.1.2']")
	public WebElement extraCreditText_Instructor_part4;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span")
	public WebElement assignmentInfoText_AllParts;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part2']/span[@data-reactid='.0.2.1.1.1.1.0']")
	public WebElement assignmentInfoText_part1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part2']/strong/span[@data-reactid='.0.2.1.1.1.1.1.0']")
	public WebElement assignmentInfoText_part2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part2']/strong/span[@data-reactid='.0.2.1.1.1.1.1.1']")
	public WebElement assignmentInfoText_Part3;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part2']/span[@data-reactid='.0.2.1.1.1.1.2']")
	public WebElement assignmentInfoText_Part4;
	
	@FindBy(how = How.XPATH, using = "//div[@class='assignment-info-box-r']/span[@class='part2']/span[@data-reactid='.0.2.1.1.1.1.3']")
	public WebElement assignmentInfoText_Part5;
	
	List<WebElement> AllQuestions;

	// Initializing Web Driver and PageFactory.
	public StudentAssignmentPage() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		WebDriverWait wait;
		try {
			wait = new WebDriverWait(driver, 300);
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(BeginAssignment),
					ExpectedConditions.elementToBeClickable(ResumeAssignment),
					ExpectedConditions.elementToBeClickable(ReviewAssignment)));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(HeaderScore));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click KeepUsingTrialAccess on student assignment page,  Method: {method} ")
	public void keepUsingTrialAccess() throws Exception {
		try {
			// WebDriverWait wait = new WebDriverWait(driver, 30);
			// wait.until(ExpectedConditions.elementToBeClickable(KeepUsingTrialAccess));
			waitForElementPresent(
					By.xpath("//button[@type='button']/span[contains(text(),'Keep Using Trial Access')]"));
			driver.findElement(By.xpath("//button[@type='button']/span[contains(text(),'Keep Using Trial Access')]"))
					.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public String getAssignmentInfoBox() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(AssignmentInfoBox));
        System.out.println(AssignmentInfoBox.getAttribute("title"));
		return AssignmentInfoBox.getAttribute("title");
	}

	@Step("Check BEGIN ASSIGNMENT button displayed,  Method: {method} ")
	public Boolean isBeginAssignmentButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(BeginAssignment));
			return BeginAssignment.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on BEGIN ASSIGNMENT button,  Method: {method} ")
	public void beginAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(BeginAssignment));
			BeginAssignment.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check RESUME ASSIGNMENT button displayed,  Method: {method} ")
	public boolean isResumeAssignmentDisplayed() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeAssignment));
			return ResumeAssignment.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}

	@Step("Click on RESUME ASSIGNMENT button,  Method: {method} ")
	public void resumeAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeAssignment));
			ResumeAssignment.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check REVIEW ASSIGNMENT button displayed,  Method: {method} ")
	public boolean isReviewAssignmentButtonDisplayed() {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(ReviewAssignment));
				return ReviewAssignment.isDisplayed();
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
			}
		}

	@Step("Click on REVIEW ASSIGNMENT button,  Method: {method} ")
	public void reviewAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ReviewAssignment));
			ReviewAssignment.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Check BEGIN EXAM button displayed,  Method: {method} ")
	public Boolean isBeginExamButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(BeginExam));
			return BeginExam.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Click on BEGIN EXAM button,  Method: {method} ")
	public void beginExamClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(BeginExam));
			BeginExam.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Check RESUME EXAM button displayed,  Method: {method} ")
	public Boolean isResumeExamButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeExam));
			return ResumeExam.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	
	@Step("Click on RESUME EXAM button,  Method: {method} ")
	public void resumeExamClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeExam));
			ResumeExam.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Check REVIEW EXAM button displayed,  Method: {method} ")
	public Boolean isReviewExamButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ReviewExam));
			return ReviewExam.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	
	@Step("Click on REVIEW EXAM button,  Method: {method} ")
	public void reviewExamClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ReviewExam));
			ReviewExam.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	public String getExamText() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(examText));
		return examText.getText();
	}
	

	@Step("Check Confirm overlay displayed with 'No, I am not ready to begin' & 'Yes, I am ready to begin' buttons,  Method: {method} ")
	public boolean isConfirmOverlayWithYesNoToBeginAssignmentDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(TimeLimitConfirmYesButton));
			wait.until(ExpectedConditions.elementToBeClickable(TimeLimitConfirmNoButton));
			return TimeLimitConfirmYesButton.isDisplayed() && TimeLimitConfirmNoButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on 'No, I am not ready to begin' button,  Method: {method} ")
	public void clickNoIamNotReadyToBeginButton() throws Exception {
		try {
			TimeLimitConfirmNoButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on 'Yes, I am ready to begin' button,  Method: {method} ")
	public void clickYesIamReadyToBeginButton() throws Exception {
		try {
			TimeLimitConfirmYesButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public boolean isTimerIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(TimerIcon));
			return TimerIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getTimerText() throws Exception {
		return TimerText.getText();
	}

	public boolean verifyInitialAllQuestionPoints() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionPoint = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".2") + "'" + "]"));

			if (questionPoint.getText().indexOf("- /") == -1) {
				return false;
			}
		}
		return true;
	}

	public boolean verifyInitialAllQuestionAttempts() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionAttempt = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".3") + "'" + "]"));

			if (questionAttempt.getText().indexOf("- / ∞") == -1) {
				return false;
			}
		}
		return true;
	}

	public boolean verifyInitialAllQuestionStatus() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionStatus = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".4") + "'" + "]/span"));

			if (!questionStatus.getText().equals("Not Started")) {
				return false;
			}
		}
		return true;
	}

	public String getQuestionPoints(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionPoint = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".2") + "'" + "]"));

		return questionPoint.getText();
	}

	public String getQuestionAttempts(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionAttempt = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".3") + "'" + "]"));

		return questionAttempt.getText();
	}

	public String getQuestionStatus(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionStatus = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".4") + "'" + "]/span"));

		return questionStatus.getText();
	}

	public void clickQuestion(int questionNumber) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(QuestionTable));

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement question = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".0") + "'" + "]/button"));

		question.click();

		// Wait For Load Page Elements of StudentQuestionPlayer
		new StudentQuestionPlayer();
	}

	@Step("Click on Close Deadline Reached popup,  Method: {method} ")
	public void clickDeadlineReachedAlertClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3600);
			wait.until(ExpectedConditions.elementToBeClickable(CloseDeadlineReached));
			CloseDeadlineReached.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	@Step("Get Student Score on Student Assignment Page,  Method: {method} ")
	public String getHeaderScore() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(HeaderScore));
		return HeaderScore.getText().replace('\n', ' ');
	}
	
	@Step("Get Assignment OverDue Alert on Student Assignment Page,  Method: {method} ")
	public String getLatePenaltyAlert() throws Exception {
		
		String latePenaltyAlert = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part1));
				String strLatePenaltyAlert_part1 = latePenaltyText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part2));
				String strLatePenaltyAlert_part2 = latePenaltyText_part2.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part3));
				String strLatePenaltyAlert_part3 = latePenaltyText_part3.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part4));
				String strLatePenaltyAlert_part4 = latePenaltyText_part4.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part5));
				String strLatePenaltyAlert_part5 = latePenaltyText_part5.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part6));
				String strLatePenaltyAlert_part6 = latePenaltyText_part6.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part7));
				String strLatePenaltyAlert_part7 = latePenaltyText_part7.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part8));
				String strLatePenaltyAlert_part8 = latePenaltyText_part8.getText();
				
				wait.until(ExpectedConditions.visibilityOf(latePenaltyText_part9));
				String strLatePenaltyAlert_part9 = latePenaltyText_part9.getText();
				
				String latePenaltyAlertCombine = strLatePenaltyAlert_part1 + strLatePenaltyAlert_part2+ " " 
						+ strLatePenaltyAlert_part3 + strLatePenaltyAlert_part4 + strLatePenaltyAlert_part5
						+ " " + strLatePenaltyAlert_part6 + " " +strLatePenaltyAlert_part7 
						+ strLatePenaltyAlert_part8 + strLatePenaltyAlert_part9;
				
				latePenaltyAlert = latePenaltyAlertCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return latePenaltyAlert;

		}
	
	@Step("Get Assignment Extra Credit and Time Limit Alert on Student Assignment Page,  Method: {method} ")
	public String getExtraCredit_TimeLimit_Alert_Student() throws Exception {
		
		String extraCreditAndTimeLimitAlert = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_part1));
				String strExtraCreditAndTimeLimitAlert_part1 = extraCreditText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_part2));
				String strExtraCreditAndTimeLimitAlert_part2 = extraCreditText_part2.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_part3));
				String strExtraCreditAndTimeLimitAlert_part3 = extraCreditText_part3.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_part4));
				String strExtraCreditAndTimeLimitAlert_part4 = extraCreditText_part4.getText();
				
				String extraCreditAndTimeLimitAlertCombine = strExtraCreditAndTimeLimitAlert_part1 + " " + strExtraCreditAndTimeLimitAlert_part2
						+ " " + strExtraCreditAndTimeLimitAlert_part3 + " " + strExtraCreditAndTimeLimitAlert_part4;
				
				extraCreditAndTimeLimitAlert = extraCreditAndTimeLimitAlertCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return extraCreditAndTimeLimitAlert;

		}
	
	@Step("Get Assignment Extra Credit and Time Limit Alert on Instructor Assignment Page,  Method: {method} ")
	public String getExtraCredit_TimeLimit_Alert_Instructor() throws Exception {
		
		String extraCreditAndTimeLimitAlert = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_Instructor_part1));
				String strExtraCreditAndTimeLimitAlert_part1 = extraCreditText_Instructor_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_Instructor_part2));
				String strExtraCreditAndTimeLimitAlert_part2 = extraCreditText_Instructor_part2.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_Instructor_part3));
				String strExtraCreditAndTimeLimitAlert_part3 = extraCreditText_Instructor_part3.getText();
				
				wait.until(ExpectedConditions.visibilityOf(extraCreditText_Instructor_part4));
				String strExtraCreditAndTimeLimitAlert_part4 = extraCreditText_Instructor_part4.getText();
				
				String extraCreditAndTimeLimitAlertCombine = strExtraCreditAndTimeLimitAlert_part1 + " " + strExtraCreditAndTimeLimitAlert_part2 
						+ " " +strExtraCreditAndTimeLimitAlert_part3 + strExtraCreditAndTimeLimitAlert_part4;
				
				extraCreditAndTimeLimitAlert = extraCreditAndTimeLimitAlertCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return extraCreditAndTimeLimitAlert;

		}
	
	@Step("Get Assignment GAU Until Alert on Instructor Assignment Page,  Method: {method} ")
	public String getGAUUntilAlert_Instructor() throws Exception {
		
		String latePenaltyAlert = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part1));
				String strGAAUInfo_LatePenaltyText_part1 = gauInfo_LatePenaltyText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part2));
				String strGAAUInfo_LatePenaltyText_part2 = gauInfo_LatePenaltyText_part2.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part3));
				String strGAAUInfo_LatePenaltyText_part3 = gauInfo_LatePenaltyText_part3.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part4));
				String strGAAUInfo_LatePenaltyText_part4 = gauInfo_LatePenaltyText_part4.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part5));
				String strGAAUInfo_LatePenaltyText_part5 = gauInfo_LatePenaltyText_part5.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part6));
				String strGAAUInfo_LatePenaltyText_part6 = gauInfo_LatePenaltyText_part6.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part7));
				String strGAAUInfo_LatePenaltyText_part7 = gauInfo_LatePenaltyText_part7.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part8));
				String strGAAUInfo_LatePenaltyText_part8 = gauInfo_LatePenaltyText_part8.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part9));
				String strGAAUInfo_LatePenaltyText_part9 = gauInfo_LatePenaltyText_part9.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part10));
				String strGAAUInfo_LatePenaltyText_part10 = gauInfo_LatePenaltyText_part10.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part11));
				String strGAAUInfo_LatePenaltyText_part11 = gauInfo_LatePenaltyText_part11.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part12));
				String strGAAUInfo_LatePenaltyText_part12 = gauInfo_LatePenaltyText_part12.getText();
				
				
				
				String latePenaltyAlertCombine = strGAAUInfo_LatePenaltyText_part1+ " " + strGAAUInfo_LatePenaltyText_part2 
						+ strGAAUInfo_LatePenaltyText_part3 + strGAAUInfo_LatePenaltyText_part4 + " " + strGAAUInfo_LatePenaltyText_part5
						+ strGAAUInfo_LatePenaltyText_part6 + " " + strGAAUInfo_LatePenaltyText_part7 + strGAAUInfo_LatePenaltyText_part8
						+ strGAAUInfo_LatePenaltyText_part9 + " " + strGAAUInfo_LatePenaltyText_part10 + " " + strGAAUInfo_LatePenaltyText_part11
						+ strGAAUInfo_LatePenaltyText_part12;
				
				latePenaltyAlert = latePenaltyAlertCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return latePenaltyAlert;

		}
	
	@Step("Get Assignment GAU Until Alert on Student Assignment Page,  Method: {method} ")
	public String getGAUUntilAlert_Student() throws Exception {
		
		String latePenaltyAlert = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part1));
				String strGAAUInfo_std_LatePenaltyText_part1 = gauInfo_std_LatePenaltyText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part2));
				String strGAAUInfo_std_LatePenaltyText_part2 = gauInfo_std_LatePenaltyText_part2.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part3));
				String strGAAUInfo_std_LatePenaltyText_part3 = gauInfo_std_LatePenaltyText_part3.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part4));
				String strGAAUInfo_std_LatePenaltyText_part4 = gauInfo_std_LatePenaltyText_part4.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part5));
				String strGAAUInfo_std_LatePenaltyText_part5 = gauInfo_std_LatePenaltyText_part5.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part6));
				String strGAAUInfo_std_LatePenaltyText_part6 = gauInfo_std_LatePenaltyText_part6.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part7));
				String strGAAUInfo_std_LatePenaltyText_part7 = gauInfo_std_LatePenaltyText_part7.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part8));
				String strGAAUInfo_std_LatePenaltyText_part8 = gauInfo_std_LatePenaltyText_part8.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_std_LatePenaltyText_part9));
				String strGAAUInfo_std_LatePenaltyText_part9 = gauInfo_std_LatePenaltyText_part9.getText();
				
				
				
				String latePenaltyAlertCombine = strGAAUInfo_std_LatePenaltyText_part1+ " " + strGAAUInfo_std_LatePenaltyText_part2 
						+ strGAAUInfo_std_LatePenaltyText_part3 + strGAAUInfo_std_LatePenaltyText_part4 + " " + strGAAUInfo_std_LatePenaltyText_part5
						+ " " + strGAAUInfo_std_LatePenaltyText_part6 + strGAAUInfo_std_LatePenaltyText_part7 + " " + strGAAUInfo_std_LatePenaltyText_part8
						+ strGAAUInfo_std_LatePenaltyText_part9;
				
				latePenaltyAlert = latePenaltyAlertCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return latePenaltyAlert;

		}
	
	
	
	@Step("Get Exam Settings Assignment Description on Student Assignment Page,  Method: {method} ")
	public String ExamSettings_getAssignmentDescription_SAP_BeforeGAU() throws Exception {
		
		String examSettings_AssignmentInfo = "";
		
		try {
				
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_part1));
			String assignmentInfo_Part1 = assignmentInfoText_part1.getText();
			
			//wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_part2));
			//String assignmentInfo_Part2 = assignmentInfoText_part2.getText();
			
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part3));
			String assignmentInfo_Part3 = assignmentInfoText_Part3.getText();
			
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part4));
			String assignmentInfo_Part4 = assignmentInfoText_Part4.getText();
			
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part5));
			String assignmentInfo_Part5 = assignmentInfoText_Part5.getText();
			
			String strAssignmentInfoCombine = assignmentInfo_Part1 + " " + 
			assignmentInfo_Part3 + assignmentInfo_Part4 + " " + assignmentInfo_Part5;

			examSettings_AssignmentInfo = strAssignmentInfoCombine.replaceAll("&nbsp", "");
		
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		
			return examSettings_AssignmentInfo;

		}
	
	
	@Step("Get Exam Settings Assignment Description on Student Assignment Page,  Method: {method} ")
	public String ExamSettings_getAssignmentDescription_SAP_AfterGAU() throws Exception {
		
		String examSettings_AssignmentInfo = "";
		
		try {
				
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_part1));
			String assignmentInfo_Part1 = assignmentInfoText_part1.getText();
			
			//wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_part2));
			//String assignmentInfo_Part2 = assignmentInfoText_part2.getText();
			
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part3));
			String assignmentInfo_Part3 = assignmentInfoText_Part3.getText();
			
			wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part4));
			String assignmentInfo_Part4 = assignmentInfoText_Part4.getText();
			
			String strAssignmentInfoCombine = assignmentInfo_Part1 + " " + 
					assignmentInfo_Part3 + assignmentInfo_Part4;

			examSettings_AssignmentInfo = strAssignmentInfoCombine.replaceAll("&nbsp", "");
		
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		
			return examSettings_AssignmentInfo;

		}
	
	
	@Step("Get Assignment Time Zone information on Student Assignment Page,  Method: {method} ")
	public String getAssignmentTimeZoneInfo_SAP() throws Exception {
		
		String assignmentTimeZoneInfo = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_part1));
				String assignmentTimeZoneInfo_Part1 = assignmentInfoText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(assignmentInfoText_Part3));
				String assignmentInfoTextInfo_Part3 = assignmentInfoText_Part3.getText();
				
				String strAssignmentTimeZoneInfoCombine = assignmentTimeZoneInfo_Part1 + " " +
						assignmentInfoTextInfo_Part3;

				assignmentTimeZoneInfo = strAssignmentTimeZoneInfoCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return assignmentTimeZoneInfo;

		}
	
	
	@Step("Get Assignment Time Zone information on Instructor Assignment Page,  Method: {method} ")
	public String getAssignmentTimeZoneInfo_Instructor() throws Exception {
		
		String assigmmentTimeZoneInfo = "";
		
		try {
				
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part1));
				String strGAAUInfo_LatePenaltyText_part1 = gauInfo_LatePenaltyText_part1.getText();
				
				wait.until(ExpectedConditions.visibilityOf(gauInfo_LatePenaltyText_part2));
				String strGAAUInfo_LatePenaltyText_part2 = gauInfo_LatePenaltyText_part2.getText();
				
				String assigmmentTimeZoneInfoCombine = strGAAUInfo_LatePenaltyText_part1+ " " 
				+ strGAAUInfo_LatePenaltyText_part2;
				
				assigmmentTimeZoneInfo = assigmmentTimeZoneInfoCombine.replaceAll("&nbsp", "");
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
		
			return assigmmentTimeZoneInfo;

		}
	
	
	@Step("Get Extra Credit Title Suffix on SAP,  Method: {method} ")
	public String getExtraCreditTitleSuffix_SAP() throws Exception {
		
		String extraCreditTitle;
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(extraCreditTitle_SAP));
			extraCreditTitle = extraCreditTitle_SAP.getText();
		} catch (Exception e) {
			return null;
		}
		
		return extraCreditTitle;
	}
	
	
	@Step("Follow Up is displayed as the end on SAP ?,  Method: {method} ")
	public boolean followUpDisplayedAtEnd_SAP() throws Exception {
		
		boolean  FollowUpExist;
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#realQuestionTable tbody tr').length");

		int questionsCount =  Integer.valueOf(rowCountObj.toString());
	
		
	try {
			WebElement	questionTitle = driver
						.findElement(By.xpath("//*[@id=\"realQuestionTable\"]/tbody/tr[" 
				+ questionsCount + "]/td[@class='textIndentLeft']/button[@class='btn-cell']/span[contains(text(),'Follow Up')]"));
			
			ReusableMethods.scrollIntoView(driver, questionTitle);
			FollowUpExist = questionTitle.isDisplayed();
			questionTitle.click();
			
			return FollowUpExist;

		} catch (Exception e) {
			e.getMessage(); 
			return false;
		}
	}
	
	@Step("Get all Question titles in Student Assignment page,  Method: {method} ")
	public List<String> getAllQuestionList() {
		
		List<String> questionList = new ArrayList<String>();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(QuestionTable));
		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 1; i < AllQuestions.size(); i++) {

			WebElement questionTitle = QuestionTable.findElement(
					By.xpath("//tr["+ i + "]/td[1]/button/span[@class='question-text']"));
			
			questionList.add(i-1, questionTitle.getText());
			
		}
		
		return questionList;
		
	}
	
	@Step("Get all Question Points in Student Assignment page,  Method: {method} ")
	public List<String> getAllQuestionPointsList() {
		
		List<String> questionPointsList = new ArrayList<String>();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(QuestionTable));
		AllQuestions = QuestionTable.findElements(By.tagName("tr"));
		
		for (int i = 1; i <= AllQuestions.size(); i++) {

				WebElement questionPoint = QuestionTable.findElement(By.xpath("//tr["+ i + "]/td[3]"));
			
				questionPointsList.add(i-1, questionPoint.getText());
			
		}
		
		return questionPointsList;
		
	}
	
}
