package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH8HW {

	WebDriver driver;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH8HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("Enter Correct Answer for question #1,  Method: {method} ")
	public void enterQuestion1_Correct() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		// # Part - 1

		// Correct answer
		WebElement correctAnswer = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][(text()='covalent')]]//input")));
		ReusableMethods.scrollToElement(driver, By.xpath(
				"//span[@class='multiple-option-text']/span[contains(text(),'D.')]"));
		Thread.sleep(3000);
		act.moveToElement(correctAnswer).build().perform();
        
		// Enter Correct Answer
		//correctAnswer.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswer);

		// # Part - 2
		
		//js.executeScript("window.scrollBy(0,2500)");
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_labeling_div0489591119bdeaa6bb7f086003f078f9de2221d7c100359_"));
		
		// Labeling Options
		WebElement valueD = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-4")));
		WebElement valueE = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-5")));
		WebElement valueF = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-6")));

		// Labeling Question
		WebElement moleculeCountA = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-0489591119bdeaa6bb7f086003f078f9de2221d7c100359_-1")));
		WebElement moleculeCountB = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-0489591119bdeaa6bb7f086003f078f9de2221d7c100359_-2")));
		WebElement moleculeCountC = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-0489591119bdeaa6bb7f086003f078f9de2221d7c100359_-3")));
		
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_labeling_div0489591119bdeaa6bb7f086003f078f9de2221d7c100359_"));
			
		// Drag and drop each Correct Count to its option respectively
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, valueE);
		act.dragAndDrop(valueE, moleculeCountA).build().perform();
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, valueF);
		act.dragAndDrop(valueF, moleculeCountB).build().perform();
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, valueD);
		act.dragAndDrop(valueD, moleculeCountC).build().perform();
		
	}

	@Step("Enter Incorrect Answer for question #2,  Method: {method} ")
	public void enterQuestion2_Incorrect() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Substances Part 1
		WebElement valueA = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-1']")));
		WebElement valueB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-2']")));
		WebElement valueC = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-3']")));
		WebElement valueD = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//span[@id='answer-4']")));

		// Drag type where Substance need to be dropped, Part 1.
		WebElement electronCountA = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-9386977729bdeaa6bb7f086003f078f9de2221d7c108878_-1")));
		WebElement electronCountB = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-9386977729bdeaa6bb7f086003f078f9de2221d7c108878_-2")));
		WebElement electronCountC = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-9386977729bdeaa6bb7f086003f078f9de2221d7c108878_-3")));
		WebElement electronCountD = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-9386977729bdeaa6bb7f086003f078f9de2221d7c108878_-4")));
		
		ReusableMethods.scrollToElement(driver, By.id("removeAnswer-9386977729bdeaa6bb7f086003f078f9de2221d7c108878_"));
		Thread.sleep(3000);
		
		// Drag and drop each item to its type respectively
		act.dragAndDrop(valueA, electronCountA).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueB, electronCountB).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueC, electronCountC).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueD, electronCountD).build().perform();
		Thread.sleep(2000);
	}
	
	@Step("Enter Incorrect Answer for question #2,  Method: {method} ")
	public void Question2_Incorrect() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Substances Part 1
		WebElement CLO2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
				"item-answer-5-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		WebElement CL2O7 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
				"item-answer-1-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		WebElement CLO4 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
				"item-answer-3-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		WebElement CLO3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
				"item-answer-4-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		WebElement CL2O6 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
				"item-answer-2-2208392212e0d99b793349f08101be1d61405adf7003033_")));

		// Drag type where Substance need to be dropped, Part 1.
		WebElement evenNumber = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("categorySubContainer-1-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		WebElement oddNumber = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("categorySubContainer-2-2208392212e0d99b793349f08101be1d61405adf7003033_")));
		
		
		ReusableMethods.scrollToElement(driver, By.id("sortingPreviewAnswerItemList-2208392212e0d99b793349f08101be1d61405adf7003033_"));
		Thread.sleep(2000);
		
		// Drag and drop each item to its type respectively
		act.dragAndDrop(CLO2, evenNumber).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(CL2O7, evenNumber).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(CLO4, oddNumber).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(CLO3, oddNumber).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(CL2O6, oddNumber).build().perform();
		Thread.sleep(2000);
	}
}
