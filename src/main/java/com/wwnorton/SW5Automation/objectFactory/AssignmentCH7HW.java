package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH7HW {

	WebDriver driver;
	Actions act;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")
	public WebElement QuestionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-b']")
	public WebElement QuestionTextCurrent;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-s']")
	public WebElement QuestionTextTotal;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH7HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	@Step("Drag And Drop InCorrect Answer for question #1,  Method: {method} ")
	public void question1EnterInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement part1Element = driver.findElement(By.id("part112824"));
		ReusableMethods.scrollIntoView(driver, part1Element);

		// Part 1
		// Element which needs to drag.
		WebElement part1DragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-7-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DragItem6Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));

		// Element on which need to drop.
		WebElement part1DropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));
		WebElement part1DropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-55752662480a4ae0fb6778251e96fa4defa95cf94400146_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		act.dragAndDrop(part1DragItem1Element, part1DropContainer2Element).build().perform();
		act.dragAndDrop(part1DragItem4Element, part1DropContainer1Element).build().perform();
		act.dragAndDrop(part1DragItem5Element, part1DropContainer1Element).build().perform();
		act.dragAndDrop(part1DragItem2Element, part1DropContainer1Element).build().perform();
		act.dragAndDrop(part1DragItem3Element, part1DropContainer2Element).build().perform();
		act.dragAndDrop(part1DragItem6Element, part1DropContainer2Element).build().perform();

		WebElement draggableItemList2 = driver
				.findElement(By.id("sortingPreviewAnswerItemList-55753957360a4ae0fb6778251e96fa4defa95cf94407386_"));

		//ReusableMethods.scrollIntoView(driver, draggableItemList2);
		ReusableMethods.scrollToElement(driver, By.id("sortingPreviewAnswerItemList-55753957360a4ae0fb6778251e96fa4defa95cf94407386_"));
	
		// Part 2
		// Element which needs to drag.
		WebElement part2DragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-7-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DragItem6Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));

		// Element on which need to drop.
		WebElement part2DropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));
		WebElement part2DropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-55753957360a4ae0fb6778251e96fa4defa95cf94407386_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(5000);
		/*org.openqa.selenium.interactions.Action dragAndDrop = act.clickAndHold(part2DragItem2Element).moveToElement(part2DropContainer1Element).release(part2DropContainer1Element).build();
		dragAndDrop.perform();*/
		
		
		act.dragAndDrop(part2DragItem2Element, part2DropContainer1Element).build().perform();
		//driver.findElement(By.xpath("//div[@id='answerCategoryListTitle-1-55753957360a4ae0fb6778251e96fa4defa95cf94407386_']")).click();
		act.dragAndDrop(part2DragItem3Element, part2DropContainer2Element).build().perform();
		act.dragAndDrop(part2DragItem6Element, part2DropContainer2Element).build().perform();
		act.dragAndDrop(part2DragItem1Element, part2DropContainer2Element).build().perform();
		act.dragAndDrop(part2DragItem4Element, part2DropContainer1Element).build().perform();
		act.dragAndDrop(part2DragItem5Element, part2DropContainer1Element).build().perform();

	}
	
	@Step("Drag And Drop InCorrect Answer for question #1,  Method: {method} ")
	public void question1InCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_"));

		// Element which needs to drag.
		WebElement DragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));
		WebElement DragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));
		WebElement DragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));
		WebElement DragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));
		

		// Element on which need to drop.
		WebElement DropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));
		WebElement DropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-7463939192bdeaa6bb7f086003f078f9de2221d7c101262_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem1Element);
		act.dragAndDrop(DragItem1Element, DropContainer1Element).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem2Element);
		act.dragAndDrop(DragItem2Element, DropContainer1Element).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem3Element);
		act.dragAndDrop(DragItem3Element, DropContainer2Element).build().perform();
		
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, DragItem4Element);
		act.dragAndDrop(DragItem4Element, DropContainer2Element).build().perform();

	}

	@Step("Enter In-Correct Answer for question #4,  Method: {method} ")
	public void question4EnterInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q4Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number42834307447af0dc17f3d42d2ce65824d6b33a55a507856_")));

		// Enter InCorrect Answer
		Q4Part1Element.clear();

		Q4Part1Element.sendKeys("10");

	}
	
	@Step("Enter In-Correct Answer for question #4,  Method: {method} ")
	public void question4InCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number42803270087af0dc17f3d42d2ce65824d6b33a55a504765_")));

		// Enter InCorrect Answer
		Q4Element.clear();

		Q4Element.sendKeys("10");

	}
}
