package com.wwnorton.SW5Automation.objectFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class Questions_Page {

	WebDriver driver;
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readAddquestionJson();

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14'][span[contains(text(),'ADD QUESTIONS')]]")
	public WebElement AddQuestionButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-black-14 btn-create-pool-ql']/span[contains(text(),'CREATE NEW POOL')]")
	public WebElement createPoolQuestionButton;
	
	@FindBy(how = How.XPATH, using = "//input[@class='edit-pool-title form-control']")
	public WebElement poolTitle;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary btn-continue lato-black-14 fr']/span[contains(text(),'Continue')]")
	public WebElement poolContinueButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary btn-save-exit lato-black-14 fr']/span[contains(text(),'Save & Exit')]")
	public WebElement poolSaveExitButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-black-14']/span[contains(text(),'Add and Continue')]")
	public WebElement poolAddContinueButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary btn-add-question lato-black-14 margin-left-25']/span[text()='Add Question']")
	public WebElement addPoolQuestion;
	
	
	@FindBy(how = How.XPATH, using = "//select[@data-reactid='.0.2.0.3.1.0.0.1.3']")
	public WebElement selectChapter;
	
	@FindBy(how = How.XPATH, using = "//span[@data-reactid='.0.3.2.0.0.0.1']")
	public WebElement new_selectChapter;

	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'TYPE')]")
	public WebElement TypeListBox;

	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'Numeric Entry')]")
	public WebElement NumericEntry_checkbox;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'Multiple Select')]")
	public WebElement MultipleSelect_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'Pool')]")
	public WebElement Pool_Checkbox;

	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons.glyphicons-plus']")
	public WebElement QuestionplusIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-title fl']/a")
	public WebElement HeadTitleLink;

	@FindBys(@FindBy(how = How.XPATH, using = "//table[@id='realQuestionTable']/tbody/tr"))
	public List<WebElement> questionslist;

	@FindBys(@FindBy(how = How.XPATH, using = "//span[@class='renderRichText']"))
	public List<WebElement> questionsTextlist;

	@FindBy(how = How.ID, using = "answer-1")
	public WebElement answer1;
	@FindBy(how = How.ID, using = "answer-2")
	public WebElement answer2;
	@FindBy(how = How.ID, using = "answer-3")
	public WebElement answer3;
	@FindBy(how = How.ID, using = "answer-4")
	public WebElement answer4;

	@FindBy(how = How.ID, using = "targetIconUse-6322814897bdeaa6bb7f086003f078f9de2221d7c102289_-1")
	public WebElement circleid1;
	@FindBy(how = How.ID, using = "targetIconUse-6322814897bdeaa6bb7f086003f078f9de2221d7c102289_-2")
	public WebElement circleid2;

	@FindBy(how = How.ID, using = "targGroup-6322814897bdeaa6bb7f086003f078f9de2221d7c102289_-3")
	public WebElement circleid3;

	@FindBy(how = How.ID, using = "targGroup-6322814897bdeaa6bb7f086003f078f9de2221d7c102289_-4")
	public WebElement circleid4;

	@FindBy(how = How.ID, using = "practiceBtn")
	public WebElement practiceButton;

	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']")
	public WebElement Alertbox;

	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/button[@class='close']")
	public WebElement Alertbuttonclose;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'REVIEW ASSIGNMENT')]")
	public WebElement ReviewAssignmentbutton;

	@FindBy(how = How.CLASS_NAME, using = "late-penalty-alert")
	public WebElement Late_work_Popup;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span")
	public WebElement GotItButton;

	@FindBy(how = How.XPATH, using = "//div[@class='search-text-input']/input[@type='text']")
	public WebElement SearchQuestionTextbox;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-search']")
	public WebElement Searchbutton;
	
	@FindBy(how = How.XPATH, using = "//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")
	public WebElement questionplusIcon;
	
	@FindBy(how = How.XPATH, using = "//tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='ant-checkbox']")
	public WebElement questionCheckBox;

	@FindBy(how = How.XPATH, using = "//b[span[contains(text(),'Adaptive')]]")
	public WebElement AdpativeSwitchText;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[2]/td[8]/div/span[@class='glyphicons glyphicons-pencil pencilIcon']")
	public WebElement editQuestionPencilIcon; 
	
	@FindBy(how = How.XPATH, using = "//button[@id='ltiSavelQuestion']/span[@class='btn-new-color-2020']")
	public WebElement btnCopyQuestionSave;
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons glyphicons-chevron-left']")
	public WebElement returnToEDitAssignment;
	
	@FindBy(how = How.XPATH, using = "//div[@class='edit-question-title-content']/input[@id='questionTitle']")
	public WebElement editQuestionTitle;
	
	@FindBy(how = How.XPATH, using = "//span[@class='text']/span[contains(text(),'Copy of Determining Phase Changes from Chemical Equations')]")
	public WebElement copyQuestionTitle;
	
	@FindBy(how = How.XPATH, using = "//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/thead/tr/th[@class='defaultOrder textIndentLeft meta-content-bold']/span[@class='meta-content-regular']")
	public WebElement questionCount;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'CHAPTER')]")
	public WebElement chapterFilter;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'Chapter 1: Particles of Matter: Measurement and the Tools of Science')]")
	public WebElement chapter1_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'OBJECTIVE')]")
	public WebElement objectiveFilter;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'1.C Classify forms of matter and understand how they differ.')]")
	public WebElement objective1_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'SERIES')]")
	public WebElement seriesFilter;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'EOC: End of Chapter')]")
	public WebElement series1_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'DIFFICULTY')]")
	public WebElement difficultyFilter;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'Hard')]")
	public WebElement difficultyHard_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-select']/div/span[contains(text(),'AUTHOR')]")
	public WebElement authorFilter;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ant-tree-title'][contains(.,'My Questions')]")
	public WebElement author_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='meta-content-bold fr']/a[contains(text(),'Clear all filters')]")
	public WebElement clearAllFilters_link;
	
	@FindBy(how = How.XPATH, using = "//img[@class='pool-icon-32Lc3Z']")
	public WebElement poolQuestionIcon;
	
	

	public Questions_Page() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	

	@Step("Click on the Add Questions Button,  Method: {method} ")
	public void addQuestions() {
		ReusableMethods.scrollIntoView(driver, AdpativeSwitchText);
		AddQuestionButton.click();

	}
	
	@Step("Get total Question count on the Questions Library page,  Method: {method} ")
	public int questionsCount() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/thead/tr/th[@class='defaultOrder textIndentLeft meta-content-bold']/span[@class='meta-content-regular']")));
		ReusableMethods.scrollIntoView(driver, questionCount);
		String str = questionCount.getText();
		String[] parts = str.split("()", 2);
		String PartTwo =  parts[1];
		
		String[] countStr = PartTwo.split(" ", 2);
		String count = countStr[0].toString();
		int intCount = Integer.valueOf(count);

		return intCount;
	}
	
	@Step("Apply Chapter Filter in Question Library,  Method: {method} ")
	public void applyChapterFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, chapterFilter);
		chapterFilter.click();
		Thread.sleep(3000);
		chapter1_Checkbox.click();
		chapterFilter.click();
	}
	
	@Step("Apply Objective Filter in Question Library,  Method: {method} ")
	public void applyObjectiveFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, objectiveFilter);
		objectiveFilter.click();
		Thread.sleep(3000);
		objective1_Checkbox.click();
		objectiveFilter.click();
	}
	
	@Step("Apply Series Filter in Question Library,  Method: {method} ")
	public void applySeriesFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, seriesFilter);
		seriesFilter.click();
		Thread.sleep(3000);
		series1_Checkbox.click();
		seriesFilter.click();
	}
	
	@Step("Apply Type Filter in Question Library,  Method: {method} ")
	public void applyTypeFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, TypeListBox);
		TypeListBox.click();
		Thread.sleep(3000);
		MultipleSelect_Checkbox.click();
		TypeListBox.click();
	}
	
	@Step("Apply Pool Type Filter in Question Library,  Method: {method} ")
	public void applyTypeFilter_Pool() throws Exception{
		ReusableMethods.scrollIntoView(driver, TypeListBox);
		TypeListBox.click();
		Thread.sleep(3000);
		Pool_Checkbox.click();
		TypeListBox.click();
	}
	
	@Step("Apply Difficulty Filter in Question Library,  Method: {method} ")
	public void applyDifficultyFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, difficultyFilter);
		difficultyFilter.click();
		Thread.sleep(3000);
		difficultyHard_Checkbox.click();
		difficultyFilter.click();
	}
	
	@Step("Apply Author Filter in Question Library,  Method: {method} ")
	public void applyAuthorFilter() throws Exception{
		ReusableMethods.scrollIntoView(driver, authorFilter);
		authorFilter.click();
		Thread.sleep(3000);
		author_Checkbox.click();
		authorFilter.click();
	}
	
	@Step("Clear All Filters in Question Library,  Method: {method} ")
	public void clearAllFilters() throws Exception{
		ReusableMethods.scrollIntoView(driver, clearAllFilters_link);
		clearAllFilters_link.click();
	}
	
	@Step("Edit an existing Question,  Method: {method} ")
	public String editQuestion() throws Exception {
		
		ReusableMethods.scrollIntoView(driver, editQuestionPencilIcon);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", editQuestionPencilIcon);
		
		//driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(editQuestionTitle));
		String strQuestionTitle = editQuestionTitle.getAttribute("value");

		ReusableMethods.scrollIntoView(driver, btnCopyQuestionSave);
		//((JavascriptExecutor)driver).executeScript("arguments[0].click();", btnCopyQuestionSave);
		btnCopyQuestionSave.click();
		
		Thread.sleep(10000);
		returnToEDitAssignment.click();
		
		//driver.switchTo().defaultContent();
		
		return strQuestionTitle;
		
	}
	
	@Step("Click on the Create New Pool Question Button,  Method: {method} ")
	public void clickCreateNewPoolQuestionsButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(createPoolQuestionButton));
		ReusableMethods.scrollIntoView(driver, createPoolQuestionButton);
		createPoolQuestionButton.click();

	}
	
	@Step("Add Questions to Assignment,  Method: {method} ")
	public void addQuestionsToAssignment(String searchKey, String ques) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[@class='questionLibraryLst']/tr")));
		
		WebElement questionTitle;
		WebElement questionPlusSign;
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
		
		
		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(searchKey);
		Thread.sleep(3000);
		Searchbutton.click();
		
		Thread.sleep(3000);
		
		for(int i=1; i<= 10; i++)
		{ 
			questionPlusSign =
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr["
							+ i	+ "]/td[1]/span"));

			questionTitle = 
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr[" 
							+ i	+ "]/td[3]/div[@class='queTitle-queLibrary']"));

			if (questionTitle.getText().equalsIgnoreCase(ques)) {
				ReusableMethods.scrollIntoView(driver, questionPlusSign);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionPlusSign);
				break;
			}
		}

	}
	
	
	@Step("Add Questions to Assignment,  Method: {method} ")
	public void addLargePoolQuestionsToAssignment(String searchKey, int noOfQuestions) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[@class='questionLibraryLst']/tr")));

		WebElement questionPlusSign;
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
		
		
		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(searchKey);
		Thread.sleep(3000);
		Searchbutton.click();
		
		Thread.sleep(3000);
		
		int counter=1;
		while (counter<= noOfQuestions)
		{ 
			questionPlusSign =
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr/td/span[@class='glyphicons glyphicons-plus']"));
			
			ReusableMethods.scrollIntoView(driver, questionPlusSign);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionPlusSign);

			counter++;
			
		}

	}
	
	
	@Step("Add Questions to Assignment,  Method: {method} ")
	public void addExistingPoolQuestionsToAssignment(String searchKey, int noOfPoolQuestions) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[@class='questionLibraryLst']/tr")));

		WebElement questionPlusSign;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
		
		
		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(searchKey);
		Thread.sleep(3000);
		Searchbutton.click();
		
		Thread.sleep(3000);
		int counter=1;
		while (counter<= noOfPoolQuestions)
		{ 
			Thread.sleep(2000);
			questionPlusSign =
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr[" + counter + "]/td/span[@class='glyphicons glyphicons-plus']"));
			
			ReusableMethods.scrollIntoView(driver, questionPlusSign);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionPlusSign);

			counter++;
			
		}

	}
	
	
	@Step("Create New Pool Question,  Method: {method} ")
	public void createNewPoolQuestions(String poolQuestionName, String searchKey, String ques1, String ques2) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@class='edit-pool-title form-control']")));
		
		poolTitle.sendKeys(poolQuestionName);
		poolContinueButton.click();
		
		WebElement questionTitle;
		WebElement questionCheckbox;
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[@class='questionLibraryLst']/tr")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
		
		
		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(searchKey);
		Thread.sleep(3000);
		Searchbutton.click();
		
		Thread.sleep(3000);
		
		for(int i=1; i<= 10; i++)
		{ 
			questionCheckbox =
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr["
							+ i	+ "]/td[1]/span/input[@type='checkbox']"));

			questionTitle = 
					driver.findElement(By.xpath("//table/tbody[@class='questionLibraryLst']/tr[" 
							+ i	+ "]/td[3]/div[@class='queTitle-queLibrary']"));

			if (questionTitle.getText().equalsIgnoreCase(ques1)) {
				ReusableMethods.scrollIntoView(driver, questionCheckbox);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionCheckbox);
			}

			if (questionTitle.getText().equalsIgnoreCase(ques2)) {
				ReusableMethods.scrollIntoView(driver, questionCheckbox);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionCheckbox);
				break;
			}
		} 
			
		
		wait.until(ExpectedConditions.elementToBeClickable(poolAddContinueButton));
		poolAddContinueButton.click();
		
		//wait.until(ExpectedConditions.visibilityOf(poolSaveExitButton));
		Thread.sleep(3000);
		Select oselectChapter = new Select(selectChapter);
		oselectChapter.selectByValue("1729");
		poolSaveExitButton.click();
		
		Thread.sleep(3000);
		WebElement poolQuestionTitle;
		WebElement addPoolQuestionTitle;
		
		for(int i=1; i<=5; i++) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tbody[@class='questionLibraryLst']/tr[1]/td[3]/div/span")));
			poolQuestionTitle = driver.findElement(By.xpath("//tbody[@class='questionLibraryLst']/tr[" + i + "]/td[3]/div/span"));
			addPoolQuestionTitle = driver.findElement(By.xpath("//tbody[@class='questionLibraryLst']/tr[" + i + "]/td[1]/span"));
			if(poolQuestionTitle.getText().equalsIgnoreCase(poolQuestionName)) {
				addPoolQuestionTitle.click();
				break;
			}
		}

	}
	
	
	@Step("Create New Pool Question,  Method: {method} ")
	public String createMultiplePoolQuestions(String questionsSetName, int noOfQuestions) throws InterruptedException {
			
			WebDriverWait wait = new WebDriverWait(driver, 50);

			Thread.sleep(3000);
			clickCreateNewPoolQuestionsButton();

			String PoolTitleSuffix = "CustomPoolQuestion ";
			int suffixCharCount = 5;

			String PoolQuestionName = PoolTitleSuffix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();

			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//input[@class='edit-pool-title form-control']")));

			poolTitle.sendKeys(PoolQuestionName);
			poolContinueButton.click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
			Thread.sleep(3000);
 
				JsonElement element = jsonobject.get(questionsSetName);
				JsonObject obj = element.getAsJsonObject();
				Set<String> questionsKeys = obj.keySet();
				Iterator<String> it = questionsKeys.iterator();
	
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//table/tbody[@class='questionLibraryLst']/tr")));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
	
				WebElement questionCheckbox;
	
				int counter = 0;
				while (it.hasNext()) {
					String key = it.next();
					String value = obj.get(key).getAsString();
	
					SearchQuestionTextbox.clear();
					ReusableMethods.waitForPageLoad(driver);
					SearchQuestionTextbox.sendKeys(value);
					Thread.sleep(3000);
					Searchbutton.click();
	
					WebDriverWait wait1 = new WebDriverWait(driver, 50);
					wait1.until(ExpectedConditions.elementToBeClickable(
							By.xpath("//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']")));
	
					questionCheckbox = driver.findElement(
							By.xpath("//table/tbody[@class='questionLibraryLst']/tr/td/span/input[@type='checkbox']"));
					questionCheckbox.click();
					Thread.sleep(1000);
					SearchQuestionTextbox.clear();
					Thread.sleep(1000);
	
					wait.until(ExpectedConditions.elementToBeClickable(poolAddContinueButton));
					poolAddContinueButton.click();
	
					Thread.sleep(3000);
					Select oselectChapter = new Select(selectChapter);
					oselectChapter.selectByValue("1729");
	
					if (counter != noOfQuestions) {
	
						Thread.sleep(2000);
						wait.until(ExpectedConditions.elementToBeClickable(addPoolQuestion));
						ReusableMethods.scrollIntoView(driver, addPoolQuestion);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", addPoolQuestion);
	
						List<WebElement> saveAndContinueButton = wait
								.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
										"//button[@class='btn-submit-s btn-primary fr lato-regular-14' and text()='Save and Continue']")));
	
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", saveAndContinueButton.get(0));
	
					}
	
					else {
						ReusableMethods.scrollIntoView(driver, poolSaveExitButton);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", poolSaveExitButton);
					}
	
					counter++;
				}
				
				return PoolQuestionName;

		}
	
	
	@Step("Add newly created Pool Questions to Assignment ,  Method: {method} ")
	public void addNewPoolQuestionsToAssignment(String PoolQuestionName ) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement poolQuestionTitle;
		WebElement addPoolQuestionTitle;

		Thread.sleep(3000);
		for (int j = 1; j <= 5; j++) {
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//tbody[@class='questionLibraryLst']/tr[1]/td[3]/div/span")));
			poolQuestionTitle = driver
					.findElement(By.xpath("//tbody[@class='questionLibraryLst']/tr[" + j + "]/td[3]/div/span"));
			addPoolQuestionTitle = driver
					.findElement(By.xpath("//tbody[@class='questionLibraryLst']/tr[" + j + "]/td[1]/span"));
			if (poolQuestionTitle.getText().equalsIgnoreCase(PoolQuestionName)) {
				addPoolQuestionTitle.click();
				break;
			}
		}

	}
	
	
	@Step("Create Large Pool Question,  Method: {method} ")
	public void createLargePoolQuestions(String poolQuestionName, String questionsSetName, int noOfQuestions) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@class='edit-pool-title form-control']")));
		
		poolTitle.sendKeys(poolQuestionName);
		poolContinueButton.click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		Thread.sleep(3000);
		
		JsonElement element = jsonobject.get(questionsSetName);
		JsonObject obj = element.getAsJsonObject();
		Set<String> questionsKeys = obj.keySet();
		Iterator<String> it = questionsKeys.iterator();
		
		WebElement questionCheckbox;
		int counter = 0;
		while (it.hasNext()) {
			String key = it.next();
			String value = obj.get(key).getAsString();

			SearchQuestionTextbox.clear();
			ReusableMethods.waitForPageLoad(driver);
			SearchQuestionTextbox.sendKeys(value);
			Thread.sleep(3000);
			Searchbutton.click();
			
			WebDriverWait wait1 = new WebDriverWait(driver, 50);
			wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(
					"//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']")));
			
			questionCheckbox =
					driver.findElement(By.xpath(
							"//table/tbody[@class='questionLibraryLst']/tr/td/span/input[@type='checkbox']"));
			questionCheckbox.click();
			Thread.sleep(1000);
			SearchQuestionTextbox.clear();
			Thread.sleep(1000);
			
			wait.until(ExpectedConditions.elementToBeClickable(poolAddContinueButton));
			poolAddContinueButton.click();
			
			Thread.sleep(3000);
			Select oselectChapter = new Select(selectChapter);
			oselectChapter.selectByValue("1729");
			
			if (counter != noOfQuestions) {
			
				Thread.sleep(2000);
				wait.until(ExpectedConditions.elementToBeClickable(addPoolQuestion));
				ReusableMethods.scrollIntoView(driver, addPoolQuestion);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", addPoolQuestion);
			
				List <WebElement> saveAndContinueButton = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
						By.xpath("//button[@class='btn-submit-s btn-primary fr lato-regular-14' and text()='Save and Continue']")));
	
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", saveAndContinueButton.get(0));
			
			}
			
			else 
				{
					ReusableMethods.scrollIntoView(driver, poolSaveExitButton);
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", poolSaveExitButton);
					//poolSaveExitButton.click();
				}
			
			counter++;
		}

		

	}


	@Step("Select the Numeric Entry from the Type List Box ,  Method: {method} ")
	public void SelectTypeListbox() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='dropdown-select']/div/span[contains(text(),'TYPE')]")));
		
		TypeListBox.click();
		Thread.sleep(5000);
		NumericEntry_checkbox.click();
		driver.findElement(By.xpath("//input[@type='text']")).click();
		Thread.sleep(5000);
		questionsLibraryList();

	}

	@Step("Click the Head Title Link ,  Method: {method} ")
	public void headTitleLink() throws InterruptedException {

		HeadTitleLink.click();

	}

	@Step("Submit the Answer for DragDrop,  Method: {method} ")
	public void submitAnswerdragdrop() throws InterruptedException {
		Thread.sleep(3000);
		// Using Action class for drag and drop.
		Actions act = new Actions(driver);
		Thread.sleep(1000);
		// Drag and drop each item to its type respectively
		act.dragAndDrop(answer2, circleid4).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer3, circleid2).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer4, circleid1).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1, circleid3).build().perform();

	}

	@Step("Select the Questions from QuestionPlayer ,  Method: {method} ")
	public void BeginAssignment() throws InterruptedException {

		// driver.findElement(By.xpath("//div[@class='question-btn']/a")).click();
		String QuestionTitle = jsonobject.getAsJsonObject("Question").get("QuestionText").getAsString();
		String q1 = QuestionTitle.trim();
		Thread.sleep(8000L);
		WebDriverWait waitq = new WebDriverWait(driver, 5);
		waitq.until(ExpectedConditions.presenceOfElementLocated(By.id("realQuestionTable")));
		List<WebElement> questionID = driver.findElements(By.xpath("//table[@id='realQuestionTable']/tbody/tr"));
		int qIDsize = questionID.size();
		System.out.println(qIDsize);
		for (int i = 1; i <= qIDsize; i++) {
			List<WebElement> questionText = questionID.get(i)
					.findElements(By.xpath("//span[@id='renderRichText" + i + "']"));

			for (int j = 0; j < questionText.size(); j++) {
				String questiondetails = questionText.get(j).getText();
				String q2 = questiondetails.trim();

				if (q1.equalsIgnoreCase(q2)) {
					try {
						questionText.get(j).click();
						Thread.sleep(5000L);
					} catch (StaleElementReferenceException e) {
						questionText.get(j).click();
					}

				}
			}

		}

		Thread.sleep(5000);
	}

	@Step("Wait for Alert pop up to appear on Question player ,  Method: {method} ")
	public void alertPopup() {
		WebDriverWait wait = new WebDriverWait(driver, 2500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='head-bar modal-header']")));
		Alertbuttonclose.click();

	}

	@Step("Wait for Late Work Alert pop up to appear on Question player ,  Method: {method} ")
	public void lateWorkAlertPopup() {
		WebDriverWait wait = new WebDriverWait(driver, 2500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span")));
		GotItButton.click();
	}

	@Step("Verify Review Assignment Button is displayed on  Question player ,  Method: {method} ")
	public void ReviewAssignmentButton() {
		WebDriverWait wait8 = new WebDriverWait(driver, 10);
		wait8.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='question-btn']/a/span")));
		ReviewAssignmentbutton.click();
	}

	
	public void questionsLibraryList() throws InterruptedException {
		List<WebElement> questionslibList = driver.findElements(By.xpath(
				"//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/tbody/tr/td/span[starts-with(@class,'glyphicons glyphicons-plus')]"));
		for (int i = 0; i < questionslibList.size(); i++) {
			if (i == 3) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionslibList.get(i));
				//questionslibList.get(i).click();
				break;
			}
		}
	}

	public void questionsLibraryList_add() throws InterruptedException {
		List<WebElement> questionslibList = driver.findElements(By.xpath(
				"//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/tbody/tr/td/span"));
		
		List<WebElement> poolQuestionList = driver.findElements(By.xpath(
				"//table[@class='newQlPageTable table table-bordered table-condensed table-hover']/tbody/tr/td/img[@class='pool-icon-32Lc3Z']")); 
		int counter = 0;
		for (int i = 0; i < questionslibList.size(); i++) {
			if(poolQuestionList.get(i).isDisplayed() == false) {
					questionslibList.get(i).click();
					counter++;
					Thread.sleep(2000);
					if (counter == 6) {
						break;
					}
				
			}
			

		}
	}

	@Step("select searchQuestion text box on QuestionPlayer ,  Method: {method} ")
	public void searchQuestions() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(SearchQuestionTextbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", SearchQuestionTextbox);
		//SearchQuestionTextbox.click();
	}
}
