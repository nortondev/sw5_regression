package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH18HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH18HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	@Step("Enter Correct Answers for question #1,  Method: {method} ")
	public void question1_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_shortanswer_div4220871749bdeaa6bb7f086003f078f9de2221d7c107091_")));
		ReusableMethods.scrollIntoView(driver, element_Part1);

		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_shortanswer4220871749bdeaa6bb7f086003f078f9de2221d7c107091__")));
		item1Element_Part1.sendKeys("Fe");
		
		WebElement item2Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_shortanswer4220879792bdeaa6bb7f086003f078f9de2221d7c103124__")));
		item2Element_Part1.sendKeys("O");
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("wafer_ps_number4229756004bdeaa6bb7f086003f078f9de2221d7c101245_")));
		item1Element_Part2.sendKeys("12");
		
	}
	
	
	@Step("Drag and drop Correct image icons for question #4,  Method: {method} ")
	public void question4DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-12055746207571ef78727aebc6bbfca0410ae33b8503104_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-12055746207571ef78727aebc6bbfca0410ae33b8503104_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-12055746207571ef78727aebc6bbfca0410ae33b8503104_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-12055746207571ef78727aebc6bbfca0410ae33b8503104_")));
		WebElement dragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-12055746207571ef78727aebc6bbfca0410ae33b8503104_")));
		

		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//li[@siteorder=\"1\"]")));
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//li[@siteorder=\"2\"]")));
		WebElement dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//li[@siteorder=\"3\"]")));
		WebElement dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//li[@siteorder=\"4\"]")));
		WebElement dropContainer5Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//li[@siteorder=\"5\"]")));
			
			ReusableMethods.scrollToElement(driver, By.id("item-answer-1-12055746207571ef78727aebc6bbfca0410ae33b8503104_"));
		
			// Enter Correct Answer
				// Dragged and dropped.
				ReusableMethods.moveToElement(driver, dragItem1Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element, dropContainer5Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem2Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element, dropContainer2Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem3Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem3Element, dropContainer1Element).build().perform();
				
				ReusableMethods.moveToElement(driver, dragItem4Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element, dropContainer3Element).build().perform();	
				
				ReusableMethods.moveToElement(driver, dragItem5Element);
				Thread.sleep(2000);
				act.dragAndDrop(dragItem5Element, dropContainer4Element).build().perform();	

		
	}


	@Step("Drag and drop Correct image icons for question #6,  Method: {method} ")
	public void question6DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_radio_div7003536736182c2b3f0f36edc0765e4459cf60f01401227_"));
		
		WebElement item1Element_Part1 = driver.findElement(By.xpath("//input[@value=\"1\"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part1);
		//item1Element_Part1.click();
		
		
		// Element which needs to drag.
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id("item-answer-1-7003787320182c2b3f0f36edc0765e4459cf60f01405492_"));
				
				WebElement dragItem1Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-1-7003787320182c2b3f0f36edc0765e4459cf60f01405492_")));				
				WebElement dragItem2Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-2-7003787320182c2b3f0f36edc0765e4459cf60f01405492_")));
				WebElement dragItem3Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-3-7003787320182c2b3f0f36edc0765e4459cf60f01405492_")));
				WebElement dragItem4Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-4-7003787320182c2b3f0f36edc0765e4459cf60f01405492_")));
				WebElement dragItem5Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.id("item-answer-5-7003787320182c2b3f0f36edc0765e4459cf60f01405492_")));
				
				
				// Element on which need to drop.
				
				WebElement dropContainer1Element = driver.findElement(By.xpath("//div[@class='portlet-sorted-content  portlet-sorted-content-horizontal' and contains(text(),'1')]"));
				WebElement dropContainer2Element = driver.findElement(By.xpath("//div[@class='portlet-sorted-content  portlet-sorted-content-horizontal' and contains(text(),'2')]"));
				WebElement dropContainer3Element = driver.findElement(By.xpath("//div[@class='portlet-sorted-content  portlet-sorted-content-horizontal' and contains(text(),'3')]"));
				WebElement dropContainer4Element = driver.findElement(By.xpath("//div[@class='portlet-sorted-content  portlet-sorted-content-horizontal' and contains(text(),'4')]"));
				WebElement dropContainer5Element = driver.findElement(By.xpath("//div[@class='portlet-sorted-content  portlet-sorted-content-horizontal' and contains(text(),'5')]"));
				
				
				// Enter Correct Answer
				// Dragged and dropped.
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem1Element_Part2, dropContainer1Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem5Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem2Element_Part2, dropContainer4Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem2Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem3Element_Part2, dropContainer5Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem4Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem4Element_Part2, dropContainer3Element).build().perform();
				
				//executor.executeScript("arguments[0].click();", dragItem1Element);
				
				Thread.sleep(2000);
				act.dragAndDrop(dragItem5Element_Part2, dropContainer2Element).build().perform();
		
	}
	
	@Step("Drag and drop Correct image icons for question #7,  Method: {method} ")
	public void question7_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("answerKeyDiv16416746597571ef78727aebc6bbfca0410ae33b8501724_")));
		
		WebElement item1Element = driver.findElement(By.id("answerKeyDiv16416746597571ef78727aebc6bbfca0410ae33b8501724_"));
		item1Element.clear();
		item1Element.sendKeys("-290");
		
		
	}
	
	@Step("Drag and drop Correct image icons for question #9,  Method: {method} ")
	public void question9Mixed_CorrectAnswer() throws Exception {

		Actions builder = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		WebElement item1Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(
						By.id("wafer_ps_number10912435171ed5c6c648049fa0b7b7d2c611b1d14708353_")));
		
		item1Element_Part1.sendKeys("2.37");
		
		
		// Element which needs to drag.
				
				Thread.sleep(2000);
				ReusableMethods.scrollToElement(driver, 
						By.id("sortingPreviewAnswerItemList-10916400541ed5c6c648049fa0b7b7d2c611b1d14703704_"));
				
				
				// Drag and drop each Correct Count to its option respectively
				Thread.sleep(2000);
				WebElement dragItem1Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(
								By.id("item-answer-1-10916400541ed5c6c648049fa0b7b7d2c611b1d14703704_")));
				ReusableMethods.scrollIntoView(driver, dragItem1Element_Part2);
				WebElement dropContainer2Element_Part2 = driver.findElement(
						By.id("categorySubContainer-2-10916400541ed5c6c648049fa0b7b7d2c611b1d14703704_"));

				Action dragAndDrop1 = builder.clickAndHold(dragItem1Element_Part2)
				 		.moveToElement(dropContainer2Element_Part2)
				 		.release(dropContainer2Element_Part2)
					 	.build();
				 	dragAndDrop1.perform();
				 	
				
				// Drag and drop each Correct Count to its option respectively
				Thread.sleep(2000);
				WebElement dragItem2Element_Part2 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(
								By.id("item-answer-2-10916400541ed5c6c648049fa0b7b7d2c611b1d14703704_")));
				ReusableMethods.scrollIntoView(driver, dragItem2Element_Part2);
				WebElement dropContainer1Element_Part2 = driver.findElement(
						By.id("categorySubContainer-1-10916400541ed5c6c648049fa0b7b7d2c611b1d14703704_"));

				Action dragAndDrop2 = builder.clickAndHold(dragItem2Element_Part2)
				 		.moveToElement(dropContainer1Element_Part2)
				 		.release(dropContainer1Element_Part2)
					 	.build();
				 	dragAndDrop2.perform();
				
			
			// Drag and drop each Correct Count to its option respectively		
				Thread.sleep(2000);
				ReusableMethods.scrollToElement(driver,
						By.id("sortingPreviewAnswerItemList-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_"));
				
				Thread.sleep(2000);
				WebElement dragItem1Element_Part3 = wait.until(ExpectedConditions
						.visibilityOfElementLocated(
								By.id("item-answer-1-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_")));
				ReusableMethods.scrollIntoView(driver, dragItem1Element_Part3);
				WebElement dropContainer2Element_Part3 = driver.findElement(
						By.id("categorySubContainer-2-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_"));

				Action dragAndDrop3 = builder.clickAndHold(dragItem1Element_Part3)
				 		.moveToElement(dropContainer2Element_Part3)
				 		.release(dropContainer2Element_Part3)
					 	.build();
				 	dragAndDrop3.perform();
				
				
				 // Drag and drop each Correct Count to its option respectively
					Thread.sleep(2000);
					WebElement dragItem2Element_Part3 = wait.until(ExpectedConditions
							.visibilityOfElementLocated(
									By.id("item-answer-2-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_")));
					ReusableMethods.scrollIntoView(driver, dragItem2Element_Part3);
					WebElement dropContainer1Element_Part3 = driver.findElement(
							By.id("categorySubContainer-1-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_"));

					Action dragAndDrop4 = builder.clickAndHold(dragItem2Element_Part3)
					 		.moveToElement(dropContainer1Element_Part3)
					 		.release(dropContainer1Element_Part3)
						 	.build();
					 	dragAndDrop4.perform();
				
				// Drag and drop each Correct Count to its option respectively
					Thread.sleep(2000);
					WebElement dragItem3Element_Part3 = wait.until(ExpectedConditions
							.visibilityOfElementLocated(
									By.id("item-answer-3-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_")));
					ReusableMethods.scrollIntoView(driver, dragItem3Element_Part3);
					WebElement dropContainer1_1Element_Part3 = driver.findElement(
							By.id("categorySubContainer-1-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_"));

					Action dragAndDrop5 = builder.clickAndHold(dragItem3Element_Part3)
					 		.moveToElement(dropContainer1_1Element_Part3)
					 		.release(dropContainer1_1Element_Part3)
						 	.build();
						dragAndDrop5.perform();
				
				// Drag and drop each Correct Count to its option respectively
					Thread.sleep(2000);
					WebElement dragItem4Element_Part3 = wait.until(ExpectedConditions
							.visibilityOfElementLocated(
									By.id("item-answer-4-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_")));
					ReusableMethods.scrollIntoView(driver, dragItem4Element_Part3);
					WebElement dropContainer2_1Element_Part3 = driver.findElement(
							By.id("categorySubContainer-2-10919319701ed5c6c648049fa0b7b7d2c611b1d14705062_"));

					Action dragAndDrop6 = builder.clickAndHold(dragItem4Element_Part3)
					 		.moveToElement(dropContainer2_1Element_Part3)
					 		.release(dropContainer2_1Element_Part3)
						 	.build();
						dragAndDrop6.perform();

	}


}
