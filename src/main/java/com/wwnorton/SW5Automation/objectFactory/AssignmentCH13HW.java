package com.wwnorton.SW5Automation.objectFactory;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH13HW {

	WebDriver driver;
	Actions act;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")
	public WebElement QuestionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-b']")
	public WebElement QuestionTextCurrent;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-s']")
	public WebElement QuestionTextTotal;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH13HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}
	
	@Step("Drag And Drop Correct Answer for question #1,  Method: {method} ")
	public void question1DragAndDropCorrectAnswer() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		ReusableMethods.scrollToElement(driver, By.id("answerItemList-0610118681eacfa2ee6ab4a7dd7c97206babbede9500676_"));
		Thread.sleep(2000);

		// Labeling Options
		WebElement imgA = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-1-0610118681eacfa2ee6ab4a7dd7c97206babbede9500676_")));
		WebElement imgB = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-2-0610118681eacfa2ee6ab4a7dd7c97206babbede9500676_")));
		WebElement imgC = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-3-0610118681eacfa2ee6ab4a7dd7c97206babbede9500676_")));
		

		// Labeling Question
		WebElement dropA = driver.findElement(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'1')]"));
		WebElement dropB = driver.findElement(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'2')]"));
		WebElement dropC = driver.findElement(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'3')]"));
		

		// Drag and drop each item to its type respectively
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, imgA);
		act.dragAndDrop(imgA, dropC).build().perform();
		
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, imgB);
		act.dragAndDrop(imgB, dropB).build().perform();
		
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, imgC);
		act.dragAndDrop(imgC, dropA).build().perform();
		
	}
	
	@Step("Drag And Drop Correct Answer for question #1,  Method: {method} ")
	public void question3EnterCorrectAnswer() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement components = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='wafer_prts_div multiple-component']")));
		ReusableMethods.scrollIntoView(driver, components);
		
		WebElement radioOption1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='1']")));
		WebElement radioOption2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='2']")));
		WebElement radioOption4 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='4']")));
		WebElement radioOption5 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='5']")));
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioOption1);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioOption2);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioOption4);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioOption5);
	}
	
	@Step("Drag And Drop Correct Answer for question #4,  Method: {method} ")
	public void question4DragAndDropCorrectAnswer() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		ReusableMethods.scrollToElement(driver, 
				By.id("answer-item-panel-10662525641ed5c6c648049fa0b7b7d2c611b1d14705387_"));
		Thread.sleep(2000);

		// Labeling Options
		WebElement tenMins = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-1-10662525641ed5c6c648049fa0b7b7d2c611b1d14705387_")));
		WebElement fifteenMins = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-2-10662525641ed5c6c648049fa0b7b7d2c611b1d14705387_")));
		WebElement tweentyFiveMins = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("item-answer-3-10662525641ed5c6c648049fa0b7b7d2c611b1d14705387_")));
		

		// Labeling Question
		List<WebElement> drop1 = driver.findElements(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'1')]"));
		List<WebElement> drop2 = driver.findElements(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'2')]"));
		List<WebElement> drop3 = driver.findElements(By.xpath("//div[starts-with(@id,'itemContent-sorted-') and contains(text(),'3')]"));
		

		// Drag and drop each item to its type respectively
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, tenMins);
		act.dragAndDrop(tenMins, drop1.get(1)).build().perform();
		
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, fifteenMins);
		act.dragAndDrop(fifteenMins, drop2.get(1)).build().perform();
		
		Thread.sleep(2000);
		ReusableMethods.moveToElement(driver, tweentyFiveMins);
		act.dragAndDrop(tweentyFiveMins, drop3.get(1)).build().perform();
		
	}

	@Step("Drag And Drop Correct Answer for question #10,  Method: {method} ")
	public void question10DragAndDropCorrectAnswer() throws Exception {
		//ReusableMethods.scrollByY(driver, 150);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		ReusableMethods.scrollToElement(driver, By.id("answer-1"));
		Thread.sleep(2000);

		
		ReusableMethods.scrollToElement(driver, By.id("answer-1"));
		
		Thread.sleep(2000);

		// Labeling Options
		WebElement valueA = driver.findElement(By.id("answer-1"));
		WebElement valueB = driver.findElement(By.id("answer-2"));
		WebElement valueC = driver.findElement(By.id("answer-3"));
		WebElement valueD = driver.findElement(By.id("answer-4"));
		WebElement valueG = driver.findElement(By.id("answer-7"));

		// Labeling Question
		WebElement dropA = driver.findElement(By.id("targGroup-706103829141251ddfae8c542f8efaff3e32e9377c01674_-4"));
		WebElement dropB = driver.findElement(By.id("targGroup-706103829141251ddfae8c542f8efaff3e32e9377c01674_-2"));
		WebElement dropC = driver.findElement(By.id("targGroup-706103829141251ddfae8c542f8efaff3e32e9377c01674_-1"));
		WebElement dropD = driver.findElement(By.id("targGroup-706103829141251ddfae8c542f8efaff3e32e9377c01674_-3"));
		WebElement dropG = driver.findElement(By.id("targGroup-706103829141251ddfae8c542f8efaff3e32e9377c01674_-5"));

		// Drag and drop each item to its type respectively
		act.dragAndDrop(valueA, dropA).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueB, dropB).build().perform();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, dropC);
		act.dragAndDrop(valueC, dropC).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueD, dropD).build().perform();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, valueG);

		act.dragAndDrop(valueG, dropG).build().perform();
	}

	@Step("Drag And Drop Correct Answer for question #11,  Method: {method} ")
	public void question11DragAndDropCorrectAnswer() throws Exception {

		ReusableMethods.scrollByY(driver, 400);
		ReusableMethods.scrollToElement(driver, By.id("item-answer-1-464852448046cdeba34ad9dd3895e73d3972b368c507117_"));
	
	    Thread.sleep(2000);


		//ReusableMethods.scrollByY(driver, 400);
		
		ReusableMethods.scrollToElement(driver, By.id("item-answer-1-464852448046cdeba34ad9dd3895e73d3972b368c507117_"));
		
		Thread.sleep(2000);

		// Element which needs to drag.
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-464852448046cdeba34ad9dd3895e73d3972b368c507117_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-464852448046cdeba34ad9dd3895e73d3972b368c507117_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-464852448046cdeba34ad9dd3895e73d3972b368c507117_")));
				
		// Element on which need to drop.
		WebElement dropContainer1Element = driver
				.findElement(By.id("categorySubContainer-1-464852448046cdeba34ad9dd3895e73d3972b368c507117_"));
		WebElement dropContainer2Element = driver
				.findElement(By.id("categorySubContainer-2-464852448046cdeba34ad9dd3895e73d3972b368c507117_"));
		WebElement dropContainer3Element = driver
				.findElement(By.id("categorySubContainer-3-464852448046cdeba34ad9dd3895e73d3972b368c507117_"));

		// Enter Correct Answer
		// Dragged and dropped.
		act.dragAndDrop(dragItem1Element, dropContainer2Element).build().perform();
		act.dragAndDrop(dragItem2Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem3Element, dropContainer3Element).build().perform();
	}

	@Step("Drag And Drop Correct Answer for question #14,  Method: {method} ")
	public void question14DragAndDropCorrectAnswer() throws Exception {
		// Scrolling to Bottom of a page
		//ReusableMethods.scrollToBottom(driver);

        WebElement movetoLineBr= driver.findElement(By.xpath("//div[@class='question']/div[@id='part135558']/br"));
		// Part 1
		// Using Action class for drag and drop.
		Actions act = new Actions(driver);
		ReusableMethods.scrollToElement(driver, By.id("answer-1"));
	    Thread.sleep(2000);

		// Labeling Options
		WebElement valueA = driver.findElement(By.id("answer-1"));
		WebElement valueC = driver.findElement(By.id("answer-3"));
		WebElement valueD = driver.findElement(By.id("answer-4"));
		WebElement valueF = driver.findElement(By.id("answer-6"));
		
		// Labeling Question
		WebElement dropA = driver.findElement(By.id("targGroup-325857324446cdeba34ad9dd3895e73d3972b368c500378_-3"));
		WebElement dropC = driver.findElement(By.id("targGroup-325857324446cdeba34ad9dd3895e73d3972b368c500378_-1"));
		WebElement dropD = driver.findElement(By.id("targGroup-325857324446cdeba34ad9dd3895e73d3972b368c500378_-4"));
		WebElement dropF = driver.findElement(By.id("targGroup-325857324446cdeba34ad9dd3895e73d3972b368c500378_-2"));
        
		ReusableMethods.scrollIntoView(driver, movetoLineBr);
		
		// Drag and drop each item to its type respectively
		act.dragAndDrop(valueA, dropA).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueC, dropC).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueD, dropD).build().perform();
		Thread.sleep(2000);
		act.dragAndDrop(valueF, dropF).build().perform();
		Thread.sleep(2000);
		// Part 2
		ReusableMethods.scrollToBottom(driver);
		WebElement dropdownSelect = driver
				.findElement(By.id("wafer_ps_select78623175781c19b8b7ba6c5783f0c44bbc2cf8647705717_"));
		//dropdownSelect.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropdownSelect);

		WebElement dropdownSelectOption = driver.findElement(By.xpath(
				"//ul[@id='wafer_ps_select_option78623175781c19b8b7ba6c5783f0c44bbc2cf8647705717_']/li[contains(text(),'bimolecular.')]"));
		dropdownSelectOption.click();
		
	}
}
