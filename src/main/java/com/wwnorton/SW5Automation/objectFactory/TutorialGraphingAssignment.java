package com.wwnorton.SW5Automation.objectFactory;

import java.io.File;
import java.io.IOException;


import org.apache.commons.io.Charsets;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Files;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class TutorialGraphingAssignment {
	
	WebDriver driver;
	public static String UserDir = System.getProperty("user.dir");
	
	
	// Finding Web Elements on SW5 DLP page using PageFactory.
		
		@FindBy(how = How.XPATH, using = "//button[@title='Drag']/span")
		public WebElement dragIcon;
		
		@FindBy(how = How.ID, using = "CanvasAnimate")
		public WebElement canvasEditor;
		
		
		
	
	public TutorialGraphingAssignment() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	@Step("Submit Answer for Question 01,  Method: {method} ")
	public void submitAnswer_Question01() throws InterruptedException, IOException {
		Actions act = new Actions(driver);
	    
		WebElement iFrame = driver.findElement(
				By.xpath("//iframe[contains(@id,'resultFramewafer')]"));
		driver.switchTo().frame(iFrame);
		ReusableMethods.scrollToElement(driver, By.xpath("//div[@id='cover']"));
		Thread.sleep(2000);
		act.moveToElement(dragIcon).click().build().perform();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, canvasEditor);
		
		new WebDriverWait(driver, 25).until(ExpectedConditions
				.elementToBeClickable(By.id("CanvasAnimate")));
		
		Point locationOfGraph = canvasEditor.getLocation();
		/*Point locationOfGraph = canvasEditor.getRect().getPoint();
				//.transformDocToView(canvasEditor.nodes.first().location);
		
		Action dragNdrop = act.moveToElement(canvasEditor, locationOfGraph.x, locationOfGraph.y+11)
				.click()
				.clickAndHold(canvasEditor)
				.moveToElement(canvasEditor, locationOfGraph.x, locationOfGraph.y-22)
                .release(canvasEditor)
                .build();
		dragNdrop.perform();
		
		act.moveToElement(canvasEditor, locationOfGraph.x+174, locationOfGraph.y+187)
		 	.clickAndHold(canvasEditor).moveByOffset(locationOfGraph.x+174, locationOfGraph.y+154)
			.release(canvasEditor).build().perform();
		
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(canvasEditor, locationOfGraph.x+9, locationOfGraph.y+7).perform();*/
		//clickAndHold(canvasEditor).moveByOffset(8,2).release().perform();
		
		/*String os = System.getProperty("os.name");
		if (os.equals("WINDOWS")){
		   Keys.chord(Keys.CONTROL, "o");
		}else{
		   Keys.chord(Keys.COMMAND, "0");
		}
		*/
	    Action dragNdrop = act
	    			.contextClick(canvasEditor)
	                .moveToElement(canvasEditor, locationOfGraph.x+13, locationOfGraph.y+13)
	                .clickAndHold(canvasEditor)
	                .moveByOffset(locationOfGraph.x+14, locationOfGraph.y+13)
	                .release(canvasEditor)
	                .build();
				dragNdrop.perform();
				
		
		String fileContents = Files.toString(new File(UserDir +"/src/test/resources/GraphQues01.js"), Charsets.UTF_8);
		((JavascriptExecutor) driver).executeScript(fileContents);         
		//js.executeScript(fileContents);
		
		
		driver.switchTo().parentFrame();
		//driver.switchTo().defaultContent();

	}
}