package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class SW5DLPInstructor {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement StudentSet;

	@FindBy(how = How.XPATH, using = "//*[@id=\"activity_list_table\"]/tbody")
	public WebElement AssignmentTitle;

	@FindBy(how = How.ID, using = "create_custom_swfb_assignment_button")
	public WebElement Create_New_Assignment;
	
	@FindBy(how = How.XPATH, using = "//div[@id=\"activity_list_table_wrapper\"]/table/tbody")
	public WebElement SelectAssignment;
	

	// Initializing Web Driver and PageFactory.

	public SW5DLPInstructor(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Select New Student Set ID by Visible Text on SW5 DLP page,  Method: {method} ")
	public void selectSSByTitle(String SSTitle) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByVisibleText(SSTitle);

	}

	@Step("Select New Student Set ID by Value on SW5 DLP page,  Method: {method} ")
	public void selectSSByValue(String SSTitleValue) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByValue(SSTitleValue);

	}

	@Step("Select New Student Set ID on SW5 DLP page,  Method: {method} ")
	public int assignmentRowCount() throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver,10);
		// wait.until(ExpectedConditions.visibilityOfAllElements(AssignmentTitle));
		// List<WebElement> assignmentRows =
		// AssignmentTitle.findElements(By.tagName("tr"));
		// int rowsCount = assignmentRows.size();

		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#activity_list_table tbody tr').length");

		return Integer.valueOf(rowCountObj.toString());

	}

	@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
	public void selectByPartOfVisibleText(String value) {
		List<WebElement> optionElements = StudentSet.findElements(By.tagName("option"));

		for (WebElement optionElement : optionElements) {
			if (optionElement.getText().contains(value)) {
				System.out.println(optionElement.getText());
				optionElement.click();
				break;
			}
		}

	}

	@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
	public void ClickAssignment() {
		Create_New_Assignment.click();
	}
	
	@Step("Get Avg Grade  by Assignment name on SW5 Instrucotr DLP page,  Method: {method} ")
	public String getAssignmentGradeOnInstrucotrDLP(String assignmentName) {
		
		String AssignmentGrade = null;
		
		List<WebElement> linksText = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' title_td']/a"));
		
		List<WebElement> linksGrade = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' grade_td']"));
		
		for (int j = 0; j < linksText.size(); j++) {

			String AssignmentText = linksText.get(j).getText();
			
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				
				AssignmentGrade = linksGrade.get(j).getText();
			}
		}
		
		return AssignmentGrade;
	
	}

}
