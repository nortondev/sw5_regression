package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH19HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH19HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	@Step("Select correct checkboxes for question #5,  Method: {method} ")
	public void question1_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div0162087374bdeaa6bb7f086003f078f9de2221d7c103122_"));
		
		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162087374bdeaa6bb7f086003f078f9de2221d7c103122_")));
		
		WebElement item2Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162108968bdeaa6bb7f086003f078f9de2221d7c102177_")));
		
		WebElement item3Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_shortanswer3082530573bdeaa6bb7f086003f078f9de2221d7c101565__")));
		
		item1Element_Part1.sendKeys("5");
		item2Element_Part1.sendKeys("6");
		item3Element_Part1.sendKeys("B");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div0162177497bdeaa6bb7f086003f078f9de2221d7c104483_"));
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162177497bdeaa6bb7f086003f078f9de2221d7c104483_")));
		
		WebElement item2Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162182072bdeaa6bb7f086003f078f9de2221d7c103227_")));
		
		WebElement item3Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_shortanswer3082801464bdeaa6bb7f086003f078f9de2221d7c101699__")));
		
		item1Element_Part2.sendKeys("6");
		item2Element_Part2.sendKeys("5");
		item3Element_Part2.sendKeys("C");
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div0162238863bdeaa6bb7f086003f078f9de2221d7c104471_"));
		
		WebElement item1Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162238863bdeaa6bb7f086003f078f9de2221d7c104471_")));
		
		WebElement item2Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number0162253624bdeaa6bb7f086003f078f9de2221d7c105220_")));
		
		WebElement item3Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_shortanswer3082989493bdeaa6bb7f086003f078f9de2221d7c107842__")));
		
		item1Element_Part3.sendKeys("6");
		item2Element_Part3.sendKeys("7");
		item3Element_Part3.sendKeys("C");
			
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div0163439293bdeaa6bb7f086003f078f9de2221d7c105141_"));
		
		WebElement radioElement_Part4 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[contains(@value,'3')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radioElement_Part4);
		
		
	}
	
	
	@Step("Select correct checkboxes for question #5,  Method: {method} ")
	public void question5_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div27858479750cd9d0f1ccb4b38dfecf49186b4546db09328_"));
		
		WebElement item2Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27858479750cd9d0f1ccb4b38dfecf49186b4546db09328_']"
						+ "/span/label/span/input[contains(@value,'2')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item2Element_Part1);	

		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div27860072860cd9d0f1ccb4b38dfecf49186b4546db01975_"));
		
		WebElement item3Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27860072860cd9d0f1ccb4b38dfecf49186b4546db01975_']"
						+ "/span/label/span/input[contains(@value,'3')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item3Element_Part2);
		
		WebElement item4Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27860072860cd9d0f1ccb4b38dfecf49186b4546db01975_']"
						+ "/span/label/span/input[contains(@value,'4')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item4Element_Part2);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div27861316150cd9d0f1ccb4b38dfecf49186b4546db08543_"));
		
		WebElement item3Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27861316150cd9d0f1ccb4b38dfecf49186b4546db08543_']"
						+ "/span/label/span/input[contains(@value,'3')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item3Element_Part3);
		
		WebElement item4Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27861316150cd9d0f1ccb4b38dfecf49186b4546db08543_']"
						+ "/span/label/span/input[contains(@value,'4')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item4Element_Part3);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div27862580200cd9d0f1ccb4b38dfecf49186b4546db09086_"));
		
		WebElement item1Element_Part4 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27862580200cd9d0f1ccb4b38dfecf49186b4546db09086_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part4);
		
		WebElement item2Element_Part4 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_checkbox_div27862580200cd9d0f1ccb4b38dfecf49186b4546db09086_']"
						+ "/span/label/span/input[contains(@value,'2')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item2Element_Part4);
		
	}


	@Step("Drag and drop Correct image icons for question #6,  Method: {method} ")
	public void question11_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div33934932430cd9d0f1ccb4b38dfecf49186b4546db08503_"));

		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div33934932430cd9d0f1ccb4b38dfecf49186b4546db08503_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part1);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div33936094580cd9d0f1ccb4b38dfecf49186b4546db02121_"));
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div33936094580cd9d0f1ccb4b38dfecf49186b4546db02121_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part2);
		
	}
	
	
	@Step("Drag and drop Correct image icons for question #6,  Method: {method} ")
	public void question14_PartialCorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div51406823840cd9d0f1ccb4b38dfecf49186b4546db01683_"));

		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("wafer_ps_number51406823840cd9d0f1ccb4b38dfecf49186b4546db01683_")));
		
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part1);
		item1Element_Part1.sendKeys("5");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div51415086590cd9d0f1ccb4b38dfecf49186b4546db03861_"));
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@class='everySingleOption multi-btn-withoutIndex']/input[contains(@value,'3')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part2);
		
	}

}
