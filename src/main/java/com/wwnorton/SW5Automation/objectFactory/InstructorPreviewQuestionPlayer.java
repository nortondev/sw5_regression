package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class InstructorPreviewQuestionPlayer {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-title fl']/a/span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-title fl']/a")
	public WebElement HeaderTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-score head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderScore;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-timeleft head-dash-list']/span[@class='glyphicons glyphicons-stopwatch']")
	public WebElement TimerIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-timeleft head-dash-list']/div[@class='head-big-txt']")
	public WebElement TimerText;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement HeaderEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//span[@class='question-info-text']/span[@class='b current-attempt-num']")
	public WebElement HeaderRemainingAttempt;

	@FindBy(how = How.XPATH, using = "//div[@class='question_player_div']//section[@class='container-box question-intro']//h1")
	public WebElement QuestionTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page icon-page-disabled prev-question']")
	public WebElement QuestionPreviousDisabled;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question icon-page-disabled']")
	public WebElement QuestionNextDisabled;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//button[@id='viewSolution']")
	public WebElement ViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//button[@id='tryAgain']")
	public WebElement TryAgainButton;

	@FindBy(how = How.XPATH, using = "//button[@id='practiceBtn']")
	public WebElement PracticeButton;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/div/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	@FindBy(how = How.XPATH, using = "//div[@role='dialog']/div[@class='fade in modal']")
	public WebElement Feedback;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div/button[@class='close']")
	public WebElement CloseFeedback;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='head-bar modal-header']/h4[@class='modal-title']/span[@class='btn-normal']")
	public WebElement ConfirmViewSolutionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-body']/div[@class='alertBodyIsViewSolution']")
	public WebElement ConfirmViewSolutionMessageText;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-body']/div")
	public WebElement AlertMessageText;

	@FindBy(how = How.XPATH, using = "//div[@class='question_player_div']/article/div[@class='container-box question-list']/div")
	public WebElement SolutionAndAttempts;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'VIEW SOLUTION')]]")
	public WebElement FeedbackViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'TRY AGAIN')]]")
	public WebElement FeedbackTryAgainButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'CONTINUE')]]")
	public WebElement FeedbackContinueButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='btn-normal']/span[contains(text(),'VIEW SOLUTION')]]")
	public WebElement ConfirmPopupViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[contains(text(),'CANCEL')]")
	public WebElement ConfirmPopupCancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'PRACTICE')]]")
	public WebElement FeedbackPracticeButton;

	@FindBy(how = How.XPATH, using = "//span[@class='isPcDev']")
	public WebElement QuestionCompletedStatus;

	@FindBy(how = How.XPATH, using = "//span[@class='isNotPcDev']")
	public WebElement CompletedStatus;

	@FindBy(how = How.XPATH, using = "//button[@id='checkPracticeBtn']")
	public WebElement CheckPracticeButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='head-bar modal-header']/h4/span[contains(text(),'Are you sure you want to leave this question?')]")
	public WebElement LeaveQuestion;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[contains(text(),'YES, LEAVE')]")
	public WebElement LeaveQuestionYesLeave;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'NO, STAY HERE')]]")
	public WebElement LeaveQuestionStayHere;

	@FindBy(how = How.XPATH, using = "//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-ok icon-circle-ok']")
	public List<WebElement> FeedbackPartCorrectAnswerIconsList;

	@FindBy(how = How.XPATH, using = "//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-remove icon-circle-remove']")
	public List<WebElement> FeedbackPartInCorrectAnswerIconsList;

	@FindBy(how = How.XPATH, using = "//button[span[@id='launchTutorialBtn'][contains(text(),'LAUNCH TUTORIAL LESSON')]]")
	public WebElement LaunchTutorialLessonButton;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash-2']/div[@class='head-score']/div[@class='head-big-txt']")
	public WebElement HeaderScorePoints;
	
	
	// Initializing Web Driver and PageFactory.
	public InstructorPreviewQuestionPlayer() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		waitForLoadPageElements();
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			// AssignmentTitle
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@class='txt-headtitle assignmentNameSpan body-content']")));
			// QuestionInfoText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='question-info-text']")));
			// SubmitAnswer OR PracticeButton OR CheckPracticeButton OR TryAgainButton
			// OR ViewSolutionButton
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(SubmitAnswer),
					ExpectedConditions.elementToBeClickable(PracticeButton),
					ExpectedConditions.elementToBeClickable(CheckPracticeButton),
					ExpectedConditions.elementToBeClickable(TryAgainButton),
					ExpectedConditions.elementToBeClickable(ViewSolutionButton),
					ExpectedConditions.visibilityOf(QuestionCompletedStatus)));
			// QuestionHeaderText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")));
			// HeaderScoreText
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@class='head-score head-dash-list']/div[@class='head-big-txt']")));
			// QuestionFooterText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='question-step-txt']")));
			// FirstAttemptSection
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[not(contains(@style, 'none'))]/section/div[@class='question-attempt-title']/h2/a/div[contains(text(),'1st attempt')]")));
			// QuestionTextCurrent
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//div[@class='question-page']/span[@class='page-num-b']")));
			// QuestionTextTotal
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//div[@class='question-page']/span[@class='page-num-s']")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@class='head-box']/div[@class='head-dash']/div[@class='head-score head-dash-list']/div[@class='head-big-txt']")));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public void waitForLoadFeedbackOverlayElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			// FeedbackViewSolutionButton OR FeedbackTryAgainButton OR CheckPracticeButton
			// OR TryAgainButton
			// OR ViewSolutionButton
			// OR CloseFeedback
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton),
					ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton),
					ExpectedConditions.elementToBeClickable(FeedbackPracticeButton),
					ExpectedConditions.elementToBeClickable(CloseFeedback)));

			// FeedbackPartCorrectAnswerIconsList OR feedbackPartInCorrectAnswerIconsList
			wait.until(ExpectedConditions.or(
					ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//div[@class='question-page']/span[@class='page-num-b']")),
					ExpectedConditions.visibilityOfElementLocated(By.xpath(
							"//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-remove icon-circle-remove']"))));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(5000);
	}

	public String getHeaderDueDate() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(HeaderDueDate));
		return HeaderDueDate.getText();
	}

	@Step("Check View Solution button displayed,  Method: {method} ")
	public boolean isViewSolutionButtonDisplayed() throws Exception {
		try {
			return ViewSolutionButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Check Submit Answer button displayed,  Method: {method} ")
	public boolean isSubmitAnswerButtonDisplayed() throws Exception {
		try {
			return SubmitAnswer.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Check 'View Solution' & 'SubmitAnswer' buttons displayed,  Method: {method} ")
	public boolean isViewSolutionAndSubmitAnswerDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(SubmitAnswer));
			return ViewSolutionButton.isDisplayed() && SubmitAnswer.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getQuestionFooterMiddleText() {
		String fmt = "";

		List<WebElement> texts = QuestionFooterMiddleText.findElements(By.tagName("span"));

		for (WebElement text : texts)
			fmt += text.getText();

		return fmt;
	}

	public boolean isPreviousQuestionDisabledButtonDisplayed() {
		try {
			return QuestionPreviousDisabled.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: ");
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean isNextQuestionDisabledButtonDisplayed() {
		try {
			return QuestionNextDisabled.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	public boolean isPreviousQuestionEnabledButtonDisplayed() {
		try {
			return QuestionPrevious.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	public boolean isNextQuestionEnabledButtonDisplayed() {
		try {
			return QuestionNext.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	@Step("Click on Previous question,  Method: {method} ")
	public void clickPreviousQuestion() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(QuestionPrevious));
			QuestionPrevious.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on next question,  Method: {method} ")
	public void clickNextQuestion() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(QuestionNext));
			QuestionNext.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Submit Answer button,  Method: {method} ")
	public void clickSubmitAnswer() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(SubmitAnswer));
			SubmitAnswer.click();
			waitForLoadFeedbackOverlayElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public boolean isFeedbackOverlayDisplayed() {
		try {
			return Feedback.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	@Step("Click on Close Feedback popup,  Method: {method} ")
	public void clickFeedbackClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(CloseFeedback));
			CloseFeedback.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public boolean isAttemptDisplayed(String attemptText) {
		try {
			// WebElement attempt = Attempt.findElement(By.xpath("//div[contains(text(),'" +
			// attemptText + "')]"));
			// return attempt.isDisplayed();

			WebElement attempt = driver.findElement(By.xpath(
					"//div[not(contains(@style, 'none'))]/section/div[@class='question-attempt-title']/h2/a//div[contains(text(),'"
							+ attemptText + "')]"));
			return attempt.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	public int getAttemptCount() {
		return SolutionAndAttempts.findElements(By.xpath("//section[@practice='attempt']")).size();
	}

	@Step("Check Practice button displayed,  Method: {method} ")
	public boolean isLeaveQuestionDisplayed() throws Exception {
		try {
			return LeaveQuestion.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Feedback 'TRY AGAIN' button,  Method: {method} ")
	public void clickLeaveQuestionYesLeave() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(LeaveQuestionYesLeave));
			LeaveQuestionYesLeave.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Feedback overlay displayed with 'View Solution' & 'Try Again' option,  Method: {method} ")
	public boolean isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton));
			return FeedbackViewSolutionButton.isDisplayed() && FeedbackTryAgainButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Feedback VIEW SOLUTION button,  Method: {method} ")
	public void clickFeedbackViewSolution() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton));
			FeedbackViewSolutionButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Confirm Popup CANCEL button,  Method: {method} ")
	public void clickConfirmPopupCancelButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupCancelButton));
			ConfirmPopupCancelButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Confirm Popup VIEW SOLUTION button,  Method: {method} ")
	public void clickConfirmPopupViewSolutionButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupViewSolutionButton));
			ConfirmPopupViewSolutionButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Feedback TRY AGAIN button,  Method: {method} ")
	public void clickFeedbackTryAgain() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton));
			FeedbackTryAgainButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(3000);
		if (isSubmitAnswerButtonDisplayed() == false) {
			clickTryAgain();
		}
	}

	@Step("Check Feedback Practice button displayed,  Method: {method} ")
	public boolean isFeedbackPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackPracticeButton));
			return FeedbackPracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Feedback PRACTICE button,  Method: {method} ")
	public void clickFeedbackPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackPracticeButton));
			FeedbackPracticeButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on VIEW SOLUTION button,  Method: {method} ")
	public void clickViewSolution() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ViewSolutionButton));
			ViewSolutionButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on TRY AGAIN button,  Method: {method} ")
	public void clickTryAgain() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(TryAgainButton));
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(3000);
	}

	@Step("Check Practice button displayed,  Method: {method} ")
	public boolean isPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(PracticeButton));
			return PracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on PRACTICE button,  Method: {method} ")
	public void clickPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(PracticeButton));
			PracticeButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check CHECK PRACTICE button displayed,  Method: {method} ")
	public boolean isCheckPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(CheckPracticeButton));
			return CheckPracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on CHECK PRACTICE button,  Method: {method} ")
	public void clickCheckPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(CheckPracticeButton));
			CheckPracticeButton.click();
			waitForLoadFeedbackOverlayElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Confirm overlay displayed with 'View Solution' & 'Cancel' option,  Method: {method} ")
	public boolean isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupCancelButton));
			return ConfirmPopupViewSolutionButton.isDisplayed() && ConfirmPopupCancelButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getQuestionInfoText() throws Exception {
		return QuestionInfoText.getText();
	}

	public String getQuestionTitle() throws Exception {
		String questionTitle = QuestionTitle.getText();
		if (questionTitle.compareTo("") == 0) {
			questionTitle = QuestionTitle.getAttribute("title");
		}
		// Create a pattern to be searched
		Pattern pattern = Pattern.compile("Question");
		String[] result = pattern.split(questionTitle);
		if (result != null && result.length > 1) {
			questionTitle = result[0] + " Question" + result[1];
		}
		return questionTitle;
	}

	public String getHeaderScore() throws Exception {
		return HeaderScore.getText().replace('\n', ' ');
	}
	public String getHeaderScoreinPoints() throws Exception {
		return HeaderScorePoints.getText().replace('\n', ' ');
	}

	public String getQuestionFooterLeftText() throws Exception {
		return QuestionFooterLeftText.getText();
	}

	public String getQuestionCompletedStatusText() throws Exception {
		return QuestionCompletedStatus.getText();
	}

	public String getTimerText() throws Exception {
		return TimerText.getText();
	}

	public String getAlertMessageText() throws Exception {
		return AlertMessageText.getText();
	}

	public String getConfirmViewSolutionHeaderText() throws Exception {
		return ConfirmViewSolutionHeaderText.getText();
	}

	public String getConfirmViewSolutionMessageText() throws Exception {
		return ConfirmViewSolutionMessageText.getText();
	}

	@Step("Click on Close Timer Expired Alert popup,  Method: {method} ")
	public void clickTimerExpiredAlertClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(CloseFeedback));
			CloseFeedback.click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public void clickHeaderTitle(InstructorPreviewAssignmentPage iPreviewAP) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(HeaderTitle));
		HeaderTitle.click();
		if (iPreviewAP != null) {
			iPreviewAP.waitForLoadPageElements();
		}
	}

	public String getHeaderRemainingAttempt() throws Exception {
		return HeaderRemainingAttempt.getText();
	}

	@Step("Click on 'LAUNCH TUTORIAL LESSON' button,  Method: {method} ")
	public void clickLaunchTutorialLessonButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(LaunchTutorialLessonButton));
		LaunchTutorialLessonButton.click();
	}

	@Step("Check Feedback Continue button displayed,  Method: {method} ")
	public boolean isFeedbackContinueButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackContinueButton));
			return FeedbackContinueButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Continue button,  Method: {method} ")
	public void clickContinue() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackContinueButton));
			FeedbackContinueButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check if Question Parts are Displayed,  Method: {method} ")
	public boolean isQuestionPartDisplayed(String partText) {
		try {
			List<WebElement> partList = driver.findElements(By.xpath(
					"//div[not(contains(@style, 'none'))]/section[@issubmitted='false']/div[@class='question-containers']//div[@name='partList']//h2"));

			for (int i = 0; i < partList.size(); i++) {

				WebElement part = partList.get(i);

				if (part.getText().equals(partText))
					return true;
			}

			return false;

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}
}
