package com.wwnorton.SW5Automation.objectFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;

import ru.yandex.qatools.allure.annotations.Step;

public class UpdateStudentSet {

	WebDriver driver;
	WebDriverWait wait;
	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//table[@id='student_set_data_table']//tbody")
	WebElement allStudentSet;

	@FindBy(how = How.XPATH, using = "//*[@id=\"copy-assignment-link\"]/b")
	WebElement copyAssignmentLink;

	@FindBy(how = How.XPATH, using = "//select[@id='edit_student_set_copy_select']")
	WebElement selectStudentSetToCopy;

	@FindBy(how = How.XPATH, using = "//input[@id='edit_student_set_GAUincludeY']")
	WebElement gauDateYes;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Copy Assignments')]")
	WebElement copyAssignmentButton;

	@FindBy(how = How.XPATH, using = "/html/body/div[7]/div[3]/div/button[2]/span")
	WebElement confirmCopyAssignment;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveCopyAssignment;

	@FindBy(how = How.XPATH, using = "//div[@id='alert_dialog_outer_1']")
	WebElement warningPopUpMsg;

	@FindBy(how = How.XPATH, using = "/html/body/div[9]/div[3]/div/button/span")
	WebElement warningOkButton;

	@FindBy(how = How.XPATH, using = "//button[@id='edit_student_set_add_member']/span[contains(text(),'Add Member')]")
	public WebElement AddMemberButton;

	@FindBy(how = How.ID, using = "edit_student_set_title")
	public WebElement StudentSetTitle;

	// Add New Member to Student Set elements
	@FindBy(how = How.ID, using = "member-role-select")
	public WebElement Memberroleselect;

	@FindBy(how = How.ID, using = "edit_student_set_add_member_email")
	public WebElement EmailAddressTextbox;

	@FindBy(how = How.ID, using = "edit_student_set_lookup_button")
	public WebElement LookupButton;

	@FindBy(how = How.ID, using = "edit_student_set_member_names")
	public WebElement StudentMemeberName;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Add')]")
	public WebElement AddButton;

	@FindBy(how = How.XPATH, using = "//a[@class='link-button']/b[contains(text(),'Instructors/TAs')]")
	public WebElement InstructorsTAslinktab;

	@FindBy(how = How.XPATH, using = "//table[@id='edit_student_set_instructor_table']/tbody")
	public WebElement studentsetinsttable;

	@FindBy(how = How.XPATH, using = "//table[@id='edit_student_set_instructor_table']/tbody/tr")
	public By liststudentsetinsttable;

	@FindBy(how = How.XPATH, using = "//table[@id='edit_student_set_instructor_table']/tbody/tr/td")
	public By coltable;

	@FindBy(how = How.NAME, using = "chk-instructor")
	public WebElement InstructorsTAscheckbox;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Save')]")
	public WebElement SaveButton;

	@FindBy(how = How.XPATH, using = "//table[@id='student_set_data_table']/tbody/tr")
	public WebElement student_set_data_table;

	// Initializing Web Driver and PageFactory.

	public UpdateStudentSet(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	// Call to Json file to read test data for UI scripts.

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJsonObject.readUIJason();

	@Step("Update Existing Student Set,  Method: {method} ")
	public void editStudentSet(String TargetStudentSet) throws Exception {

		Thread.sleep(5000);
		List<WebElement> studentSetRow = allStudentSet.findElements(By.tagName("tr"));

		WebElement studentSetTitle;
		WebElement updateButton;

		// *[@id="student_set_data_table"]/tbody/tr[7]/td[2]

		for (int i = 1; i <= studentSetRow.size(); i++) {
			studentSetTitle = driver
					.findElement(By.xpath("//*[@id=\"student_set_data_table\"]/tbody/tr[" + i + "]/td[2]"));
			String StudentTitle = studentSetTitle.getText();
			if (StudentTitle.equalsIgnoreCase(TargetStudentSet)) {
				updateButton = driver.findElement(
						By.xpath("//*[@id=\"student_set_data_table\"]/tbody/tr[" + i + "]/td[6]/div/a[1]/span"));
				updateButton.click();
				break;
			}

		}

	}

	@Step("Copy assignment and settings link,  Method: {method} ")
	public void copyAssignmentnSettings(String SelectSourceStudentSet) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);

		wait.until(ExpectedConditions.visibilityOf(copyAssignmentLink));
		copyAssignmentLink.click();

		wait.until(ExpectedConditions.visibilityOf(selectStudentSetToCopy));
		selectStudentSetToCopy.click();

		Thread.sleep(1000);
		Select drpSelectStudentSet = new Select(selectStudentSetToCopy);
		drpSelectStudentSet.selectByVisibleText(SelectSourceStudentSet);

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(gauDateYes));
		gauDateYes.click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(copyAssignmentButton));
		copyAssignmentButton.click();

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(confirmCopyAssignment));
		confirmCopyAssignment.click();

		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOf(saveCopyAssignment));
			boolean saveCopyButtonExist = saveCopyAssignment.isDisplayed();

			if (saveCopyButtonExist == true) {
				saveCopyAssignment.click();
			}

		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Get Warning Message when Student is already started on Assignment,  Method: {method} ")
	public String ValidateWarningMsg() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(warningPopUpMsg));
		String studentWarningMsg = warningPopUpMsg.getText();

		wait.until(ExpectedConditions.elementToBeClickable(warningOkButton));
		warningOkButton.click();

		return studentWarningMsg;

	}

	@Step("Click the Update Link button &  AddMemeberButton Select the Role as Student,  Method: {method} ")
	public void updateStudent() throws InterruptedException {
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(AddMemberButton));
		AddMemberButton.click();
		Select selectRole = new Select(Memberroleselect);
		selectRole.selectByVisibleText("Student");

	}

	@Step("Assigning the Instructor Role,  Method: {method} ")
	public String AssignInstructorRole() {
		WebElement insttable = driver.findElement(By.xpath("//table[@id='edit_student_set_instructor_table']/tbody"));
		List<WebElement> rowVals = insttable.findElements(By.tagName("tr"));
		int rowNum = rowVals.size();
		System.out.println(rowNum);
		String cellText = null;
		String EmailName = null;
		for (int row = 0; row < rowNum; row++) {
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum = colVals.size();
			System.out.println(colNum);
			for (int col = 0; col < colNum; col++) {
				if (col == 1) {
					cellText = colVals.get(col).getText();
					// System.out.println(cellText);
					Matcher m1 = Pattern.compile("\\((.*?)\\)").matcher(cellText);
					while (m1.find()) {
						EmailName = m1.group(1);
					}
				}
			}
		}

		return EmailName;

	}

	@Step("Verify Additional Instructor are added to the member list,  Method: {method} ")
	public String VerifyAdditionalInstructore() {
		WebElement insttable = driver.findElement(By.xpath("//table[@id='student_set_data_table']/tbody"));
		List<WebElement> rowVals = insttable.findElements(By.tagName("tr"));
		int rowNum = rowVals.size();
		System.out.println(rowNum);
		String AdditionalInstColName = null;
		for (int row = 0; row < rowNum; row++) {
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum = colVals.size();
			// System.out.println(colNum);
			for (int col = 0; col < colNum; col++) {
				if (col == 2) {
					AdditionalInstColName = colVals.get(col).getText();
					// System.out.println(AdditionalInstColName);

				}
			}
		}

		return AdditionalInstColName;

	}

	@Step("Verify Students are added to the member list,  Method: {method} ")
	public String VerifyStudentinfo() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='edit_student_set_student_table']/tbody")));
		WebElement StudentTable = driver.findElement(By.xpath("//table[@id='edit_student_set_student_table']/tbody"));
		List<WebElement> rowVals = StudentTable.findElements(By.tagName("tr"));
		int rowNum = rowVals.size();
		System.out.println(rowNum);
		String StudentName = null;
		for (int row = 0; row < rowNum; row++) {
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum = colVals.size();
			// System.out.println(colNum);
			for (int col = 0; col < colNum; col++) {
				if (col == 1) {
					StudentName = colVals.get(col).getText();
					//System.out.println(StudentName);
					Matcher m1 = Pattern.compile("\\((.*?)\\)").matcher(StudentName);
					while (m1.find()) {
						StudentName = m1.group(1);
					}
				   // System.out.println(StudentName);

				}
			}
		}
		return StudentName;

	}

	@Step("Verify Students are added to the member list,  Method: {method} ")
	public void SaveButton() {
		SaveButton.click();
	}

	
	
	@Step("get the Student Names from the member list,  Method: {method} ")
	public ArrayList<String> getStudentFirstNameLastName() {
		String studentNames = null;
		ArrayList<String> studentList=new ArrayList<String>();
		WebElement StudentTable = driver.findElement(By.xpath("//table[@id='edit_student_set_student_table']/tbody"));
		List<WebElement> rowVals = StudentTable.findElements(By.tagName("tr"));
		int rowNum = rowVals.size();
		//System.out.println(rowNum);
		String studentName = null;
		for (int row = 0; row < rowNum; row++) {
			List<WebElement> colVals = rowVals.get(row).findElements(By.tagName("td"));
			int colNum = colVals.size();
			// System.out.println(colNum);
			for (int col = 0; col < colNum; col++) {
				if (col == 1) {
					studentName = colVals.get(col).getText();
					String studentNameStr = studentName.split("\\(")[0];
					
					studentList.add(studentNameStr);
					for (int i=0; i<studentList.size(); i++){
						studentNames = studentList.get(i).toString();
						LogUtil.log(studentNames);
					}

				}
				
				
			}
		}
		return (studentList);

	}
}
