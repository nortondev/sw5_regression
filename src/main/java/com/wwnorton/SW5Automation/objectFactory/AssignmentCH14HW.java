package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH14HW {

	WebDriver driver;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH14HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("Enter Correct Answer for question #1,  Method: {method} ")
	public void enterQuestion1_Correct(String listValue, String inputText) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		// Correct answer
		WebElement correctAnswer1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@class='richSelect_show']")));
		ReusableMethods.scrollIntoView(driver, correctAnswer1);
		boolean isEnabled = correctAnswer1.isEnabled();
		if(isEnabled==true){
			
		//correctAnswer1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswer1);
		
		}
		WebElement listbox = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div7114793713bdeaa6bb7f086003f078f9de2221d7c104005_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase(listValue))
		    {
		        //option.click(); // click the desired option
		    	((JavascriptExecutor) driver).executeScript("arguments[0].click();", option);
		        break;
		    }
		}
		//Some water and carbon monoxide have reacted while"+'\u00A0'+"hydrogen and carbon dioxide are produced.
		//select the Checkbox for the Answer
		ReusableMethods.scrollToBottom(driver);
		Actions act = new Actions(driver);
		Thread.sleep(2000);
		WebElement answer1 = driver.findElement(By.xpath("//span[@class='everySingleOption multiple-option-text'][contains(text(),"+inputText+")]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		act.moveToElement(answer1);
		//answer1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer1);
		
	}

	@Step("Enter Answer for question #2,  Method: {method} ")
	public void enterQuestion2(String inputText) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		// Correct answer
		WebElement correctAnswer2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@id='part112332']")));
		ReusableMethods.scrollIntoView(driver, correctAnswer2);
		Actions act = new Actions(driver);
		Thread.sleep(2000);
		WebElement answer1 = driver.findElement(By.xpath("//span[@class='everySingleOption multiple-option-text'][contains(text(),"+inputText+")]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']"));
		act.moveToElement(answer1);
		//answer1.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer1);
	}

	@Step("Enter Answer for question #4,  Method: {method} ")
	public void enterQuestion4(String inputText) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		// Correct answer
		WebElement correctAnswer4 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answerKeyDiv9547631292bdeaa6bb7f086003f078f9de2221d7c103041_")));
		ReusableMethods.scrollIntoView(driver, correctAnswer4);
		
		WebElement enterAnswer =wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number9547631292bdeaa6bb7f086003f078f9de2221d7c103041_")));
		enterAnswer.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", enterAnswer);
		enterAnswer.sendKeys(inputText);
	}
	
	
	@Step("Select InCorrect Answers for question #3,  Method: {method} ")
	public void selectQuestion3() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Correct answer
		WebElement answerOptions = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("wafer_ps_checkbox_div5667351405e0d99b793349f08101be1d61405adf7004982_")));
		ReusableMethods.scrollIntoView(driver, answerOptions);
		
		Thread.sleep(2000);
		WebElement answer1 = driver.findElement(By.xpath
				("//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='1']"));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer1);
	}
	
	@Step("Select InCorrect Answers for question #5,  Method: {method} ")
	public void enterQuestion5() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		WebElement question = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("answerKeyDiv469855483041251ddfae8c542f8efaff3e32e9377c06053_")));
		ReusableMethods.scrollIntoView(driver, question);
		
		// InCorrect answer
		WebElement answerTextBox = wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//input[@id='wafer_ps_number469855483041251ddfae8c542f8efaff3e32e9377c06053_']")));
		
		Actions act = new Actions(driver);
		Thread.sleep(2000);
		act.moveToElement(answerTextBox);
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answerTextBox);
		((JavascriptExecutor) driver).executeScript("arguments[0].value='10';", answerTextBox);

		//answerTextBox.sendKeys("10");
	}
	
	@Step("Enter Answer for question #7,  Method: {method} ")
	public void enterQuestion7() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		WebElement question = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("answerKeyDiv4023696592eacfa2ee6ab4a7dd7c97206babbede9502969_")));
		ReusableMethods.scrollIntoView(driver, question);
		
		// InCorrect answer
		WebElement answerTextBox = wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//input[@id='wafer_ps_number4023696592eacfa2ee6ab4a7dd7c97206babbede9502969_']")));
		
		
		Actions act = new Actions(driver);
		Thread.sleep(2000);
		act.moveToElement(answerTextBox);
		
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answerTextBox);
		((JavascriptExecutor) driver).executeScript("arguments[0].value='27';", answerTextBox);
		//enterAnswer.sendKeys(inputText);
	}
}
