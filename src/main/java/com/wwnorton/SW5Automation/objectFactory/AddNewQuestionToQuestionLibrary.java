package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AddNewQuestionToQuestionLibrary {

	WebDriver driver;

	// Finding Web Elements on New Question page using PageFactory.

	@FindBy(how = How.XPATH, using = "//a[span[@class='txt-headtitle assignmentNameSpan body-content']]")
	public WebElement questionLibrary;

	@FindBy(how = How.XPATH, using = "//input[@id='questionTitle']")
	public WebElement questionTitleTextbox;

	@FindBy(how = How.XPATH, using = "//select[@id='chapter']")
	public WebElement chapterSelect;

	@FindBy(how = How.XPATH, using = "//select[@id='sesctionReference']")
	public WebElement sectionEbookReferenceSelect;

	@FindBy(how = How.XPATH, using = "//input[@id='questionNumber']")
	public WebElement questionNumberTextbox;

	@FindBy(how = How.XPATH, using = "//span[@id='grade-tip-text-span']")
	public WebElement gradeTipTextLabel;

	@FindBy(how = How.XPATH, using = "//h3[contains(., 'Question Text*')]")
	public WebElement questionTextHeader;

	@FindBy(how = How.XPATH, using = "//a[@id='cke_43']")
	public WebElement openResponseButton;
	

	@FindBy(how = How.XPATH, using = "//div[@id='cke_1_contents']//iframe")
	public WebElement questionAreaIframe;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cke_contents cke_reset']/iframe")
	public List <WebElement> feedbackIframe;
	

	@FindBy(how = How.XPATH, using = "//div[@class='openresponse-img']")
	public WebElement openResponseImgDiv;
	

	@FindBy(how = How.XPATH, using = "//div[@class='orsavedquestion']")
	public WebElement openResponseDiv;

	@FindBy(how = How.XPATH, using = "//button[@id='LtiPreviewBut']")
	public WebElement launchStudentViewButton;

	@FindBy(how = How.XPATH, using = "//button[@id='ltiCancelQuestion']")
	public WebElement cancelButton;

	@FindBy(how = How.XPATH, using = "//button[@id='ltiSavelQuestion']")
	public WebElement saveQuestionButton;

	@FindBy(how = How.XPATH, using = "//input[@id='questionIntro']")
	public WebElement questionIntroCheckbox;

	@FindBy(how = How.XPATH, using = "//div[@id='saveBusy']")
	public WebElement saveBusyDiv;
	
	@FindBy(how = How.XPATH, using = "//body[@class='ck_body cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement questionAreaEditableBody;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']")
	public WebElement modalCloseButton;
	
	@FindBy(how = How.XPATH, using = "//a[@id='feedback']")
	public WebElement feedbackTab;
	
	@FindBy(how = How.XPATH, using = "//body[contains(@class,'ck_body ')]")
	public WebElement feedbackBody;
	
	@FindBy(how = How.XPATH, using = "//li/input[@value='isOther']")
	public List<WebElement> applyFeedbackDefaultOption;
	
	
	@FindBy(how = How.XPATH, using = "//div[@id='feedbackTabButton']/i")
	public WebElement feedbackPlusButton;
	
	
	// DropDown List Controls

		
		@FindBy(how = How.XPATH, using = "//a[@id='cke_29']")
		public WebElement dropDownListButton;
		
		@FindBy(how = How.XPATH, using = "//div[@class='wafer_prts_div dropdown-list-component-edit']")
		public WebElement dropDownImgDiv;
		
		
		@FindBy(how = How.XPATH, using = "//ul[@class='select-option-ul']")
		public WebElement dropDownListQuestionAnswerBody;
		
		
		@FindBy(how = How.XPATH, using = "//li[@class='select_options_div']/div/div[contains(@id,'a7c02c06de9d911640047accf10')]")
		public List<WebElement> dropDownListQuestionAnswerOptions;
	
		
		@FindBy(how = How.XPATH, using = "//input[@name='isAnswer' and @value='1']")
		public WebElement dropDownListQuestionModalAnswer;
		
		
		
	// Multiple Choice Controls
		
		@FindBy(how = How.XPATH, using = "//a[@id='cke_30']")
		public WebElement multiChoiceButton;
		
		@FindBy(how = How.XPATH, using = "//span[@class='wafer_prts_div']")
		public WebElement multiQuestionImgDiv;
		
		@FindBy(how = How.XPATH, using = "//body[@class='ck_body cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
		public WebElement multiChoiceQuestionAnswerOptions;
		
		@FindBy(how = How.XPATH, using = "//a[@class='optionLabel' and @value='2']")
		public WebElement clickOptionB;
		
		@FindBy(how = How.XPATH, using = "//a[@class='optionLabel' and @value='3']")
		public WebElement clickOptionC;
		
		@FindBy(how = How.XPATH, using = "//a[@class='optionLabel' and @value='4']")
		public WebElement clickOptionD;
		
		@FindBy(how = How.XPATH, using = "//input[@name='isAnswer' and @value='3']")
		public WebElement multiChoiceQuestionModalAnswer;
		
	
	// Multiple Choice Controls
		
		@FindBy(how = How.XPATH, using = "//a[@id='cke_31']")
		public WebElement multiSelectButton;
		
		@FindBy(how = How.XPATH, using = "//span[@class='ui-dialog-title']")
		public WebElement dialogTitle;
		
		@FindBy(how = How.XPATH, using = "//input[@name='isAnswer' and @value='2']")
		public WebElement multiSelectQuestionModalAnswer2;
		
		@FindBy(how = How.XPATH, using = "//input[@name='isAnswer' and @value='4']")
		public WebElement multiSelectQuestionModalAnswer4;
		
		
		
	// Numeric Entry Controls
		
		@FindBy(how = How.XPATH, using = "//a[@id='cke_32']")
		public WebElement numericEntryButton;
		
		@FindBy(how = How.XPATH, using = "//div[@class='wafer_prts_div numeric-component-edit']")
		public WebElement numericEntryQuestionImgDiv;
		
		@FindBy(how = How.XPATH, using = "//input[@id='answerKey']")
		public WebElement numericEntryAnswerKey;
		
		@FindBy(how = How.XPATH, using = "//input[@name='tolerTypeName' and @value='2']")
		public WebElement numericOption;
		
		
		// Short Answer Controls
		
		@FindBy(how = How.XPATH, using = "//a[@id='cke_33']")
		public WebElement shortAnswerButton;
	
		@FindBy(how = How.XPATH, using = "//div[@class='wafer_prts_div shortanswer-component-edit']")
		public WebElement shortAnswerQuestionImgDiv;
	
		@FindBy(how = How.XPATH, using = "//input[@id='inputShortAnswer']")
		public WebElement shortAnswerAnswerKey;
	
		@FindBy(how = How.XPATH, using = "//input[@id='matchCase']")
		public WebElement matchCaseOption;
		
		

	// OpenResponsePopup Controls

	@FindBy(how = How.XPATH, using = "//button[@id='closeOpenResponse']")
	public WebElement openResponsePopupCloseButton;
	
	

	@FindBy(how = How.XPATH, using = "//div[@id='cke_questionArea']//iframe")
	public WebElement openResponsePopupQuestionAreaIframe;

	@FindBy(how = How.XPATH, using = "//div[@id='modelAnswerPanel']//iframe")
	public WebElement openResponsePopupModelAnswerAreaIframe;

	@FindBy(how = How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement openResponsePopupQuestionAreaEditableBody;

	@FindBy(how = How.XPATH, using = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']")
	public WebElement openResponsePopupModelAnswerAreaEditableBody;

	@FindBy(how = How.XPATH, using = "//div[@class= 'open-response-question-container']//li[@id='question']")
	public WebElement openResponsePopupQuestionTab;

	@FindBy(how = How.XPATH, using = "//div[@class= 'open-response-question-container']//li[@id='modelAns']")
	public WebElement openResponsePopupModelAnswerTab;

	@FindBy(how = How.XPATH, using = "//button[@id='cancelBtn']")
	public WebElement openResponsePopupCancelButton;

	@FindBy(how = How.XPATH, using = "//button[@id='saveBtn']")
	public WebElement popUpSaveButton;
	
	@FindBy(how = How.XPATH, using = "//div[@id='saveBusy']")
	public WebElement saveBusyIcon;
	

	// Initializing Web Driver and PageFactory.
	public AddNewQuestionToQuestionLibrary(WebDriver driver) throws Exception {
		
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		waitForLoadPageElements();
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			ReusableMethods.waitForPageLoad(driver);

			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(launchStudentViewButton));
			wait.until(ExpectedConditions.elementToBeClickable(cancelButton));
			wait.until(ExpectedConditions.elementToBeClickable(saveQuestionButton));
			wait.until(ExpectedConditions.elementToBeClickable(questionTitleTextbox));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		System.out.println("Page readyState");
	}

	@Step("Enter QuestionTitle,  Method: {method} ")
	public void enterQuestionTitle(String questionTitle) {
		questionTitleTextbox.sendKeys(questionTitle);
	}

	@Step("Select the Chapter option based on index,  Method: {method} ")
	public void selectChapterOption(int chapterIndex) throws Exception {
		Select chapterDropdown = new Select(chapterSelect);
		chapterDropdown.selectByIndex(chapterIndex);
		Thread.sleep(3000);
	}

	@Step("User select the sectionEbookReference option based on text,  Method: {method} ")
	public void selectSectionEbookReferenceOption(String text) {

		Select sectionEbookReferenceDropdown = new Select(sectionEbookReferenceSelect);
		sectionEbookReferenceDropdown.selectByVisibleText(text);
	}

	@Step("Enter QuestionNumber,  Method: {method} ")
	public void enterQuestionNumber(String questionNumber) {
		questionNumberTextbox.sendKeys(questionNumber);
	}

	@Step("Click on Open Response Button,  Method: {method} ")
	public void clickOpenResponseButton() throws Exception {
		openResponseButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(openResponseImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(openResponseImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(openResponsePopupCloseButton));
	}
	

	@Step("Enter QuestionText,  Method: {method} ")
	public void enterQuestionText(String questionText) {
		driver.switchTo().frame(openResponsePopupQuestionAreaIframe);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(openResponsePopupQuestionAreaEditableBody));
		openResponsePopupQuestionAreaEditableBody.sendKeys(questionText);

		driver.switchTo().parentFrame();
	}

	@Step("Click on Model Answer Tab,  Method: {method} ")
	public void clickModelAnswerTab() {
		openResponsePopupModelAnswerTab.click();
	}

	@Step("Enter Model Answer,  Method: {method} ")
	public void enterModelAnswer(String modelAnswer) {
		driver.switchTo().frame(openResponsePopupModelAnswerAreaIframe);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(openResponsePopupModelAnswerAreaEditableBody));
		openResponsePopupModelAnswerAreaEditableBody.sendKeys(modelAnswer);

		driver.switchTo().parentFrame();
	}

	@Step("Click on Close Open Response Popup,  Method: {method} ")
	public void clickCloseOpenResponsePopup() {
		openResponsePopupCloseButton.click();
	}

	@Step("Click on Save Open Response Question Answer details in popup,  Method: {method} ")
	public void clickSaveOpenResponsePopup() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", popUpSaveButton);
		//popUpSaveButton.click();

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(saveQuestionButton));
	}

	@Step("Click on Save Open Response Question,  Method: {method} ")
	public void clickSaveQuestionButton() throws Exception {
		saveQuestionButton.click();
		Thread.sleep(10000);
	}
	
	
	@Step("Enter Queestion Text,  Method: {method} ")
	public void enterQuestionName(String questionText) {
		driver.switchTo().frame(questionAreaIframe);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(questionAreaEditableBody));
		questionAreaEditableBody.sendKeys(questionText);
		questionAreaEditableBody.sendKeys(Keys.ENTER);

		driver.switchTo().parentFrame();
	}
	
	@Step("Click on DropDown List Button,  Method: {method} ")
	public void clickDropDownListButton() throws Exception {
		dropDownListButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(dropDownImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(dropDownImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(modalCloseButton));
	}
	
	
	@Step("Click on DropDown List Button,  Method: {method} ")
	public void clickMultiChoiceButton() throws Exception {
		multiChoiceButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(multiQuestionImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(multiQuestionImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(modalCloseButton));
	}
	
	
	@Step("Click on DropDown List Button,  Method: {method} ")
	public void clickMultiSelectButton() throws Exception {
		multiSelectButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(multiQuestionImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(multiQuestionImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(modalCloseButton));
	}
	
	
	@Step("Click on Numeric Entry Button,  Method: {method} ")
	public void clickNumericentryButton() throws Exception {
		numericEntryButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(numericEntryQuestionImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(numericEntryQuestionImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(modalCloseButton));
	}
	
	
	@Step("Click on Short Answer Button,  Method: {method} ")
	public void clickShortAnswerButton() throws Exception {
		shortAnswerButton.click();
		Thread.sleep(2000);
		driver.switchTo().frame(questionAreaIframe);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(shortAnswerQuestionImgDiv));

		Actions actions = new Actions(driver);
		actions.doubleClick(shortAnswerQuestionImgDiv).perform();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		wait.until(ExpectedConditions.elementToBeClickable(modalCloseButton));
	}
	
	
	@Step("Enter DropDownAnswerOptions,  Method: {method} ")
	public void enterDropDownAnswerOptions() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(dropDownListQuestionAnswerBody));
		
		dropDownListQuestionAnswerOptions.get(0).sendKeys("Eggs");
		Thread.sleep(1000);
		dropDownListQuestionAnswerOptions.get(1).sendKeys("Milk");
		Thread.sleep(1000);
		dropDownListQuestionAnswerOptions.get(2).sendKeys("Fruits");
		Thread.sleep(1000);
		dropDownListQuestionAnswerOptions.get(3).sendKeys("Meat");
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDownListQuestionModalAnswer);
		
	}
	
	
	@Step("Enter  Multiple Choice Answers,  Method: {method} ")
	public void enterMultiChoiceAnswerOptions() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.switchTo().frame(feedbackIframe.get(6));
		wait.until(ExpectedConditions.elementToBeClickable(multiChoiceQuestionAnswerOptions));
		
		Thread.sleep(1000);
		multiChoiceQuestionAnswerOptions.sendKeys("10");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		clickOptionB.click();
		
		driver.switchTo().frame(feedbackIframe.get(7));
		multiChoiceQuestionAnswerOptions.sendKeys("20");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		clickOptionC.click();
		
		driver.switchTo().frame(feedbackIframe.get(8));
		multiChoiceQuestionAnswerOptions.sendKeys("30");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", multiChoiceQuestionModalAnswer);
		
		Thread.sleep(1000);
		clickOptionD.click();
		driver.switchTo().frame(feedbackIframe.get(9));
		multiChoiceQuestionAnswerOptions.sendKeys("40");
		driver.switchTo().parentFrame();
		
	}
	
	
	@Step("Enter Multiple Select Answers,  Method: {method} ")
	public void enterMultiSelectAnswerOptions() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.switchTo().frame(feedbackIframe.get(6));
		wait.until(ExpectedConditions.elementToBeClickable(multiChoiceQuestionAnswerOptions));
		
		Thread.sleep(1000);
		multiChoiceQuestionAnswerOptions.sendKeys("5");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		clickOptionB.click();
		
		driver.switchTo().frame(feedbackIframe.get(7));
		multiChoiceQuestionAnswerOptions.sendKeys("7");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", multiSelectQuestionModalAnswer2);
		
		Thread.sleep(1000);
		clickOptionC.click();
		
		driver.switchTo().frame(feedbackIframe.get(8));
		multiChoiceQuestionAnswerOptions.sendKeys("9");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		clickOptionD.click();
		driver.switchTo().frame(feedbackIframe.get(9));
		multiChoiceQuestionAnswerOptions.sendKeys("11");
		driver.switchTo().parentFrame();
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", multiSelectQuestionModalAnswer4);
		
	}
	
	
	@Step("Enter Numeric Entry Answers,  Method: {method} ")
	public void enterNumericEntryAnswerOptions() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(numericEntryAnswerKey));
		
		Thread.sleep(1000);
		numericEntryAnswerKey.sendKeys("25");
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", numericOption);
		
	}
	
	
	@Step("Enter Short Answer Answers,  Method: {method} ")
	public void shortAnswerOptions() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(shortAnswerAnswerKey));
		
		Thread.sleep(1000);
		shortAnswerAnswerKey.sendKeys("Mango");
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", matchCaseOption);
		
	}
	
	
	@Step("Click Feedback Tab,  Method: {method} ")
	public void clickFeedbackTab() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(feedbackTab));
		feedbackTab.click();
	}
	
	
	@Step("Enter Feedback Options,  Method: {method} ")
	public void enterFeedbackOptions(String modelAnswer, String defaultModelAnswer, int option, int iFrame, int  defaultiFrame) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		driver.switchTo().frame(feedbackIframe.get(iFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(modelAnswer);
		driver.switchTo().parentFrame();
		
		WebElement applyFeedbackOption = driver.findElement(By.xpath("//li/input[@value='" + option + "']"));
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", applyFeedbackOption);
		
		
		if(dialogTitle.getText().equalsIgnoreCase("Multiple-select")) {
			
			int  additonalOption = option+2;
			WebElement applyFeedbackAdditionalOption = driver.findElement(By.xpath("//li/input[@value='" + additonalOption + "']"));
			
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", applyFeedbackAdditionalOption);
		
		}
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", feedbackPlusButton);
		
		driver.switchTo().frame(feedbackIframe.get(defaultiFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(defaultModelAnswer);
		driver.switchTo().parentFrame();
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", applyFeedbackDefaultOption.get(1));
		
		Thread.sleep(2000);
		popUpSaveButton.click();

		
	}
	
	
	@Step("Enter Feedback Options,  Method: {method} ")
	public void enterFeedbackOptions_NumericEntry(String modelAnswer, String defaultModelAnswer, int iFrame, int  defaultiFrame) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		driver.switchTo().frame(feedbackIframe.get(iFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(modelAnswer);
		driver.switchTo().parentFrame();
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", feedbackPlusButton);
		
		List <WebElement> ruleType = driver.findElements(By.xpath("//select[contains(@id,'ruleOperator-')]"));
		wait.until(ExpectedConditions.visibilityOf(ruleType.get(1)));
		Select drpRuleType = new Select(ruleType.get(1));
		drpRuleType.selectByValue("other");
		
		driver.switchTo().frame(feedbackIframe.get(defaultiFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(defaultModelAnswer);
		driver.switchTo().parentFrame();
		
		Thread.sleep(2000);
		popUpSaveButton.click();
	
	}
	
	
	@Step("Enter Feedback Options,  Method: {method} ")
	public void enterFeedbackOptions_ShortAnswer(String modelAnswer, String defaultModelAnswer, int iFrame, int  defaultiFrame) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		driver.switchTo().frame(feedbackIframe.get(iFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(modelAnswer);
		driver.switchTo().parentFrame();
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", feedbackPlusButton);

		List <WebElement> ruleType = driver.findElements(By.xpath("//select[contains(@id,'ruleOperator-')]"));
		wait.until(ExpectedConditions.visibilityOf(ruleType.get(1)));
		Select drpRuleType = new Select(ruleType.get(1));
		drpRuleType.selectByValue("default");
		
		driver.switchTo().frame(feedbackIframe.get(defaultiFrame));
		wait.until(ExpectedConditions.elementToBeClickable(feedbackBody));
		ReusableMethods.scrollIntoView(driver, feedbackBody);
		feedbackBody.sendKeys(defaultModelAnswer);
		driver.switchTo().parentFrame();
		
		Thread.sleep(2000);
		popUpSaveButton.click();
	
	}
	

	@Step("Click on Question Library link,  Method: {method} ")
	public void clickQuestionLibrary() throws Exception {
		questionLibrary.click();

		Thread.sleep(10000);
	}
}
