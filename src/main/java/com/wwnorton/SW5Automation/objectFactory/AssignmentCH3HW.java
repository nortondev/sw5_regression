package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH3HW {

	WebDriver driver;
	Actions act;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")
	public WebElement QuestionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-b']")
	public WebElement QuestionTextCurrent;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-s']")
	public WebElement QuestionTextTotal;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH3HW(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	@Step("Enter In-Correct Answer for question #1,  Method: {method} ")
	public void question1EnterInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number932652965241251ddfae8c542f8efaff3e32e9377c04893_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number932664689841251ddfae8c542f8efaff3e32e9377c01594_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number932675492541251ddfae8c542f8efaff3e32e9377c06345_")));

		// Enter InCorrect Answer
		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys("111");
		Q1Part2Element.sendKeys("222");
		Q1Part3Element.sendKeys("333");
	}
	
	
	@Step("Enter In-Correct Answer for question #1,  Method: {method} ")
	public void question1InCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-1014846459bdeaa6bb7f086003f078f9de2221d7c101111_"));
	
		List<WebElement> items = driver.findElements(
				By.xpath("//ul[@class='sorting-preview-item-ul']/li[contains(@id, item-answer-)]"));

		// Drag type where element/compound need to be dropped.
		WebElement SameEmpiricalFormula = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-1014846459bdeaa6bb7f086003f078f9de2221d7c101111_")));
		

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Drag and drop each item to its type respectively
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, items.get(0));
		act.dragAndDrop(items.get(0), SameEmpiricalFormula).build().perform();
		
	}
	
}
