package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH16HW {
	
	WebDriver driver;
	Actions act;
		
		

		// Initializing Web Driver and PageFactory.
		public AssignmentCH16HW() {

			this.driver = BaseDriver.getDriver();
			PageFactory.initElements(driver, this);
			act = new Actions(driver);
		}
		
		
		
		@Step("Enter Incorrect answer for question #2,  Method: {method} ")
		public void question2_IncorrectAnswer(String answer) throws Exception {

			WebDriverWait wait = new WebDriverWait(driver, 50);
			
				// Element which needs to drag.
				WebElement itemElement1 = wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.id("wafer_ps_number25737751430cd9d0f1ccb4b38dfecf49186b4546db00365_")));
			
				itemElement1.sendKeys(answer);
			
			}
		
		@Step("Enter Incorrect answer for question #2,  Method: {method} ")
		public void question2_EnterIncorrectAnswer(String answer) throws Exception {

			WebDriverWait wait = new WebDriverWait(driver, 50);
			
				// Element which needs to drag.
				WebElement itemElement1 = wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.id("wafer_ps_number37921620550cd9d0f1ccb4b38dfecf49186b4546db05151_")));
				                                       
				itemElement1.sendKeys(answer);
			
			}
		
		
		@Step("Drag and drop Correct image icons for question #1,  Method: {method} ")
		public void question1DragAndDrop_CorrectAnswer() throws Exception {

			WebDriverWait wait = new WebDriverWait(driver, 50);
			
			// Using Action class for drag and drop.
			Actions act = new Actions(driver);

			// Terms
			WebElement img1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"item-answer-1-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));
			WebElement img3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"item-answer-3-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));
			WebElement img4 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"item-answer-4-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));
			WebElement img5 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"item-answer-5-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));

			// Terms container.
			WebElement containerA = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"categorySubContainer-1-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));
			WebElement containerB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(
					"categorySubContainer-2-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_")));
			
					// Enter Correct Answer
					// Dragged and dropped.
			ReusableMethods.scrollToElement(driver, By.id("sortingPreviewAnswerItemList-7643832448bdeaa6bb7f086003f078f9de2221d7c108866_"));		
			
			act.dragAndDrop(img1, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(img3, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(img4, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(img5, containerB).build().perform();
					
					
		}



		@Step("Drag and drop Correct image icons for question #5,  Method: {method} ")
		public void question5DragAndDrop_CorrectAnswer() throws Exception {

			WebDriverWait wait = new WebDriverWait(driver, 50);
			
			// Using Action class for drag and drop.
			Actions act = new Actions(driver);

			// Terms
			WebElement valueA = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-3']")));
			WebElement valueB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-4']")));
			WebElement valueC = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@realid='item-5']")));

			// Terms container.
			WebElement containerA = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='1']")));
			WebElement containerB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='2']")));
			WebElement containerC = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@role='tabpanel'][not(contains(@style,'display: none;'))][starts-with(@id,'container')]//li[@siteorder='3']")));	
					// Enter Correct Answer
					// Dragged and dropped.
			ReusableMethods.scrollToElement(driver, By.id("item-sorted-5-4296507429bdeaa6bb7f086003f078f9de2221d7c109704_"));		
			act.dragAndDrop(valueC, containerA).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueA, containerB).build().perform();
			Thread.sleep(2000);
			act.dragAndDrop(valueB, containerC).build().perform();
					
					
		}


	}

		
		
		