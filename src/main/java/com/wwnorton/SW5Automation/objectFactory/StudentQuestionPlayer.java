package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class StudentQuestionPlayer{

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-title fl']/a/span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-title fl']/a")
	public WebElement HeaderTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-score head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderScore;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-timeleft head-dash-list']/span[@class='glyphicons glyphicons-stopwatch']")
	public WebElement TimerIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-dash']/div[@class='head-timeleft head-dash-list']/div[@class='head-big-txt']")
	public WebElement TimerText;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement HeaderEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-info-box']/span[@class='question-info-text']/span[4]")
	public WebElement examSettings_GAUText;
	

	@FindBy(how = How.XPATH, using = "//span[@class='question-info-text']/span[@class='b current-attempt-num']")
	public WebElement HeaderRemainingAttempt;

	@FindBy(how = How.XPATH, using = "//section[@class='container-box question-intro']//h1")
	public WebElement QuestionTitle;
	
	@FindBy(how = How.XPATH, using = "//section[@class='container-box question-intro']//h1//span[@class='question-no']")
	public WebElement QuestionNo;
	
	@FindBy(how = How.XPATH, using = "//div[@class='seebookpage']/div/span[@class='icon icon-08x icon-ebook']")
	public WebElement eBookLink;
	
	@FindBy(how = How.XPATH, using = "//span[@class='isNotMobileDev']")
	public WebElement eBookPageLink;
	
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-part-see-periodic-table' and @style='']/button[@class='see-periodic-table body-content']")
	public WebElement periodicTableButton;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Periodic Table']")
	public WebElement periodicTablePopUp;
	
	@FindBy(how = How.XPATH, using = "//button[@class='close']/span")
	public WebElement periodicTablePopUp_Close;

	
	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page icon-page-disabled prev-question']")
	public WebElement QuestionPreviousDisabled;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question icon-page-disabled']")
	public WebElement QuestionNextDisabled;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//button[@id='viewSolution']")
	public WebElement ViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//button[@id='tryAgain']")
	public WebElement TryAgainButton;

	@FindBy(how = How.XPATH, using = "//button[@id='practiceBtn']")
	public WebElement PracticeButton;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/div/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-attempt-judge']/span[@title='Correct']")
	public WebElement CorrectAnswerIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-attempt-judge']/span[@title='Partial']")
	public WebElement PartialAnswerIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-attempt-judge']/span[@title='Incorrect']")
	public WebElement IncorrectAnswerIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-attempt-judge']/span[@title='In progress']")
	public WebElement InProgressrIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	@FindBy(how = How.XPATH, using = "//div[@role='dialog']/div[@class='fade in modal']")
	public WebElement Feedback;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div/button[@class='close']")
	public WebElement CloseFeedback;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='head-bar modal-header']/h4[@class='modal-title']/span[@class='btn-normal']")
	public WebElement ConfirmViewSolutionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-body']/div[@class='alertBodyIsViewSolution']")
	public WebElement ConfirmViewSolutionMessageText;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-body']/div")
	public WebElement AlertMessageText;

	@FindBy(how = How.XPATH, using = "//div[@class='question_player_div']/article/div[@class='container-box question-list']/div")
	public WebElement SolutionAndAttempts;
	
	//@FindBy(how = How.XPATH, using = "//a[@class='collapseA']/div[@class='judge-title'][contains(text(),'Solution ')]")
	//public WebElement viewSolutionHeader;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'VIEW SOLUTION')]]")
	public WebElement FeedbackViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'TRY AGAIN')]]")
	public WebElement FeedbackTryAgainButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'CONTINUE')]]")
	public WebElement FeedbackContinueButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='btn-normal btn-new-color-2020']/span[contains(text(),'VIEW SOLUTION')]]")
	public WebElement ConfirmPopupViewSolutionButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[contains(text(),'CANCEL')]")
	public WebElement ConfirmPopupCancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[@class='isNotMobileDev']/span[contains(text(),'PRACTICE')]]")
	public WebElement FeedbackPracticeButton;

	@FindBy(how = How.XPATH, using = "//span[@class='isPcDev']")
	public WebElement QuestionCompletedStatus;

	@FindBy(how = How.XPATH, using = "//span[@class='isNotPcDev']")
	public WebElement CompletedStatus;

	@FindBy(how = How.XPATH, using = "//button[@id='checkPracticeBtn']")
	public WebElement CheckPracticeButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='head-bar modal-header']/h4/span[contains(text(),'Are you sure you want to leave this question?')]")
	public WebElement LeaveQuestion;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'YES, LEAVE')]]")
	public WebElement LeaveQuestionYesLeave;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'NO, STAY HERE')]]")
	public WebElement LeaveQuestionStayHere;

	@FindBy(how = How.XPATH, using = "//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-ok icon-circle-ok icon-status-color-2020']")
	public List<WebElement> FeedbackPartCorrectAnswerIconsList;

	@FindBy(how = How.XPATH, using = "//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-remove icon-circle-remove']")
	public List<WebElement> FeedbackPartInCorrectAnswerIconsList;

	@FindBy(how = How.XPATH, using = "//button[span[@id='launchTutorialBtn'][contains(text(),'LAUNCH TUTORIAL LESSON')]]")
	public WebElement LaunchTutorialLessonButton;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash-2']/div[@class='head-score']/div[@class='head-big-txt']/div[@class='as-in-point-score']")
	public WebElement HeaderScorePoints;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-body']/div[contains(text(),'Are you sure you want to submit your answer?')]")
	public WebElement AreyouSuresubmityouranswerpopup;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer']/button[@class='btn-submit-s btn-info fr lato-regular-14']/span/span[@class='btn-normal']")
	public WebElement NoLetMeFinsihButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-footer']/button[@class='btn-submit-s btn-primary fr lato-regular-14']/span[contains(text(), 'YES')]")
	public WebElement YesFinsihButton;
	
	@FindBy(how = How.XPATH, using ="//a[@class='collapseA']/div[@class='judge-title'][contains(text(),' Practice Attempt')]")
	public WebElement PracticeAttemptsection;
	
	@FindBy(how = How.XPATH, using ="//a[@class='collapseA']/div[@class='judge-title'][contains(text(),'Solution ')]")
	public WebElement Solutionsection;
	
	@FindBy(how = How.XPATH, using ="//div[contains(text(), 'Your work is overdue')]")
	public WebElement latePenaltyInfo;
	
	@FindBy(how = How.XPATH, using ="//*[@id='main-content']/div/div[2]/div[@class='extra-credit-message-content clearfix']")
	public WebElement extraCreditInfo;
	
	@FindBy(how = How.XPATH, using ="//div[@class='adaptive-logo']/span[@class='type' and contains(text(),'Warm Up')]")
	public WebElement warmUp_SQP;
	
	@FindBy(how = How.XPATH, using ="//div[@class='adaptive-logo']/span[@class='type' and contains(text(),'Follow Up')]")
	public WebElement followUp_SQP;
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicons glyphicons-chevron-left']")
	public WebElement returnToAssignmentPage;
	
	@FindBy(how = How.XPATH, using = "//button[@id='continueBtn']")
	public WebElement adaptiveContinueButton;

	@FindBy(how = How.XPATH, using = "//div[@class='head-score head-dash-list exam-Text']")
	public WebElement examSettings_HeaderStudentScore;
	
	
	
	// Initializing Web Driver and PageFactory.
	public StudentQuestionPlayer() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		waitForLoadPageElements();
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);

			// AssignmentTitle
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@class='txt-headtitle assignmentNameSpan body-content']")));
			// QuestionInfoText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='question-info-text']")));
			// SubmitAnswer OR PracticeButton OR CheckPracticeButton OR TryAgainButton
			// OR ViewSolutionButton
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(SubmitAnswer),
					ExpectedConditions.elementToBeClickable(PracticeButton),
					ExpectedConditions.elementToBeClickable(CheckPracticeButton),
					ExpectedConditions.elementToBeClickable(TryAgainButton),
					ExpectedConditions.elementToBeClickable(ViewSolutionButton),
					ExpectedConditions.visibilityOf(QuestionCompletedStatus)));
			// QuestionHeaderText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")));
			// HeaderScoreText
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@class='head-score head-dash-list']/div[@class='head-big-txt']")));
			// QuestionFooterText
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='question-step-txt']")));
			// FirstAttemptSection
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[not(contains(@style, 'none'))]/section/div[@class='question-attempt-title']/h2/a/div[contains(text(),'1st attempt')]")));
			// QuestionTextCurrent
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//div[@class='question-page']/span[@class='page-num-b']")));
			// QuestionTextTotal
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//div[@class='question-page']/span[@class='page-num-s']")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//div[@class='head-box']/div[@class='head-dash']/div[@class='head-score head-dash-list']/div[@class='head-big-txt']")));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public void waitForLoadFeedbackOverlayElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);

			// FeedbackViewSolutionButton OR FeedbackTryAgainButton OR CheckPracticeButton
			// OR TryAgainButton
			// OR ViewSolutionButton
			// OR CloseFeedback
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton),
					ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton),
					ExpectedConditions.elementToBeClickable(FeedbackPracticeButton),
					ExpectedConditions.elementToBeClickable(CloseFeedback)));

			// FeedbackPartCorrectAnswerIconsList OR feedbackPartInCorrectAnswerIconsList
			wait.until(ExpectedConditions.or(
					ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//div[@class='question-page']/span[@class='page-num-b']")),
					ExpectedConditions.visibilityOfElementLocated(By.xpath(
							"//div[@class='open-explanation']/ul[@class='isNotMobileDev']//span[@class='glyphicons glyphicons-circle-remove icon-circle-remove']"))));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(5000);
	}

	public String getHeaderDueDate() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(HeaderDueDate));
		return HeaderDueDate.getText();
	}

	@Step("Check View Solution button displayed,  Method: {method} ")
	public boolean isViewSolutionButtonDisplayed() throws Exception {
		try {
			return ViewSolutionButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check Try Again button displayed,  Method: {method} ")
	public boolean isTryAgainButtonDisplayed() throws Exception {
		try {
			return TryAgainButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Check Submit Answer button displayed,  Method: {method} ")
	public boolean isSubmitAnswerButtonDisplayed() throws Exception {
		try {
			return SubmitAnswer.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check Solution View,  Method: {method} ")
	public boolean isViewSolutionDisplayed() throws Exception {
		try {
				
			ReusableMethods.scrollToElement(
					driver, By.xpath("//div[@class='judge-title' and contains(text(),'Solution')]"));
			List<WebElement> viewSolutionHeader = driver.findElements(By.xpath("//div[@class='judge-title' and contains(normalize-space(.),'Solution')]"));
			
			for(int i=0; i<viewSolutionHeader.size(); i++){
				
				String solutionTextEle = viewSolutionHeader.get(i).getText();
				
				if(solutionTextEle.equalsIgnoreCase("Solution")){
					return solutionTextEle != null;
				}
				
			}
				return false;
		
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return true;
		}
			
	}
	

	@Step("Check 'View Solution' & 'SubmitAnswer' buttons displayed,  Method: {method} ")
	public boolean isViewSolutionAndSubmitAnswerDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(SubmitAnswer));
			return ViewSolutionButton.isDisplayed() && SubmitAnswer.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check 'Correct Answer' displayed,  Method: {method} ")
	public boolean isCorrectAnswerIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(CorrectAnswerIcon));
			return CorrectAnswerIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check 'Partial Answer' displayed,  Method: {method} ")
	public boolean isPartialAnswerIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(PartialAnswerIcon));
			return PartialAnswerIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	
	@Step("Check 'Incorrect Answer' displayed,  Method: {method} ")
	public boolean isIncorrectAnswerIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(IncorrectAnswerIcon));
			return IncorrectAnswerIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check 'In Progress' displayed,  Method: {method} ")
	public boolean isInProgressIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(InProgressrIcon));
			return InProgressrIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	

	public String getQuestionFooterMiddleText() {
		String fmt = "";

		List<WebElement> texts = QuestionFooterMiddleText.findElements(By.tagName("span"));

		for (WebElement text : texts)
			fmt += text.getText();

		return fmt;
	}
	
	public boolean iseBookLinkDisplayed() {
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@class='seebookpage']/div/span[@class='icon icon-08x icon-ebook']")));
				
			return eBookLink.isDisplayed();
			
		} catch (Exception e) {
			System.out.println("Exception Handlled: ");
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public boolean isPeriodicTableDisplayed() {
		
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(10);
				
				List<WebElement> elements = driver.findElements(By.xpath
						("//div[@class='question-part-see-periodic-table' and @style='']"
								+ "/button[@class='see-periodic-table body-content']"));
				if (elements.size() >= 2) {
					for(int i=0; i<elements.size(); i++) {
						elements.get(i).isDisplayed();
					}
				} else {
					periodicTableButton.isDisplayed();
				}
				
				
				return true;
		
		} catch (Exception e) {
			System.out.println("Exception Handlled: ");
			System.out.println(e.getMessage());
			return false;
		}
	}
	

	public boolean isPreviousQuestionDisabledButtonDisplayed() {
		try {
			return QuestionPreviousDisabled.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: ");
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean isNextQuestionDisabledButtonDisplayed() {
		try {
			return QuestionNextDisabled.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	public boolean isPreviousQuestionEnabledButtonDisplayed() {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(QuestionPrevious));
				
				return QuestionPrevious.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	public boolean isNextQuestionEnabledButtonDisplayed() {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(QuestionNext));
				
				return QuestionNext.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}
	
	

	@Step("Click on Previous question,  Method: {method} ")
	public void clickPreviousQuestion() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(QuestionPrevious));
			QuestionPrevious.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on next question,  Method: {method} ")
	public void clickNextQuestion() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")));
			QuestionNext.click();
			//waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Submit Answer button,  Method: {method} ")
	public void clickSubmitAnswer() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(SubmitAnswer));
			SubmitAnswer.click();
			waitForLoadFeedbackOverlayElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
		
		@Step("Click on No Let Me Finish Button,  Method: {method} ")
		public void clickNoLetMeFinishButton() throws Exception {
		
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.elementToBeClickable(NoLetMeFinsihButton));
				NoLetMeFinsihButton.click();
		}
		
		@Step("Click on Yes Button,  Method: {method} ")
		public void clickYesFinishButton() throws Exception {
		
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.elementToBeClickable(YesFinsihButton));
				YesFinsihButton.click();
		}

	@Step("Check Feedback Overlay is displayed,  Method: {method} ")
	public boolean isFeedbackOverlayDisplayed() {
		try {
			return Feedback.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}

	@Step("Click on Close Feedback popup,  Method: {method} ")
	public void clickFeedbackClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(CloseFeedback));
			CloseFeedback.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Attempt View is displayed in Feedback Overlay,  Method: {method} ")
	public boolean isAttemptDisplayed(String attemptText) {
		try {
			
				 WebElement attempt = driver.findElement(By.xpath("//div[@class='open-explanation-title' and contains(text(),'" + attemptText + "')]"));
				 return attempt.isDisplayed();

				//WebElement attempt = driver.findElement(By.xpath(
				 //			"//div[not(contains(@style, 'none'))]/section/div[@class='question-attempt-title']/h2/a//div[contains(text(),'"
					//			+ attemptText + "')]"));
				//return attempt.isDisplayed();
		
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
		}
	}
	
	@Step("Check Attempt View is displayed in SQP,  Method: {method} ")
	public boolean isAttemptViewDisplayed(String attemptText) {
		try {
			
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@class='judge-title' and contains(text(),'"
							+ attemptText + "')]")));

			ReusableMethods.scrollToElement(
					driver,
					By.xpath("//div[@class='judge-title' and contains(text(),'"
							+ attemptText + "')]"));
			List<WebElement> attemptView = driver.findElements(By
					.xpath("//div[@class='judge-title' and contains(normalize-space(.),'"
							+ attemptText + "')]"));
			for(int i=0; i<attemptView.size(); i++){
				String attemptTextEle = attemptView.get(i).getText();
				if(attemptTextEle.equalsIgnoreCase(attemptText)){
					return attemptTextEle != null;
					}
				
			}
			return false;
			

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return true;
		}
		
		
	}
	

	public int getAttemptCount() {
		return SolutionAndAttempts.findElements(By.xpath("//section[@practice='attempt']")).size();
	}

	@Step("Check Practice button displayed,  Method: {method} ")
	public boolean isLeaveQuestionDisplayed() throws Exception {
		try {
			return LeaveQuestion.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check Submit Your Answer Pop-Up is displayed,  Method: {method} ")
	public boolean isSubmitYourAnswerDisplayed() throws Exception {
		try {
			return AreyouSuresubmityouranswerpopup.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Feedback 'TRY AGAIN' button,  Method: {method} ")
	public void clickLeaveQuestionYesLeave() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(LeaveQuestionYesLeave));
			LeaveQuestionYesLeave.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Feedback overlay displayed with 'View Solution' & 'Try Again' option,  Method: {method} ")
	public boolean isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton));
			return FeedbackViewSolutionButton.isDisplayed() && FeedbackTryAgainButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Check Feedback overlay displayed with 'Try Again' option,  Method: {method} ")
	public boolean isFeedbackOverlayWithTryAgainButtonDisplayed() throws Exception {
		
		boolean TryButtonExist = true;
		
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton));
				
				if(FeedbackTryAgainButton.isDisplayed() == true) {
					TryButtonExist = true;
				}
				
				return TryButtonExist;
				
			}
				
		catch (Exception e) {
			
			return false;
		
		}

	}
	
	
	@Step("Check Feedback overlay displayed with 'View Solution' option,  Method: {method} ")
	public boolean isFeedbackOverlayWithViewSolutionButtonDisplayed() throws Exception {
		
		boolean ViewButtonExist = false;
		
		try {
			
				Thread.sleep(2000);
				if(FeedbackViewSolutionButton.isDisplayed() == true) {
					ViewButtonExist = true;
				}
				
				return ViewButtonExist;
				
			}
				
		catch (Exception e) {
			
			return false;
		
		}

	}
	

	@Step("Click on Feedback VIEW SOLUTION button,  Method: {method} ")
	public void clickFeedbackViewSolution() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackViewSolutionButton));
			FeedbackViewSolutionButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Confirm Popup CANCEL button,  Method: {method} ")
	public void clickConfirmPopupCancelButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupCancelButton));
			ConfirmPopupCancelButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Confirm Popup VIEW SOLUTION button,  Method: {method} ")
	public void clickConfirmPopupViewSolutionButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupViewSolutionButton));
			ConfirmPopupViewSolutionButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Feedback TRY AGAIN button,  Method: {method} ")
	public void clickFeedbackTryAgain() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackTryAgainButton));
			FeedbackTryAgainButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(3000);
		if (isSubmitAnswerButtonDisplayed() == false) {
			clickTryAgain();
		}
	}

	@Step("Check Feedback Practice button displayed,  Method: {method} ")
	public boolean isFeedbackPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackPracticeButton));
			return FeedbackPracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Feedback PRACTICE button,  Method: {method} ")
	public void clickFeedbackPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackPracticeButton));
			FeedbackPracticeButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on VIEW SOLUTION button,  Method: {method} ")
	public void clickViewSolution() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ViewSolutionButton));
			ViewSolutionButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on TRY AGAIN button,  Method: {method} ")
	public void clickTryAgain() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(TryAgainButton));
			TryAgainButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(3000);
	}

	@Step("Check Practice button displayed,  Method: {method} ")
	public boolean isPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(PracticeButton));
			return PracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on PRACTICE button,  Method: {method} ")
	public void clickPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(PracticeButton));
			PracticeButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check CHECK PRACTICE button displayed,  Method: {method} ")
	public boolean isCheckPracticeButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(CheckPracticeButton));
			return CheckPracticeButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on CHECK PRACTICE button,  Method: {method} ")
	public void clickCheckPractice() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(CheckPracticeButton));
			CheckPracticeButton.click();
			waitForLoadFeedbackOverlayElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Confirm overlay displayed with 'View Solution' & 'Cancel' option,  Method: {method} ")
	public boolean isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupViewSolutionButton));
			wait.until(ExpectedConditions.elementToBeClickable(ConfirmPopupCancelButton));
			return ConfirmPopupViewSolutionButton.isDisplayed() && ConfirmPopupCancelButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getQuestionInfoText() throws Exception {
		return QuestionInfoText.getText();
	}

	public String getQuestionTitle() throws Exception {
		String questionTitle = QuestionTitle.getText();
		if (questionTitle.compareTo("") == 0) {
			questionTitle = QuestionTitle.getAttribute("title");
		}
		// Create a pattern to be searched
		Pattern pattern = Pattern.compile("Question");
		String[] result = pattern.split(questionTitle);
		if (result != null && result.length > 1) {
			questionTitle = result[0] + " Question" + result[1];
		}
		return questionTitle;
	}

	public String getHeaderScore() throws Exception {
		return HeaderScore.getText().replace('\n', ' ');
	}
	public String getHeaderScoreinPoints() throws Exception {
		//return HeaderScorePoints.getText().replace('\n', ' ');
		return HeaderScorePoints.getText();
	}

	public String getQuestionFooterLeftText() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/div/span[@class='question-step-txt']")));
		return QuestionFooterLeftText.getText();
	}

	public String getQuestionCompletedStatusText() throws Exception {
		return QuestionCompletedStatus.getText();
	}

	public String getTimerText() throws Exception {
		return TimerText.getText();
	}

	public String getAlertMessageText() throws Exception {
		return AlertMessageText.getText();
	}
	
	public String getAreYouSureSubmitAnswergetText() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='modal-content']/div[@class='modal-body']/div")));
		return AreyouSuresubmityouranswerpopup.getText();
	}

	public String getConfirmViewSolutionHeaderText() throws Exception {
		return ConfirmViewSolutionHeaderText.getText();
	}

	public String getConfirmViewSolutionMessageText() throws Exception {
		return ConfirmViewSolutionMessageText.getText();
	}

	@Step("Click on Close Timer Expired Alert popup,  Method: {method} ")
	public void clickTimerExpiredAlertClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(CloseFeedback));
			//CloseFeedback.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", CloseFeedback);
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public void clickHeaderTitle(StudentAssignmentPage sap) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(HeaderTitle));
		HeaderTitle.click();
		if (sap != null) {
			sap.waitForLoadPageElements();
		}
	}

	public String getHeaderRemainingAttempt() throws Exception {
		return HeaderRemainingAttempt.getText();
	}

	@Step("Click on 'LAUNCH TUTORIAL LESSON' button,  Method: {method} ")
	public void clickLaunchTutorialLessonButton() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(LaunchTutorialLessonButton));
		LaunchTutorialLessonButton.click();
	}

	@Step("Check Feedback Continue button displayed,  Method: {method} ")
	public boolean isFeedbackContinueButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackContinueButton));
			return FeedbackContinueButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on Continue button,  Method: {method} ")
	public void clickContinue() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(FeedbackContinueButton));
			FeedbackContinueButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	@Step("Click on Adaptive Continue button,  Method: {method} ")
	public void clickAdaptiveContinueButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(adaptiveContinueButton));
			adaptiveContinueButton.click();
			waitForLoadPageElements();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	

	@Step("Check if Question Parts are Displayed,  Method: {method} ")
	public boolean isQuestionPartDisplayed(String partText) {
		try {
			List<WebElement> partList = driver.findElements(By.xpath(
					"//div[not(contains(@style, 'none'))]/section[@issubmitted='false']/div[@class='question-containers']//div[@name='partList']//h2"));

			for (int i = 0; i < partList.size(); i++) {

				WebElement part = partList.get(i);

				if (part.getText().equals(partText))
					return true;
			}

			return false;

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
	}
	
	@Step("Check if Late Penalty Information is Displayed,  Method: {method} ")
	public boolean isLatePenaltyInfoDisplayed() {
		
		boolean latePenaltyInfoExist = true;
		
		try {
				if(latePenaltyInfoExist == true) {	
					latePenaltyInfo.isDisplayed();
			}

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return false;
		}
		
		return latePenaltyInfoExist;
	}
	
	@Step("Get Extra Credit Information is displayed in SQP,  Method: {method} ")
	public String getExtraCreditInfo() {
		
		boolean getExtraCreditInfoExist = true;
		
		try {
				if(getExtraCreditInfoExist == true) {	
					extraCreditInfo.isDisplayed();
			}

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
			return null;
		}
		
		return extraCreditInfo.getText();
	}
	
	
	@Step("Warm-Up Assginemtn is displayed?,  Method: {method} ")
	public boolean warmUpAssignment_SQP() {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(warmUp_SQP));
				
				ReusableMethods.scrollIntoView(driver, warmUp_SQP);
				return warmUp_SQP.isDisplayed();

			} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Follow-Up Assginemtn is displayed?,  Method: {method} ")
	public boolean followUpAssignment_SQP() {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(followUp_SQP));
				
				ReusableMethods.scrollIntoView(driver, followUp_SQP);
				return followUp_SQP.isDisplayed();

			} catch (Exception e) {
			return false;
		}
	}
	
	
	@Step("Multiple Question Parts are displayed?,  Method: {method} ")
	public boolean multipleQuestions_SQP(String partNumber) {
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				WebElement multipleParts = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//div[@class='question-part-title']/h2[text()='" + partNumber + "']")));
				
				ReusableMethods.scrollIntoView(driver, multipleParts);
				return multipleParts.isDisplayed();

			} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Get Header Student Score in Exam Settings,  Method: {method} ")
	public String getExamSettings_HeaderStudentScore() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(examSettings_HeaderStudentScore));
		return examSettings_HeaderStudentScore.getText();
		
	}
	
}
