package com.wwnorton.SW5Automation.objectFactory;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class ClassActivityReportPage {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//span[@class='head-dash-title']")
	public WebElement ClassActivityReportHeader;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//h1[@id='activity-name-area']/span[@class='report-assignment-title']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//div[@id='column-chart']//*[@class='highcharts-title']")
	public WebElement ColumnChartScore;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//div[@id='pie-chart']//*[@class='highcharts-title']")
	public WebElement PieChartScore;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//div[@id='line-chart']//*[@class='highcharts-title']")
	public WebElement AverageTimeSpent;

	@FindBy(how = How.ID, using = "chart-export")
	public WebElement ExportButton;

	@FindBy(how = How.XPATH, using = "//div[@class='tab-search']/span")
	public WebElement StudentSearchIcon;

	@FindBy(how = How.XPATH, using = "//input[@class='icon-tab-search-text']")
	public WebElement StudentSearchSearchText;

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Overall score')]/following-sibling::td")
	public WebElement StudentOverallScore;

	@FindBy(how = How.XPATH, using = "//table[@class='report-student-table table table-bordered table-condensed table-hover']/tbody/tr/td[@class='textIndentLeft']/span[@class='glyphicons glyphicons-resize-full cursor-pointer fr']")
	public WebElement StudentNameCursonPointer;

	@FindBy(how = How.XPATH, using = "//div[@class='head-bar modal-header']/button[@type='button']/span")
	public WebElement StudentDetailCloselink;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//ul/li[contains(text(),'STUDENTS')]")
	public WebElement StudentsTab;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//ul/li[contains(text(),'QUESTIONS')]")
	public WebElement QuestionsTab;

	@FindBy(how = How.XPATH, using = "//div[@id='assignment-pdf-container']//ul/li[contains(text(),'OBJECTIVES')]")
	public WebElement ObjectivesTab;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-deselected'][contains(text(),'.CSV')]")
	public WebElement csvButtonDeselect;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info'][contains(text(),'DOWNLOAD')]")
	public WebElement DownloadButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-title fl']/a/span[contains(text(),'Smartwork 5')]")
	public WebElement SmartWork5Link;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-normal btn-submit-s btn-primary ml15 lato-regular-14 fr']/span[contains(text(),'CLASS ACTIVITY REPORT')]")
	public WebElement CARButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class = 'review-txt fl width-50-percent']/p[@id = 'dueDate-area']")
	public WebElement gauText;
	
	@FindBy(how = How.XPATH, using = "//h1[@id='activity-name-area']/span[@class = 'ExtraCreditBesideTitle']")
	public WebElement extraCreditTitle_CAR;
	

	// Initializing Web Driver and PageFactory.
	public ClassActivityReportPage() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

		waitForLoadPageElements();
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 300);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			wait.until(ExpectedConditions.elementToBeClickable(ExportButton));

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		Thread.sleep(8000);
	}

	@Step("Get CAR Average score on assignment,  Method: {method} ")
	public String getCARAverageScoreOnAssignment() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='assignment-pdf-container']//div[@id='column-chart']//*[@class='highcharts-title']")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'chart-left']//*[@class = 'highcharts-title']")));
		return ColumnChartScore.getText();
	}
	
	@Step("Click the Export Button,  Method: {method} ")
	public void clickExportButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ExportButton));
		ExportButton.click();
	}
	
	@Step("select the .CSV or PDF to download the contents,  Method: {method} ")
	public void clickCSVorPDFButton(String fileformatName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ExportButton));
		WebElement clickfileName = driver.findElement(By.xpath("//button[@class='btn-submit-s btn-deselected'][contains(text(),'"+fileformatName+"')]"));
		ReusableMethods.clickWebElement(driver, clickfileName);
	}
	
	@Step("Click the Download button to Download the PDF or CSV file,  Method: {method} ")
	public void clickdownloadButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ExportButton));
		DownloadButton.click();
	}
	
	
	

	@Step("Get CAR % of students completed the assignment,  Method: {method} ")
	public String getCARStudentsCompletedTheAssignmentScore() throws Exception {
		return PieChartScore.getText();
	}

	@Step("Click Student Tab open Student Detail Overlay popup Button for first student,  Method: {method} ")
	public void clickCARStudentsTabOpenStudentDetailOverlayButton(String studentName) throws Exception {
		if (studentName != null && studentName.isEmpty() == false) {
			List<WebElement> studentDetailButtons = driver.findElements(By.xpath(
					"//div[@id='assignment-pdf-container']//table[@class='report-student-table table table-bordered table-condensed table-hover']//tr[td/span[contains(text(),'"
							+ studentName
							+ "')]]/td[1]/span[@class='glyphicons glyphicons-resize-full cursor-pointer fr']"));
			if (studentDetailButtons.size() == 1) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", studentDetailButtons.get(0));
				//studentDetailButtons.get(0).click();
			}
		}
	}

	@Step("Click the Student Search Icon,  Method: {method} ")
	public void clickStudentSearchIcon() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);

			wait.until(ExpectedConditions.elementToBeClickable(StudentSearchIcon));
			StudentSearchIcon.click();

		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Enter a Student Name in icon tab text box,  Method: {method} ")
	public void enterStudentName(String studentNameOnCAR) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(StudentSearchSearchText));
		StudentSearchSearchText.click();
		Thread.sleep(1000);
		StudentSearchSearchText.clear();
		Thread.sleep(1000);
		StudentSearchSearchText.sendKeys(studentNameOnCAR);
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.ENTER).build().perform();

	}

	@Step("Click the Student Name link,  Method: {method} ")
	public void clickStudentNamelink(String studentNameOnCAR) throws Exception {
		StudentNameCursonPointer.click();

	}

	@Step("get the Students Overall Score,  Method: {method} ")
	public String getStudentOverallScore() throws Exception {
		Thread.sleep(3000);
		return StudentOverallScore.getText();

	}

	@Step("close the Student Detail Page,  Method: {method} ")
	public void closeStudentDetailPage() throws Exception {

		StudentDetailCloselink.click();

	}

	@Step("Get total students count,  Method: {method} ")
	public int getTotalStudentsCount() throws Exception {
		int retVal = 0;
		List<WebElement> dataRows = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-student-table table table-bordered table-condensed table-hover']/tbody/tr"));
		retVal = dataRows.size();
		return retVal;
	}

	@Step("Get Students Tab student score for given student,  Method: {method} ")
	public String getCARStudentsTabStudentScore(String studentName) throws Exception {
		String retVal = "";
		if (studentName != null && studentName.isEmpty() == false) {
			List<WebElement> studentTabScores = driver.findElements(By.xpath(
					"//div[@id='assignment-pdf-container']//table[@class='report-student-table table table-bordered table-condensed table-hover']//tr[td/span[contains(.,'"
							+ studentName + "')]]/td[2]"));
			if (studentTabScores.size() == 1) {
				retVal = studentTabScores.get(0).getText();
			}
		}
		return retVal;
	}

	@Step("Get Students Tab questions answered for given student,  Method: {method} ")
	public String getCARStudentsTabQuestionsAnswered(String studentName) throws Exception {
		String retVal = "";
		if (studentName != null && studentName.isEmpty() == false) {
			List<WebElement> studentTabScores = driver.findElements(By.xpath(
					"//div[@id='assignment-pdf-container']//table[@class='report-student-table table table-bordered table-condensed table-hover']//tr[td/span[contains(.,'"
							+ studentName + "')]]/td[3]"));
			if (studentTabScores.size() == 1) {
				retVal = studentTabScores.get(0).getText();
			}
		}
		return retVal;
	}

	@Step("Get Students Tab incorrect attempts for given student,  Method: {method} ")
	public String getCARStudentsTabIncorrectAttempts(String studentName) throws Exception {
		String retVal = "";
		if (studentName != null && studentName.isEmpty() == false) {
			List<WebElement> studentTabScores = driver.findElements(By.xpath(
					"//div[@id='assignment-pdf-container']//table[@class='report-student-table table table-bordered table-condensed table-hover']//tr[td/span[contains(text(),'"
							+ studentName + "')]]/td[4]"));
			if (studentTabScores.size() == 1) {
				retVal = studentTabScores.get(0).getText();
			}
		}
		return retVal;
	}

	@Step("Click Students Tab,  Method: {method} ")
	public void clickStudentsTab() throws Exception {
		StudentsTab.click();
		Thread.sleep(5000);
	}

	@Step("Click Questions Tab,  Method: {method} ")
	public void clickQuestionsTab() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='assignment-pdf-container']//ul/li[contains(text(),'QUESTIONS')]")));
		QuestionsTab.click();
		Thread.sleep(5000);
	}

	@Step("Click Objectives Tab,  Method: {method} ")
	public void clickObjectivesTab() throws Exception {
		ObjectivesTab.click();
		Thread.sleep(5000);
	}

	@Step("Get Questions Tab total Questions count,  Method: {method} ")
	public int getTotalQuestionsCount() throws Exception {
		int retVal = 0;
		List<WebElement> dataRows = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']/tbody/tr"));
		retVal = dataRows.size();
		return retVal;
	}

	@Step("Get Questions Tab Average Scores List,  Method: {method} ")
	public List<String> getCARQuestionsTabAverageScoresList() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> questionsTabAverageScores = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']//td[2]"));
		if (questionsTabAverageScores.size() > 0) {
			for (WebElement questionsTabAverageScore : questionsTabAverageScores) {
				retVal.add(questionsTabAverageScore.getText());
			}
		}
		return retVal;
	}

	@Step("Get Questions Tab Student Submissions List,  Method: {method} ")
	public List<String> getCARQuestionsTabStudentSubmissionsList() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> questionsTabStudentSubmissions = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']//td[3]"));
		if (questionsTabStudentSubmissions.size() > 0) {
			for (WebElement questionsTabStudentSubmission : questionsTabStudentSubmissions) {
				retVal.add(questionsTabStudentSubmission.getText());
			}
		}
		return retVal;
	}

	@Step("Get Questions Tab Average Attempts List,  Method: {method} ")
	public List<String> getCARQuestionsTabAverageAttemptsList() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> questionsTabAverageAttempts = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']//td[4]"));
		if (questionsTabAverageAttempts.size() > 0) {
			for (WebElement questionsTabAverageAttempt : questionsTabAverageAttempts) {
				retVal.add(questionsTabAverageAttempt.getText());
			}
		}
		return retVal;
	}

	@Step("Get Questions Tab Average Time Spent List,  Method: {method} ")
	public List<String> getCARQuestionsTabAverageTimeSpentList() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> questionsTabAverageTimeSpentList = driver.findElements(By.xpath(
				"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']//td[5]"));
		if (questionsTabAverageTimeSpentList.size() > 0) {
			for (WebElement questionsTabAverageTimeSpent : questionsTabAverageTimeSpentList) {
				retVal.add(questionsTabAverageTimeSpent.getText());
			}
		}
		return retVal;
	}

	public List<WebElement> getPieChartSlices(String percentValue) throws Exception {
		List<WebElement> pieChartSlices = driver.findElements(By.xpath(
				"//*[@class='highcharts-data-labels highcharts-series-0 highcharts-tracker']//*[contains(text(),'"
						+ percentValue + "')]"));
		return pieChartSlices;
	}

	@Step("Click Questions Tab open Question Detail Overlay popup Button for given question,  Method: {method} ")
	public void clickCARQuestionsTabOpenQuestionDetailOverlayButton(int questionNumber) throws Exception {
		if (questionNumber > 0) {
			List<WebElement> questionDetailButtons = driver.findElements(By.xpath(
					"//div[@id='assignment-pdf-container']//table[@class='report-question-table table table-bordered table-condensed table-hover']//tr/td[1]/span[@class='glyphicons glyphicons-resize-full cursor-pointer fr']"));
			if (questionDetailButtons.size() > 0) {
				ReusableMethods.moveToElement(driver, questionDetailButtons.get(questionNumber - 1));
				Thread.sleep(1000);
				questionDetailButtons.get(questionNumber - 1).click();
			}
		}
	}
	
	@Step("Click the Smartwork 5 link, Method: {method} ")
	public void clickSmartwork5LinkfromCarPage(){
			SmartWork5Link.click();
}		
	
	@Step("Click the Car Button from Assignment page, Method: {method} ")
	public void clickCarbutton(){
			CARButton.click();
}
	
	@Step("Get GAU Text, Method: {method} ")
	public String getGAUText_CarPage() throws Exception{
			
		return gauText.getText();
}
	
	
}
