package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH20HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;


	// Initializing Web Driver and PageFactory.
	public AssignmentCH20HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	
	@Step("Select Incorrect answers for question #1,  Method: {method} ")
	public void question1_IncorrectAnswers() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div69051388990cd9d0f1ccb4b38dfecf49186b4546db01598_"));
		
		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div69051388990cd9d0f1ccb4b38dfecf49186b4546db01598_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part1);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div69052900190cd9d0f1ccb4b38dfecf49186b4546db06388_"));
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div69052900190cd9d0f1ccb4b38dfecf49186b4546db06388_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part2);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div69053901130cd9d0f1ccb4b38dfecf49186b4546db06677_"));
		
		WebElement item1Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div69053901130cd9d0f1ccb4b38dfecf49186b4546db06677_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part3);

		
	}
	
	
	@Step("Select Correct answers for question #2,  Method: {method} ")
	public void question2_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div69034816530cd9d0f1ccb4b38dfecf49186b4546db05658_"));

		WebElement itemElement_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@id='wafer_ps_number69034816530cd9d0f1ccb4b38dfecf49186b4546db05658_']")));
		
		itemElement_Part1.sendKeys("7");
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div69036099720cd9d0f1ccb4b38dfecf49186b4546db08384_"));

		WebElement itemElement_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@id='wafer_ps_number69036099720cd9d0f1ccb4b38dfecf49186b4546db08384_']")));
		
		itemElement_Part2.sendKeys("9");
		
	}
	
	@Step("Select Incorrect answers for question #3,  Method: {method} ")
	public void question3_IncorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToBottom(driver);
		
		// Switch to the frame using the frame id
		driver.switchTo().frame("wafer_ps_chemi_div4106642072c72f57d0ab305d01cd629db7fee5b68605651__attempt1-mol2D");
		ReusableMethods.waitForPageLoad(driver);
		
		WebElement itemElement_Img = wait.until(ExpectedConditions.elementToBeClickable
				(By.xpath("//div[@role='button' and @title='Ethene']")));
		
		Thread.sleep(2000);
		itemElement_Img.sendKeys("");
		itemElement_Img.click();

		//Switch back to default window
		driver.switchTo().defaultContent();
		
	}
	
	@Step("Select Incorrect answers for question #1,  Method: {method} ")
	public void question1_DragNDrop_IncorrectAnswers() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		

		// Element on which need to drop.
		WebElement dropContainer1Element = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id
						("categorySubContainer-1-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		WebElement dropContainer2Element = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id
						("categorySubContainer-2-5145224643e0d99b793349f08101be1d61405adf7005407_")));
		

		ReusableMethods.scrollToElement(driver, By.id
				("sortingPreviewAnswerItemList-5145224643e0d99b793349f08101be1d61405adf7005407_"));

		// Enter Correct Answer
		// Dragged and dropped.
		

		ReusableMethods.moveToElement(driver, dragItem1Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem1Element, dropContainer1Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem2Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem2Element, dropContainer1Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem3Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem3Element, dropContainer2Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem4Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem4Element, dropContainer2Element).build().perform();	

		
	}
	
	@Step("Select Correct answers for question #3,  Method: {method} ")
	public void question3_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_number_div5553224150e0d99b793349f08101be1d61405adf7000537_"));

		WebElement itemElement1_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@id='wafer_ps_number5553224150e0d99b793349f08101be1d61405adf7000537_']")));
		
		itemElement1_Part1.sendKeys("12");
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div5553292163e0d99b793349f08101be1d61405adf7001103_"));

		List <WebElement> itemElement1_Part2 = driver.findElements(By.xpath
				("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='2']"));
		
		List <WebElement> itemElement2_Part2 = driver.findElements(By.xpath
				("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='1']"));
		
		//itemElement1_Part2.get(0).click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", itemElement1_Part2.get(0));
		//itemElement2_Part2.get(0).click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", itemElement2_Part2.get(0));
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_checkbox_div5553598963e0d99b793349f08101be1d61405adf7006550_"));

		List <WebElement> itemElement1_Part3 = driver.findElements(By.xpath
				("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='1']"));
		
		List <WebElement> itemElement2_Part3 = driver.findElements(By.xpath
				("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='3']"));
		
		//itemElement1_Part3.get(1).click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", itemElement1_Part3.get(1));
		//itemElement2_Part3.get(1).click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", itemElement2_Part3.get(1));
		
	}

}
