package com.wwnorton.SW5Automation.objectFactory;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Step;

public class CreateCustomAssignment {

	WebDriver driver;
	AddNewQuestionToQuestionLibrary addNewQuestion;
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readAddquestionJson();

	// Initializing Web Driver and PageFactory.
	public CreateCustomAssignment() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//a[@role='button']/span[contains(text(),'Create New Assignment')]")
	public WebElement createAssignmentButton;
	@FindBy(how = How.XPATH, using = "//a[@role='button']/span[contains(text(),'Create Fully Adaptive Assignment')]")
	public WebElement createFullyAdaptiveAssignmentButton;

	@FindBy(how = How.XPATH, using = "//div[@type='text']/input[@placeholder ='Your assignment name']")
	public WebElement AssignmentName;

	@FindBy(how = How.XPATH, using = "//div[@class='search-text-input']/input[@type='text']")
	public WebElement SearchQuestionTextbox;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-search']")
	public WebElement Searchbutton;

	@FindBy(how = How.XPATH, using = "//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")
	public WebElement questionplusIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='circle-num']")
	public WebElement QuestionsCount;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s lato-black-14']/span[contains(text(),'YOUR CURRENT ASSIGNMENT')]")
	public WebElement YourCurrentAssignmentButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn-submit-s btn-info lato-black-14'][contains(text(),'GO TO ASSIGNMENT SETTINGS')]")
	public WebElement GoToAssignmentSettingsButton;

	@FindBy(how = How.XPATH, using = "//span[@class='total-points table-total']/span[@class='total-points-value']")
	public WebElement TotalPoints;

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//button[span[contains(text(),'CREATE NEW QUESTION')]]")
	public WebElement createNewQuestion;
	
	@FindBy(how = How.XPATH, using = "//button[@class='close']")
	public WebElement closeButton;
	

	
	
	@Step("Click the Create Assignment Button,  Method: {method} ")
	public void createAssignment() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(createAssignmentButton));
		//createAssignmentButton.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", createAssignmentButton);

	}

	
	@Step("Click the Create Create Fully Adaptive Assignment Button,  Method: {method} ")
	public void createFullyAdaptiveAssignment() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(createFullyAdaptiveAssignmentButton));
		//createFullyAdaptiveAssignmentButton.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", createFullyAdaptiveAssignmentButton);

	}
	
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public String createNewCustomQuestion_MetaData(String metaDataTitle) throws Exception {
		//WebDriverWait wait = new WebDriverWait(driver, 100);
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		Thread.sleep(5000);
		
		String retQuestionValue = "";
		String Question_guid = GetRandomId.randomAlphaNumeric(4).toLowerCase();
		Question_guid = Question_guid.replaceAll("[^a-zA-Z0-9]+", "a");
		retQuestionValue = metaDataTitle + "-" + Question_guid;
		
			addNewQuestion = new AddNewQuestionToQuestionLibrary(driver);
			addNewQuestion.enterQuestionTitle(retQuestionValue);
			addNewQuestion.selectChapterOption(1);
			addNewQuestion.selectChapterOption(1);// Chapter 1: Particles of Matter: Measurement and the Tools of Science
	
			addNewQuestion.selectSectionEbookReferenceOption("1.1: How and Why");
	
			addNewQuestion.enterQuestionNumber(Question_guid);

		return retQuestionValue;
	}
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void createNewCustomQuestion_DropDownList(String questionName, String modelAnswer, String DefaultModelAnswer, int Choice) throws Exception {
		
		int feedbackiFrame = 6;
		int feedbackDefaultiFrame = 7;
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);
		
		addNewQuestion.enterQuestionName(questionName);
		
		addNewQuestion.clickDropDownListButton();
	
		addNewQuestion.enterDropDownAnswerOptions();
	
		addNewQuestion.clickFeedbackTab();
	
		addNewQuestion.enterFeedbackOptions(modelAnswer, DefaultModelAnswer, Choice, feedbackiFrame, feedbackDefaultiFrame);
	
		addNewQuestion.clickSaveQuestionButton();
	}
	
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void createNewCustomQuestion_MultiChoice(String questionName, String modelAnswer, String DefaultModelAnswer, int Choice) throws Exception {
		
		int feedbackiFrame = 9;
		int feedbackDefaultiFrame = 10;
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);
		
		addNewQuestion.enterQuestionName(questionName);
		
		addNewQuestion.clickMultiChoiceButton();
	
		addNewQuestion.enterMultiChoiceAnswerOptions();
	
		addNewQuestion.clickFeedbackTab();
	
		addNewQuestion.enterFeedbackOptions(modelAnswer, DefaultModelAnswer, Choice, feedbackiFrame, feedbackDefaultiFrame);
	
		addNewQuestion.clickSaveQuestionButton();
	}
	
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void createNewCustomQuestion_MultiSelect(String questionName, String modelAnswer, String DefaultModelAnswer, int Choice) throws Exception {
		
		int feedbackiFrame = 10;
		int feedbackDefaultiFrame = 11;
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);
		
		addNewQuestion.enterQuestionName(questionName);
		
		addNewQuestion.clickMultiSelectButton();
	
		addNewQuestion.enterMultiSelectAnswerOptions();
	
		addNewQuestion.clickFeedbackTab();
	
		addNewQuestion.enterFeedbackOptions(modelAnswer, DefaultModelAnswer, Choice, feedbackiFrame, feedbackDefaultiFrame);
	
		addNewQuestion.clickSaveQuestionButton();
	}
	
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void createNewCustomQuestion_NumericEntry(String questionName, String modelAnswer, String DefaultModelAnswer) throws Exception {
		
		int feedbackiFrame = 7;
		int feedbackDefaultiFrame = 8;
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);
		
		addNewQuestion.enterQuestionName(questionName);
		
		addNewQuestion.clickNumericentryButton();
	
		addNewQuestion.enterNumericEntryAnswerOptions();
	
		addNewQuestion.clickFeedbackTab();
	
		addNewQuestion.enterFeedbackOptions_NumericEntry(modelAnswer, DefaultModelAnswer, feedbackiFrame, feedbackDefaultiFrame);
	
		addNewQuestion.clickSaveQuestionButton();
	}
	
	
	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void createNewCustomQuestion_ShortAnswer(String questionName, String modelAnswer, String DefaultModelAnswer) throws Exception {
		
		int feedbackiFrame = 6;
		int feedbackDefaultiFrame = 7;
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);
		
		addNewQuestion.enterQuestionName(questionName);
		
		addNewQuestion.clickShortAnswerButton();
	
		addNewQuestion.shortAnswerOptions();
	
		addNewQuestion.clickFeedbackTab();
	
		addNewQuestion.enterFeedbackOptions_ShortAnswer(modelAnswer, DefaultModelAnswer, feedbackiFrame, feedbackDefaultiFrame);
	
		addNewQuestion.clickSaveQuestionButton();
	}
	

	@Step("Create New Custom Question in QuestionPlayer ,  Method: {method} ")
	public void addNewCustomQuestion(String questionName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		addNewQuestion.clickQuestionLibrary();
	
		wait.until(ExpectedConditions.elementToBeClickable(SearchQuestionTextbox));
		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(questionName);
		Thread.sleep(3000);
		Searchbutton.click();
		WebDriverWait wait1 = new WebDriverWait(driver, 50);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")));
		questionplusIcon.click();
		Thread.sleep(2000);
		
	}

	

	@Step("Add the Custom Question in QuestionPlayer ,  Method: {method} ")
	public void addCustomQuestions(String questionsSetName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		Thread.sleep(3000);
		JsonElement element = jsonobject.get(questionsSetName);
		JsonObject obj = element.getAsJsonObject();
		Set<String> questionsKeys = obj.keySet();
		Iterator<String> it = questionsKeys.iterator();
		int counter = 0;
		while (it.hasNext()) {
			String key = it.next();
			String value = obj.get(key).getAsString();

			if (counter == 0 && questionsSetName.compareTo(SW5Constants.QUESTION_SET_5) == 0) {
				value = getOpenResponseCustomQuestion(value);
			}

			SearchQuestionTextbox.clear();
			ReusableMethods.waitForPageLoad(driver);
			SearchQuestionTextbox.sendKeys(value);
			Thread.sleep(3000);
			Searchbutton.click();
			WebDriverWait wait1 = new WebDriverWait(driver, 100);
			wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(
					"//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")));
			questionplusIcon.click();
			Thread.sleep(1000);
			SearchQuestionTextbox.clear();
			Thread.sleep(1000);

			counter++;
		}
	}
	
	@Step("Add the Custom Question in QuestionLibrary ,  Method: {method} ")
	public void addCustomQuestionsWithQuestionID(String questionsSetName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loading-tbody out']")));
		Thread.sleep(3000);
		JsonElement element = jsonobject.get(questionsSetName);
		JsonObject obj = element.getAsJsonObject();
		Set<String> questionsKeys = obj.keySet();
		Iterator<String> it = questionsKeys.iterator();
		int counter = 0;
		while (it.hasNext()) {
			String key = it.next();
			String value = obj.get(key).getAsString();

			if (counter == 0 && questionsSetName.compareTo(SW5Constants.QUESTION_SET_5) == 0) {
				value = getOpenResponseCustomQuestion(value);
			}

			SearchQuestionTextbox.clear();
			ReusableMethods.waitForPageLoad(driver);
			SearchQuestionTextbox.sendKeys(value);
			Thread.sleep(3000);
			Searchbutton.click();
			WebDriverWait wait1 = new WebDriverWait(driver, 100);
			wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(
					"//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")));
			questionplusIcon.click();
			Thread.sleep(1000);
			SearchQuestionTextbox.clear();
			Thread.sleep(1000);

			counter++;
		}
	}
	
	
	@Step("Add the Custom Question in QuestionLibrary with Question ID ,  Method: {method} ")
	public void addQuestionsWithQuestionID(String questionsID) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(
				By.xpath("//div[@class='loading-tbody out']")));
		
		Thread.sleep(3000);

		SearchQuestionTextbox.clear();
		ReusableMethods.waitForPageLoad(driver);
		SearchQuestionTextbox.sendKeys(questionsID);

		Thread.sleep(3000);
		Searchbutton.click();

		WebDriverWait wait1 = new WebDriverWait(driver, 50);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//table/tbody[@class='questionLibraryLst']/tr/td[@class='text-center']/span[@class='glyphicons glyphicons-plus']")));
		questionplusIcon.click();

		Thread.sleep(1000);
		SearchQuestionTextbox.clear();
		Thread.sleep(1000);

	}
	
	
	@Step("Get the Custom Question in QuestionPlayer ,  Method: {method} ")
	public String getCustomQuestion(String questionText) throws Exception {
		String retVal = "";
		String guid = GetRandomId.randomAlphaNumeric(4).toLowerCase();
		guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
		retVal = questionText + "-" + SW5Constants.GUID_PREFIX_OPEN_RESPONSE_QUESTION + "-" + guid;

		clickCreateNewQuestion();

		addNewQuestion = new AddNewQuestionToQuestionLibrary(driver);

		addNewQuestion.enterQuestionTitle(retVal);

		addNewQuestion.selectChapterOption(1);// Chapter 1: Particles of Matter: Measurement and the Tools of Science

		addNewQuestion.selectSectionEbookReferenceOption("1.6: States of Matter");

		addNewQuestion.enterQuestionNumber(guid);

		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);

		addNewQuestion.clickQuestionLibrary();

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(SearchQuestionTextbox));
		SearchQuestionTextbox.click();

		return retVal;
	}

	@Step("Get the Open Response Type Custom Question in QuestionPlayer ,  Method: {method} ")
	public String getOpenResponseCustomQuestion(String questionText) throws Exception {
		String retVal = "";
		String guid = GetRandomId.randomAlphaNumeric(4).toLowerCase();
		guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
		retVal = questionText + "-" + SW5Constants.GUID_PREFIX_OPEN_RESPONSE_QUESTION + "-" + guid;

		clickCreateNewQuestion();

		addNewQuestion = new AddNewQuestionToQuestionLibrary(driver);

		addNewQuestion.enterQuestionTitle(retVal);

		addNewQuestion.selectChapterOption(1);// Chapter 1: Particles of Matter: Measurement and the Tools of Science

		addNewQuestion.selectSectionEbookReferenceOption("1.6: States of Matter");

		addNewQuestion.enterQuestionNumber(guid);

		ReusableMethods.scrollIntoView(driver, addNewQuestion.questionIntroCheckbox);

		addNewQuestion.clickOpenResponseButton();

		addNewQuestion.enterQuestionText(SW5Constants.OPEN_RESPONSE_QUESTION);

		addNewQuestion.clickModelAnswerTab();

		addNewQuestion.enterModelAnswer(SW5Constants.OPEN_RESPONSE_QUESTION_ANSWER);

		addNewQuestion.clickSaveOpenResponsePopup();

		addNewQuestion.clickSaveQuestionButton();

		addNewQuestion.clickQuestionLibrary();

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(SearchQuestionTextbox));
		SearchQuestionTextbox.click();

		return retVal;
	}

	@Step("Get the Assignment Count from Question Player Page ,  Method: {method} ")
	public String getQuestioncount() throws InterruptedException {
		String questionsCount = QuestionsCount.getText();
		LogUtil.log("Added Questions Count: " + questionsCount);
		return questionsCount;
	}

	@Step("Click the Your Current Assignment Button,  Method: {method} ")
	public void yourCurrentAssignment() throws InterruptedException {
		YourCurrentAssignmentButton.click();
		Thread.sleep(1000);
		GoToAssignmentSettingsButton.click();
		Thread.sleep(3000);
	}

	@Step("Assert the total points on assignment is adding up each question points on edit assignment page, Method: {method} ")
	public void getTotalPoints() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("publish")));
		String points = TotalPoints.getText();
		Assert.assertEquals(TotalPoints.getText(), points);

	}

	@Step("get the Custom Assignment Title, Method: {method} ")
	public String getAssignmentTitle() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("publish")));
		return AssignmentTitle.getText();
	}

	@Step("Click the CREATE NEW QUESTION Button,  Method: {method} ")
	public void clickCreateNewQuestion() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(createNewQuestion));
		createNewQuestion.click();
	}
	
	
	@Step("Check Maximum Large Pool Questions Exception is displayed,  Method: {method} ")
	public boolean isMaximumLargePoolQuestionsExceptionDisplayed() throws Exception {
		
		boolean maximumExceptionExist = false;
		
		try {	
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//div[@class='head-bar modal-header']/h4")));
				
				WebElement	maximumException_part1 = driver.findElement(By.xpath
						("//div[@class='modal-body']/div/div/span[contains(text(), 'Adding')]"));
				
				maximumExceptionExist = maximumException_part1.isDisplayed();
				
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
				
				return maximumExceptionExist;
			
		} catch (Exception e) {
				return maximumExceptionExist;
			}
	}
}
