package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class ManageStudentSetsPage {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//div[@id='add_new_student_set']/span[contains(text(),'Create New Student Set')]")
	public WebElement Create_New_Student_Set_button;

	@FindBy(how = How.XPATH, using = "//div[@id='student_set_data_table_filter']/label/input[@type='search']")
	public WebElement Search_Text;

	@FindBy(how = How.XPATH, using = "//a[@role='button']/span[contains(text(),'Update')]")
	public WebElement Updatebutton;

	@FindBy(how = How.XPATH, using = "//a[@role='button']/span[contains(text(),'x')]")
	public WebElement Deletebutton;

	@FindBy(how = How.CSS, using = ".link-close")
	public WebElement closelink;
	
	@FindBy(how = How.XPATH, using = "//a[@id='btnClose']/span")
	public WebElement closelink2;
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[@class='ui-button-text'][contains(text(),'Continue')]")
	public WebElement continueButton;

	@FindBy(how = How.XPATH, using = "//div[@id='general_dialog_outer_2']/a[1]")
	public WebElement closelink1;

	public ManageStudentSetsPage() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Search the Student ID and Click the Update Button,  Method: {method} ")
	public void searchStudentID() {
		Search_Text.click();
		Updatebutton.click();
	}

	@Step("Click the Close link (X) from Manage Student set Page,  Method: {method} ")
	public void clickcloselink() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("$('a[class^=\"link-close\"]').click()");
		

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loading_message")));
	}
	
	@Step("Click the continue Button from Exit before Save popup,  Method: {method} ")
	public void clickContinueButton() throws InterruptedException {
		continueButton.click();
	}

}