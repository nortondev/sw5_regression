package com.wwnorton.SW5Automation.objectFactory;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.seleniumtests.uipage.htmlelements.FrameElement;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH9HW {
	
	WebDriver driver;
	
	
	// Finding Web Elements on SW5 DLP page using PageFactory.

		@FindBy(how = How.XPATH, using = "//div[@class='mjs-comboButtonBackground']/div[contains(@class,'gwt-ToggleButton')]/img")
		public List<WebElement> imageButtons;
		
		@FindBy(how = How.XPATH, using = "//div[@class='mjs-comboButtonBackground']/div[contains(@class,'gwt-ToggleButton') and @title='Single [1]']/img")
		public WebElement singleLineButton;
			
		@FindBy(how = How.XPATH, using = "//div[@class='mjs-buttonContainer-inline']/div[contains(@class,'gwt-ToggleButton') and @title='Single [1]']/img")
		public WebElement singleLineButton_Panel;
		
		@FindBy(how = How.XPATH, using = "//div[@class='mjs-buttonContainer-inline']/div[contains(@class,'gwt-ToggleButton') and @title='Double [2]']/img")
		public WebElement doubleLineButton_Panel;
		
		@FindBy(how = How.XPATH, using = "//div[@class='html-face']/span[text()='H']")
		public WebElement HButton;
		
		@FindBy(how = How.XPATH, using = "//div[@class='html-face']/span[text()='C']")
		public WebElement CButton;
		
		@FindBy(how = How.XPATH, using = "//div[@class='html-face']/span[text()='O']")
		public WebElement OButton;
		
		@FindBy(how = How.XPATH, using = "//div[@class='mjs-comboButtonBackground']/div[contains(@class,'gwt-ToggleButton') and @title='Increase lone pair']/img")
		public WebElement lonePairButton;
		
		@FindBy(how = How.XPATH, using = "//canvas[@id='canvas']")
		public WebElement canvasEditor;
		
		
	
	public AssignmentCH9HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	@Step("Submit Answer for Question 01,  Method: {method} ")
	public void submitAnswer_Question01() throws InterruptedException, AWTException {
		Actions act = new Actions(driver);
		List<WebElement> iFrames = driver.findElements(By.xpath("//iframe[contains(@id,'wafer_ps_chemi_')]"));
		WebElement frame1 = iFrames.get(0);
			//Point frame1Location = frame1.getLocation();
		driver.switchTo().frame(frame1);
	    
		ReusableMethods.scrollToElement(driver, By.xpath("//div[@id='sketch']"));
		Thread.sleep(2000);
		
		//Point locationOfLineButton = frame1Location.moveBy(singleLineButton.getLocation().x, 
		//		singleLineButton.getLocation().y);
		
		//ReusableMethods.scrollIntoView(driver, imageButtons.get(2));
		//Point locationOfLineButton = imageButtons.get(2).getLocation();
		//System.out.println("X coordinate is: "+singleLineButton.getLocation().getX());
		//System.out.println("Y coordinate is: "+singleLineButton.getLocation().getY());
		
		//Robot robot = new Robot(); 
		//robot.mouseMove(locationOfLineButton.x , locationOfLineButton.y);
		
		//act.moveToElement(imageButtons.get(2), locationOfLineButton.x, locationOfLineButton.y).
		//		click().build().perform();
		
		
		Point locationOfLineButton = singleLineButton.getLocation();
		//act.moveToElement(singleLineButton, locationOfLineButton.x, locationOfLineButton.y).
				//		click().build().perform();
		
		//singleLineButton.click();
		
		act.moveToElement(singleLineButton, 3, 14).click().build().perform();
		
		Thread.sleep(2000);
		act.moveToElement(doubleLineButton_Panel).click().build().perform();
		
		Thread.sleep(2000);
		act.moveToElement(canvasEditor).click().build().perform();
		
		//Thread.sleep(2000);
		//ReusableMethods.moveToElement(driver, doubleLineButton_Panel);
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", doubleLineButton_Panel);
		
		Point canvasEditorLocation = canvasEditor.getLocation(); 
		//Thread.sleep(2000);
		//act.moveToElement(canvasEditor, canvasEditorLocation.x, canvasEditorLocation.y).click().perform();
		//act.moveToElement(canvasEditor, 453, 274).click().perform();
		
		Thread.sleep(2000);
		act.moveToElement(canvasEditor, 4, 5).click().perform();

		Thread.sleep(2000);
		act.moveToElement(HButton).click().perform();
		
		Thread.sleep(2000);
		act.moveToElement(canvasEditor, 11, 12).click().perform();
		
		//driver.switchTo().defaultContent();
	
		
	}
	
	
	@Step("Submit Answer Part 1 for Question 01,  Method: {method} ")
	public void submitCorrectAnswer_Question01_Part1() throws InterruptedException, NoSuchFrameException {
		Actions act = new Actions(driver);
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//List<WebElement> iFrames = driver.findElements(
		//		By.xpath("//iframe[contains(@class,'sketcher-frame StudentPadParent')]"));
		
			WebElement iFrame1 = driver.findElement(
						By.xpath("//iframe[contains(@id,'wafer_ps_chemi_div9603428625')]"));

			driver.switchTo().frame(iFrame1);
	    
			ReusableMethods.scrollToElement(driver, By.xpath("//div[@id='sketch']"));
		
			Thread.sleep(3000);
			String os = System.getProperty("os.name");
			if (os.equals("Mac OS X")){
			//js.executeScript("arguments[0].click();", canvasEditor);
			//canvasEditor.sendKeys(Keys.chord(Keys.COMMAND, "o"));
			//act.sendKeys(canvasEditor, Keys.chord(Keys.COMMAND, "o")).build().perform();
			//act.click(canvasEditor).sendKeys(Keys.chord(Keys.COMMAND, "o")).build().perform();
			
			Action select= act.click(canvasEditor).keyDown(Keys.COMMAND)
		            .sendKeys("o")
		            .keyUp(Keys.COMMAND)
		            .build();
					select.perform();
			
			}else {
					Action select= act.click(canvasEditor).keyDown(Keys.CONTROL)
		            .sendKeys("o")
		            .keyUp(Keys.CONTROL)
		            .build();
					select.perform();
			}

			Thread.sleep(2000);
			WebElement quesString = driver.findElement(By.xpath("//textarea[@class='gwt-TextArea']"));
			quesString.sendKeys("<cml><MDocument><MChemicalStruct><molecule molID='m1'><atomArray><atom id='a1' elementType='C' x2='-5.916666666666667' y2='1.3125'/><atom id='a2' elementType='C' x2='-4.582987544838631' y2='0.5425000000000001'/><atom id='a3' elementType='C' x2='-3.2493084230105955' y2='1.3125'/><atom id='a4' elementType='O' x2='-3.2493084230105955' y2='2.8525' lonePair='2'/><atom id='a5' elementType='H' x2='-1.9156293011825598' y2='0.5425000000000001'/><atom id='a6' elementType='H' x2='-4.582987544838631' y2='-0.9974999999999999'/><atom id='a7' elementType='H' x2='-6.077640500098853' y2='2.844063718867141'/><atom id='a8' elementType='H' x2='-7.448230385533808' y2='1.1515261665678134'/></atomArray><bondArray><bond atomRefs2='a1 a2' order='2' id='b1'/><bond atomRefs2='a2 a3' order='1' id='b2'/><bond atomRefs2='a3 a5' order='1' id='b3'/><bond atomRefs2='a2 a6' order='1' id='b4'/><bond atomRefs2='a1 a7' order='1' id='b5'/><bond atomRefs2='a1 a8' order='1' id='b6'/><bond atomRefs2='a3 a4' order='2' id='b7'/></bondArray></molecule></MChemicalStruct><MElectronContainer occupation='0 0' radical='0' id='o1'><MElectron atomRefs='m1.a4' difLoc='0.0 0.0 0.0'/><MElectron atomRefs='m1.a4' difLoc='0.0 0.0 0.0'/></MElectronContainer><MElectronContainer occupation='0 0' radical='0' id='o2'><MElectron atomRefs='m1.a4' difLoc='0.0 0.0 0.0'/><MElectron atomRefs='m1.a4' difLoc='0.0 0.0 0.0'/></MElectronContainer></MDocument></cml>");
			Thread.sleep(2000);
			WebElement replaceButton = driver.findElement(By.xpath("//button[text()='Replace']"));
			replaceButton.click();
		
			//driver.switchTo().frame(0);
			driver.switchTo().parentFrame();

	}
	
	@Step("Submit Answer Part 2 for Question 01,  Method: {method} ")
	public void submitCorrectAnswer_Question01_Part2() throws InterruptedException, NoSuchFrameException {
		Actions act = new Actions(driver);
		
	//Submit second Molecular Drawing Question.
			ReusableMethods.scrollToElement(driver, 
				By.xpath("//div[@id='wafer_ps_chemi_div9863531522bdeaa6bb7f086003f078f9de2221d7c107107__attempt1-mol2DDiv']"));
		
			WebElement iFrame2 = driver.findElement(
				By.xpath("//iframe[contains(@id,'wafer_ps_chemi_div9863531522')]"));

			driver.switchTo().frame(iFrame2);
		
			Thread.sleep(3000);
			String os = System.getProperty("os.name");
			if (os.equals("Mac OS X")){
			Action select= act.click(canvasEditor).keyDown(Keys.COMMAND)
		            .sendKeys("o")
		            .keyUp(Keys.COMMAND)
		            .build();
					select.perform();
			
			}else {
					Action select= act.click(canvasEditor).keyDown(Keys.CONTROL)
		            .sendKeys("o")
		            .keyUp(Keys.CONTROL)
		            .build();
					select.perform();
			}

			Thread.sleep(2000);
			WebElement quesString = driver.findElement(By.xpath("//textarea[@class='gwt-TextArea']"));
			quesString.sendKeys("<cml><MDocument><MChemicalStruct><molecule molID='m1'><atomArray><atom id='a1' elementType='C' x2='-8.125' y2='2.625'/><atom id='a2' elementType='S' x2='-6.585' y2='2.625' lonePair='2'/><atom id='a3' elementType='H' x2='-9.665' y2='2.625'/><atom id='a4' elementType='H' x2='-8.125' y2='4.165'/><atom id='a5' elementType='H' x2='-8.125' y2='1.085'/><atom id='a6' elementType='H' x2='-5.045' y2='2.625'/></atomArray><bondArray><bond atomRefs2='a1 a2' order='1' id='b1'/><bond atomRefs2='a2 a6' order='1' id='b2'/><bond atomRefs2='a1 a4' order='1' id='b3'/><bond atomRefs2='a1 a3' order='1' id='b4'/><bond atomRefs2='a1 a5' order='1' id='b5'/></bondArray></molecule></MChemicalStruct><MElectronContainer occupation='0 0' radical='0' id='o1'><MElectron atomRefs='m1.a2' difLoc='0.0 0.0 0.0'/><MElectron atomRefs='m1.a2' difLoc='0.0 0.0 0.0'/></MElectronContainer><MElectronContainer occupation='0 0' radical='0' id='o2'><MElectron atomRefs='m1.a2' difLoc='0.0 0.0 0.0'/><MElectron atomRefs='m1.a2' difLoc='0.0 0.0 0.0'/></MElectronContainer></MDocument></cml>");
			Thread.sleep(2000);
			WebElement replaceButton = driver.findElement(By.xpath("//button[text()='Replace']"));
			replaceButton.click();
		
			driver.switchTo().parentFrame();
			//driver.switchTo().defaultContent();
	}
}



