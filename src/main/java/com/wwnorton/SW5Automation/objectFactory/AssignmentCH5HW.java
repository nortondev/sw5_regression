package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH5HW {
	
	WebDriver driver;
	
	public AssignmentCH5HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	@Step("Submit the Answer for CH5HW Assignment,  Method: {method} ")
	public void submitAnswerCH5HWAssignment() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@id='answerKeyDiv0315555549c72f57d0ab305d01cd629db7fee5b68605386_']/input"))
				.sendKeys("-1234.7");
	}


}
