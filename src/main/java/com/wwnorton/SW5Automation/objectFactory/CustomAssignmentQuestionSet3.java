package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CustomAssignmentQuestionSet3 {
	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279202403_')]/span")
	public WebElement Q1H2OPart1Listbox;

	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279209271_')]/span")
	public WebElement Q1H2OPart2Listbox;

	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279207782_')]/span")
	public WebElement Q1H2OPart3Listbox;

	@FindBy(how = How.ID, using = "wafer_ps_select9129382476bdeaa6bb7f086003f078f9de2221d7c106789_")
	public WebElement Q3Part1ListBox;

	@FindBy(how = How.ID, using = "wafer_ps_select9129884331bdeaa6bb7f086003f078f9de2221d7c108827_")
	public WebElement Q3Part2ListBox;

	public CustomAssignmentQuestionSet3() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("select the Question 1 Incorrect answers from the list box,   Method: {method}")
	public void question1IncorrectAnswers() throws Exception {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(Q1H2OPart1Listbox));
		Q1H2OPart1Listbox.click();
		WebElement listbox = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div981976075986a110cd3be354d036b9df8df736279202403_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options) {
			if (option.getText().equalsIgnoreCase("vaporization") == false) {
				option.click(); // click the desired option
				break;
			}
		}

		ReusableMethods.scrollIntoView(driver, Q1H2OPart2Listbox);

		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until(ExpectedConditions.elementToBeClickable(Q1H2OPart2Listbox));
		Q1H2OPart2Listbox.click();
		WebElement listbox2 = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div981986636086a110cd3be354d036b9df8df736279209271_']/ul"));
		List<WebElement> options2 = listbox2.findElements(By.tagName("li"));
		for (WebElement option2 : options2) {
			if (option2.getText().equalsIgnoreCase("deposition") == false) {
				option2.click(); // click the desired option
				break;
			}
		}
		WebDriverWait wait2 = new WebDriverWait(driver, 10);
		wait2.until(ExpectedConditions.elementToBeClickable(Q1H2OPart3Listbox));
		ReusableMethods.scrollIntoView(driver, Q1H2OPart3Listbox);
		Q1H2OPart3Listbox.click();
		WebElement listbox3 = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div981996772586a110cd3be354d036b9df8df736279207782_']/ul"));
		List<WebElement> options3 = listbox3.findElements(By.tagName("li"));
		for (WebElement option3 : options3) {
			if (option3.getText().equalsIgnoreCase("melting") == false) {
				option3.click(); // click the desired option
				break;
			}
		}
	}

	@Step("select the Question 1 answer from the list box,   Method: {method}")
	public void question1Answers() throws Exception {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(Q1H2OPart1Listbox));
		Q1H2OPart1Listbox.click();
		WebElement listbox = driver.findElement(
				By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279202403_')]/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options) {
			if (option.getText().equalsIgnoreCase("vaporization")) {
				option.click(); // click the desired option
				break;
			}
		}
		ReusableMethods.scrollIntoView(driver, Q1H2OPart2Listbox);

		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until(ExpectedConditions.elementToBeClickable(Q1H2OPart2Listbox));
		Q1H2OPart2Listbox.click();
		WebElement listbox2 = driver.findElement(
				By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279209271_')]/ul"));
		List<WebElement> options2 = listbox2.findElements(By.tagName("li"));
		for (WebElement option2 : options2) {
			if (option2.getText().equalsIgnoreCase("deposition")) {
				option2.click(); // click the desired option
				break;
			}
		}
		WebDriverWait wait2 = new WebDriverWait(driver, 10);
		wait2.until(ExpectedConditions.elementToBeClickable(Q1H2OPart3Listbox));
		ReusableMethods.scrollIntoView(driver, Q1H2OPart3Listbox);
		Q1H2OPart3Listbox.click();
		WebElement listbox3 = driver.findElement(
				By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279207782_')]/ul"));
		List<WebElement> options3 = listbox3.findElements(By.tagName("li"));
		for (WebElement option3 : options3) {
			if (option3.getText().equalsIgnoreCase("melting")) {
				option3.click(); // click the desired option
				break;
			}
		}
	}

	@Step("select the Question 2 Incorrect answer,   Method: {method}")
	public void question2IncorrectAnswers() throws Exception {
		Thread.sleep(10000);

		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']")));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_checkbox_div1632445399bdeaa6bb7f086003f078f9de2221d7c100693_']]")));

		// Uncheck all checked checkboxes if any
		List<WebElement> part1CheckedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div1632445399bdeaa6bb7f086003f078f9de2221d7c100693_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : part1CheckedCheckBoxes) {
			checkedCheckBox.click();
		}

		WebElement answer1 = driver.findElement(By.xpath(
				"//span[contains(text(),'Size increases as we move left to right across a period.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer1.click();

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_checkbox_div1632591108bdeaa6bb7f086003f078f9de2221d7c106971_']]")));

		// Uncheck all checked checkboxes if any
		List<WebElement> part2CheckedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div1632591108bdeaa6bb7f086003f078f9de2221d7c106971_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : part2CheckedCheckBoxes) {
			ReusableMethods.scrollIntoView(driver, checkedCheckBox);
			checkedCheckBox.click();
		}

		WebElement answer3 = driver.findElement(By.xpath(
				"//span[contains(text(),'Anions are always smaller than the atoms from which they form.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer3.click();
	}

	@Step("select the Question 2 answer,   Method: {method}")
	public void question2Answers() throws Exception {
		Thread.sleep(5000);

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']")));

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_checkbox_div1632445399bdeaa6bb7f086003f078f9de2221d7c100693_']]")));

		// Uncheck all checked checkboxes if any
		List<WebElement> part1CheckedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div1632445399bdeaa6bb7f086003f078f9de2221d7c100693_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : part1CheckedCheckBoxes) {
			checkedCheckBox.click();
		}

		WebElement answer1 = driver.findElement(By.xpath(
				"//span[contains(text(),'Size increases as we move from top to bottom down a group.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer1.click();

		WebElement answer2 = driver.findElement(By.xpath(
				"//span[contains(text(),'Size decreases as we move left to right across a period.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer2.click();

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_checkbox_div1632591108bdeaa6bb7f086003f078f9de2221d7c106971_']]")));

		// Uncheck all checked checkboxes if any
		List<WebElement> part2CheckedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div1632591108bdeaa6bb7f086003f078f9de2221d7c106971_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : part2CheckedCheckBoxes) {
			checkedCheckBox.click();
		}

		WebElement answer3 = driver.findElement(By.xpath(
				"//span[contains(text(),'Anions are always larger than the atoms from which they form.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer3.click();

		WebElement answer4 = driver.findElement(By.xpath(
				"//span[contains(text(),'Cations are always smaller than the atom from which they form.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer4.click();
	}

	@Step("select the Question 3 Incorrect answer,   Method: {method}")
	public void question3IncorrectAnswers() throws Exception {
		Thread.sleep(5000);

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(Q3Part1ListBox));
		ReusableMethods.scrollIntoView(driver, Q3Part1ListBox);
		Q3Part1ListBox.click();
		WebElement listbox = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div9129382476bdeaa6bb7f086003f078f9de2221d7c106789_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options) {
			if (option.getText().equalsIgnoreCase("Condition A") == false) {
				option.click(); // click the wrong option
				break;
			}
		}
		ReusableMethods.scrollIntoView(driver, Q3Part2ListBox);

		Q3Part2ListBox.click();
		WebElement listbox1 = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div9129884331bdeaa6bb7f086003f078f9de2221d7c108827_']/ul"));
		List<WebElement> options1 = listbox1.findElements(By.tagName("li"));
		for (WebElement option : options1) {
			if (option.getText().equalsIgnoreCase("Condition B") == false) {
				option.click(); // click the wrong option
				break;
			}
		}
	}

	@Step("select the Question 3 answer,   Method: {method}")
	public void question3Answers() throws Exception {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(Q3Part1ListBox));
		ReusableMethods.scrollIntoView(driver, Q3Part1ListBox);
		Q3Part1ListBox.click();
		WebElement listbox = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div9129382476bdeaa6bb7f086003f078f9de2221d7c106789_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options) {
			if (option.getText().equalsIgnoreCase("Condition A")) {
				option.click(); // click the desired option
				break;
			}
		}

		ReusableMethods.scrollIntoView(driver, Q3Part2ListBox);
		Q3Part2ListBox.click();
		WebElement listbox1 = driver.findElement(
				By.xpath("//div[@id='wafer_ps_select_option_div9129884331bdeaa6bb7f086003f078f9de2221d7c108827_']/ul"));
		List<WebElement> options1 = listbox1.findElements(By.tagName("li"));
		for (WebElement option : options1) {
			if (option.getText().equalsIgnoreCase("Condition B")) {
				option.click(); // click the desired option
				break;
			}
		}
	}

	@Step("select the Question 4 Incorrect answer,   Method: {method}")
	public void question4IncorrectAnswers() throws Exception {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement answer7checkbox1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[contains(text(),'Ketchup')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']")));

		// Uncheck all checked checkboxes if any
		List<WebElement> checkedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div2621773397f17398bec3a73cf1a643545477bb924904675_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : checkedCheckBoxes) {
			checkedCheckBox.click();
		}

		answer7checkbox1.click();
	}

	@Step("select the Question 4 answer,   Method: {method}")
	public void question4Answers() throws Exception {
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement answer7checkbox1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[contains(text(),'Ready-to-eat Jell-O')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']")));

		WebElement answer7checkbox2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[contains(text(),'Sea salt')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']")));

		// Uncheck all checked checkboxes if any
		List<WebElement> checkedCheckBoxes = driver.findElements(By.xpath(
				"//span[@id='wafer_ps_checkbox_div2621773397f17398bec3a73cf1a643545477bb924904675_']//input[@type='checkbox' and @checked='checked']"));

		for (WebElement checkedCheckBox : checkedCheckBoxes) {
			checkedCheckBox.click();
		}

		answer7checkbox1.click();

		answer7checkbox2.click();
	}

	/*@Step("select the Question 5 Incorrect answer,   Method: {method}")
	public void question5IncorrectAnswers() throws Exception {
		Thread.sleep(5000);

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_radio_div495062447836e609193adb47540a6a417fab6b10c401551_']]")));

		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement answer9Radiobox1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[@id='wafer_ps_radio_div495062447836e609193adb47540a6a417fab6b10c401551_']//span[contains(text(),'Mixture of elements')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']")));
		answer9Radiobox1.click();
		Thread.sleep(2000);

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_radio_div495066529736e609193adb47540a6a417fab6b10c402845_']]")));

		WebElement answer9Radiobox2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[@id='wafer_ps_radio_div495066529736e609193adb47540a6a417fab6b10c402845_']//span[contains(text(),'Pure element')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']")));
		answer9Radiobox2.click();
		Thread.sleep(2000);
	}

	@Step("select the Question 5 answer,   Method: {method}")
	public void question5Answers() throws Exception {
		Thread.sleep(5000);

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_radio_div495062447836e609193adb47540a6a417fab6b10c401551_']]")));

		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement answer9Radiobox1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[@id='wafer_ps_radio_div495062447836e609193adb47540a6a417fab6b10c401551_']//span[contains(text(),'Pure element')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']")));
		answer9Radiobox1.click();
		Thread.sleep(2000);

		ReusableMethods.scrollIntoView(driver, driver.findElement(By.xpath(
				"//div[@class='question'][div/span[@id='wafer_ps_radio_div495066529736e609193adb47540a6a417fab6b10c402845_']]")));

		WebElement answer9Radiobox2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[@id='wafer_ps_radio_div495066529736e609193adb47540a6a417fab6b10c402845_']//span[contains(text(),'Mixture of elements')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']")));
		answer9Radiobox2.click();
		Thread.sleep(2000);
	} */
	
	@Step("select the Question 5 answer,   Method: {method}")
	public void question5IncorrectAnswers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input")));
		Actions act = new Actions(driver);
		WebElement elementPart1=driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495062447836e')]"));
	    ReusableMethods.scrollIntoView(driver, elementPart1);
		WebElement answer1 = driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495062447836e')]/span/label/span/input[@value='4']"));
		act.moveToElement(answer1);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer1);
		//answer1.click();
		WebElement elementPart2=driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495066529736e')]"));
	    ReusableMethods.scrollIntoView(driver, elementPart2);
		WebElement answer2 = driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495066529736e')]/span/label/span/input[@value='2']"));
		act.moveToElement(answer2);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer2);
		//answer2.click();
		
	}
	
	@Step("select the Question 5 answer,   Method: {method}")
	public void question5Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input")));
		Actions act = new Actions(driver);
		WebElement elementPart1=driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495062447836e')]"));
	    ReusableMethods.scrollIntoView(driver, elementPart1);
		WebElement answer1 = driver.findElement(By.xpath("//span[contains(@id,'wafer_ps_radio_div495062447836e')]/span/label/span/input[@value='1']"));
		act.moveToElement(answer1);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer1);
		//answer1.click();
		WebElement elementPart2=driver.findElement(
				By.xpath("//span[contains(@id,'wafer_ps_radio_div495066529736e')]"));
	    ReusableMethods.scrollIntoView(driver, elementPart2);
		WebElement answer2 = driver.findElement(By.xpath("//span[contains(@id,'wafer_ps_radio_div495066529736e')]/span/label/span/input[@value='3']"));
		act.moveToElement(answer2);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", answer2);
		//answer2.click();
		
	}
}
