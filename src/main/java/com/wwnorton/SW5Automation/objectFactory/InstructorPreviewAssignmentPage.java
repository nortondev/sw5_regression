package com.wwnorton.SW5Automation.objectFactory;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class InstructorPreviewAssignmentPage {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Keep Using Trial Access'")
	public WebElement KeepUsingTrialAccess;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash-2']/div[@class='head-score']/div[@class='head-big-txt']")
	public WebElement HeaderScore;
	
	@FindBy(how = How.XPATH, using = "//span[@class='exam-mode-text']")
	public WebElement examText;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash']//div[@class='head-timeleft head-dash-list']/span[@class='glyphicons glyphicons-stopwatch']")
	public WebElement TimerIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='chapter-head-dash']//div[@class='head-timeleft head-dash-list']/div[@class='head-big-txt']")
	public WebElement TimerText;

	@FindBy(how = How.XPATH, using = "//div[@class='container-box assignment-info-box']/div[@class='assignment-info-box-r']")
	public WebElement AssignmentInfoBox;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'BEGIN ASSIGNMENT')]")
	public WebElement BeginAssignment;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'RESUME ASSIGNMENT')]")
	public WebElement ResumeAssignment;

	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'REVIEW ASSIGNMENT')]")
	public WebElement ReviewAssignment;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-btn']/a/span[contains(text(),'BEGIN EXAM')]")
	public WebElement BeginExam;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn-normal btn-submit-s btn-primary ml15 lato-regular-14 fr']/span[contains(text(),'CLASS ACTIVITY REPORT')]")
	public WebElement CARButton;
	

	@FindBy(how = How.XPATH, using = "//table[@id='realQuestionTable']/tbody")
	public WebElement QuestionTable;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'Yes, I am ready to begin')]]")
	public WebElement TimeLimitConfirmYesButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='modal-footer']/button[span[contains(text(),'No, I am not ready to begin')]]")
	public WebElement TimeLimitConfirmNoButton;

	@FindBy(how = How.XPATH, using = "//div[@class='modal-content'][div/div[contains(text(),'You have reached the deadline of this activity!')]]/div/button[@class='close']")
	public WebElement CloseDeadlineReached;
	
	@FindBy(how = How.XPATH, using = "//div[@class='head-box']/div[@class='head-preview-logo fr']")
	public WebElement PreviewIcon;
	

	List<WebElement> AllQuestions;

	// Initializing Web Driver and PageFactory.
	public InstructorPreviewAssignmentPage() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		WebDriverWait wait;
		try {
			wait = new WebDriverWait(driver, 300);
			wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(BeginAssignment),
					ExpectedConditions.elementToBeClickable(ResumeAssignment),
					ExpectedConditions.elementToBeClickable(ReviewAssignment)));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(HeaderScore));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click KeepUsingTrialAccess on student assignment page,  Method: {method} ")
	public void keepUsingTrialAccess() throws Exception {
		try {
			// WebDriverWait wait = new WebDriverWait(driver, 30);
			// wait.until(ExpectedConditions.elementToBeClickable(KeepUsingTrialAccess));
			waitForElementPresent(
					By.xpath("//button[@type='button']/span[contains(text(),'Keep Using Trial Access')]"));
			driver.findElement(By.xpath("//button[@type='button']/span[contains(text(),'Keep Using Trial Access')]"))
					.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public String getAssignmentInfoBox() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(AssignmentInfoBox));

		return AssignmentInfoBox.getAttribute("title");
	}

	@Step("Check BEGIN ASSIGNMENT button displayed,  Method: {method} ")
	public Boolean isBeginAssignmentButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(BeginAssignment));
			return BeginAssignment.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on BEGIN ASSIGNMENT button,  Method: {method} ")
	public void beginAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(BeginAssignment));
			BeginAssignment.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check RESUME ASSIGNMENT button displayed,  Method: {method} ")
	public boolean isResumeAssignmentDisplayed() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeAssignment));
			return ResumeAssignment.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}

	@Step("Click on RESUME ASSIGNMENT button,  Method: {method} ")
	public void resumeAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ResumeAssignment));
			ResumeAssignment.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check REVIEW ASSIGNMENT button displayed,  Method: {method} ")
	public boolean isReviewAssignmentButtonDisplayed() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ReviewAssignment));
			return ReviewAssignment.isDisplayed();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}

	@Step("Click on REVIEW ASSIGNMENT button,  Method: {method} ")
	public void reviewAssignmentClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ReviewAssignment));
			ReviewAssignment.click();
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	@Step("Check BEGIN EXAM button displayed,  Method: {method} ")
	public Boolean isBeginExamButtonDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(BeginExam));
			return BeginExam.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Click on BEGIN EXAM button,  Method: {method} ")
	public void beginExamClick() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(BeginExam));
			BeginExam.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	@Step("Click on CAR button,  Method: {method} ")
	public void clickCARButton() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(CARButton));
			CARButton.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Check Confirm overlay displayed with 'No, I am not ready to begin' & 'Yes, I am ready to begin' buttons,  Method: {method} ")
	public boolean isConfirmOverlayWithYesNoToBeginAssignmentDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.elementToBeClickable(TimeLimitConfirmYesButton));
			wait.until(ExpectedConditions.elementToBeClickable(TimeLimitConfirmNoButton));
			return TimeLimitConfirmYesButton.isDisplayed() && TimeLimitConfirmNoButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@Step("Click on 'No, I am not ready to begin' button,  Method: {method} ")
	public void clickNoIamNotReadyToBeginButton() throws Exception {
		try {
			TimeLimitConfirmNoButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on 'Yes, I am ready to begin' button,  Method: {method} ")
	public void clickYesIamReadyToBeginButton() throws Exception {
		try {
			TimeLimitConfirmYesButton.click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public boolean isTimerIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(TimerIcon));
			return TimerIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getTimerText() throws Exception {
		return TimerText.getText();
	}
	
	@Step("Get all Question titles in Instructor Assignment page,  Method: {method} ")
	public List<String> getAllQuestionList() {

		List<String> questionList = new ArrayList<String>();
		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 1; i < AllQuestions.size(); i++) {

			WebElement questionTitle = QuestionTable.findElement(
					By.xpath("//tr["+ i + "]/td[1]/button/span[@class='question-text']"));
			
			questionList.add(i-1, questionTitle.getText());
			
		}
		
		return questionList;
		
	}

	public boolean verifyInitialAllQuestionPoints() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionPoint = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".2") + "'" + "]"));

			if (questionPoint.getText().indexOf("- /") == -1) {
				return false;
			}
		}
		return true;
	}

	public boolean verifyInitialAllQuestionAttempts() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionAttempt = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".3") + "'" + "]"));

			if (questionAttempt.getText().indexOf("- / ∞") == -1) {
				return false;
			}
		}
		return true;
	}

	public boolean verifyInitialAllQuestionStatus() {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		for (int i = 0; i < AllQuestions.size(); i++) {

			String questionReactId = AllQuestions.get(i).getAttribute("data-reactid");

			WebElement questionStatus = AllQuestions.get(i)
					.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".4") + "'" + "]/span"));

			if (!questionStatus.getText().equals("Not Started")) {
				return false;
			}
		}
		return true;
	}

	public String getQuestionPoints(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionPoint = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".2") + "'" + "]"));

		return questionPoint.getText();
	}

	public String getQuestionAttempts(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionAttempt = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".3") + "'" + "]"));

		return questionAttempt.getText();
	}

	public String getQuestionStatus(int questionNumber) {

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement questionStatus = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".4") + "'" + "]/span"));

		return questionStatus.getText();
	}

	public void clickQuestion(int questionNumber) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(QuestionTable));

		AllQuestions = QuestionTable.findElements(By.tagName("tr"));

		String questionReactId = AllQuestions.get(questionNumber - 1).getAttribute("data-reactid");

		WebElement question = AllQuestions.get(questionNumber - 1)
				.findElement(By.xpath("//td[@data-reactid=" + "'" + (questionReactId + ".0") + "'" + "]/button"));

		question.click();

		// Wait For Load Page Elements of StudentQuestionPlayer
		new StudentQuestionPlayer();
	}

	@Step("Click on Close Deadline Reached popup,  Method: {method} ")
	public void clickDeadlineReachedAlertClose() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3600);
			wait.until(ExpectedConditions.elementToBeClickable(CloseDeadlineReached));
			CloseDeadlineReached.click();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public String getHeaderScore() throws Exception {
		return HeaderScore.getText().replace('\n', ' ');
	}
	
	public String getExamText() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(examText));
		return examText.getText();
	}
	
	public boolean isPreviewIconDisplayed() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(PreviewIcon));
			return PreviewIcon.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
}
