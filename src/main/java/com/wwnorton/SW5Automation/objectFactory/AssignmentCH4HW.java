package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH4HW {

	WebDriver driver;
	Actions act;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")
	public WebElement QuestionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-b']")
	public WebElement QuestionTextCurrent;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-s']")
	public WebElement QuestionTextTotal;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH4HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	@Step("Enter In-Correct Answer for question #1,  Method: {method} ")
	public void question1EnterInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293232682486a110cd3be354d036b9df8df736279207296_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293390274586a110cd3be354d036b9df8df736279204728_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293458835686a110cd3be354d036b9df8df736279208586_")));

		// Enter InCorrect Answer
		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys("10");
		Q1Part2Element.sendKeys("20");
		Q1Part3Element.sendKeys("30");
	}
	
	@Step("Drag And Drop InCorrect Answer for question #1,  Method: {method} ")
	public void question1DragAndDropInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_"));
		
		// Element which needs to drag.
		WebElement dragItem1Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement dragItem2Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));

		WebElement dragItem3Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement dragItem4Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		

		// Element on which need to drop.
		
		WebElement StrongElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement WeakElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement NonElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-3-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part1);
		act.dragAndDrop(dragItem1Element_Part1, StrongElectrolyte).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part1);
		act.dragAndDrop(dragItem2Element_Part1, WeakElectrolyte).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part1);
		act.dragAndDrop(dragItem3Element_Part1, NonElectrolyte).build().perform();
		
		
		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_"));
		
		// Element which needs to drag.
		WebElement dragItem1Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement dragItem2Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));

		WebElement dragItem3Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement dragItem4Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		

		// Element on which need to drop.
		
		WebElement StrongAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-1-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement WeakAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-2-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement NonAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-3-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part2);
		act.dragAndDrop(dragItem1Element_Part2, StrongAcid).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem2Element_Part2);
		act.dragAndDrop(dragItem2Element_Part2, WeakAcid).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem3Element_Part2);
		act.dragAndDrop(dragItem3Element_Part2, NonAcid).build().perform();
	}
	
	
	@Step("Drag And Drop Correct Answer for question #1,  Method: {method} ")
	public void question1DragAndDropCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_"));
		
		// Element which needs to drag.
		WebElement dragItem1Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement dragItem2Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));

		WebElement dragItem3Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement dragItem4Element_Part1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		

		// Element on which need to drop.
		
		WebElement StrongElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement WeakElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));
		
		WebElement NonElectrolyte = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-3-5768735236eacfa2ee6ab4a7dd7c97206babbede9506750_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part1);
		act.dragAndDrop(dragItem1Element_Part1, StrongElectrolyte).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem3Element_Part1);
		act.dragAndDrop(dragItem3Element_Part1, StrongElectrolyte).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem2Element_Part1);
		act.dragAndDrop(dragItem2Element_Part1, WeakElectrolyte).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem4Element_Part1);
		act.dragAndDrop(dragItem4Element_Part1, NonElectrolyte).build().perform();
		
		
		ReusableMethods.scrollToElement(driver, 
				By.id("sortingPreviewAnswerItemList-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_"));
		
		// Element which needs to drag.
		WebElement dragItem1Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement dragItem2Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));

		WebElement dragItem3Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement dragItem4Element_Part2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		

		// Element on which need to drop.
		
		WebElement StrongAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-1-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement WeakAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-2-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));
		
		WebElement NonAcid = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("subAnswerCategoryList-3-5769564276eacfa2ee6ab4a7dd7c97206babbede9502517_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem3Element_Part2);
		act.dragAndDrop(dragItem3Element_Part2, StrongAcid).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem2Element_Part2);
		act.dragAndDrop(dragItem2Element_Part2, WeakAcid).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem1Element_Part2);
		act.dragAndDrop(dragItem1Element_Part2, NonAcid).build().perform();
		Thread.sleep(1000);
		ReusableMethods.moveToElement(driver, dragItem4Element_Part2);
		act.dragAndDrop(dragItem4Element_Part2, NonAcid).build().perform();
	}
	

	@Step("Enter Correct Answer for question #1,  Method: {method} ")
	public void question1EnterCorrectAnswerUsingSolutionSection() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Enter Correct Answer
		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293232682486a110cd3be354d036b9df8df736279207296_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293390274586a110cd3be354d036b9df8df736279204728_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number293458835686a110cd3be354d036b9df8df736279208586_")));

		WebElement Q1Part1SolutionElement = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("wafer_ps_number293232682486a110cd3be354d036b9df8df736279207296__solution")));
		WebElement Q1Part2SolutionElement = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("wafer_ps_number293390274586a110cd3be354d036b9df8df736279204728__solution")));
		WebElement Q1Part3SolutionElement = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("wafer_ps_number293458835686a110cd3be354d036b9df8df736279208586__solution")));

		String Q1Part1SolutionText = Q1Part1SolutionElement.getAttribute("value");
		String Q1Part2SolutionText = Q1Part2SolutionElement.getAttribute("value");
		String Q1Part3SolutionText = Q1Part3SolutionElement.getAttribute("value");

		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys(Q1Part1SolutionText);
		Q1Part2Element.sendKeys(Q1Part2SolutionText);
		Q1Part3Element.sendKeys(Q1Part3SolutionText);
	}

	@Step("Drag And Drop Correct Answer for question #5,  Method: {method} ")
	public void question5DragAndDropCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		// Element which needs to drag.
		//Glucose
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Lead Nitrate
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Sodium Chliride
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Perchloric Acid
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Ethanol
		WebElement dragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-341191281886a110cd3be354d036b9df8df736279205232_")));

		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-341191281886a110cd3be354d036b9df8df736279205232_")));

		// Enter Correct Answer
		// Dragged and dropped.
		act.dragAndDrop(dragItem2Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem3Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem4Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem1Element, dropContainer2Element).build().perform();
		act.dragAndDrop(dragItem5Element, dropContainer2Element).build().perform();
	}

	@Step("Drag And Drop InCorrect Answer for question #5,  Method: {method} ")
	public void question5DragAndDropInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Lead Nitrate
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Sodium Chliride
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Perchloric Acid
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		//Ethanol
		WebElement dragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-341191281886a110cd3be354d036b9df8df736279205232_")));

		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-341191281886a110cd3be354d036b9df8df736279205232_")));
		
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-341191281886a110cd3be354d036b9df8df736279205232_")));

		// Enter In-Correct Answer
		// Dragged and dropped.
		act.dragAndDrop(dragItem1Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem4Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem5Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem2Element, dropContainer1Element).build().perform();
		act.dragAndDrop(dragItem3Element, dropContainer2Element).build().perform();
	}

	@Step("Enter In-Correct Answer for question #1,  Method: {method} ")
	public void question6EnterInCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64308009553c4a0055bb2da4257d6df04c95ce0b5902429_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64308617913c4a0055bb2da4257d6df04c95ce0b5903580_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64309059063c4a0055bb2da4257d6df04c95ce0b5906086_")));

		// Enter InCorrect Answer
		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys("10");
		Q1Part2Element.sendKeys("20");
		Q1Part3Element.sendKeys("30");
	}
	
	@Step("Enter In-Correct Answer for question #7,  Method: {method} ")
	public void question7InCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285181833c4a0055bb2da4257d6df04c95ce0b5901515_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285018463c4a0055bb2da4257d6df04c95ce0b5901605_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285369083c4a0055bb2da4257d6df04c95ce0b5909869_")));

		// Enter InCorrect Answer
		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys("1");
		Q1Part2Element.sendKeys("2");
		Q1Part3Element.sendKeys("3");
	}
	
	@Step("Enter Correct Answer for question #7,  Method: {method} ")
	public void question7CorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//div[@class='judge-title' and contains(text(), 'Solution ')]"));

		WebElement Q1Part1Element_Solution = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285181833c4a0055bb2da4257d6df04c95ce0b5901515__solution")));
		String Answer01 = Q1Part1Element_Solution.getAttribute("value");
		WebElement Q1Part2Element_Solution = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285018463c4a0055bb2da4257d6df04c95ce0b5901605__solution")));
		String Answer02 = Q1Part2Element_Solution.getAttribute("value");
		WebElement Q1Part3Element_Solution = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285369083c4a0055bb2da4257d6df04c95ce0b5909869__solution")));
		String Answer03 = Q1Part3Element_Solution.getAttribute("value");

		WebElement Q1Part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285181833c4a0055bb2da4257d6df04c95ce0b5901515_")));
		WebElement Q1Part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285018463c4a0055bb2da4257d6df04c95ce0b5901605_")));
		WebElement Q1Part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number64285369083c4a0055bb2da4257d6df04c95ce0b5909869_")));
		
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//div[@class='judge-title' and contains(text(), ' Practice Attempt')]"));
		
		// Enter InCorrect Answer
		Q1Part1Element.clear();
		Q1Part2Element.clear();
		Q1Part3Element.clear();

		Q1Part1Element.sendKeys(Answer01);
		Q1Part2Element.sendKeys(Answer02);
		Q1Part3Element.sendKeys(Answer03);
	}
	
	@Step("Enter In-Correct Answer for question #1,  Method: {method} ")
	public void question4InCorrectAnswer() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement Q1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number58210335933c4a0055bb2da4257d6df04c95ce0b5907342_")));

		// Enter InCorrect Answer
		Q1Element.clear();
		Q1Element.sendKeys("10");

	}

}
