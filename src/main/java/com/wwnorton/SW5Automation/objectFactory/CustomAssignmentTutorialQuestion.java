package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CustomAssignmentTutorialQuestion {

	WebDriver driver;
	WebDriverWait wait;

	// Initializing Web Driver and PageFactory.
	public CustomAssignmentTutorialQuestion() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 50);
	}

	@Step("Enter Correct Answer for question #1,  Method: {method} ")
	public void enterQuestion1_Correct() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
        ReusableMethods.scrollToBottom(driver);
		// Correct answer
		WebElement correctAnswer = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'Gold will have the highest final temperature because it has the lowest heat capacity.')]]//input[not(@disabled)]")));

		// Enter Correct Answer
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", correctAnswer);
		//correctAnswer.click();
	}

	@Step("Enter Answer for question #2 Correct,  Method: {method} ")
	public void enterQuestion2_Correct() throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver, 50);

		
		 // Elements / compounds WebElement helium = wait.until(ExpectedConditions
		
		 /*
		 * .visibilityOfElementLocated(By.id(
		 * "itemContent-6-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * WebElement nitrogen = wait.until(ExpectedConditions
		 * .visibilityOfElementLocated(By.id(
		 * "itemContent-4-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * WebElement gold = wait.until(ExpectedConditions
		 * .visibilityOfElementLocated(By.id(
		 * "itemContent-3-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * WebElement ethanol = wait.until(ExpectedConditions
		 * .visibilityOfElementLocated(By.id(
		 * "itemContent-5-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * WebElement sodiumChloride = wait.until(ExpectedConditions
		 * .visibilityOfElementLocated(By.id(
		 * "itemContent-2-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * 
		 * // Drag type where element/compound need to be dropped. WebElement
		 * elementContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
		 * By.id(
		 * "categorySubContainer-1-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * WebElement coumpoundContainer =
		 * wait.until(ExpectedConditions.visibilityOfElementLocated( By.id(
		 * "categorySubContainer-2-031257714086a110cd3be354d036b9df8df736279209443_")));
		 * 
		 * // Using Action class for drag and drop. Actions act = new Actions(driver);
		 * 
		 * // Drag and drop each item to its type respectively act.dragAndDrop(helium,
		 * elementContainer).build().perform(); act.dragAndDrop(nitrogen,
		 * elementContainer).build().perform(); act.dragAndDrop(gold,
		 * elementContainer).build().perform();
		 * 
		 * act.dragAndDrop(ethanol, coumpoundContainer).build().perform();
		 * act.dragAndDrop(sodiumChloride, coumpoundContainer).build().perform();
		 */

		// Elements / compounds WebElement helium = wait.until(ExpectedConditions
		
		WebElement helium = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("itemContent-7-031257714086a110cd3be354d036b9df8df736279209443_")));
		WebElement nitrogen = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("itemContent-8-031257714086a110cd3be354d036b9df8df736279209443_")));
		WebElement gold = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("itemContent-3-031257714086a110cd3be354d036b9df8df736279209443_")));
		WebElement ethanol = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("itemContent-5-031257714086a110cd3be354d036b9df8df736279209443_")));
		WebElement sodiumChloride = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("itemContent-2-031257714086a110cd3be354d036b9df8df736279209443_")));

		// Drag type where element/compound need to be dropped.
		WebElement elementContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-031257714086a110cd3be354d036b9df8df736279209443_")));
		WebElement coumpoundContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-031257714086a110cd3be354d036b9df8df736279209443_")));

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Drag and drop each item to its type respectively
		act.dragAndDrop(helium, elementContainer).build().perform();
		act.dragAndDrop(nitrogen, elementContainer).build().perform();
		act.dragAndDrop(gold, elementContainer).build().perform();

		act.dragAndDrop(ethanol, coumpoundContainer).build().perform();
		act.dragAndDrop(sodiumChloride, coumpoundContainer).build().perform();
		
	}

	public void enterTutorialQuestion1_Correct() {
		// WebDriverWait wait = new WebDriverWait(driver, 50);

		// Correct answer
		WebElement correctAnswer = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'196.97 g Au = 1 mol')]]//input[not(@disabled)]")));

		// Enter Correct Answer
		correctAnswer.click();

	}

	public void enterTutorialQuestion2_Correct() {

		// WebDriverWait wait = new WebDriverWait(driver, 50);

		// Input Fields of masses to moles
		WebElement inputAu = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number5206971133c72f57d0ab305d01cd629db7fee5b68609791_")));

		WebElement inputPt = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number5206971133c72f57d0ab305d01cd629db7fee5b68606327_")));

		WebElement inputMg = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number5206971133c72f57d0ab305d01cd629db7fee5b68609623_")));

		// Enter Correct Answer
		inputAu.sendKeys("0.0508");
		inputPt.sendKeys("0.0513");
		inputMg.sendKeys("0.4110");

	}

	public void enterTutorialQuestion3_Correct() {

		// WebDriverWait wait = new WebDriverWait(driver, 50);

		// Input field for temperature of the gold
		WebElement input = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number5207001142c72f57d0ab305d01cd629db7fee5b68602129_")));

		// Enter Correct Answer
		input.sendKeys("30.4");

	}
}
