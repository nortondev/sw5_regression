package com.wwnorton.SW5Automation.objectFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class SW5DLPPage {

	WebDriver driver;
	String AssignmentText;
	JavascriptExecutor js = (JavascriptExecutor) driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement StudentSet;

	@FindBy(how = How.XPATH, using = "//*[@id=\"activity_list_table\"]/tbody")
	public WebElement AssignmentTitle;

	@FindBy(how = How.ID, using = "create_custom_swfb_assignment_button")
	public WebElement Create_New_Assignment;

	@FindBy(how = How.ID, using = "swfb_no_student_set_btn")
	public WebElement JoinStudentSetIDbutton;

	@FindBy(how = How.ID, using = "class_registration_id")
	public WebElement classRegistrationID;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement okButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog']/div")
	public WebElement studentinfo;

	@FindBy(how = How.XPATH, using = "//button[@id='login_button']")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	WebElement Signin_or_Register;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button'][//span[@id='gear_button_username']]")
	public WebElement gear_button_username;
	
	@FindBy(how = How.ID, using = "create_custom_swfb_adaptive_assignment_button")
	public WebElement Create_FullyAdaptive_Assignment;

	@FindBy(how = How.XPATH, using = "//b[contains(text(),'Sign Out')]")
	public WebElement signOut;
	
	@FindBy(how = How.XPATH, using = "//span[@class = 'extra_credit_assignment_title' and contains(text(),' (Extra Credit)')]")
	public WebElement extraCreditTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@id=\"activity_list_table_wrapper\"]/table/tbody")
	public WebElement SelectAssignment;
	

	// Initializing Web Driver and PageFactory.

	public SW5DLPPage() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Select New Student Set ID by Visible Text on SW5 DLP page,  Method: {method} ")
	public void selectSSByTitle(String SSTitle) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByVisibleText(SSTitle);

	}

	@Step("Select New Student Set ID by Value on SW5 DLP page,  Method: {method} ")
	public void selectSSByValue(String SSTitleValue) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(StudentSet));
		Select drpStudentSet = new Select(StudentSet);
		drpStudentSet.selectByValue(SSTitleValue);

	}

	@Step("Get Assignment Row Count on Instructor DLP page,  Method: {method} ")
	public int assignmentRowCount() throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver,10);
		// wait.until(ExpectedConditions.visibilityOfAllElements(AssignmentTitle));
		// List<WebElement> assignmentRows =
		// AssignmentTitle.findElements(By.tagName("tr"));
		// int rowsCount = assignmentRows.size();

		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object rowCountObj = js.executeScript("return $('#activity_list_table tbody tr').length");

		return Integer.valueOf(rowCountObj.toString());

	}
	
	@Step("Get Assignment Name on Instructor DLP page,  Method: {method} ")
	public String getAssignmentName(String strAssignmentName) throws Exception {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);
		
		String AssignmentTitle = null;
		
		try {
				List<WebElement> eleAssignmentTitle = driver.findElements(
						By.xpath("//tr[td/a[contains(text(),'" + strAssignmentName + "')]]/td[@class=' title_td']/a"));
			
					if (eleAssignmentTitle.size() == 1) {
						AssignmentTitle = eleAssignmentTitle.get(0).getText();
					}
					
			} catch(Exception e) {e.getMessage();}
			
			return AssignmentTitle;
		}
	
	
	@Step("Check Assignment Name on Instructor DLP page,  Method: {method} ")
	public boolean checkAssignmentName(String strAssignmentName) throws Exception {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);
		
		String AssignmentTitle;
		
		try {
				List<WebElement> eleAssignmentTitle = driver.findElements(
						By.xpath("//tr[td/a[contains(text(),'" + strAssignmentName + "')]]/td[@class=' title_td']/a"));
			
					if (eleAssignmentTitle.size() == 1) {
						AssignmentTitle = eleAssignmentTitle.get(0).getText();
					}
					
			} catch(Exception e) {
				e.getMessage();
				return false;
			}
			
			return true;
		}

	@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
	public void selectByPartOfVisibleText(String value) throws Exception {
		List<WebElement> optionElements = StudentSet.findElements(By.tagName("option"));

		for (WebElement optionElement : optionElements) {
			if (optionElement.getText().contains(value)) {
				System.out.println(optionElement.getText());
				optionElement.click();
				try {
					WebDriverWait wait = new WebDriverWait(driver, 50);
					wait.until(ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody")));
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loading_message")));
				} catch (Exception e) {
					System.out.println("Exception Handlled: " + e.getMessage());
				}
				break;
			}
		}

	}

	@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
	public void ClickAssignment() {
		Create_New_Assignment.click();
	}

	@Step("Select Assignment from Assignment Title Column,  Method: {method} ")
	public String ClickAssignmentlink(String AssignmentTitle) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody")));
		List<WebElement> links = driver.findElements(By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody"));
		for (int i = 0; i < links.size(); i++) {
			List<WebElement> linkstext = links.get(i).findElements(By.xpath("//tr/td[@class=' title_td']/a"));
			for (int j = 0; j < linkstext.size(); j++) {
				AssignmentText = linkstext.get(j).getText();
				if (AssignmentText.equalsIgnoreCase(AssignmentTitle)) {
					LogUtil.log("The Assignment Name " + AssignmentTitle);
					// Assert.assertSame("The AssignmentName is" , "CH1H4W", AssignmentTitle);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", linkstext.get(j));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkstext.get(j));
					//linkstext.get(j).click();
				}
			}

		}
		return AssignmentTitle;

	}

	@Step("Select Assignment from Assignment Title Column,  Method: {method} ")
	public void ClickAssignmentlinkbylinkText(String AssignmentTitle) {
		WebElement AssignmentNameclick = driver.findElement(By.linkText(AssignmentTitle));
		AssignmentNameclick.click();
	}

	private void scrollToElement(WebElement el) {
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", el);
		}
	}

	public void waitAndClick(WebElement el) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10, 200);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("CH9HW")));
			wait.until(ExpectedConditions.elementToBeClickable(el)).click();

		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			scrollToElement(el);
			el.click();
		}
	}

	@Step("New Student clicks the Join a Student Set Now button ,  Method: {method} ")
	public void joinstudentset() {
		JoinStudentSetIDbutton.click();
	}

	@Step("New Student enters the class Registration ID & Click Ok Button ,  Method: {method} ")
	public void registrationID(String ID) {
		classRegistrationID.sendKeys(ID);
		okButton.click();
	}

	@Step("get the student set information,  Method: {method} ")
	public void studentsetinfo() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='login_dialog']/div")));
		System.out.println(studentinfo.getText());
		LogUtil.log("The Joined Student information" + studentinfo.getText());
		okButton.click();
	}

	@Step("Get Submitted Grades associated with assignmentTitle,  Method: {method} ")
	public String getSubmittedGrades(String assignmentTitle) {
		String retVal = "";
		List<WebElement> submittedGrades = driver.findElements(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' submissions_td']/a"));
		if (submittedGrades.size() == 1) {
			retVal = submittedGrades.get(0).getText();
		}
		return retVal;
	}

	@Step("Get Average TimeSpent associated with assignmentTitle,  Method: {method} ")
	public String getAverageTimeSpent(String assignmentTitle) {
		String retVal = "";
		List<WebElement> averageTimeSpent = driver.findElements(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' time_td']/a"));
		if (averageTimeSpent.size() == 1) {
			retVal = averageTimeSpent.get(0).getText();
		}
		return retVal;
	}

	@Step("Get Average Grade associated with assignmentTitle,  Method: {method} ")
	public String getAverageGradeText(String assignmentTitle) {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		String retVal = "";
		List<WebElement> averageGrades = driver.findElements(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' grade_td']"));
		if (averageGrades.size() == 1) {
			retVal = averageGrades.get(0).getText();
		}
		return retVal;
	}
	
	@Step("Get GAU Date associated with assignmentTitle in Instructor DLP,  Method: {method} ")
	public String getGAUDate_Instructor(String assignmentTitle) {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		String retGAUDateText = "";
		List<WebElement> gauDateText = driver.findElements(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class='gau_td sorting_1']"));
		if (gauDateText.size() == 1) {
			retGAUDateText = gauDateText.get(0).getText();
		}
		
		return retGAUDateText;
	}
	
	
	@Step("Get GAU Date associated with assignmentTitle in Student DLP,  Method: {method} ")
	public String getGAUDate_Student(String assignmentTitle) throws Exception {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		String GAUDateText = "";
		List<WebElement> gauDateText = driver.findElements(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' gau_td']"));
		if (gauDateText.size() == 1) {
			GAUDateText = gauDateText.get(0).getText();
		}
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		Date gauDate = dateFormat.parse(GAUDateText);

		String day = new SimpleDateFormat("dd").format(gauDate);
		String month = new SimpleDateFormat("M").format(gauDate);
		String year = new SimpleDateFormat("yy").format(gauDate);
		
		return month+"/"+day+"/"+year;
	}
	
	
	@Step("Get GAU Date associated with assignmentTitle in Student DLP,  Method: {method} ")
	public String getSystemGAUDate_Student(int numberOfDays) throws Exception {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, numberOfDays);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		String strGAUDate = dateFormat.format(cal.getTime());
		Date gauDate = dateFormat.parse(strGAUDate);

		String day = new SimpleDateFormat("dd").format(gauDate);
		String month = new SimpleDateFormat("M").format(gauDate);
		String year = new SimpleDateFormat("yy").format(gauDate);
		
		return month+"/"+day+"/"+year;
	}
	
	
	@Step("Check Reports button associated with assignmentTitle is displayed,  Method: {method} ")
	public boolean isReportsButtonDisplayedForAssignmentTitle(String assignmentTitle) {
		try {
			List<WebElement> reportsButtons = driver.findElements(By.xpath(
					"//tr[td/a[contains(text(),'" + assignmentTitle + "')]]//a/span[contains(text(),'Reports')]"));
			if (reportsButtons.size() == 1) {
				return true;
			}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}

	@Step("Click Reports button associated with assignmentTitle,  Method: {method} ")
	public void clickReportsButton(String assignmentTitle) {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		List<WebElement> reportsButtons = driver.findElements(By
				.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]//a[span[contains(text(),'Reports')]]"));
		if (reportsButtons.size() == 1) {
			//reportsButtons.get(0).click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", reportsButtons.get(0));
		}
	}
	
	
	@Step("Click Copy button associated with assignmentTitle,  Method: {method} ")
	public void clickCopyButton(String assignmentTitle) {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		
		List<WebElement> assignments = driver.findElements(By.xpath
				("//tbody/tr/td[@class=' title_td']/a"));
		
		for(int i=1; i<=assignments.size(); i++) {
		
			WebElement assignmentName = driver.findElement(By.xpath
				("//tbody/tr["+ i +"]/td[@class=' title_td']/a"));
			WebElement copyButton = driver.findElement(By.xpath
				("//tbody/tr["+ i +"]/td[@class=' title_td']/span/a[text()='COPY']"));
		
				if (assignmentName.getText().equals(assignmentTitle)) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", copyButton);
					break;
				}
		}
	}
	
	@Step("Check Publish checkbox associated with assignmentTitle,  Method: {method} ")
	public boolean checkPublishCheckbox(String assignmentTitle) {
		ReusableMethods.waitVisibilityOfElement(driver, By.id("product_contents"));
		
		try {
				WebElement publishCheckboxes = driver.findElement(By
						.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' published_td']/span[@class='css_checkbox published_cell_span' and @data-row-class='is_published']"));
						publishCheckboxes.isDisplayed();
						return true;
			}catch(Exception e) {
				e.getMessage();
				return false;
		}
	}
	
	
	@Step("Click on Create Fully Adaptive Assignment button,  Method: {method} ")
	public void clickFullAdaptiveButton() {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(Create_FullyAdaptive_Assignment));
		Create_FullyAdaptive_Assignment.click();
		
		
	}
	
	@Step("Check Create Fully Adaptive Assignment button is displayed,  Method: {method} ")
	public boolean checkFullAdaptiveButton() {
		
		boolean FullAdaptiveButtonExist = false;
		
		try {
				FullAdaptiveButtonExist = Create_FullyAdaptive_Assignment.isDisplayed();
				if(FullAdaptiveButtonExist == true) {
					
					return true;
				}
		} catch (Exception e) { e.getMessage(); }
		
		return FullAdaptiveButtonExist;
	
	}
	
	@Step("Scroll To AssignmentTitle,  Method: {method} ")
	public void scrollToAssignmentTitle(String assignmentTitle) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]")));
		
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]"));
		
	}
	
	@Step("Check Extra Credit checkbox associated with assignmentTitle,  Method: {method} ")
	public void checkExtraCreditCheckBox(String assignmentTitle) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		WebElement extraCreditCheckBox = driver.findElement(By.xpath
				("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' extra_credit_td']/span[@class='css_checkbox extra_credit_cell_span']"));
		
		wait.until(ExpectedConditions.elementToBeClickable(extraCreditCheckBox));
		extraCreditCheckBox.click();
		
	}
	
	
	@SuppressWarnings("unused")
	@Step("Get Extra Credit by Assignment name on SW5 Student DLP page,  Method: {method} ")
	public String getExtraCreditOnInstructorDLP(String assignmentName) {
		
		String extraCreditText = null;
		
		List<WebElement> linksText = SelectAssignment.findElements
				(By.xpath("//tr/td[@class=' title_td']/a"));
		
		List<WebElement> extraCreditTitles = SelectAssignment.findElements
				(By.xpath("//span[@class='extra_credit_assignment_title' and contains(text(),' (Extra Credit)')]"));
		
		firstLoop: for (int i = 0; i < linksText.size(); i++) {

			String AssignmentText = linksText.get(i).getText();
			
			if (AssignmentText.equalsIgnoreCase(assignmentName)) {
				
				for (int j = 0; j < extraCreditTitles.size(); j++) {
				
					extraCreditText = extraCreditTitles.get(j).getText();
					break firstLoop;
				}
			}
		}
		
		return extraCreditText;
	
	}

	@Step("Logout of SW5 application,  Method: {method} ")
	public void logoutSW5() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(gear_button_username));
		gear_button_username.click();

		wait.until(ExpectedConditions.elementToBeClickable(signOut));
		signOut.click();
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(loginButton),
				ExpectedConditions.elementToBeClickable(Signin_or_Register)));
	}
}
