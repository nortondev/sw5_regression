package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH22HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH22HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	
	@Step("Enter Correct answer for question #1,  Method: {method} ")
	public void question1_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Element which needs to drag.
		WebElement Item1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number90922503020cd9d0f1ccb4b38dfecf49186b4546db01575_")));
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_number90922503020cd9d0f1ccb4b38dfecf49186b4546db01575_"));
		Item1Element.sendKeys("4");
				
	}


	@Step("Select Correct aanswers for question #4,  Method: {method} ")
	public void question4_CorrectAnswer() throws Exception {

WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div03846770970cd9d0f1ccb4b38dfecf49186b4546db03155_"));
		
		WebElement item1Element_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div03846770970cd9d0f1ccb4b38dfecf49186b4546db03155_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part1);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div03848343550cd9d0f1ccb4b38dfecf49186b4546db08501_"));
		
		WebElement item1Element_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div03848343550cd9d0f1ccb4b38dfecf49186b4546db08501_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part2);
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, By.id
				("wafer_ps_radio_div03850702500cd9d0f1ccb4b38dfecf49186b4546db09767_"));
		
		WebElement item1Element_Part3 = wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//span[@id='wafer_ps_radio_div03850702500cd9d0f1ccb4b38dfecf49186b4546db09767_']"
						+ "/span/label/span/input[contains(@value,'1')]")));
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element_Part3);
		
	}
	
	@Step("Enter Correct answer for question #2,  Method: {method} ")
	public void question2_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Enter correct answers
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_number_div963318394046cdeba34ad9dd3895e73d3972b368c503492_"));
		
		WebElement Item1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number963318394046cdeba34ad9dd3895e73d3972b368c503492_")));
		
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_number_div963321868446cdeba34ad9dd3895e73d3972b368c501539_"));
		
		WebElement Item2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number963321868446cdeba34ad9dd3895e73d3972b368c501539_")));
		
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_number_div963332871546cdeba34ad9dd3895e73d3972b368c501537_"));
		
		WebElement Item3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number963332871546cdeba34ad9dd3895e73d3972b368c501537_")));
		
		ReusableMethods.scrollToElement(driver, 
				By.id("wafer_ps_radio_div963344817346cdeba34ad9dd3895e73d3972b368c500320_"));
		
		WebElement Item4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='3']")));
		
		Item1Element.sendKeys("9");
		Thread.sleep(1000);
		Item2Element.sendKeys("8");
		Thread.sleep(1000);
		Item3Element.sendKeys("6");
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", Item4Element);
				
	}
	
	@Step("Enter Correct answer for question #3,  Method: {method} ")
	public void question3_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Element which needs to drag.
		WebElement Item1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_shortanswer42549180081ed5c6c648049fa0b7b7d2c611b1d14705690__")));
		
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_shortanswer_div42549180081ed5c6c648049fa0b7b7d2c611b1d14705690_"));
		Item1Element.sendKeys("+3");
				
	}

}