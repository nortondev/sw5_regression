package com.wwnorton.SW5Automation.objectFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TimeZone;

import ru.yandex.qatools.allure.annotations.Step;

public class StudentDetailOverlayPage {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Overall score')]/following-sibling::td")
	public WebElement OverallScore;

	@FindBy(how = How.XPATH, using = "//div[@id='student-detail-pdf-container']/div/button[@class='close']")
	public WebElement CloseStudentDetailOverlay;
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-info btn-title-reset ml15 lato-regular-14']/span[contains(text(),'EDIT')]")
	public WebElement EditButton;
	
	@FindBy(how = How.XPATH, using ="//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Time spent')]/following-sibling::*")
	public WebElement TimeSpentData;
	
	@FindBy(how = How.XPATH, using ="//div[@id='student-detail-pdf-container']//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Assigned question completed')]/following-sibling::*")
	public WebElement AssignedQuestionCompleted;
	
	@FindBy(how = How.XPATH, using ="//div[@class='txt-overall meta-content-regular']/div[2]/div")
	public WebElement LateworkText;
	
	@FindBy(how = How.XPATH, using ="//span[@class='ant-switch-inner'][contains(text(),'ON')]")
	public WebElement PublishON;
	
	@FindBy(how = How.XPATH, using ="//span[@class='ant-switch-inner'][contains(text(),'OFF')]")
	public WebElement TimeLimitOff;
	
	@FindBy(how = How.XPATH, using ="//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']")
	public WebElement GAUdate;
	
	@FindBy(how = How.XPATH, using ="//div[@type='select']/select[@class='body-content form-control']")
	public WebElement GAUTime;
	
	@FindBy(how = How.XPATH, using ="//div[@type='select']/select[@class='form-control']")
	public WebElement UngradedPracticeText;
	
	@FindBy(how = How.XPATH, using ="//span[@class='ant-calendar-picker']/span/input[@disabled]")
	public WebElement DisabledGAUDate;
	
	@FindBy(how = How.XPATH, using ="//div[@class='timeZoneSpan']/div[@type='select' and @disabled]")
	public List<WebElement> DisabledGAUTime;
	
	@FindBy(how = How.XPATH, using ="//select[@type='select']/option[text()='Exam mode: never allow ungraded practice']")
	public WebElement DisabledUngradedPracticeOption;
	
	@FindBy(how = How.XPATH, using ="//div[@class='examMode-title-msg']/span[text()='You cannot edit Attempts in Exam Mode']")
	public WebElement ExamSettings_QuestionTitleMsg;
	
	
	@FindBy(how = How.XPATH, using ="//span[text()='You cannot change this setting in Exam Mode']")
	public List<WebElement> ExamSettingsInfo;
	
	@FindBy(how = How.XPATH, using ="//span[@class='ant-switch-inner' and text()='OFF']")
	public List<WebElement> PublishOff;
	
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-act btn-primary lato-regular-14 fr btn-primary']/span[contains(text(),'SAVE')]")
	public WebElement SaveButton;
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-info lato-regular-14 fr']/span[contains(text(),'CANCEL')]")
	public WebElement CancelButton;
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-info lato-regular-14']/span[contains(text(),'RESET ALL RESULTS')]")
	public WebElement ResetAllResultButton;
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET')]")
	public WebElement ResetButton;
	
	@FindBy(how = How.XPATH, using ="//tbody[@class='stuDetailTabEdit']/tr[1]/td[6]/button/span")
	public WebElement ResetIcon_EditSDO;
	
	@FindBy(how = How.XPATH, using ="//tbody[@class='stuDetailTabEdit']/tr[3]/td[2]/div/input")
	public WebElement updateScore_EditSDO; 
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET ALL RESULTS')]")
	public WebElement PopUpRESETALLRESULTSButton;
	
	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Average score')]/following-sibling::td")
	public WebElement QuestionDetailAverageScore;
	
	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Average time spent')]/following-sibling::td")
	public WebElement QuestionDetailAverageTimeSpent;
	
	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Student submissions')]/following-sibling::td")
	public WebElement QuestionDetailStudentSubmission;
	
	@FindBy(how = How.XPATH, using = "//table[@class='tb-overall']/tbody/tr/td[contains(text(),'Item ID')]/following-sibling::td")
	public WebElement QuestionDetailItemID;
	
	@FindBy(how = How.XPATH, using ="//div[@class='modal-body']/div/span[@class='qd-full-credit cantSelect']")
	public WebElement QuestionDetailGiveCredit;
		
	@FindBy(how = How.XPATH, using ="//div[@class='modal-content']/div[@class='modal-footer']/button[@class='btn-submit-s btn-primary fr lato-regular-14'][contains(text(),'GIVE FULL CREDIT')]")
	public WebElement QuestionDetailGiveFullCreditButton;
	
	@FindBy(how = How.XPATH, using ="//button[@class='btn-primary btn-act btn-submit-s lato-regular-14 fr']/span[contains(text(),'EXPORT')]")
	public WebElement studendetailExportbutton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']")
	public WebElement calendarDate; 

	@FindBy(how = How.XPATH, using = "//tbody[@class='ant-calendartbody']/tr/td[@class='ant-calendar-cell ant-calendar-today']/span")
	public WebElement currentDate;
	
	@FindBy(how = How.XPATH, using = "//div[@class='general-settings']/span[@class='main-heading']")
	public WebElement settingsLabel;
	
	@FindBy(how = How.XPATH, using = "//div[@type='select']/select[@class='body-content form-control']")
	public WebElement SelectTime;
	
	@FindBy(how = How.XPATH, using = "//table/tbody[@class='stuDetailTabEdit']")
	public WebElement QuestionTable;
	
	@FindBy(how = How.XPATH, using = "//input[@class='inputTimeLimit']")
	public WebElement timeLimitInput;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title']/span[@class='sub-section-heading' and text()='Invalid Time Limit']")
	public WebElement InvalidTimeLimitPopUp;
	
	@FindBy(how = How.XPATH, using = "//div[@class='body-content']")
	public WebElement InvalidTimeLimitMsg;
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-content']/div[@class='head-bar modal-header']/button[@class='close']")
	public WebElement InvalidTimeLimitPopUp_Close;
	
	
	List<WebElement> AllQuestions;
	
	
	// Initializing Web Driver and PageFactory.
	public StudentDetailOverlayPage() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

		//waitForLoadPageElements();
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public void waitForLoadPageElements() throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			wait.until(ExpectedConditions.elementToBeClickable(CloseStudentDetailOverlay));
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Click on Save Button,  Method: {method} ")
	public void clickSaveButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(SaveButton));
		SaveButton.click();
	}
	
	@Step("Click on Reset All results Button,  Method: {method} ")
	public void clickResetAllResultsButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions.elementToBeClickable(ResetAllResultButton));
		ResetAllResultButton.click();
	}
	
	
	@Step("Click on Reset Button,  Method: {method} ")
	public void clickResetButton() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET')]")));
		ResetButton.click();
	}
	
	@Step("Click on Reset Icon for 1st Question in Edit Student Details Overlay,  Method: {method} ")
	public void clickResetIcon_EditSDO() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tbody[@class='stuDetailTabEdit']/tr[1]/td[6]/button/span")));
		
		ReusableMethods.scrollIntoView(driver, ResetIcon_EditSDO);
		ResetIcon_EditSDO.click();
	}
	
	@Step("Click on ResetAll Button on Pop Up,  Method: {method} ")
	public void clickResetAllResultsButtonOnPopUp() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn-submit-s btn-primary lato-regular-14 fr']/span[contains(text(),'RESET ALL RESULTS')]")));
		PopUpRESETALLRESULTSButton.click();
	}
	
	@Step("Update the Score for 3rd Question in Edit Student Details Overlay,  Method: {method} ")
	public void updateScore_EditSDO() throws Exception {
		Thread.sleep(1000);
		ReusableMethods.scrollIntoView(driver, updateScore_EditSDO);
		updateScore_EditSDO.clear();
		updateScore_EditSDO.sendKeys("2");
	}

	
	@Step("Click on Give Credit All Student link,  Method: {method} ")
	public void clickGiveCreditAllStudentLink() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='modal-body']/div")));
		QuestionDetailGiveCredit.click();
	}
	
	@Step("Click on Give Credit Full Button on Pop Up k,  Method: {method} ")
	public void clickGiveCreditButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(QuestionDetailGiveFullCreditButton));
		QuestionDetailGiveFullCreditButton.click();
	}
	
	
	@Step("Click on close StudentDetailOverlay button,  Method: {method} ")
	public void clickCloseStudentDetailOverlay() throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='student-detail-pdf-container']/div/button[@class='close']"))); 
			ReusableMethods.scrollIntoView(driver, CloseStudentDetailOverlay);
			CloseStudentDetailOverlay.click();
			
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Get Overall Score for student,  Method: {method} ")
	public String getOverallScore() {
		return OverallScore.getText();
	}
	
	@Step("Total of time spent for each question in Time Spent column,  Method: {method}")
	public List<String> getdataTimeSpentColumn() {
		//No of columns 
		List<String> retVal = new ArrayList<String>();
		WebElement table = driver.findElement(By.xpath("//tbody[@class='stuDetailTabShow']"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		for (WebElement allrows : rows) { 
			List<WebElement> columns = allrows.findElements(By.xpath("//td[@class='text-center textIndentLeft']"));
			for(int i=0; i<columns.size(); i++){
				if(i==4){
					String timeSpentCol = columns.get(i).getText();
					
					retVal.add(timeSpentCol);
					
				}
				
			}
		}
		return retVal;
			
	}
	
	@Step("Get Student Detail Overlay Time Spent Data,  Method: {method} ")
	public List<String> getStudentDetailTimeSpentData() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentTimeSpentList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td[5][@class='text-center textIndentLeft']"));
		int timelength =studentTimeSpentList.size();
		if (timelength > 0) {
			for (WebElement studentTimeSpent : studentTimeSpentList) {
				retVal.add(studentTimeSpent.getText());
			}
		}
		return retVal;
	}
	
	
	@Step("Click the Circle Arrow on Student Detail,  Method: {method} ")
	public void getStudentDetailCircleArrow(int position) throws Exception {
	
		List<WebElement> studentCircleArrowList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td/span[@class='glyphicons glyphicons-circle-arrow-right cursor-pointer fr']"));
		int timelength =studentCircleArrowList.size();
		for(int i=0; i<timelength; i++){
			WebElement clickCursor = studentCircleArrowList.get(i);
			if((i==position) && (clickCursor.isEnabled())){
			clickCursor.click();
			} else {
				boolean arrowDisabled = driver.findElement(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td/span[@class='glyphicons glyphicons-circle-arrow-right cursor-pointer fr']")).isEnabled();
				Assert.assertTrue(arrowDisabled,"The Arrow is Disabled");
			}
			
		}
		
	}
	
	public int getTimeSpent() throws ParseException{
		
		String timeSpent =TimeSpentData.getText();
		String tokens[] = timeSpent.split(":");
        int hours = Integer.parseInt(tokens[0]);
        int minutes = Integer.parseInt(tokens[1]);
        int seconds = Integer.parseInt(tokens[2]);
        int totalTimeSeconds = hours*60*60+minutes*60+seconds;
        
       
        /*hours = totalTimeSeconds / 3600;
        minutes = (totalTimeSeconds % 3600) / 60;
        seconds = totalTimeSeconds % 60;
        String a  =String.format("%02d:%02d:%02d", hours, minutes, seconds);
        int timeString = Integer.parseInt(a);
        System.out.println(timeString);*/
		return totalTimeSeconds; 
	     
		//return TimeSpentData.getText();	
		
		
	}
	public String getAssignedQuestionCompleted(){
		return AssignedQuestionCompleted.getText();
	}
	
	
	
	//Assert Grades are accepted until date is visible as per assignment GAU
	@Step("Get Grades are accepted until date is visible as per assignment GAU,  Method: {method} ")
	public String getGradeText(){
		WebElement gradeAcceptedText= driver.findElement(By.xpath("//div[@class='txt-overall meta-content-regular']/div[2]/span[1]"));
		String gtext =gradeAcceptedText.getText();
		WebElement gradedateText= driver.findElement(By.xpath("//div[@class='txt-overall meta-content-regular']/div[2]/b[1]"));
		String gaudateText=gradedateText.getText();
		WebElement gradeatText= driver.findElement(By.xpath("//div[@class='txt-overall meta-content-regular']/div[2]/span[2]"));
		String gattext =gradeatText.getText();
		WebElement gradetimeText= driver.findElement(By.xpath("//div[@class='txt-overall meta-content-regular']/div[2]/b[2]"));
		String gauTimeText=gradetimeText.getText();
		String gauCombinedText=gtext + " " + gaudateText + " " + gattext + " " + gauTimeText;
		return gauCombinedText;
		
	}
	
	@Step("Get Late work Text,  Method: {method} ")
	public String getlateworkText(){
		return LateworkText.getText();
		
	}
	
	@Step("Get Student detailsb total Questions count,  Method: {method} ")
	public int getStudentDetailTotalQuestionsCount() throws Exception {
		int retVal = 0;
		List<WebElement> dataRows = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td[@class='textIndentLeft']"));
		retVal = dataRows.size();
		return retVal;
	}
	
	@Step("Get Student Detail Overlay Score  Data,  Method: {method} ")
	public List<String> getStudentDetailScoreData() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentScoreList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td[2][@class='text-center textIndentSec']"));
		int score =studentScoreList.size();
		if (score > 0) {
			for (WebElement studentScore : studentScoreList) {
				retVal.add(studentScore.getText());
			}
		}
		return retVal;
	}
	
	@Step("Get Student Detail Overlay Attempts Data,  Method: {method} ")
	public List<String> getStudentDetailAttempts() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentAttemptsList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td[3][@class='text-center textIndentSec'][contains(text(),'Unlimited')]"));
		int attempts =studentAttemptsList.size();
		if (attempts > 0) {
			for (WebElement studentAttempts : studentAttemptsList) {
				retVal.add(studentAttempts.getText());
			}
		}
		return retVal;
	}
	
	@Step("Get Student Details Student Submissions List,  Method: {method} ")
	public List<String> getStudentDetailStudentSubmissionsList() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentSubmissionDate = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabShow']/tr/td[4][@class='text-center textIndentSec']"));
		if (studentSubmissionDate.size() > 0) {
			for (WebElement studentSubDate : studentSubmissionDate) {
				retVal.add(studentSubDate.getText());
			}
		}
		return retVal;
	}
	
	
	@Step("Get Attempts Count from Question player,  Method: {method} ")
	public int getAttemptCount() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='attempt-2']/h2/a/div[contains(text(),'attempt')]")));
		int retVal = 0;
		List<WebElement> dataRows = driver.findElements(By.xpath(
				"//*[@id='attempt-2']/h2/a/div[contains(text(),'attempt')]"));
		retVal = dataRows.size();
		return retVal;
		
	}
	
	
	@Step("Instructor Click the Edit Button on Student Detail Page,  Method: {method} ")
	public void clickEditButton(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn-submit-s btn-info btn-title-reset ml15 lato-regular-14']")));
		if(EditButton.isEnabled()){
			EditButton.click();
			
		}
	}
	
	
	@Step("Instructor Click Publish Off Button on Student Detail Page,  Method: {method} ")
	public void clickPublishOffButton(){
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(PublishOff.get(0)));
		
		try {
				if(PublishOff.get(0).isDisplayed() == true){
					PublishOff.get(0).click();
				}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	@Step("Student Validate Publish button ON or OFF,  Method: {method} ")
	public boolean isPublishONorOFF(){
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='ant-switch-inner'][contains(text(),'ON')]")));
		return PublishON.getText().equalsIgnoreCase("ON");
		
	}

	@Step("Student Validate GAUDate,  Method: {method} ")
	public String getGauDate(){
		String retDate = GAUdate.getAttribute("value");
		return retDate;
		
	}
	
	@Step("Student Validate GAUTime,  Method: {method} ")
	public String getGauTime(){
		Select select = new Select(GAUTime);
		String retTime  = select.getFirstSelectedOption().getText();
		
		return retTime ;
		
	}
	@Step("Get Late work text  for student,  Method: {method} ")
	public String getStudentDetailLateWorkText() {
		WebElement lateWorkText=driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/span[contains(text(),'Late work ')]"));
		String ltext =lateWorkText.getText();
		WebElement isText= driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/span[contains(text(),'is')]"));
		String istext=isText.getText();
		WebElement acceptedText= driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/span[contains(text(),' accepted')]"));
		String accptedtext =acceptedText.getText();
		WebElement oneDayAfterText= driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/b[1]"));
		String gauDayAfterText=oneDayAfterText.getText();
		WebElement theGAUtext= driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/span[contains(text(),' the GAU at')]"));
		String gautext =theGAUtext.getText();
		WebElement penaltyText= driver.findElement(By.xpath("//div[@class='fix-contain']/ul[@class='fix-cont-type1 ']/li[4]/div/span/b[2]"));
		String gaupenaltyText=penaltyText.getText();
		String gauCombinedText=ltext +" "+istext +" "+ accptedtext+" "+ gauDayAfterText+" "+ gautext+" "+ gaupenaltyText;
		return gauCombinedText;
	}
	
	@Step("Ungraded Practice should be visible with default option as NEVER.,  Method: {method} ")
	public String getUngradedPracticeText() {
		Select select = new Select(UngradedPracticeText);
		String retUngradedPractice  = select.getFirstSelectedOption().getText();
		return retUngradedPractice ;
				
	}
	@Step("Time Limit should be off,  Method: {method} ")
	public boolean getTimeLimitOnOFFStatus() {
		return TimeLimitOff.getText().equalsIgnoreCase("OFF");
				
	}
	
	@Step("Get Student Detail Tab Edit Score  Data,  Method: {method} ")
	public List<String> getStudentDetailEditScoreData() throws Exception {
		List<String> retScore = new ArrayList<String>();
		
		List<WebElement> studentScoreList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[2][@class='text-center textIndentSec']/div"));
		List<WebElement> studentPointList =driver.findElements(By.xpath("//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[2][@class='text-center textIndentSec']/span"));
		int score =studentScoreList.size();
		
		for(int i=0; i< score; i++){
			String ele1 = studentScoreList.get(i).getAttribute("value");
			String ele2 = studentPointList.get(i).getText();
			retScore.add(ele1 + ele2);
		}
		return retScore;
		
	}
	
	
	@Step("Get Student Detail Tab Edit Attempts  Data,  Method: {method} ")
	public List<String> getStudentDetailEditAttemptsData() throws Exception {
		List<String> retAttempts = new ArrayList<String>();
		
		List<WebElement> studentAttemptsList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[3][@class='text-center textIndentSec']/span"));
		List<WebElement> studentUnlimitedList =driver.findElements(By.xpath("//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[3][@class='text-center textIndentSec']/div"));
		int score =studentAttemptsList.size();
		
		for(int i=0; i< score; i++){
			String ele1 = studentAttemptsList.get(i).getText();
			String ele2 = studentUnlimitedList.get(i).getAttribute("value");
			retAttempts.add(ele1 + ele2);
		}
		return retAttempts;
		
	}
	
	@Step("Get Student Details Student Submissions Time in Edit Mode,  Method: {method} ")
	public List<String> getStudentDetailStudentSubmissionsTimeListEdit() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentSubmissionDateList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr/td[4][@class='text-center textIndentSec']"));
		if (studentSubmissionDateList.size() > 0) {
			for (WebElement studentSubDate : studentSubmissionDateList) {
				retVal.add(studentSubDate.getText());
			}
		}
		return retVal;
	}
	
	@Step("Get Student Detail Overlay Time Spent Data on Edit Mode,  Method: {method} ")
	public List<String> getStudentDetailTimeSpentDataOnEdit() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> studentTimeSpentListEdit = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[5][@class='text-center textIndentSec']"));
		int timelength =studentTimeSpentListEdit.size();
		if (timelength > 0) {
			for (WebElement studentTimeSpent : studentTimeSpentListEdit) {
				retVal.add(studentTimeSpent.getText());
			}
		}
		return retVal;
	}
	
	@Step("Check the Reset Button on Edit Mode and Reset the question,  Method: {method} ")
	public void getResetQuestiononEditMode(int questionNumber) throws Exception {
		
		List<WebElement> studentQuestionresetList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[@class='text-center textIndentLeft']/button[@class='btn-submit-s btn-info reset']"));
		int resetButtons =studentQuestionresetList.size();
		for(int i=0; i<resetButtons; i++){
			WebElement clickResetButton = studentQuestionresetList.get(i);
			if((i==questionNumber) && (clickResetButton.isEnabled())){
				clickResetButton.click();
				break;
			}
		}
	}
	
	@Step("Updated the Student Score ob Edit Mode ,  Method: {method} ")
	public void updateStudentScoreOnEditMode(int updateQuestionNumber, String score) throws Exception {
			
		List<WebElement> studentScoreList = driver.findElements(By.xpath(
				"//tbody[@class='stuDetailTabEdit']/tr[@class='student-detail-type']/td[2][@class='text-center textIndentSec']/div[@type='text']/input"));
		int scoreSize =studentScoreList.size();
		
		for(int i=0; i< scoreSize; i++){
			WebElement updateQuestionScore = studentScoreList.get(i);
			if(i==updateQuestionNumber){
				updateQuestionScore.clear();
				updateQuestionScore.sendKeys(score);
				break;
			}
	
		}
	}
	//Question TAB and Question details
	
	@Step("Click Question Number on Questions Tab ,  Method: {method} ")
	public void clickQuestionNumberonQuestionTab(int questionNumberonQuestiontab) throws Exception {
			
		List<WebElement> questionsList = driver.findElements(By.xpath(
				"//table[@class='report-question-table table table-bordered table-condensed table-hover']/tbody/tr/td[@class='textIndentLeft']/span[@class='glyphicons glyphicons-resize-full cursor-pointer fr']"));
		int questionNumberSize =questionsList.size();
		
		for(int i=0; i< questionNumberSize; i++){
			WebElement clickQuestion = questionsList.get(i);
			if(i==questionNumberonQuestiontab){
				clickQuestion.click();
				break;
			}
	
		}
	}
	
	@Step("Get Average score on Question details,  Method: {method} ")
	public String getQuestiondetailAverageScore() throws Exception {
		return QuestionDetailAverageScore.getText();
	}
	
	@Step("Get Average time Spent on Question details,  Method: {method} ")
	public String getQuestiondetailAverageTimeSpent() throws Exception {
		return QuestionDetailAverageTimeSpent.getText();
	}
	
	@Step("Get Student Submission on Question details,  Method: {method} ")
	public String getQuestiondetailStudentSubmission() throws Exception {
		return QuestionDetailStudentSubmission.getText();
	}
	
	@Step("Get Item ID on Question details,  Method: {method} ")
	public String getQuestiondetailItemID() throws Exception {
		return QuestionDetailItemID.getText();
	}
	
	@Step("Get Question Detail Overlay List of Student,  Method: {method} ")
	public List<String> getQuestiondetailListOfStudent() throws Exception {
		List<String> retVal = new ArrayList<String>();
		List<WebElement> listOfStudents = driver.findElements(By.xpath(
				"//table[@class='question-detail-table table table-striped table-condensed table-hover']/tbody[@class='stuDetailTabShow']/tr[@class='question-detail-type']/td[1][@class='textIndentLeft']"));
		int studentList =listOfStudents.size();
		if (studentList > 0) {
			for (WebElement studentlist : listOfStudents) {
				retVal.add(studentlist.getText());
			}
		}
		return retVal;
	}
	
	
	@Step("Set Student  GAU Exception Date in Student Details Overlay,  Method: {method} ")
	public void setGAUExceptionDate(String gauDate) throws Exception {
		
		String date;
		String caldt;
		
		String dateArray[] = gauDate.split("/");
		date = dateArray[1];
		
		driver.findElement(By.xpath("//input[@placeholder='mm/dd/yyyy']")).click();

		//Select the date
		
		List<WebElement> rows, cols;
		//rows = cal.findElements(By.tagName("tr"));
		rows = driver.findElements(By.xpath("//tbody[@class='ant-calendartbody']//tr"));

		firstLoop: for (int i = 0; i <rows.size(); i++) {
			//cols = rows.get(i).findElements(By.tagName("td"));
			cols = driver.findElements(By.xpath("//tbody[@class='ant-calendartbody']//tr")).get(i).findElements(By.tagName("td"));

			for (int j = 0; j < cols.size(); j++) {

				caldt = cols.get(j).getText();
				if (caldt.equals(date)) {
					cols.get(j).click();
					break firstLoop;
				}
			}
			
		}
		
	}
	
	@Step("Enter GAU date,  Method: {method} ")
	public void selectCurrentGAUDate() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(calendarDate));
		calendarDate.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//input[@class='ant-calendar-picker-input ant-input']")));
		currentDate.click();
		
	}
	
	@Step("User Set the GAU to +30 mins of current date & time,  Method: {method} ")
	public String getSetGAUTime() {
		String time = TimeZone.getthirtyMinutesTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1")) {
			time = "0" + time;
		}
		
		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}
	
	
	@Step("Get Student Exception applied text in Student Details Overlay,  Method: {method} ")
	public String getExceptionAppliedText() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		WebElement ExceptionEle = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//i[contains(text(), '*This student has exceptions applied.')]")));
		
		return ExceptionEle.getText();
		
	}
	
	@Step("Get Time Limit Information in Student Details Overlay,  Method: {method} ")
	public String getTimeLimitInfo(String timeLimit) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);

		WebElement timeLimitEle_Part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='txt-overall meta-content-regular']/div/span[contains(text(),'This assignment has a time limit of ')]")));
		WebElement timeLimitEle_Part2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='txt-overall meta-content-regular']/div/b[contains(text(),'"+ timeLimit +" min')]")));
		
		return timeLimitEle_Part1.getText() + " " +timeLimitEle_Part2.getText();
		
	}
	
	@Step("User select the GAU Time,  Method: {method} ")
	public void setGAUTime(String time) {
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);
	}
	
	
	@Step("Get Disabled GAU Time,  Method: {method} ")
	public boolean GAUTime_IsDisabled() throws Exception {
		
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(DisabledGAUDate));
				DisabledGAUDate.isDisplayed();
				DisabledGAUTime.get(0).isDisplayed();
				DisabledGAUTime.get(1).isDisplayed();
				ExamSettingsInfo.get(0).isDisplayed();
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
		}
		
		return true;
	}
	
	
	@Step("Get Exam Settings Mode examSettingsMode_Ungraded Practice Info,  Method: {method} ")
	public boolean examSettingsMode_UngradedPracticeInfo() throws Exception {
		
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(DisabledUngradedPracticeOption));
				DisabledUngradedPracticeOption.isDisplayed();
				ExamSettingsInfo.get(1).isDisplayed();
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
		}
		
		return true;
	}
	
	@Step("Get Exam Settings Question Title Msg,  Method: {method} ")
	public String getExamSettings_QuestionTitleMsg() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(ExamSettings_QuestionTitleMsg));
		
		return ExamSettings_QuestionTitleMsg.getText();
	}
	
	
	@Step("Get all Question Attempts in Student Assignment page,  Method: {method} ")
	public List<String> getAllQuestionAttemptsList() {
		
		List<String> questionAttemptsList = new ArrayList<String>();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(QuestionTable));
		AllQuestions = QuestionTable.findElements(By.tagName("tr"));
		
		for (int i = 1; i <= AllQuestions.size(); i++) {

				WebElement questionAttempt = QuestionTable.findElement(By.xpath("//tr["+ i + "]/td[3]/div/select/option[@selected]"));
			
				questionAttemptsList.add(i-1, questionAttempt.getText());
			
		}
		
		return questionAttemptsList;
		
	}
	
	@Step("Get Exam Settings Mode Invalid Time Limit PopUp is  displayed?,  Method: {method} ")
	public boolean examSettingsMode_InvalidTimeLimitPopUp() throws Exception {
		
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOf(InvalidTimeLimitPopUp));
				InvalidTimeLimitPopUp.isDisplayed();
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
		}
		
		return true;
	}
	
	
	@Step("Get Exam Settings Invalid Time Limit Msg,  Method: {method} ")
	public String getExamSettings_InvalidTimeLimitMsg() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(InvalidTimeLimitMsg));
		
		return InvalidTimeLimitMsg.getText();
	}
	

	@Step("Get Exam Settings Mode Updated Invalid Time Limit is  displayed?,  Method: {method} ")
	public boolean examSettingsMode_UpdatedInvalidTimeLimit(int updatedTimeLimit) throws Exception {
		
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 25);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='inputTimeLimit' and @value='" + updatedTimeLimit + "']")));
				driver.findElement(By.xpath("//input[@class='inputTimeLimit' and @value='" + updatedTimeLimit + "']")).isDisplayed();
			
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
				return false;
		}
		
		return true;
	}
	
	
}


