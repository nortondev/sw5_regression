package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH21HW {

WebDriver driver;
Actions act;
StudentQuestionPlayer sqp;
	
	

	// Initializing Web Driver and PageFactory.
	public AssignmentCH21HW(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		act = new Actions(driver);
	}
	
	
	
	@Step("Drag and drop Correct image icons for question #1,  Method: {method} ")
	public void question1DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-407781760241251ddfae8c542f8efaff3e32e9377c07412_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-407781760241251ddfae8c542f8efaff3e32e9377c07412_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-407781760241251ddfae8c542f8efaff3e32e9377c07412_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-407781760241251ddfae8c542f8efaff3e32e9377c07412_")));
		                                           

		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[starts-with(@id,'categorySubContainer-1')]")));
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[starts-with(@id,'categorySubContainer-2')]")));
		WebElement dropContainer3Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[starts-with(@id,'categorySubContainer-3')]")));
		WebElement dropContainer4Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[starts-with(@id,'categorySubContainer-4')]")));
			
		ReusableMethods.scrollToElement(driver,
				By.id("item-answer-1-407781760241251ddfae8c542f8efaff3e32e9377c07412_"));

		// Enter Correct Answer
		// Dragged and dropped.
		ReusableMethods.moveToElement(driver, dragItem4Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem4Element, dropContainer1Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem2Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem2Element, dropContainer2Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem3Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem3Element, dropContainer3Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem1Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem1Element, dropContainer4Element).build().perform();
		
	}


	@Step("Drag and drop Correct image icons for question #4,  Method: {method} ")
	public void question4DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		// Element which needs to drag.
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-832061720641251ddfae8c542f8efaff3e32e9377c08986_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-832061720641251ddfae8c542f8efaff3e32e9377c08986_")));
		WebElement dragIte1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-832061720641251ddfae8c542f8efaff3e32e9377c08986_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-832061720641251ddfae8c542f8efaff3e32e9377c08986_")));
		                                           
		
		// Element on which need to drop.
		WebElement dropContainer1Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[@id='categorySubContainer-1-832061720641251ddfae8c542f8efaff3e32e9377c08986_']")));
		WebElement dropContainer2Element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//ul[@id='categorySubContainer-2-832061720641251ddfae8c542f8efaff3e32e9377c08986_']")));
		
		WebElement questionListElement = driver.findElement(
				By.xpath("//ul[@id='sortingPreviewAnswerItemList-832061720641251ddfae8c542f8efaff3e32e9377c08986_']"));
		
		ReusableMethods.scrollIntoView(driver, questionListElement);
		
		Thread.sleep(2000);
		//ReusableMethods.scrollToElement(driver, By.id("item-answer-1-832061720641251ddfae8c542f8efaff3e32e9377c08986_"));
		questionListElement.click();
		
		// Dragged and dropped.

		ReusableMethods.moveToElement(driver, dragIte1Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragIte1Element, dropContainer1Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem3Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem3Element, dropContainer1Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem2Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem2Element, dropContainer2Element).build().perform();

		ReusableMethods.moveToElement(driver, dragItem4Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem4Element, dropContainer2Element).build().perform();
		
	}
	
	
	@Step("Drag and drop Correct image icons for question #3,  Method: {method} ")
	public void question3DragAndDrop_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver,
				By.id("sortingPreviewAnswerItemList-8983620343182c2b3f0f36edc0765e4459cf60f01408254_"));
		
		// Element which needs to drag.
		WebElement dragItem1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-1-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-2-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-3-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem5Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem6Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dragItem7Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-7-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		                                           

		ReusableMethods.scrollToElement(driver, 
				By.id("answerCategoryTable-8983620343182c2b3f0f36edc0765e4459cf60f01408254_"));
		
		// Element on which need to drop.
		WebElement dropContainerMetal = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		WebElement dropContainerNonMetal = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-8983620343182c2b3f0f36edc0765e4459cf60f01408254_")));
		

		// Enter Correct Answer
		// Dragged and dropped.
		ReusableMethods.moveToElement(driver, dragItem1Element);
		Thread.sleep(2000);
		act.dragAndDrop(dragItem1Element, dropContainerNonMetal).build().perform();

		ReusableMethods.moveToElement(driver, dragItem2Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem2Element, dropContainerNonMetal).build().perform();
		act.dragAndDrop(dragItem2Element, dropContainerNonMetal)
		.release().build().perform();
		
		ReusableMethods.moveToElement(driver, dragItem3Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem3Element, dropContainerMetal).build().perform();
		act.dragAndDrop(dragItem3Element, dropContainerMetal)
		.release().build().perform();
		
		ReusableMethods.moveToElement(driver, dragItem4Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem4Element, dropContainerMetal).build().perform();
		act.dragAndDrop(dragItem4Element, dropContainerMetal)
		.release().build().perform();
		
		ReusableMethods.moveToElement(driver, dragItem5Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem5Element, dropContainerNonMetal).build().perform();
		act.dragAndDrop(dragItem5Element, dropContainerNonMetal)
		.release().build().perform();
		
		ReusableMethods.moveToElement(driver, dragItem6Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem6Element, dropContainerMetal).build().perform();
		act.dragAndDrop(dragItem6Element, dropContainerMetal)
		.release().build().perform();
		
		ReusableMethods.moveToElement(driver, dragItem7Element);
		Thread.sleep(2000);
		//act.dragAndDrop(dragItem7Element, dropContainerMetal).build().perform();
		act.dragAndDrop(dragItem7Element, dropContainerMetal)
		.release().build().perform();
		
	}
	
	@Step("Multi Select Answers for question #19,  Method: {method} ")
	public void question19MultiSelect_CorrectAnswer() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		ReusableMethods.scrollToElement(driver, By.id("wafer_ps_checkbox_div468713875941251ddfae8c542f8efaff3e32e9377c09185_"));
		
		WebElement item1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='1']")));
		
		WebElement item2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='3']")));
		
		WebElement item3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='5']")));
		
		WebElement item4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='everySingleOption multi-btn-withoutIndex']/input[@value='7']")));
		                                           
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item1Element);
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item2Element);
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item3Element);
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", item4Element);
		
	}

}
