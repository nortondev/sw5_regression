package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;

import ru.yandex.qatools.allure.annotations.Step;

public class CreateNewSS {
	WebDriver driver;
	SW5DLPPage SW5DLPpage;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "/html/body/div[6]")
	WebElement selectSSPopUp;

	// @FindBy(how = How.XPATH, using = ")
	@FindBy(how = How.XPATH, using = "/html/body/div[6]/div[3]/div/button")
	WebElement selectSSOkButton;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']//img")
	WebElement gearMenuIcon;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Manage Student Sets')]")
	WebElement manageStudentSet;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create New Student Set')]")
	WebElement createNewStudentSet;

	@FindBy(how = How.XPATH, using = "//input[@id='create_class_start_radio_copy']")
	WebElement createNewStudentSet_Copy;

	@FindBy(how = How.XPATH, using = "//select[@id='create_class_copy_select']")
	WebElement selectSourceStudentSet;

	@FindBy(how = How.XPATH, using = "//*[@id=\"create_class_copy_id\"]")
	WebElement otherInstructorSSID;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text' and contains(text(),'Next')]")
	WebElement createNewStudentSet_Next;

	@FindBy(how = How.XPATH, using = "//input[@id='new_student_set_title']")
	WebElement ssTitle;

	@FindBy(how = How.ID, using = "ddlcountry")
	WebElement ssCountry;
	
	@FindBy(how = How.ID, using = "new_student_set_state")
	public WebElement ssState;

	@FindBy(how = How.XPATH, using = "//input[@id='new_student_set_school_text_input']")
	WebElement ssSchool;

	@FindBy(how = How.XPATH, using = "//input[@id='new_student_set_start_date']")
	WebElement startDateWindow;

	@FindBy(how = How.XPATH, using = "//*[@id=\"ui-datepicker-div\"]/table/tbody")
	WebElement allStartDate;

	@FindBy(how = How.XPATH, using = "//input[@id='new_student_set_end_date']")
	WebElement endDateWindow;
	
	@FindBy(how = How.XPATH, using = "//div[@id='manage_student_sets_div']/h3")
	public WebElement clickoutside;

	@FindBy(how = How.XPATH, using = "//*[@id=\"ui-datepicker-div\"]/table/tbody")
	WebElement allEndDate;

	@FindBy(how = How.XPATH, using = "//input[@id='GAUincludeY']")
	WebElement gauDateIncludeY;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create Student Set')]")
	WebElement createSSButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]")
	WebElement ConfirmSSButton;

	@FindBy(how = How.XPATH, using = "//*[@id=\"student_set_data_table\"]/tbody/tr[1]/td[2]")
	WebElement ssConfirmation;

	@FindBy(how = How.XPATH, using = "/html/body/div[7]/div[3]/div/button/span")
	WebElement createConfirmButton;

	@FindBy(how = How.XPATH, using = "/html/body/div[7]/div[3]/div/button[3]/span")
	WebElement copyConfirmButton;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text' and contains(text(),'OK')]")
	WebElement okButton;

	// Initializing Web Driver and PageFactory.

	public CreateNewSS(WebDriver driver) {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	// Call to Json file to read test data for UI scripts.

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	com.google.gson.JsonObject jsonObj = readJsonObject.readUIJason();

	@Step("Navigate to Manage Student set,  Method: {method} ")
	public void manageStudentSet() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(3);

		try {
			wait.until(ExpectedConditions.visibilityOf(selectSSOkButton));
			boolean selectSSPopUpExist = selectSSOkButton.isDisplayed();

			if (selectSSPopUpExist == true) {
				selectSSOkButton.click();
			}
		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		Thread.sleep(3000);
		gearMenuIcon.click();

		wait.until(ExpectedConditions.visibilityOf(manageStudentSet));
		manageStudentSet.click();

	}

	@Step("Click on Create New Student set,  Method: {method} ")
	public void clickCreateNewStudentSet() throws Exception {

		Thread.sleep(3000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(createNewStudentSet).click().build().perform();

	}

	@Step("Create New Student set from Scratch,  Method: {method} ")
	public void createNewStudentSetFromScratch(String StudentSetTitle, String SchoolName,
			String StartDate, String EndDate) throws Exception {

		// Call to CSV data file to read test data
		// ReadCSVFile.readCSVData();

		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(createNewStudentSet_Next));
		createNewStudentSet_Next.click();

		wait.until(ExpectedConditions.visibilityOf(ssTitle));
		// ssTitle.sendKeys(ReadCSVFile.title);
		String guid = GetRandomId.randomAlphaNumeric(4).toLowerCase();
		guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
		ssTitle.sendKeys(StudentSetTitle + guid);

		wait.until(ExpectedConditions.visibilityOf(ssState));
		ssState.click();

		Thread.sleep(1000);
		Select drpCountry = new Select(ssCountry);
		//drpSSState.selectByValue(ReadCSVFile.state);
		drpCountry.selectByValue("USA");
		
		Thread.sleep(1000);
		Select drpState = new Select(ssState);
		drpState.selectByValue("USA_AZ");
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(ssSchool));
		// ssSchool.sendKeys(ReadCSVFile.school);
		ssSchool.sendKeys(SchoolName);

		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(startDateWindow));
		startDateWindow.click();

		String strStartDate[] = (StartDate.split(" ")[0]).split("/");

		List<WebElement> startDateColumns = allStartDate.findElements(By.tagName("td"));

		for (WebElement startDate : startDateColumns) {

			if (startDate.getText().equals(strStartDate[1])) {
				Actions act2 = new Actions(driver);
				act2.moveToElement(startDate).click().build().perform();
				break;
			}
		}

		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(endDateWindow));
		endDateWindow.click();

		String strEndDate[] = (EndDate.split(" ")[0]).split("/");

		List<WebElement> endDateColumns = allStartDate.findElements(By.tagName("td"));

		for (WebElement endDate : endDateColumns) {

			if (endDate.getText().equals(strEndDate[1])) {
				Actions act2 = new Actions(driver);
				act2.moveToElement(endDate).click().build().perform();
				break;
			}

		}

		wait.until(ExpectedConditions.visibilityOf(createSSButton));
		createSSButton.click();

	}

	@Step("Create New Student set Copying Student Set from Same Instructor,  Method: {method} ")
	public void createNewStudentSetWithCopySameInstructor(String SourceStudentSet) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		Thread.sleep(3000);
		createNewStudentSet_Copy.click();

		wait.until(ExpectedConditions.visibilityOf(selectSourceStudentSet));
		//selectSourceStudentSet.click();
		Thread.sleep(1000);
		//Select drpSourceSS = new Select(selectSourceStudentSet);
		//drpSourceSS.selectByVisibleText(SourceStudentSet);
		
		List<WebElement> optionElements = selectSourceStudentSet.
				findElements(By.tagName("option"));

		for (WebElement optionElement : optionElements) {
			if (optionElement.getText().contains(SourceStudentSet)) {
				System.out.println(optionElement.getText());
				optionElement.click();
				break;
			}
		}

		wait.until(ExpectedConditions.visibilityOf(createNewStudentSet_Next));
		createNewStudentSet_Next.click();

	}

	@Step("Create New Student set Copying Student Set from Other Instructor,  Method: {method} ")
	public void createNewStudentSetWithCopyOtherInstructor(String studentSetOption, String StudetnSetID)
			throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		Thread.sleep(3000);
		//wait.until(ExpectedConditions.visibilityOf(createNewStudentSet_Copy));
		createNewStudentSet_Copy.click();

		wait.until(ExpectedConditions.visibilityOf(selectSourceStudentSet));
		selectSourceStudentSet.click();
		Thread.sleep(1000);
		Select drpSourceSS = new Select(selectSourceStudentSet);
		drpSourceSS.selectByValue(studentSetOption);

		wait.until(ExpectedConditions.visibilityOf(otherInstructorSSID));
		otherInstructorSSID.sendKeys(StudetnSetID);

		wait.until(ExpectedConditions.visibilityOf(createNewStudentSet_Next));
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", createNewStudentSet_Next);
		createNewStudentSet_Next.click();

	}

	@Step("Enter Student Information to Copy New Student Set,  Method: {method} ")
	public void studentSetInformationCopy(String StudentSetTitle, String SchoolName,
			String StartDate, String EndDate) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(ssTitle));
		ssTitle.clear();
		ssTitle.sendKeys(StudentSetTitle);

		wait.until(ExpectedConditions.visibilityOf(ssCountry));
		ssState.click();
		//Keep the below lines of code as-is
		/*Thread.sleep(1000);
		Select drpSSState = new Select(ssState);
		drpSSState.selectByValue(SchoolState);*/
		
		Thread.sleep(1000);
		Select drpCountry = new Select(ssCountry);
		// drpSSState.selectByValue(ReadCSVFile.state);
		drpCountry.selectByValue("USA");
		
		
		Thread.sleep(1000);
		Select drpState = new Select(ssState);
		drpState.selectByValue("USA_AZ");

		wait.until(ExpectedConditions.visibilityOf(ssSchool));
		ssSchool.sendKeys(SchoolName);

		/* wait.until(ExpectedConditions.visibilityOf(startDateWindow));
		startDateWindow.click();

		String strStartDate[] = (StartDate.split(" ")[0]).split("/");

		List<WebElement> startDateColumns = allStartDate.findElements(By.tagName("td"));

		for (WebElement startDate : startDateColumns) {

			if (startDate.getText().equals(strStartDate[1])) {
				Actions act2 = new Actions(driver);
				act2.moveToElement(startDate).click().build().perform();
				break;
			}
		}

		wait.until(ExpectedConditions.visibilityOf(endDateWindow));
		endDateWindow.click();

		String strEndDate[] = (EndDate.split(" ")[0]).split("/");

		List<WebElement> endDateColumns = allStartDate.findElements(By.tagName("td"));

		for (WebElement endDate : endDateColumns) {

			if (endDate.getText().equals(strEndDate[1])) {
				Actions act2 = new Actions(driver);
				act2.moveToElement(endDate).click().build().perform();
				break;
			}
		} */
		
		startDateWindow.click();
		// Student_SelectDate.click();
		startDateWindow.sendKeys(GetDate.getCurrentDate());
		endDateWindow.click();
		// Student_SelectDate.click();
		endDateWindow.sendKeys(GetDate.getNextmonthDate());
		clickoutside.click();

		wait.until(ExpectedConditions.visibilityOf(gauDateIncludeY));
		gauDateIncludeY.click();

		wait.until(ExpectedConditions.visibilityOf(createSSButton));
		createSSButton.click();
		
		try {
			wait.until(ExpectedConditions.visibilityOf(ConfirmSSButton));
			boolean confirmButtonExist = ConfirmSSButton.isDisplayed();

			if (confirmButtonExist == true) {
				ConfirmSSButton.click();
			}
		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

	}

	@Step("Click Confirm button to create New Student Set,  Method: {method} ")
	public String confirmNewStudentSet() throws Exception {

		Thread.sleep(5000);

		try {
			boolean confirmButtonExist = copyConfirmButton.isDisplayed();

			if (confirmButtonExist == true) {
				copyConfirmButton.click();
			}
		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.elementToBeClickable(ssConfirmation));
		String studentSetConfirmation = ssConfirmation.getText();
		//System.out.println(ssConfirmation);

		Thread.sleep(5000);

		try {
			boolean okButtonExist = okButton.isDisplayed();

			if (okButtonExist == true) {
				okButton.click();
			}
		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		return studentSetConfirmation;

	}

	@Step("Close Manage Student Set Pop-up,  Method: {method} ")
	public void closeManageStudentSetPopUp() throws Exception {

		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("$('a[class^=\"link-close\"]').click()");
	}

}
