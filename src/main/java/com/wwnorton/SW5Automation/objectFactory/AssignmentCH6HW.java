package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH6HW {

	WebDriver driver;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH6HW() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("Enter Correct Answer for question #1,  Method: {method} ")
	public void enterQuestion1_Correct() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Substances Part 1
		WebElement solid = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-4-306744134486a110cd3be354d036b9df8df736279205947_")));
		WebElement liquid = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-306744134486a110cd3be354d036b9df8df736279205947_")));
		WebElement gas = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-306744134486a110cd3be354d036b9df8df736279205947_")));

		// Drag type where Substance need to be dropped, Part 1.
		WebElement solidContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-306744134486a110cd3be354d036b9df8df736279205947_")));
		WebElement liquidContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-306744134486a110cd3be354d036b9df8df736279205947_")));
		WebElement gasContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-3-306744134486a110cd3be354d036b9df8df736279205947_")));

		// Drag and drop each item to its type respectively
		act.dragAndDrop(solid, solidContainer).build().perform();
		act.dragAndDrop(liquid, liquidContainer).build().perform();
		act.dragAndDrop(gas, gasContainer).build().perform();

		// Substances Part 2
		WebElement carbonDioxide1 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-5-306931982086a110cd3be354d036b9df8df736279209764_")));
		WebElement carbonDioxide2 = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-6-306931982086a110cd3be354d036b9df8df736279209764_")));
		WebElement nitogen = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("item-answer-7-306931982086a110cd3be354d036b9df8df736279209764_")));

		// Drag type where Substance need to be dropped, Part 2.
		WebElement carbonDioxideContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-1-306931982086a110cd3be354d036b9df8df736279209764_")));
		WebElement nitrogenContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id("categorySubContainer-2-306931982086a110cd3be354d036b9df8df736279209764_")));

		// Drag and drop each item to its type respectively
		act.dragAndDrop(carbonDioxide1, carbonDioxideContainer).build().perform();
		act.dragAndDrop(carbonDioxide2, carbonDioxideContainer).build().perform();
		act.dragAndDrop(nitogen, nitrogenContainer).build().perform();

	}

	public void enterQuestion2_Incorrect() throws Exception {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");

		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement incorrectAnswer = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'The helium atoms in the balloon would move faster.')]][label/span/input[not(contains(@disabled,'disabled'))]]//input")));

		Actions action = new Actions(driver);
		action.moveToElement(incorrectAnswer);

		// Enter InCorrect Answer
		incorrectAnswer.click();
	}

	public void enterQuestion3_Incorrect() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);

		// Using Action class for drag and drop.
		Actions act = new Actions(driver);

		// Labeling Options
		WebElement valueA = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-1")));
		WebElement valueB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-2")));
		WebElement valueC = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-3")));
		WebElement valueD = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("answer-4")));

		// Labeling Question
		WebElement closedEndValveOpen = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-07253904703c4a0055bb2da4257d6df04c95ce0b5904343_-1")));
		WebElement openEndValveOpen = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-07253904703c4a0055bb2da4257d6df04c95ce0b5904343_-2")));
		WebElement openEndValveClosed = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-07253904703c4a0055bb2da4257d6df04c95ce0b5904343_-3")));
		WebElement closedEndValveClosed = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("targGroup-07253904703c4a0055bb2da4257d6df04c95ce0b5904343_-4")));

		// Drag and drop each item to its type respectively
		act.dragAndDrop(valueA, closedEndValveOpen).build().perform();
		act.dragAndDrop(valueB, openEndValveClosed).build().perform();

		// Interchanging labeling of closedEndValveClosed and openEndValveOpen to make
		// it incorrect
		act.dragAndDrop(valueD, closedEndValveClosed).build().perform();
		act.dragAndDrop(valueC, openEndValveOpen).build().perform();
	}
}
