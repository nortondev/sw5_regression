package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CustomAssignmentAnswers {
	WebDriver driver;
	
	
	@FindBy(how = How.ID, using = "wafer_ps_number650905810446cdeba34ad9dd3895e73d3972b368c504924_")
	public WebElement Question1HydrogenAtomsTextbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number652790153846cdeba34ad9dd3895e73d3972b368c500388_")
	public WebElement Question1HydrogenMoleculesTextbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number4379224464182c2b3f0f36edc0765e4459cf60f01404722_")
	public WebElement Question1HeliumAtomsTextbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number4379299905182c2b3f0f36edc0765e4459cf60f01407961_")
	public WebElement Question1HeliumMoleculesTextbox;
	
	@FindBy(how = How.XPATH, using = "//span[@class='isPcDev']")
	public WebElement Question1completedText;
	
	@FindBy(how = How.ID, using = "wafer_ps_number527410655741251ddfae8c542f8efaff3e32e9377c07691_")
	public WebElement Question2Textbox;
	
	@FindBy(how = How.ID, using ="wafer_ps_number921949568786a110cd3be354d036b9df8df736279202249_")
	public WebElement Question3CarbonAtomsTextbox;
	
	@FindBy(how = How.ID, using ="wafer_ps_number921986091886a110cd3be354d036b9df8df736279200939_")
	public WebElement Question3HydrogenAtomsTextbox;
	
	@FindBy(how = How.ID, using ="wafer_ps_number922007721786a110cd3be354d036b9df8df736279208720_")
	public WebElement Question3OxygenAtomsTextbox;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279202403_')]/span")
	public WebElement H2OPart1Listbox;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279209271_')]/span")
	public WebElement H2OPart2Listbox;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@aria-owns,'a110cd3be354d036b9df8df736279207782_')]/span")
	public WebElement H2OPart3Listbox;
	
		
	@FindBy(how = How.ID, using = "wafer_ps_select9129382476bdeaa6bb7f086003f078f9de2221d7c106789_")
	public WebElement Question6Part1ListBox;
	
	@FindBy(how = How.ID, using = "wafer_ps_select9129884331bdeaa6bb7f086003f078f9de2221d7c108827_")
	public WebElement Question6Part2ListBox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number61568850700cd9d0f1ccb4b38dfecf49186b4546db06374_")
	public WebElement Question10Part1Textbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number61571384060cd9d0f1ccb4b38dfecf49186b4546db09706_")
	public WebElement Question10Part2Textbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number622849807341251ddfae8c542f8efaff3e32e9377c04209_")
	public WebElement Question11Part1Textbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number492173838141251ddfae8c542f8efaff3e32e9377c01081_")
	public WebElement Question11Part2Textbox;
	
	@FindBy(how = How.ID, using = "wafer_ps_number597004414341251ddfae8c542f8efaff3e32e9377c09848_")
	public WebElement Question12Part1Textbox;
	@FindBy(how = How.ID, using = "wafer_ps_number597015445641251ddfae8c542f8efaff3e32e9377c00612_")
	public WebElement Question12Part2Textbox;
	@FindBy(how = How.ID, using = "wafer_ps_number597027358741251ddfae8c542f8efaff3e32e9377c07065_")
	public WebElement Question12Part3Textbox;
	@FindBy(how = How.ID, using = "wafer_ps_number597035155041251ddfae8c542f8efaff3e32e9377c05743_")
	public WebElement Question12Part4Textbox;
	
	@FindBy(how = How.XPATH, using = "//div[@id='part44285']")
	public WebElement Question13_Part1;
	
	@FindBy(how = How.XPATH, using = "//span[@id='wafer_ps_radio_div1685475010f17398bec3a73cf1a643545477bb924903527_']/span/label/span/input[@type='radio' and @value='3']")
	public WebElement Question13_Part1_RadioOption3;
	
	@FindBy(how = How.XPATH, using = "//span[@id='wafer_ps_radio_div1462560490f17398bec3a73cf1a643545477bb924901389_']/span/label/span/input[@type='radio' and @value='1']")
	public WebElement Question13_Part2_RadioOption1;
	
	
	public CustomAssignmentAnswers() {
		// TODO Auto-generated constructor stub
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
		
	}


	@Step("Question 1 Answers,   Method: {method}")
	public void question1Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number650905810446cdeba34ad9dd3895e73d3972b368c504924_")));
		Question1HydrogenAtomsTextbox.sendKeys("0");
		Question1HydrogenMoleculesTextbox.sendKeys("4");
		Question1HeliumAtomsTextbox.sendKeys("5");
		Question1HeliumMoleculesTextbox.sendKeys("0");
		
		
	}
	
	@Step("Question 1 Incorrect Answers,   Method: {method}")
	public void question1_IncorrectAnswers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number650905810446cdeba34ad9dd3895e73d3972b368c504924_")));
		Question1HydrogenAtomsTextbox.sendKeys("0");
		Question1HydrogenMoleculesTextbox.sendKeys("4");
		Question1HeliumAtomsTextbox.sendKeys("0");
		Question1HeliumMoleculesTextbox.sendKeys("5");
		
		
	}
	
	@Step("Question 2 Answer,   Method: {method}")
	public void question2Answer(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number527410655741251ddfae8c542f8efaff3e32e9377c07691_")));
		Question2Textbox.clear();
		Question2Textbox.sendKeys("150");
				
	}
	
	@Step("Question 2 Incorrect Answer,   Method: {method}")
	public void question2_IncorrectAnswer(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number527410655741251ddfae8c542f8efaff3e32e9377c07691_")));
		Question2Textbox.sendKeys("100");
				
	}
	
	@Step("Question 3 Answers,   Method: {method}")
	public void question3Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number921949568786a110cd3be354d036b9df8df736279202249_")));
		Question3CarbonAtomsTextbox.sendKeys("3");
		Question3HydrogenAtomsTextbox.sendKeys("8");
		Question3OxygenAtomsTextbox.sendKeys("1");
				
	}
	
	@Step("Question 3 Incorrect Answers,   Method: {method}")
	public void question3_IncorrectAnswers(){
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number921949568786a110cd3be354d036b9df8df736279202249_")));
		Question3CarbonAtomsTextbox.sendKeys("3");
		Question3HydrogenAtomsTextbox.sendKeys("4");
		Question3OxygenAtomsTextbox.sendKeys("2");
				
	}
	
	@Step("select the Question 4 Correct answer from the list box,   Method: {method}")
	public void question4Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(H2OPart1Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart1Listbox);
		//H2OPart1Listbox.click();
		WebElement listbox = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279202403_')]/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("vaporization"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		ReusableMethods.scrollToBottom(driver);
		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until(ExpectedConditions.elementToBeClickable(H2OPart2Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart2Listbox);
		//H2OPart2Listbox.click();
		WebElement listbox2 = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279209271_')]/ul"));
		List<WebElement> options2 = listbox2.findElements(By.tagName("li"));
		for (WebElement option2 : options2)
		{
		    if (option2.getText().equalsIgnoreCase("deposition"))
		    {
		        option2.click(); // click the desired option
		        break;
		    }
		}
		WebDriverWait wait2 = new WebDriverWait(driver, 10);
		wait2.until(ExpectedConditions.elementToBeClickable(H2OPart3Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart3Listbox);
		//H2OPart3Listbox.click();
		WebElement listbox3 = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279207782_')]/ul"));
		List<WebElement> options3 = listbox3.findElements(By.tagName("li"));
		for (WebElement option3 : options3)
		{
		    if (option3.getText().equalsIgnoreCase("melting"))
		    {
		        option3.click(); // click the desired option
		        break;
		    }
		}
		
		
	}
	
	
	@Step("select the Question 4 Incorrect answer from the list box,   Method: {method}")
	public void question4_IncorrectAnswers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(H2OPart1Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart1Listbox);
		//H2OPart1Listbox.click();
		WebElement listbox = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279202403_')]/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("deposition"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		ReusableMethods.scrollToBottom(driver);
		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until(ExpectedConditions.elementToBeClickable(H2OPart2Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart2Listbox);
		//H2OPart2Listbox.click();
		WebElement listbox2 = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279209271_')]/ul"));
		List<WebElement> options2 = listbox2.findElements(By.tagName("li"));
		for (WebElement option2 : options2)
		{
		    if (option2.getText().equalsIgnoreCase("melting"))
		    {
		        option2.click(); // click the desired option
		        break;
		    }
		}
		WebDriverWait wait2 = new WebDriverWait(driver, 10);
		wait2.until(ExpectedConditions.elementToBeClickable(H2OPart3Listbox));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", H2OPart3Listbox);
		//H2OPart3Listbox.click();
		WebElement listbox3 = driver.findElement(By.xpath("//div[contains(@id,'a110cd3be354d036b9df8df736279207782_')]/ul"));
		List<WebElement> options3 = listbox3.findElements(By.tagName("li"));
		for (WebElement option3 : options3)
		{
		    if (option3.getText().equalsIgnoreCase("vaporization"))
		    {
		        option3.click(); // click the desired option
		        break;
		    }
		}

		
	}
	

	@Step("select the Question 5 answer,   Method: {method}")
	public void question5Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//input[@type='checkbox']")));
		Actions act = new Actions(driver);
		WebElement elementPart1=driver.findElement(By.id("part106454"));
	    ReusableMethods.scrollIntoView(driver, elementPart1);
		WebElement answer1 = driver.findElement(By.xpath("//span[@class='everySingleOption multiple-option-text'][contains(text(),'Size increases as we move from top to bottom down a group.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		act.moveToElement(answer1);
		answer1.click();
		WebElement answer2 = driver.findElement(By.xpath("//span[@class='everySingleOption multiple-option-text'][contains(text(),'Size decreases as we move left to right across a period.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		act.moveToElement(answer2);
		answer2.click();
		ReusableMethods.scrollToBottom(driver);
		WebElement answer3 = driver.findElement(By.xpath("//span[contains(text(),'Anions are always larger than the atoms from which they form.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer3.click();
		
		WebElement answer4 = driver.findElement(By.xpath("//span[contains(text(),'Cations are always smaller than the atom from which they form.')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer4.click();

	} 



	
	@Step("select the Question 6 answer,   Method: {method}")
	public void question6Answers(){
		ReusableMethods.scrollToBottom(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("wafer_ps_select9129382476bdeaa6bb7f086003f078f9de2221d7c106789_")));
		Question6Part1ListBox.click();
		WebElement listbox = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div9129382476bdeaa6bb7f086003f078f9de2221d7c106789_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("Condition A"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		
		Question6Part2ListBox.click();
		WebElement listbox1 = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div9129884331bdeaa6bb7f086003f078f9de2221d7c108827_']/ul"));
		List<WebElement> options1 = listbox1.findElements(By.tagName("li"));
		for (WebElement option : options1)
		{
		    if (option.getText().equalsIgnoreCase("Condition B"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		
	}
	
	@Step("select the Question 7 answer,   Method: {method}")
	public void question7Answers() throws InterruptedException{
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Ready-to-eat Jell-O')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']")));
		WebElement answer7checkbox1 = driver.findElement(By.xpath("//span[contains(text(),'Ready-to-eat Jell-O')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer7checkbox1.click();
		WebElement answer7checkbox2 = driver.findElement(By.xpath("//span[contains(text(),'Sea salt')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='checkbox']"));
		answer7checkbox2.click();
	}
	
	
	@Step("select the Question 8 answer,   Method: {method}")
	public void question8Answers() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		Actions act = new Actions(driver);
		WebElement elementPart1=driver.findElement(By.id("part109481"));
	    ReusableMethods.scrollIntoView(driver, elementPart1);
		WebElement correctAnswerPart1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'11, 11, 13')]]//input[@value='1' and not(@disabled)]")));
		//WebElement answer8Radiobox1 =driver.findElement(By.xpath("//span[contains(text(),'11, 11, 13')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']"));
		act.moveToElement(correctAnswerPart1);
		correctAnswerPart1.click();
		ReusableMethods.scrollToBottom(driver);
		WebElement correctAnswerPart2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'5, 6, 6')]]//input[@value='1' and not(@disabled)]")));
		act.moveToElement(correctAnswerPart2);
		correctAnswerPart2.click();
		
		WebElement correctAnswerPart3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'(b) and (c)')]]//input[@value='1' and not(@disabled)]")));
		act.moveToElement(correctAnswerPart3);
		
		correctAnswerPart3.click();
	}
	
	/*@Step("select the Question 9 answer,   Method: {method}")
	public void question9Answers() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@id,'36e609193adb47540a6a417fab6b10c401551_')]/span/label/span[contains(text(),'Pure element')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']")));
		ReusableMethods.scrollToElement(driver, By.xpath("//div[@class='question-part-title']/h2[contains(text(),'Part 1')]"));
		WebElement answer9Radiobox1 =driver.findElement(By.xpath("//span[contains(@id,'36e609193adb47540a6a417fab6b10c401551_')]/span/label/span[contains(text(),'Pure element')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']"));
		wait.until(ExpectedConditions.elementToBeClickable(answer9Radiobox1));
		executor.executeScript(
				"arguments[0].click();", answer9Radiobox1);

		ReusableMethods.scrollToBottom(driver);
		WebElement answer9Radiobox2=driver.findElement(By.xpath("//span[contains(@id,'36e609193adb47540a6a417fab6b10c402845_')]/span/label/span[contains(text(),'Mixture of elements')]/preceding-sibling::span[@class='everySingleOption multi-btn-withIndex']/input[@type='radio']"));
		executor.executeScript(
				"arguments[0].click();", answer9Radiobox2);
		//answer9Radiobox2.click();
		
	}*/
	
	@Step("select the Question 9 answer,   Method: {method}")
	public void question9Answers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input")));
		Actions act = new Actions(driver);
		WebElement elementPart1=driver.findElement(By.id("part215596"));
	    ReusableMethods.scrollIntoView(driver, elementPart1);
		WebElement answer1 = driver.findElement(By.xpath("//span[contains(@id,'wafer_ps_radio_div8670644657')]/span/label/span/input[@value='3']"));
		act.moveToElement(answer1);
		answer1.click();
		WebElement elementPart2=driver.findElement(By.id("part215597"));
	    ReusableMethods.scrollIntoView(driver, elementPart2);
		WebElement answer2 = driver.findElement(By.xpath("//span[contains(@id,'wafer_ps_radio_div8810559658')]/span/label/span/input[@value='1']"));
		act.moveToElement(answer2);
		answer2.click();
		
	}
	
	@Step("select the Question 10 answer,   Method: {method}")
	public void question10Answers(){
		ReusableMethods.scrollToBottom(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number61568850700cd9d0f1ccb4b38dfecf49186b4546db06374_")));
		WebElement question10Element = driver.findElement(By.id("wafer_ps_number61568850700cd9d0f1ccb4b38dfecf49186b4546db06374_"));
		ReusableMethods.scrollIntoView(driver, question10Element);
		Question10Part1Textbox.click();
		Question10Part1Textbox.sendKeys("2.5");
		ReusableMethods.scrollToBottom(driver);
		Question10Part2Textbox.click();
		Question10Part2Textbox.sendKeys("720");
	}
		
	@Step("select the Question 11 answer,   Method: {method}")
	public void question11Answers() throws InterruptedException{
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number622849807341251ddfae8c542f8efaff3e32e9377c04209_")));
		Question11Part1Textbox.click();
		Question11Part1Textbox.sendKeys("131.02");
		
	}
	
	@Step("select the Question 12 answer,   Method: {method}")
	public void question12Answers() throws InterruptedException{
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("wafer_ps_number597004414341251ddfae8c542f8efaff3e32e9377c09848_")));
		Question12Part1Textbox.sendKeys("40");
		Question12Part2Textbox.sendKeys("-387");
		ReusableMethods.scrollToBottom(driver);
		Question12Part3Textbox.sendKeys("396");
		Question12Part4Textbox.sendKeys("253");
	}
	
	@Step("select the Question 13 answer,   Method: {method}")
	public void question13Answers() throws InterruptedException{
		Thread.sleep(3000);
		
		try {
			
				boolean Question01Exist = question13_Part1_Exist();
				
				if(Question01Exist == true) {
						Question13_Part1_RadioOption3.click();
					}
				else {
						Question13_Part2_RadioOption1.click();
				}	
		} catch(Exception e){
				e.getMessage();
			}
		
	}
	
	public boolean question13_Part1_Exist() throws InterruptedException{
		Thread.sleep(3000);
		boolean Question01Exist = false;
		try {
			
				Question01Exist = Question13_Part1.isDisplayed();
				
		} catch(Exception e){
				e.getMessage();
			}
		
		return Question01Exist;
	}
	
	
	@Step("Answer Custom Multi Parts multi Module Question,   Method: {method}")
	public void CustomMultiParts_MultiModuleQuestionAnswers(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement multiplePart2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='question-part-title']/h2[text()='Part 2']")));
		
		ReusableMethods.scrollIntoView(driver, multiplePart2);
		
		WebElement questionPart2 = driver.findElement(
				By.xpath("//input[@type = 'text']"));
		    
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionPart2);
		questionPart2.sendKeys("144");
		
		WebElement multiplePart3 = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//span[contains(@id,'wafer_ps_radio_')]")));
		
		WebElement questionPart3 = driver.findElement(
				By.xpath("//span[@class='everySingleOption multi-btn-withIndex']/input[@value='2']"));
		
		ReusableMethods.scrollIntoView(driver, multiplePart3);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", questionPart3);
		
	}
	
}
