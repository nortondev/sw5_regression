package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH1HW {

	WebDriver driver;

	// Finding Web Elements on SW5 DLP page using PageFactory.

	@FindBy(how = How.XPATH, using = "//span[@class='txt-headtitle assignmentNameSpan body-content']")
	public WebElement AssignmentTitle;

	@FindBy(how = How.XPATH, using = "//div[class='head-box']/div[@class='head-dash']/div[@class='head-duedate head-dash-list']/div[@class='head-big-txt']")
	public WebElement HeaderDueDate;

	@FindBy(how = How.XPATH, using = "//div[class='question-info-box']/span[@class='question-info-text']")
	public WebElement QuestionInfoText;

	@FindBy(how = How.XPATH, using = "//h1[span[@class='question-no'] and span[@class='question-title'] and span[@class='question-point']]")
	public WebElement QuestionHeaderText;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-b']")
	public WebElement QuestionTextCurrent;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/span[@class='page-num-s']")
	public WebElement QuestionTextTotal;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-left icon-page prev-question']")
	public WebElement QuestionPrevious;

	@FindBy(how = How.XPATH, using = "//div[@class='question-page']/button[@class='glyphicons glyphicons-chevron-right icon-page next-question']")
	public WebElement QuestionNext;

	@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
	public WebElement SubmitAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-step']/span[@class='question-step-txt']")
	public WebElement QuestionFooterLeftText;

	@FindBy(how = How.XPATH, using = "//div[@class='footer']/div[@class='footer-box']/div[@class='question-page']")
	private WebElement QuestionFooterMiddleText;

	// Initializing Web Driver and PageFactory.
	public AssignmentCH1HW(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public boolean submitAnswerDisplayed() {
		try {
			if (SubmitAnswer.isDisplayed())
				return true;
			else
				return false;
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}

	public String getQuestionFooterMiddleText() {
		String fmt = null;

		List<WebElement> texts = QuestionFooterMiddleText.findElements(By.tagName("span"));

		for (WebElement text : texts)
			fmt += text.getText();

		return fmt;
	}

	@Step("Click on next question,  Method: {method} ")
	public void ClickOnNextQuestion() throws Exception {
		try {
			if (QuestionNext.isEnabled()) {
				QuestionNext.click();
				Thread.sleep(5000);
			}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Click on Previous question,  Method: {method} ")
	public void ClickOnPreviousQuestion() throws Exception {
		try {
			if (QuestionPrevious.isEnabled()) {
				QuestionPrevious.click();
				Thread.sleep(5000);
			}
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public void submitQuestion1_Correct() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);

			// Elements / compounds
			/*WebElement helium = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-7-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement nitrogen = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-4-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement gold = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-3-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement ethanol = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-5-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement sodiumChloride = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-2-031257714086a110cd3be354d036b9df8df736279209443_")));*/
			
			WebElement helium = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-7-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement nitrogen = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-8-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement gold = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-3-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement ethanol = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-5-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement sodiumChloride = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("itemContent-2-031257714086a110cd3be354d036b9df8df736279209443_")));

			// Drag type where element/compound need to be dropped.
			WebElement elementContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("categorySubContainer-1-031257714086a110cd3be354d036b9df8df736279209443_")));
			WebElement coumpoundContainer = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.id("categorySubContainer-2-031257714086a110cd3be354d036b9df8df736279209443_")));

			// Using Action class for drag and drop.
			Actions act = new Actions(driver);

			// Drag and drop each item to its type respectively
			act.dragAndDrop(helium, elementContainer).build().perform();
			act.dragAndDrop(nitrogen, elementContainer).build().perform();
			act.dragAndDrop(gold, elementContainer).build().perform();

			act.dragAndDrop(ethanol, coumpoundContainer).build().perform();
			act.dragAndDrop(sodiumChloride, coumpoundContainer).build().perform();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	public void Question2Answer_Correct() {
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 50);

				ReusableMethods.scrollToElement(driver, 
						By.id("answerItemList-309014000486a110cd3be354d036b9df8df736279204600_"));
			
				WebElement glycine_part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-1-309014000486a110cd3be354d036b9df8df736279204600_")));
				WebElement propane_part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-4-309014000486a110cd3be354d036b9df8df736279204600_")));
				WebElement H2O2_part1 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-3-309014000486a110cd3be354d036b9df8df736279204600_")));
				
				ReusableMethods.scrollToElement(driver, 
						By.id("answerSortedItemList-309014000486a110cd3be354d036b9df8df736279204600_"));
	
				// Drag type where element/compound need to be dropped.
				List<WebElement> itemContainer_1 = driver.findElements(By.xpath("//li[@siteorder='1']"));
				List<WebElement> itemContainer_2 = driver.findElements(By.xpath("//li[@siteorder='2']"));
				List<WebElement> itemContainer_3 = driver.findElements(By.xpath("//li[@siteorder='3']"));
	
				// Using Action class for drag and drop.
				Actions act = new Actions(driver);
	
				// Drag and drop each item to its type respectively
				ReusableMethods.moveToElement(driver, H2O2_part1);
				act.dragAndDrop(H2O2_part1, itemContainer_1.get(0)).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, glycine_part1);
				act.dragAndDrop(glycine_part1, itemContainer_2.get(0)).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, propane_part1);
				act.dragAndDrop(propane_part1, itemContainer_3.get(0)).build().perform();
				
				
				ReusableMethods.scrollToElement(driver, 
						By.id("answerItemList-309038024486a110cd3be354d036b9df8df736279202265_"));
				
				Thread.sleep(2000);
				WebElement glycine_part2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-1-309038024486a110cd3be354d036b9df8df736279202265_")));
				WebElement propane_part2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-4-309038024486a110cd3be354d036b9df8df736279202265_")));
				WebElement H2O2_part2 = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-3-309038024486a110cd3be354d036b9df8df736279202265_")));
				
				
				// Drag and drop each item to its type respectively
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, propane_part2);
				act.dragAndDrop(propane_part2, itemContainer_1.get(1)).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, H2O2_part2);
				act.dragAndDrop(H2O2_part2, itemContainer_2.get(1)).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, glycine_part2);
				act.dragAndDrop(glycine_part2, itemContainer_3.get(1)).build().perform();
	
				
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
	}
	
	public void Question4Answer_Correct() {
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 50);

				ReusableMethods.scrollToElement(driver, 
						By.id("sortingPreviewAnswerItemList-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_"));
			
				WebElement gasoline = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-10-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement water = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-11-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement forensic = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-12-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement dust = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-7-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement coffee = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-2-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				
				ReusableMethods.scrollToElement(driver, 
						By.id("answerCategoryTable-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_"));
	
				// Drag type where element/compound need to be dropped.
				WebElement filtration = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-1-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement distillation = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-2-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
				WebElement chromatography = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-3-6875260421bdeaa6bb7f086003f078f9de2221d7c101047_")));
	
				// Using Action class for drag and drop.
				Actions act = new Actions(driver);
	
				// Drag and drop each item to its type respectively
				ReusableMethods.moveToElement(driver, dust);
				act.dragAndDrop(dust, filtration).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, gasoline);
				act.dragAndDrop(gasoline, distillation).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, water);
				act.dragAndDrop(water, distillation).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, forensic);
				act.dragAndDrop(forensic, chromatography).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, coffee);
				act.dragAndDrop(coffee, chromatography).build().perform();
	
				
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
	}

	public void submitQuestion2_Partial() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement part1Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number921949568786a110cd3be354d036b9df8df736279202249_")));
		WebElement part2Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number921986091886a110cd3be354d036b9df8df736279200939_")));
		WebElement part3Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("wafer_ps_number922007721786a110cd3be354d036b9df8df736279208720_")));
		/*WebElement part4Element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@id='wafer_ps_chequation_div522892334986a110cd3be354d036b9df8df736279205577_']/div[@id='522892334986a110cd3be354d036b9df8df736279205577_']/span[@class='mq-root-block']/span")));*/
		// Enter InCorrect Answer
		part1Element.clear();
		part2Element.clear();
		part3Element.clear();
		//part4Element.clear();
        ReusableMethods.scrollIntoView(driver, part1Element);
		part1Element.sendKeys("3");
		ReusableMethods.scrollIntoView(driver, part2Element);
		part2Element.sendKeys("2");
		ReusableMethods.scrollIntoView(driver, part3Element);
		part3Element.sendKeys("1");
	/*	ReusableMethods.scrollIntoView(driver, part4Element);
		part4Element.click();*/
		/*Actions action = new  Actions(driver); 
		WebElement enterFormualText =driver.findElement(By.xpath("//span[@class='mq-root-block mq-hasCursor']/span"));
		action.moveToElement(enterFormualText);
		action.click();
		action.sendKeys("C3H8O");
		action.build().perform();*/
	}
}
