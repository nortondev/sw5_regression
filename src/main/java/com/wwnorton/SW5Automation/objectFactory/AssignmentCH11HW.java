package com.wwnorton.SW5Automation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.SW5Automation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH11HW {
	WebDriver driver;
	
	
	// Finding Web Elements on SW5 Question player using PageFactory.

		@FindBy(how = How.XPATH, using = "//div[@aria-owns='wafer_ps_select_option_div9240007756182c2b3f0f36edc0765e4459cf60f01401646_']/span")
		public WebElement solventAnswer1;
		
		@FindBy(how = How.XPATH, using = "//div[@aria-owns='wafer_ps_select_option_div9240167792182c2b3f0f36edc0765e4459cf60f01404137_']/span")
		public WebElement solventAnswer2;
		
		@FindBy(how = How.XPATH, using = "//div[@aria-owns='wafer_ps_select_option_div9240314036182c2b3f0f36edc0765e4459cf60f01405748_']/span")
		public WebElement solventAnswer3;
		
		@FindBy(how = How.XPATH, using = "//div[@class='modal-footer']/button[contains(text(),'YES')]")
		public WebElement YesButton;
		@FindBy(how = How.XPATH, using = "//button[@id='submitBtn']")
		public WebElement SubmitAnswer;
		
		@FindBy(how = How.XPATH,using = "//input[@type='text'][contains(@id,'')]")
		public WebElement Q10textbox;
		
		@FindBy(how = How.ID,using = "answer-1")
		public WebElement answer1A;
		@FindBy(how = How.ID,using = "answer-2")
		public WebElement answer1B;
		@FindBy(how = How.ID,using = "answer-3")
		public WebElement answer1C;
		@FindBy(how = How.ID,using = "answer-4")
		public WebElement answer1D;
		@FindBy(how = How.ID,using = "answer-5")
		public WebElement answer1E;
		@FindBy(how = How.ID,using = "answer-6")
		public WebElement answer1F;
		
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-1")
		public WebElement tragetID1;
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-2")
		public WebElement tragetID2;
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-3")
		public WebElement tragetID3;
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-4")
		public WebElement tragetID4;
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-5")
		public WebElement tragetID5;
		
		@FindBy(how = How.ID,using = "targetIconUse-74391031187571ef78727aebc6bbfca0410ae33b8505997_-6")
		public WebElement tragetID6;
		
	public AssignmentCH11HW(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	public void waitForElementPresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
		
	}
	@Step("Waitfor Submit Answer Button is displayed,   Method: {method}")
	public boolean submitAnswerDisplayed() {
		try {
			if (SubmitAnswer.isDisplayed())
				return true;
			else
				return false;
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
		return false;
	}
	@Step("select the first answer from the list box,   Method: {method}")
	public void solventAnswer1(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(solventAnswer1));
		solventAnswer1.click();
		WebElement listbox = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div9240007756182c2b3f0f36edc0765e4459cf60f01401646_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("right"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		
	}
	@Step("select the second answer from the list box,   Method: {method}")
	public void solventAnswer2(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(solventAnswer2));
		solventAnswer2.click();
		WebElement listbox = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div9240167792182c2b3f0f36edc0765e4459cf60f01404137_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("left"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
			
	}
	@Step("select the third answer from the list box,   Method: {method}")
	public void solventAnswer3(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(solventAnswer3));
		solventAnswer3.click();
		WebElement listbox = driver.findElement(By.xpath("//div[@id='wafer_ps_select_option_div9240314036182c2b3f0f36edc0765e4459cf60f01405748_']/ul"));
		List<WebElement> options = listbox.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
		    if (option.getText().equalsIgnoreCase("right"))
		    {
		        option.click(); // click the desired option
		        break;
		    }
		}
		
		
	}
	
	@Step("Submit the Answer for DragDrop for Question4,  Method: {method} ")
	public void submitAnswerdragdropCH11HW() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(answer1A));
		// Using Action class for drag and drop.
		Actions act = new Actions(driver);
		Thread.sleep(1000);
		// Drag and drop each item to its type respectively
		act.dragAndDrop(answer1A, tragetID4).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1B, tragetID2).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1C, tragetID3).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1D, tragetID1).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1E, tragetID6).build().perform();
		Thread.sleep(1000);
		act.dragAndDrop(answer1F, tragetID5).build().perform();
	}
	
	
	
}
