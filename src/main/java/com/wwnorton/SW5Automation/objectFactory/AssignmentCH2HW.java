package com.wwnorton.SW5Automation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class AssignmentCH2HW {
	
	WebDriver driver;
		
	@FindBy(how = How.ID, using = "wafer_ps_shortanswer1506469736d344c96d6bd4782903ec064820aaabc209898__")
	public WebElement part1Question;

	@FindBy(how = How.ID, using = "wafer_ps_shortanswer1506487112d344c96d6bd4782903ec064820aaabc201318__")
	public WebElement part2Question;

	@FindBy(how = How.ID, using = "wafer_ps_shortanswer1506499710d344c96d6bd4782903ec064820aaabc205842__")
	public WebElement part3Question;
	
	@FindBy(how = How.XPATH, using = "//input[@name='0162087374bdeaa6bb7f086003f078f9de2221d7c103122_-false']")
	public WebElement protonsTextbox1;

	@FindBy(how = How.XPATH, using = "//input[@name='0162108968bdeaa6bb7f086003f078f9de2221d7c102177_-false']")
	public WebElement neutronsTextbox1;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'wafer_ps_shortanswer3082530573bdeaa6bb7f086003f078f9de2221d7c101565__')]")
	public WebElement ElementSymbolTextbox1;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'0162177497bdeaa6bb7f086003f078f9de2221d7c104483_-false')]")
	public WebElement protonsTextbox2;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'0162182072bdeaa6bb7f086003f078f9de2221d7c103227_-false')]")
	public WebElement neutronsTextbox2;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'wafer_ps_shortanswer3082801464bdeaa6bb7f086003f078f9de2221d7c101699__')]")
	public WebElement ElementSymbolTextbox2;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'0162238863bdeaa6bb7f086003f078f9de2221d7c104471_-false')]")
	public WebElement protonsTextbox3;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'0162253624bdeaa6bb7f086003f078f9de2221d7c105220_-false')]")
	public WebElement neutronsTextbox3;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'wafer_ps_shortanswer3082989493bdeaa6bb7f086003f078f9de2221d7c107842__')]")
	public WebElement ElementSymbolTextbox3;
	
	@FindBy(how = How.ID, using = "wafer_ps_shortanswer7711835084e0d99b793349f08101be1d61405adf7006157__")
	public WebElement Textbox1;
	
	@FindBy(how = How.ID, using = "wafer_ps_shortanswer7711905651e0d99b793349f08101be1d61405adf7006946__")
	public WebElement Textbox2;
	
	@FindBy(how = How.ID, using = "wafer_ps_shortanswer7711971682e0d99b793349f08101be1d61405adf7005364__")
	public WebElement Textbox3;
	
	public AssignmentCH2HW(WebDriver driver) {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readquestionJson();
	
	
	@Step("Submit the Answer for CH2HW Assignment,  Method: {method} ")
	public void submitAnswerCH2HWNameCompounds() throws InterruptedException {
		Thread.sleep(5000);
		String Answer1 = jsonobject.getAsJsonObject("Question").get("part1Question").getAsString();
		part1Question.sendKeys(Answer1);
		String Answer2 = jsonobject.getAsJsonObject("Question").get("part2Question").getAsString();
		part2Question.sendKeys(Answer2);
		String Answer3 = jsonobject.getAsJsonObject("Question").get("part3Question").getAsString();
		part3Question.sendKeys(Answer3);
	}
	
	public void Question3Answer_Correct() {
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 50);

				ReusableMethods.scrollToElement(driver, 
						By.id("sortingPreviewAnswerItemList-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_"));
			
				WebElement alpha = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-1-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
				WebElement beta = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-2-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
				WebElement gama = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("item-answer-3-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
				
				
				ReusableMethods.scrollToElement(driver, 
						By.id("answerCategoryTable-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_"));
	
				// Drag type where element/compound need to be dropped.
				WebElement Red = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-1-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
				WebElement Blue = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-2-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
				WebElement Green = wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.id("categorySubContainer-3-81305109881ed5c6c648049fa0b7b7d2c611b1d14706610_")));
	
				// Using Action class for drag and drop.
				Actions act = new Actions(driver);
	
				// Drag and drop each item to its type respectively
				ReusableMethods.moveToElement(driver, alpha);
				act.dragAndDrop(alpha, Red).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, beta);
				act.dragAndDrop(beta, Green).build().perform();
				Thread.sleep(1000);
				ReusableMethods.moveToElement(driver, gama);
				act.dragAndDrop(gama, Blue).build().perform();
	
				
			} catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
			}
	}
	
	
	public void Question11Answer_Correct() throws InterruptedException {
		Thread.sleep(3000);
		Textbox1.sendKeys("carbon tetrachloride");
		Textbox2.sendKeys("dinitrogen tetroxide");
		Textbox3.sendKeys("dinitrogen monoxide");

	}
	
	@Step("Submit the Answer for nuclide,  Method: {method} ")
	public void submitAnswernuclide() throws InterruptedException {
		Thread.sleep(3000);
		// Using Action class for drag and drop.
		protonsTextbox1.sendKeys("5");
		neutronsTextbox1.sendKeys("6");
		ElementSymbolTextbox1.sendKeys("B");
		protonsTextbox2.sendKeys("6");
		neutronsTextbox2.sendKeys("5");
		ElementSymbolTextbox2.sendKeys("C");
		protonsTextbox3.sendKeys("6");
		neutronsTextbox3.sendKeys("7");
		ElementSymbolTextbox3.sendKeys("C");
		// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		scrollToBottom(driver);
		WebElement correctAnswer = driver.findElement(By.xpath(
				"//span[label/span[@class='everySingleOption multiple-option-text'][contains(text(),'Part 2 and Part 3')]]//span/input"));
		try {
			correctAnswer.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public void scrollToBottom(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
}
