/**
 * 
 */
package com.wwnorton.SW5Automation.utilities;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TimeZone {
	private static final String DATE_FORMAT = "hh:mm:ss a z";
	@SuppressWarnings("unused")
	private static final DateTimeFormatter formatter = DateTimeFormatter
			.ofPattern(DATE_FORMAT);
	static LocalTime minutes;
	
	/*public static void main(String[] args){
	  System.out.println(getthirtyMinutesTime());
	 
	}*/
	 
	public static String getTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if(zoneName.equalsIgnoreCase("America/New_York")){
			ZoneId fromTimeZone = ZoneId.of("America/New_York"); 
			LocalDateTime today = LocalDateTime.now(); 	
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(60);
		}
		 else if(zoneName.equalsIgnoreCase("Asia/Kolkata")){
		ZoneId fromTimeZone = ZoneId.of("Asia/Kolkata"); // Source timezone
		ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target timezone

		LocalDateTime today = LocalDateTime.now(); // Current time

		// Zoned date time at source timezone
		ZonedDateTime currentISTime = today.atZone(fromTimeZone);

		// Zoned date time at target timezone
		ZonedDateTime currentETime = currentISTime
				.withZoneSameInstant(toTimeZone);

		minutes = currentETime.toLocalTime().plusMinutes(60);
		/*
		 * if(minutes.isBefore(LocalTime.of(06, 00)) &&
		 * (minutes.isAfter(LocalTime.of(12,00)))){ LocalTime time
		 * =minutes.now();
		 */
		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}

	public static String getthirtyMinutesTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if(zoneName.equalsIgnoreCase("America/New_York")){
			ZoneId fromTimeZone = ZoneId.of("America/New_York"); 
			LocalDateTime today = LocalDateTime.now(); 	
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(10);
		}
		 else if(zoneName.equalsIgnoreCase("Asia/Kolkata")){
		ZoneId fromTimeZone = ZoneId.of("Asia/Kolkata"); // Source timezone
		ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target timezone
	
		LocalDateTime today = LocalDateTime.now(); // Current time

		// Zoned date time at source timezone
		ZonedDateTime currentISTime = today.atZone(fromTimeZone);

		// Zoned date time at target timezone
		ZonedDateTime currentETime = currentISTime
				.withZoneSameInstant(toTimeZone);

		minutes = currentETime.toLocalTime().plusMinutes(10);
		
		 }
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}

	public static String getNearestHourQuarter(LocalTime dt2) {
		int hour = dt2.getHour();
		int minutes = dt2.getMinute();

		String meridiam1 = dt2.format(DateTimeFormatter.ofPattern("a"));

		if (minutes > 30) {
			hour = hour + 1;
			minutes = 0;
		} else if (minutes > 0) {
			minutes = 30;
		}

		int hour_12_format = hour % 12;
		if (hour_12_format == 0 && minutes == 0) {
			if (meridiam1.compareTo("AM") == 0 || hour == 24) {
				hour_12_format = 11;
				minutes = 59;
				meridiam1 = "PM";
			} else {
				hour_12_format = 12;
				minutes = 0;
				meridiam1 = "PM (Noon)";
			}
		} else if (hour_12_format < 6 && meridiam1.compareTo("AM") == 0) {
			hour_12_format = 6;
			minutes = 0;
		} else if (hour_12_format == 0 && meridiam1.compareTo("PM") == 0) {
			hour_12_format = 12;
		}

		return hour_12_format + ":" + String.format("%02d", minutes) + " "
				+ meridiam1;

	}

}
