package com.wwnorton.SW5Automation.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetDate {
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yy");

	public static String getCurrentDate() {

		Date currentDate = new Date();
		String SystemDate = (dateFormat.format(currentDate));
		return SystemDate;
	}

	public static String getCurrentDate2() {

		Date currentDate = new Date();
		String SystemDate = (dateFormat2.format(currentDate));
		return SystemDate;
	}

	public static String getCurrentDate3() {

		Date currentDate = new Date();
		String day = new SimpleDateFormat("dd").format(currentDate);
		day = day + getDateSuffix(Integer.parseInt(day));
		String month = new SimpleDateFormat("MMMMM").format(currentDate);
		String year = new SimpleDateFormat("yyyy").format(currentDate);

		return month + " " + day + ", " + year;
	}
	
	public static String getFormattedDate(int noOfDays) throws Exception {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, noOfDays);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");		
		String strDate =  dateFormat.format(cal.getTime());
		Date date = dateFormat.parse(strDate);
		
		String day = new SimpleDateFormat("d").format(date);
		day = day + getDateSuffix(Integer.parseInt(day));
		
		String month = new SimpleDateFormat("MMMMM").format(date);
		String year = new SimpleDateFormat("yyyy").format(date);

		return month + " " + day + ", " + year;
	}

	public static String getNextmonthDate() {
		// convert date to calendar

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();

		String nextmonthdate = (dateFormat.format(currentDatePlusOne));
		return nextmonthdate;

	}

	public String addDaystoCalendar(String addedDays) {
		// convert date to calendar
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		// c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, Calendar.DATE + 10); // same with
													// c.add(Calendar.DAY_OF_MONTH,
													// 1);

		// convert calendar to date
		Date currentDatePlusTendays = c.getTime();

		addedDays = (dateFormat.format(currentDatePlusTendays));
		System.out.println(addedDays);
		return addedDays;

	}

	public static String getDateSuffix(int day) {
		switch (day) {
		case 1:
		case 21:
		case 31:
			return ("st");

		case 2:
		case 22:
			return ("nd");

		case 3:
		case 23:
			return ("rd");

		default:
			return ("th");
		}
	}
	
	public static String addDays(int numberOfDays) {
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, numberOfDays);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");		
		String date =  dateFormat.format(cal.getTime());
		
		return date;
		
		}

}
