package com.wwnorton.SW5Automation.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class ReadUIJsonFile {

	public static String UserDir = System.getProperty("user.dir");
	// Read Json test data from testData.json file with the parameters and values.

	
	Properties prop = new Properties();
	
		
		
	
	public JsonObject readUIJason() {

		JsonObject rootObject = null;
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir +"/src/test/resources/config.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			prop.load(input);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String url = prop.getProperty("testurl");

		try {

			JsonParser parser = new JsonParser();
			if(url.equalsIgnoreCase("https://digital.wwnorton.com/chem6")){
			JsonReader jReader = new JsonReader(new FileReader(UserDir + "/src//test/resources/SW5ProductionuiData.json"));
			jReader.setLenient(true);
			JsonElement rootElement = parser.parse(jReader);
			rootObject = rootElement.getAsJsonObject();
			}else if(url.equalsIgnoreCase("https://smartwork5-stg2.wwnorton.com/chem6")) {
			JsonReader jReader = new JsonReader(new FileReader(UserDir + "/src/test/resources/SW5Stg2uiData.json"));
			jReader.setLenient(true);
			JsonElement rootElement = parser.parse(jReader);
			rootObject = rootElement.getAsJsonObject();
			}else if (url.equalsIgnoreCase("https://dlp-auth-stg.wwnorton.net/chem6")) {
			//else if (url.equalsIgnoreCase("https://digital-preview.wwnorton.com/chem5")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "/src/test/resources/SW5Stg2uiData.json"));
				//		+ "/src//test/resources/SW5ProductionuiData.json"));
				jReader.setLenient(true);
				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			}else if (url.equalsIgnoreCase("https://dlp-sw5-stg.wwnorton.net/chem6")) {
					JsonReader jReader = new JsonReader(new FileReader(UserDir
							+ "/src/test/resources/SW5StgData.json"));
					jReader.setLenient(true);
					JsonElement rootElement = parser.parse(jReader);
					rootObject = rootElement.getAsJsonObject();
				}else if (url.equalsIgnoreCase("https://dlp-sw5-qa.wwnorton.net/chem6")) {
					JsonReader jReader = new JsonReader(new FileReader(UserDir
							+ "/src/test/resources/SW5StgData.json"));
					jReader.setLenient(true);
					JsonElement rootElement = parser.parse(jReader);
					rootObject = rootElement.getAsJsonObject();
				}else if (url.equalsIgnoreCase("https://dlp-redis-qa.wwnorton.net/chem6")) {
					JsonReader jReader = new JsonReader(new FileReader(UserDir
							+ "/src/test/resources/SW5StgData.json"));
					jReader.setLenient(true);
					JsonElement rootElement = parser.parse(jReader);
					rootObject = rootElement.getAsJsonObject();
				}
			

		}

		catch (Exception e)

		{
			e.printStackTrace();
		}

		return rootObject;

	}

	public JsonObject readquestionJson() {

		JsonObject jsonobject = null;

		String userdir = System.getProperty("user.dir") + "/src/test/resources/QuestionAnswer.json";
		try {
			JsonParser jsonParser = new JsonParser();
			JsonReader reader = new JsonReader(new FileReader(userdir));
			reader.setLenient(true);
			JsonElement rootElement = jsonParser.parse(reader);
			jsonobject = rootElement.getAsJsonObject();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject;

	}
	
	public JsonObject readAddquestionJson() {

		JsonObject jsonobject = null;

		String userdir = System.getProperty("user.dir") + "/src/test/resources/AddCustomQuestions.json";
		try {
			JsonParser jsonParser = new JsonParser();
			JsonReader reader = new JsonReader(new FileReader(userdir));
			reader.setLenient(true);
			JsonElement rootElement = jsonParser.parse(reader);
			jsonobject = rootElement.getAsJsonObject();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject;

	}
	
	

}
