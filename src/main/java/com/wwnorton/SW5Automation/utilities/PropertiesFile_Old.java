package com.wwnorton.SW5Automation.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.jayway.restassured.RestAssured;

import ru.yandex.qatools.allure.annotations.Step;



public class PropertiesFile_Old {
	
	public static WebDriver driver = null;
    public static WebDriverWait wait;
    public static String UserDir=System.getProperty("user.dir");
    public static String Browser;
	public static String url;
	public static String nontaxonomyurl;
	public static String DriverPath;
	
	public static String baseURI;
	public static String serverPort;
	public static String basePathTerm;
	

    public WebDriver getDriver() {
    	
        return driver;
    }
	
    
    // Read Browser name, Test url and Driver file path from config.properties.
    
	public static void readPropertiesFile() throws Exception {
		
		Properties prop = new Properties();
		
		try {
			
			InputStream input = new FileInputStream(UserDir +"/src/test/resources/config.properties");
			prop.load(input);
			
			Browser = prop.getProperty("browsername");
			url = prop.getProperty("testurl");
			nontaxonomyurl = prop.getProperty("nontaxonomytesturl");
			//DriverPath = prop.getProperty("driverfile");
			baseURI = prop.getProperty("uri");
			serverPort = prop.getProperty("port");
			basePathTerm = prop.getProperty("path");
			DriverPath =UserDir +"/Drivers/";
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}	

	
	// Set Browser configurations by comparing Browser name and Diver file path.
	
	public static void setBrowserConfig() {
		
			if(Browser.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver", DriverPath + "geckodriver.exe");
            
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				//firefoxOptions.setHeadless(true);
				firefoxOptions.setCapability("marionette", true);
            
				driver = new FirefoxDriver(firefoxOptions); 
            
			}
			
			if(Browser.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverPath + "chromedriver.exe");
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("profile.default_content_setting_values.notifications", 1);
				chromePrefs.put("download.prompt_for_download", false);
				chromePrefs.put("plugins.plugins_disabled", "Chrome PDF Viewer");
				chromePrefs.put("download.default_directory", System.getProperty("user.dir")+ "\\Downloads");
				DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.setHeadless(false);
				chromeOptions.addArguments("--test-type");
				chromeOptions.addArguments("--start-maximized");
				chromeOptions.addArguments("--disable-infobars");
				chromeOptions.addArguments("--disable-extensions");
				chromeOptions.addArguments("--disable-notifications");
				chromeOptions.addArguments("--disable-gpu");
				chromeOptions.addArguments("--no-default-browser-check");
				chromeOptions.addArguments("--ignore-certificate-errors");
				chromeOptions.addArguments("--proxy-server='direct://'");
				chromeOptions.addArguments("--proxy-bypass-list=*");
				chromeOptions.setExperimentalOption("prefs", chromePrefs);
			    //chromeOptions.addArguments("--headless");
			   //chromeOptions.addArguments("--window-size=2560,1600");
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				driver = new ChromeDriver(chromeOptions);

           }
			
		}
		

	// Set Test URL based on config.properties file.
	
	public static void setURL() {
		
			driver.manage().window().maximize();
			driver.get(url);
		
		}
	
	// Set Test Non adaptive Taxonomy URL based on config.properties file.
	public static void setNonTaxonomyURL() {
		
		driver.manage().window().maximize();
		driver.get(nontaxonomyurl);
	
	}
		
	   /*
    Sets Base URI
    Before starting the test, we should set the RestAssured.baseURI
    */
    
	@Step("Set Base URI,  Method: {method} ")
    public static void setBaseURI (){
        RestAssured.baseURI = baseURI;
    }

    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Port,  Method: {method} ")
    public static void setServerPort(){
        RestAssured.port = Integer.valueOf(serverPort);
    }
    
    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Path,  Method: {method} ")
    public static void setBasePath(String basepath){
        RestAssured.basePath = basepath;
    }

    /*
    Reset Base URI (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBaseURI (){
        RestAssured.baseURI = null;
    }

    /*
    Reset Server Port (after test)
    After the test, we should reset the RestAssured.serverPort
    */
    
    @SuppressWarnings("null")
	public static void resetServerPort (){
        RestAssured.port = (Integer) null;
    }
    
    /*
    Reset base path (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBasePath(){
        RestAssured.basePath = null;
    }
	
	// Close the driver after running test script.
	
	public static void tearDownTest() throws InterruptedException {
		
			wait = new WebDriverWait(driver,5);
			driver.close();
			driver.quit();
					
		}


}
