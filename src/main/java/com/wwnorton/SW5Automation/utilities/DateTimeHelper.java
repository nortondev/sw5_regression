package com.wwnorton.SW5Automation.utilities;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeHelper {

	public static boolean IsValidDateTime(String dateTime, String format) {
		try {

			DateTimeFormatter formatter = DateTimeFormat.forPattern(format);

			@SuppressWarnings("unused")
			DateTime dt = formatter.parseDateTime(dateTime);

			return true;

		} catch (Exception e) {
			// Eat Exception
			return false;
		}
	}
}
