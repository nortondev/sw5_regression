package com.wwnorton.SW5Automation.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;

import com.jayway.restassured.RestAssured;

import ru.yandex.qatools.allure.annotations.Step;



public class PropertiesFile extends BaseDriver {

	
	public static WebDriverWait wait;
	public static String UserDir = System.getProperty("user.dir");
	//public static String browser;
	//public static String Browser;
	public static String url;
	public static String nontaxonomyurl;
	public static String DriverPath;
	
	public static String baseURI;
	public static String serverPort;
	public static String basePathTerm;

		
	// Read Browser name, Test url and Driver file path from config.properties.

	public static void readPropertiesFile() throws Exception {

		Properties prop = new Properties();

		try {

			InputStream input = new FileInputStream(UserDir +
					"/src/test/resources/config.properties");
			prop.load(input);

			//Browser = prop.getProperty("browsername");
			url = prop.getProperty("testurl");
			nontaxonomyurl = prop.getProperty("nontaxonomytesturl");
			//DriverPath = prop.getProperty("driverfile");
			baseURI = prop.getProperty("uri");
			serverPort = prop.getProperty("port");
			basePathTerm = prop.getProperty("path");
			DriverPath = UserDir +"/Drivers/";
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	// Set Browser configurations by comparing Browser name and Diver file path.
	// @Parameters({"browser"})
	public static void setBrowserConfig(String browser)
			throws IllegalAccessException {
		init_driver(browser);

	}

	// Set Test URL based on config.properties file.

	public static void setURL() {
		
		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().manage().window().maximize();
		getDriver().get(url);
	}

	// get WebSite Non Taxanomy URL
	public static void setNonTaxonomyURL() {
		
		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(nontaxonomyurl);
	}
	
	
	   /*
    Sets Base URI
    Before starting the test, we should set the RestAssured.baseURI
    */
    
	@Step("Set Base URI,  Method: {method} ")
    public static void setBaseURI (){
        RestAssured.baseURI = baseURI;
    }

    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Port,  Method: {method} ")
    public static void setServerPort(){
        RestAssured.port = Integer.valueOf(serverPort);
    }
    
    /*
    Sets Base path
    Before starting the test, we should set the RestAssured.basePath
    */
	
	@Step("Set Base Path,  Method: {method} ")
    public static void setBasePath(String basepath){
        RestAssured.basePath = basepath;
    }

    /*
    Reset Base URI (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBaseURI (){
        RestAssured.baseURI = null;
    }

    /*
    Reset Server Port (after test)
    After the test, we should reset the RestAssured.serverPort
    */
    
    @SuppressWarnings("null")
	public static void resetServerPort (){
        RestAssured.port = (Integer) null;
    }
    
    /*
    Reset base path (after test)
    After the test, we should reset the RestAssured.basePath
    */
    
    public static void resetBasePath(){
        RestAssured.basePath = null;
    }

	// Close the driver after running test script.
	public static void tearDownTest() throws InterruptedException {
	
		if(getDriver() != null){
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getDriver().close();
			getDriver().quit();
		}
	}

}