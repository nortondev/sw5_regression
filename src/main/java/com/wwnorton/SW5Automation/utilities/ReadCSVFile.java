package com.wwnorton.SW5Automation.utilities;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;



public class ReadCSVFile {
	
	public static String loginEmail;
	public static String password;
	public static String title;
	public static String state;
	public static String school;
	
	
	public static void readCSVData() throws IOException  {
    	
    	String filePath = "./src/test/resources/testData.csv";
        
    	try {
    			
    			// Create an object of file reader 
            	// CSV file path as a parameter
    			FileReader filereader = new FileReader(filePath); 
    			
    			// create testData object for CSVReader class and skip first Line
    	        CSVReader testData = new CSVReaderBuilder(filereader).withSkipLines(1).build(); 
    	        
    	     // Read CSV test data line by line
    	        String[] csvCell; 
    	        
    	        while ((csvCell = testData.readNext()) != null) 
    	        {
    	        	
//    	        	for(int i=0; i<csvCell.length; i++) 
//    	        	{
    	        		loginEmail = csvCell[0];
    	        		password = csvCell[1];
    	        		title = csvCell[2];
    	        		state = csvCell[3];
    	        		school = csvCell[4];
//    	        	}

    	        } 
    		
    		}
    	        
    	
    	catch (FileNotFoundException e) 
    	
    	{
            e.printStackTrace();
        } 
    	
    	
    	catch (IOException e) 
    	
    	{
            e.printStackTrace();
        }

    }
    
}
