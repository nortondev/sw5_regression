package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class InstructorCreateNonAdaptiveAssignmentfromscratch extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	LoginPage DLPpage;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	CreateCustomAssignment createAssignment;
	Questions_Page questionspage;
	WebDriverWait wait;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an instructor -  Create Non Adaptive Assignment from scratch with list of assignment questions and policy.")
	@Stories("AS-12 As an instructor -  Create Non Adaptive Assignment from scratch with list of assignment questions and policy. ")
	@Test
	public void CreateNonAdaptiveAssignment() throws Exception {
		driver = getDriver();
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID = createStudentset.createStudentset_ID();
		LogUtil.log(StudentID);
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage();
		Thread.sleep(2000);

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		Thread.sleep(8000);
		SW5DLPpage.ClickAssignment();

		String winHandleBefore = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 60);

		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		assignmentpage = new AssignmentPage();
		assignmentpage.editAssignmentbutton();
		String Assignmentname = "CustomAssignment NonAdaptive";
		assignmentpage.enterAssignmentName(Assignmentname);
		assignmentpage.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		assignmentpage.getSetGAUTime();
		assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
		Thread.sleep(2000);
		assignmentpage.adaptiveOff();
		assignmentpage.ShowAdditionalSettingsButton();

		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionspage = new Questions_Page();
		questionspage.addQuestions();
		
		questionspage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		assignmentpage.saveButton();
		
		Thread.sleep(3000);
		assignmentpage.publishButton();
		
		Thread.sleep(5000);
		questionspage.headTitleLink();
		
		driver.close();
		driver.switchTo().window(winHandleBefore);

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		DLPpage.logoutSW5();
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}