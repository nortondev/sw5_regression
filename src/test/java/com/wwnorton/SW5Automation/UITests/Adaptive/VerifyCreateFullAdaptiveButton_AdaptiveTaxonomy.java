package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyCreateFullAdaptiveButton_AdaptiveTaxonomy extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify Create Full adaptive button is only displayed for adaptive taxonomies.")
	@Stories("AS-66 As an Instructor - Verify Create Full adaptive button is only displayed for adaptive taxonomies.")
	@Test
	public void VerifyFullAdaptiveAssignmentButton() throws Exception {
		driver = getDriver();
		// Login as Instructor
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID1 = createStudentset.createStudentset_ID();
		System.out.println(StudentID1);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(StudentID1);
		
		Assert.assertEquals(SW5DLPpage.checkFullAdaptiveButton(), true);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		//driver.close();
		
	
	//Verify Create Full adaptive button is NOT displayed for Non adaptive taxonomy.

		//PropertiesFile.readPropertiesFile();
		//PropertiesFile.setBrowserConfig();
		PropertiesFile.setNonTaxonomyURL();
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID2 = createStudentset.createStudentset_ID();
		System.out.println(StudentID2);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(StudentID2);
		
		Assert.assertEquals(SW5DLPpage.checkFullAdaptiveButton(), false);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();

	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
