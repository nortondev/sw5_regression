package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyFollowUpAdaptive_InstructorSettings extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify Follow-Up Adaptive settings.")
	@Stories("AS-210 As an Instructor - Verify Follow-Up Adaptive settings.")
	@Test
	public void VerifyFollowUpAdaptiveSettings() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		Thread.sleep(5000);
		SW5DLPpage.ClickAssignment();
		
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		
		String winHandleBefore = driver.getWindowHandle();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		
		driver.switchTo().window(ChildWindow);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		Thread.sleep(3000);
		String AssignmentName = jsonObj.getAsJsonObject("FollowUp_PartialAdaptiveInfo")
				.get("FollowUp_AdaptiveAssignemntName").getAsString();
		
		assignment.enterAssignmentName(AssignmentName);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		//assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		assignment.adaptiveOn();
		assignment.followUp();
		assignment.selectAdaptivePoints("0");
		
		String questionsSetName = SW5Constants.QUESTION_SET_1;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestions(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		questionpage.addQuestions();
		questionpage.clickCreateNewPoolQuestionsButton();
		
		String PoolTitleSuffix = "FollowUpPoolQuestions ";
		int suffixCharCount = 5;
		String PoolQuestionTitle = PoolTitleSuffix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		String searchString = "Identify";
		String strQuestion1 = "Identify transition based on particle view";
		String strQuestion2 = "Identify states of matter based on particulate view";
		
		questionpage.createNewPoolQuestions(PoolQuestionTitle, searchString, strQuestion1, strQuestion2);
		
		createAssignment.yourCurrentAssignment();

		Thread.sleep(3000);
		assignment.adaptiveOff();
		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.adaptiveOn();
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		assignment.timeLimitOn();
		
		Thread.sleep(2000);
		Assert.assertEquals(assignment.timeLimitModal(), true);
		Assert.assertEquals(assignment.getTimeLimitDisabledMessageText().trim(),
				"Time limit has been disabled because you cannot have a time limit in your assignment when you have an adaptive portion."+
				"If this was a mistake,please turn off the adaptive portion and turn on the time limit again.");
		
		assignment.modalGotItButton.click();
		
		Thread.sleep(2000);
		assignment.PublishButton.click();
		
		Assert.assertEquals(assignment.warningLearningObjectivesModal(), true);
		Assert.assertEquals(assignment.getWarningMsg_LearningObjectives().trim(),
				"There are no learning objectives found for this activity. Please add some learning objectives and try publish.");
		
		assignment.modalGotItButton.click();
		
		wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);
		wait.until(ExpectedConditions.visibilityOf(assignment.SELECTOBJECTIVES));
		assignment.selectObejctive();

		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		driver.close();
		driver.switchTo().window(ParentWindow);
		
		int count = SW5DLPpage.assignmentRowCount()-1;
		
		try {
				WebElement adptiveTitle;
				for (int i = 1; i <=count; i++) {
					adptiveTitle = driver
						.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
				
					boolean isAdaptive = ReusableMethods.elementExist(BaseDriver.getDriver(),
							"//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");
				
					if (isAdaptive == true && adptiveTitle.getText().equalsIgnoreCase(AssignmentName)) { 
						wait = new WebDriverWait(driver, 5);
						TimeUnit.SECONDS.sleep(5);
						assignment.Assignmentlinks(AssignmentName);
						break;
					}	
				}
			} catch (Exception e) {e.getMessage(); }

		testHelper.getInstructorAssignmentWindow(driver);
		
		Thread.sleep(5000);
		assignment = new AssignmentPage();
		String assignmentTitle_Instructor = assignment.assignmentName_Ins.getText();
		
		Assert.assertEquals(assignmentTitle_Instructor.equalsIgnoreCase(AssignmentName), true);
		Assert.assertTrue(assignment.followUpTitle.getText().equalsIgnoreCase("Follow-up"));
		Assert.assertTrue(assignment.adaptivePracticeAttempt.getText().equalsIgnoreCase("Practice"));
		Assert.assertEquals(assignment.followUpDisplayedAtEnd_AssignmentPage(), true);
		
		driver.close();
		driver.switchTo().window(winHandleBefore);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		
		
		// Login as Student

		String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		new LoginAsStudent().LoginStudent(studentUserName1);
		SW5DLPstudent = new SW5DLPStudent();
		
		int assignmentCount = SW5DLPpage.assignmentRowCount();
		
		try {
				WebElement adptiveTitle;
				for (int i = 1; i <=assignmentCount; i++) {
					adptiveTitle = driver
						.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
				
					boolean isAdaptive = ReusableMethods.elementExist(BaseDriver.getDriver(),
							"//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");
				
					if (isAdaptive == true && adptiveTitle.getText().equalsIgnoreCase(AssignmentName)) { 
						wait = new WebDriverWait(driver, 5);
						TimeUnit.SECONDS.sleep(5);
						SW5DLPstudent.SelectAssignment(AssignmentName);
						break;
					}	
				}
			} catch (Exception e) {e.getMessage(); }


		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
		
		Thread.sleep(2000);
		String assignmentTitle_Std = sap.assignmentName_Std.getText();
		
		Assert.assertEquals(assignmentTitle_Std.equalsIgnoreCase(AssignmentName), true);
		Assert.assertTrue(sap.followUpTitle.getText().equalsIgnoreCase("Follow Up"));
		Assert.assertTrue(sap.adaptivePracticeAttempt.getText().equalsIgnoreCase("Practice"));
		Assert.assertEquals(sap.followUpDisplayedAtEnd_SAP(), true);

		Thread.sleep(2000);
		Assert.assertEquals(true, sqp.followUpAssignment_SQP());
		
		sqp.clickHeaderTitle(sap);
		
		Thread.sleep(2000);
		Assert.assertEquals(sap.isBeginAssignmentButtonDisplayed(), true);
		
		sap.beginAssignmentClick();
		
		Thread.sleep(2000);
		customQuestionsAnswerAssignmentStudent1();
		
		sqp.clickAdaptiveContinueButton();
		Thread.sleep(2000);
		Assert.assertEquals(true, sqp.followUpAssignment_SQP());
		
		sqp.clickHeaderTitle(sap);
		
		Thread.sleep(2000);
		Assert.assertEquals(sap.isResumeAssignmentDisplayed(), true);
		
		sap.resumeAssignmentClick();
		Thread.sleep(2000);
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				"13 OF 13 QUESTIONS COMPLETED");
		
		sqp.clickAdaptiveContinueButton();
		Thread.sleep(2000);
		Assert.assertEquals(true, sqp.followUpAssignment_SQP());
		
		driver.close();
		driver.switchTo().window(parentWindow2);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPstudent.logoutSmartwork5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
	
	
	
	public void customQuestionsAnswerAssignmentStudent1() throws Exception {
		custassignment.question1Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 13 QUESTIONS COMPLETED");
		
		// Navigate to Next Question
		sqp.clickNextQuestion();
		custassignment.question2Answer();
		
		submitCustomAssignmentAnswerAndAssert("2 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		
		submitCustomAssignmentAnswerAndAssert("3 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		
		submitCustomAssignmentAnswerAndAssert("4 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		
		submitCustomAssignmentAnswerAndAssert("5 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question6Answers();
		
		submitCustomAssignmentAnswerAndAssert("6 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question7Answers();
		
		submitCustomAssignmentAnswerAndAssert("7 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question8Answers();
		
		submitCustomAssignmentAnswerAndAssert("8 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question9Answers();
		
		submitCustomAssignmentAnswerAndAssert("9 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question10Answers();
		
		submitCustomAssignmentAnswerAndAssert("10 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question11Answers();
		
		submitCustomAssignmentAnswerAndAssert("11 OF 13 QUESTIONS COMPLETED");
		sqp.clickNextQuestion();
		
		custassignment.question12Answers();
		
		submitCustomAssignmentAnswerAndAssert("12 OF 13 QUESTIONS COMPLETED");
		
		sqp.clickNextQuestion();
		
		custassignment.question13Answers();
		submitCustomAssignmentAnswerAndAssert("13 OF 13 QUESTIONS COMPLETED");
	}
	
	
	
	private void submitCustomAssignmentAnswerAndAssert(String questionfooterText) throws Exception{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		sqp.clickSubmitAnswer();
		
		WebElement yesButton;
		boolean LetMeFinishButtonExist;
		
		try {
			
				LetMeFinishButtonExist = driver.findElement(By.xpath("//body/div/div/div/div/div/div[3]/button[2]/span[1]")).isDisplayed();
				if(LetMeFinishButtonExist == true) {
				
					yesButton = driver.findElement(By.xpath("//button[contains(text(),'YES')]"));
					yesButton.click();
				
					sqp.clickFeedbackViewSolution();
					Thread.sleep(2000);
					sqp.clickConfirmPopupViewSolutionButton();
				
			}
			
		} catch (Exception e) {
			
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				ReusableMethods.questionCloseLink(driver);
			}
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		sqp.getQuestionFooterLeftText();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionfooterText);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}

}