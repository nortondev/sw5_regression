package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorCreateWarmUpPartialAdaptiveAssignmentFromScratch extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionspage;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Create Warm Up (Partial Adaptive) Assignment from scratch.")
	@Stories("AS-13 As an Instructor - Create Warm Up (Partial Adaptive) Assignment from scratch with assignment questions, policy and learning objectives.")
	@Test
	public void CreateWarmUpPartialAdaptiveAssignment() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID = createStudentset.createStudentset_ID();
		LogUtil.log(StudentID);
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		//DLPpage.clickSW5Icon();
		//Thread.sleep(2000);
		// driver.findElement(By.xpath("//select[@id='report_class_menu']")).click();
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		Thread.sleep(8000);
		SW5DLPpage.ClickAssignment();
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		driver.switchTo().window(ChildWindow);
		Thread.sleep(8000);
		driver.switchTo().frame("swfb_iframe");
		WebDriverWait wait = new WebDriverWait(driver, 50);

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		String assignmentTitle = driver.findElement(By.xpath("//span[@class='instructor-intro-assignment-title']")).getText();
		LogUtil.log("The Assignment Title is ", assignmentTitle);
		
		assignmentpage = new AssignmentPage();
		assignmentpage.editAssignmentbutton();
		
		String Assignmentname = jsonObj.getAsJsonObject("WarmUp_PartialAdaptiveInfo")
				.get("WarmUp_PartialAdaptiveAssignemntName").getAsString();
		
		assignmentpage.enterAssignmentName(Assignmentname);
		assignmentpage.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		assignmentpage.getSetGAUTime();
		assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
		Thread.sleep(2000);
		
		assignmentpage.adaptiveOn();
		assignmentpage.warmUp();

		questionspage = new Questions_Page();
		questionspage.addQuestions();
		Thread.sleep(5000);
		WebDriverWait waitselectTypeListBox = new WebDriverWait(driver, 50);
        waitselectTypeListBox.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='dropdown-select']")));
      //questionspage.SelectTypeListbox();
        questionspage.addQuestionsToAssignment("Identify", "Identify Classes of Matter");
		Thread.sleep(3000);
		questionspage.headTitleLink();
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(assignmentpage.SELECTOBJECTIVES));
		assignmentpage.selectObejctive();
		assignmentpage.saveButton();
		Thread.sleep(5000);
		assignmentpage.publishButton();
		questionspage.headTitleLink();
		assignmentpage.adaptive_Questions("Warm-up");
		driver.switchTo().window(ParentWindow);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		assignmentpage.Assignmentlinks(Assignmentname);
        LogUtil.log(Assignmentname);
        Thread.sleep(1000);
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
