package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class Verify50Maximum_FullAdaptiveAssignments extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionspage;
	
	public String ParentWindow;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify that Instructor cannot create more than 50 Full adaptive assignments in 1 student set")
	@Stories("AS-222 - Verify that Instructor cannot create more than 50 Full adaptive assignments in 1 student set")
	@Test
	public void Create_50Maximum_FullAdaptiveAssignments() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID = createStudentset.createStudentset_ID();
		LogUtil.log(StudentID);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		
		for (int i=0; i <=50;  i++) {
			
			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(StudentID);
			
			Thread.sleep(3000);
			SW5DLPpage.clickFullAdaptiveButton();
			
			String ChildWindow = null;
			Set<String> Windowhandles = driver.getWindowHandles();
			String ParentWindow = driver.getWindowHandle();
			Windowhandles.remove(ParentWindow);
			
			String winHandle = Windowhandles.iterator().next();
			if (winHandle != ParentWindow) {
				ChildWindow = winHandle;
			}
			
			driver.switchTo().window(ChildWindow);
			
			Thread.sleep(5000);
			driver.switchTo().frame("swfb_iframe");
			
			WebDriverWait wait = new WebDriverWait(driver, 5000L);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			
			String customAssignmentPrefix = "Full_AdaptiveSettings ";
			int suffixCharCount = 5;

			assignmentpage.AssignmentName
			.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());
			assignmentpage.enterGAUDate(GetDate.getCurrentDate());

			Thread.sleep(2000);
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			Thread.sleep(2000);

			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			wait.until(ExpectedConditions.visibilityOf(assignmentpage.addOBJECTIVES));
			assignmentpage.addObejctives();

			assignmentpage.saveButton();
			
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			assignmentpage.PublishButton.click();
			
			if(i == 50) {
				Assert.assertTrue(assignmentpage.isMaximumExceptionDisplayed());
			}

			Thread.sleep(3000);
			assignmentpage.clickHeaderTitle(assignmentpage);
			
			if(i == 50) {
				Assert.assertTrue(assignmentpage.isExitWithoutSavingExceptionDisplayed());
			}
			
			//assignmentpage.adaptive_Questions("Prerequisite Concepts");
			
			System.out.println("Adaptive Assignment Count is: " + Integer.toString(i));
			
			wait = new WebDriverWait(driver, 3);
			TimeUnit.SECONDS.sleep(3);
			
			driver.close();
			driver.switchTo().window(ParentWindow);
		
		}
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
        
		SW5DLPpage.logoutSW5();
        

	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
