package com.wwnorton.SW5Automation.UITests.Adaptive;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class InstructorCreateFullAdaptiveAssignmentFromScratch extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Create Full Adaptive Assignment from scratch.")
	@Stories("AS-15 As an Instructor - Create Full Adaptive Assignment from scratch with assignment questions, policy and learning objectives.")
	@Test
	public void CreateFullAdaptiveAssignment() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String StudentID = createStudentset.createStudentset_ID();
		System.out.println(StudentID);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(StudentID);
		
		Thread.sleep(3000);
		SW5DLPpage.clickFullAdaptiveButton();
		
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		
		driver.switchTo().window(ChildWindow);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));

		assignmentpage = new AssignmentPage();
		assignmentpage.editAssignmentbutton();
		
		String Assignmentname = jsonObj.getAsJsonObject("FullAdaptiveInfo")
				.get("FullAdaptiveAssignemntName").getAsString();
		
		assignmentpage.enterAssignmentName(Assignmentname);
		assignmentpage.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		assignmentpage.getSetGAUTime();
		assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(assignmentpage.addOBJECTIVES));
		assignmentpage.addObejctives();

		assignmentpage.saveButton();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		assignmentpage.publishButton();
		Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
		
		driver.close();
		driver.switchTo().window(ParentWindow);
		
		String winHandleBefore = driver.getWindowHandle();
		
		int count = SW5DLPpage.assignmentRowCount()-1;
		
		try {
				WebElement adptiveTitle;
				for (int i = 1; i <=count; i++) {
					adptiveTitle = driver
						.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
				
					boolean isAdaptive = ReusableMethods.elementExist(BaseDriver.getDriver(),
							"//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");
				
					if (isAdaptive == true && adptiveTitle.getText().equalsIgnoreCase(Assignmentname)) { 
						wait = new WebDriverWait(driver, 5);
						TimeUnit.SECONDS.sleep(5);
						assignmentpage.Assignmentlinks(Assignmentname);
						break;
					}	
				}
			} catch (Exception e) {e.getMessage(); }

		testHelper.getInstructorAssignmentWindow(driver);
		
		Thread.sleep(5000);
		assignmentpage = new AssignmentPage();
		String assignmentTitle = assignmentpage.assignmentName_Ins.getText();
		
		Assert.assertEquals(assignmentTitle.equalsIgnoreCase(Assignmentname), true);
		
		driver.close();
		driver.switchTo().window(winHandleBefore);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}