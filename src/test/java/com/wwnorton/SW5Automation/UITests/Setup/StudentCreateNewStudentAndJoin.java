package com.wwnorton.SW5Automation.UITests.Setup;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentandJoin;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Step;

public class StudentCreateNewStudentAndJoin extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Test
	public String createNewStudentAndJoinStudentSet() throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		if (studentUserName == null) {
			studentUserName = __createNewStudentAndJoinStudentSet();
		}
		return studentUserName;
	}

	@Step
	public String createAnotherNewStudentAndJoinStudentSet(String studentUserNameContextKey) throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentUserName = (String) gContext.getAttribute(studentUserNameContextKey);

		if (studentUserName == null) {
			studentUserName = __createNewStudentAndJoinStudentSet();
		}
		return studentUserName;
	}

	private String __createNewStudentAndJoinStudentSet() throws Exception, InterruptedException {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		driver = getDriver();
		
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		if (studentSetID == null || studentSetID.isEmpty())
			throw new Exception("StudentSetID is not found in global context.");

		CreateNewStudentandJoin CSSJoin = new CreateNewStudentandJoin(driver);
		Thread.sleep(1000);
		String studentUserName = CSSJoin.CreateNewUser();
		CSSJoin.clickSW5Icon();
		CSSJoin.joinstudentset();
		CSSJoin.enterStudentSetID(studentSetID);
		CSSJoin.studentsetinfo(studentSetID);
		CSSJoin.logoutSmartwork5();

		return studentUserName;
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
