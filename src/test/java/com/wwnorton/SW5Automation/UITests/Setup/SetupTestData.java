package com.wwnorton.SW5Automation.UITests.Setup;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.CreateandAddStudentSet.InstructorCreateNewStudentSet;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Step;

public class SetupTestData extends PropertiesFile {
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeSuite
//	@org.testng.annotations.Test
	public void setUpTestData(@Optional("Chrome") String browser) throws Exception {
		setUp(browser);

		createNewStudentSet();

		createNewStudentAndJoinStudentSet();

		PropertiesFile.tearDownTest();
	}

	@Step("Open browser and goto SW5 url,  Method: {method}")
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Step("Create New StudentSet and store in global context,  Method: {method}")
	public void createNewStudentSet() throws Exception {
		InstructorCreateNewStudentSet logininst = new InstructorCreateNewStudentSet();

		String studentsetID = logininst.createNewStudentSet();
		if (studentsetID == null || studentsetID.isEmpty()) {
			throw new Exception("New student set is not generated");
		}
		// Setting studentsetID in Global Context
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		gContext.setAttribute(SW5Constants.STUDENT_SET_ID, studentsetID);
		SeleniumTestsContextManager.setGlobalContext(gContext);
	}

	@Step("Create New Student and join student set and store in global context,  Method: {method}")
	public void createNewStudentAndJoinStudentSet() throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		StudentCreateNewStudentAndJoin studentCreateNewStudentAndJoin = new StudentCreateNewStudentAndJoin();
		String studentUserName = studentCreateNewStudentAndJoin.createNewStudentAndJoinStudentSet();

		// Setting studentUserName in Global Context
		gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_1, studentUserName);
		SeleniumTestsContextManager.setGlobalContext(gContext);

		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		System.out.println("STUDENT_SET_ID => " + studentSetID);
		System.out.println("STUDENT_USER_NAME => " + studentUserName);

		LogUtil.log("STUDENT_SET_ID => " + studentSetID);
		LogUtil.log("STUDENT_USER_NAME => " + studentUserName);
	}


	
	/*@AfterSuite
	 	public void cleanupAfterTestSuite() throws Exception {
	 		// Do any cleanup activity
	-		driver.close();
	+		getDriver().close();
	+		getDriver().quit();
	 		//driver.quit();
	-	}*/
}
