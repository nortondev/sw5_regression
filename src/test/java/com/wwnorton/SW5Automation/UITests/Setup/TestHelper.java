package com.wwnorton.SW5Automation.UITests.Setup;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH11HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH12HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH13HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH14HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH15HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH16HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH1HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH2HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH3HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH4HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH5HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH6HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH7HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH8HW;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH9HW;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Step;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class TestHelper {

	@Step("Checking global context for Prerequisite values (StudentSet, StudentUsername),  Method: {method}")
	public void checkPrerequisiteInGlobalContext() throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		/* gContext.setAttribute(SW5Constants.ASSIGNMENT_CH14HW, "CH14HW"); 
 	    gContext.setAttribute(SW5Constants.STUDENT_SET_ID, "223855");
		gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_1, "automationtestaccount5@mailinator.com");
		SeleniumTestsContextManager.setGlobalContext(gContext); */

		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		if (studentSetID == null || studentSetID.isEmpty() || studentUsername == null || studentUsername.isEmpty())
			throw new Exception(
					"StudentSetID or StudentStudentUsername is null in global context. Please Add Test Case 'SetupTestData' as first test case in your current suite if not added already.");

		LogUtil.log("STUDENT_SET_ID => " + studentSetID);
		LogUtil.log("STUDENT_USER_NAME_1 => " + studentUsername);
	}

	@Step("Checking global context for Prerequisite values (StudentSet, StudentUsername for multiple students),  Method: {method}")
	public void checkPrerequisiteInGlobalContextMultipleStudents(int studentCount) throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		/* gContext.setAttribute(SW5Constants.STUDENT_SET_ID, "223855");
		gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_1, "automationtestaccount5@mailinator.com");
		gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_2, "automationtestaccount6@mailinator.com");

		gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_5, "john_mercey_m27ciprp3g@mailinator.com");
		SeleniumTestsContextManager.setGlobalContext(gContext); */

		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetID == null || studentSetID.isEmpty())
			throw new Exception(
					"StudentSetID is null in global context. Please Add Test Case 'SetupTestData' as first test case in your current suite if not added already.");

		LogUtil.log("STUDENT_SET_ID => " + studentSetID);

		for (int index = 1; index <= studentCount; index++) {
			String userNameContextKey = "";
			switch (index) {
			case 1:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_1;
				break;

			case 2:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_2;
				break;

			case 3:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_3;
				break;
			case 4:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_4;
				break;
			case 5:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_5;
				break;
			case 6:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_6;
				break;
			case 7:
				userNameContextKey = SW5Constants.STUDENT_USER_NAME_7;
				break;
			}

			if (userNameContextKey == null || userNameContextKey.isEmpty())
				throw new Exception("userNameContextKey is not found");

			String studentUsername = (String) gContext.getAttribute(userNameContextKey);
			if (studentUsername == null || studentUsername.isEmpty()) {
				StudentCreateNewStudentAndJoin studentCreateNewStudentAndJoin = new StudentCreateNewStudentAndJoin();
				String studentUserName = studentCreateNewStudentAndJoin
						.createAnotherNewStudentAndJoinStudentSet(userNameContextKey);

				// Setting studentUserName in Global Context
				gContext.setAttribute(userNameContextKey, studentUserName);
				SeleniumTestsContextManager.setGlobalContext(gContext);
			}
			studentUsername = (String) gContext.getAttribute(userNameContextKey);
			if (studentUsername == null || studentUsername.isEmpty())
				throw new Exception("StudentUsername  is null in global context for key => " + userNameContextKey);

			LogUtil.log(userNameContextKey + " => " + studentUsername);
		}
	}

	@Step("Get assignmnetTitle from global context if exists otherwise publish assignment and store in global context,  Method: {method} ")
	public String getPublishedAssignmnetTitle(String assignmnetTitleKey) throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(assignmnetTitleKey);

		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			// Publish assignment if assignmnetTitle not found in global context
			switch (assignmnetTitleKey) {
			case SW5Constants.ASSIGNMENT_CH1HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH1HW();
				break;
			case SW5Constants.ASSIGNMENT_CH2HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH2HW();
				break;
			case SW5Constants.ASSIGNMENT_CH3HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH3HW();
				break;
			case SW5Constants.ASSIGNMENT_CH4HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH4HW();
				break;
			case SW5Constants.ASSIGNMENT_CH5HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH5HW();
				break;
			case SW5Constants.ASSIGNMENT_CH6HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH6HW();
				break;
			case SW5Constants.ASSIGNMENT_CH7HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH7HW();
				break;
			case SW5Constants.ASSIGNMENT_CH8HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH8HW();
				break;
			case SW5Constants.ASSIGNMENT_CH9HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH9HW();
				break;
			case SW5Constants.ASSIGNMENT_CH11HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH11HW();
				break;
			case SW5Constants.ASSIGNMENT_CH12HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH12HW();
				break;
			case SW5Constants.ASSIGNMENT_CH13HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH13HW();
				break;
				
			case SW5Constants.ASSIGNMENT_CH14HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH14HW();
				break;
				
			case SW5Constants.ASSIGNMENT_CH15HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH15HW();
				break;
				
			case SW5Constants.ASSIGNMENT_CH16HW:
				assignmnetTitle = this.getPublishedAssignmentTitleCH16HW();
				break;

			}
		}
		return assignmnetTitle;
	}

	@Step("Logout of SW5,  Method: {method} ")
	public void logoutSmartwork5(WebDriver driver, String parentWindow, SW5DLPStudent sw5DLPStudent) throws Exception {
		try {
			driver.close();
			driver.switchTo().window(parentWindow);
			sw5DLPStudent.logoutSmartwork5();
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	@Step("Get Instructor Assignment Window,  Method: {method} ")
	public String getInstructorAssignmentWindow(WebDriver driver) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}
		driver.switchTo().frame("swfb_iframe");
		
		try {
			//WebElement missingParamterWindow = driver.findElement(By.xpath("/html/body/pre[contains(text(),'Missing required parameter.')]"));
			boolean isPresent = driver.findElements(By.xpath("/html/body/pre[contains(text(),'Missing required parameter.')]")).size() > 0;
			if (isPresent == true) {
				driver.navigate().refresh();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.log(e.getMessage());
			//System.out.println("Missing Paramter Window displayed and Refreshed");
		}
		
		Thread.sleep(3000);
		
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@class='container-box chapter-box mobileDevDescription']")));
		
		return childWindow;
	}

	@Step("Get Instructor Preview Assignment Window,  Method: {method} ")
	public void getInstructorPreviewAssignmentWindow(WebDriver driver, String mainParentWindow, String parentWindow) throws Exception {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(mainParentWindow);
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != mainParentWindow && winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);
	}

	@Step("Get Student Assignment Window,  Method: {method} ")
	public void getStudentAssignmentWindow(WebDriver driver, String parentWindow) throws Exception {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);
		/*try {
			//WebElement missingParamterWindow = driver.findElement(By.xpath("/html/body/pre[contains(text(),'Missing required parameter.')]"));
			boolean isPresent = driver.findElements(By.xpath("/html/body/pre[contains(text(),'Missing required parameter.')]")).size() > 0;
			if (isPresent == true) {
				driver.navigate().refresh();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtil.log(e.getMessage());
			//System.out.println("Missing Paramter Window displayed and Refreshed");
		}*/
		StudentAssignmentPage sap = new StudentAssignmentPage();
		sap.keepUsingTrialAccess();

		driver.switchTo().frame("swfb_iframe");

		sap.waitForLoadPageElements();
	}
	
	@Step("Get Student Assignment Window,  Method: {method} ")
	public void studentAssignmentWindow(WebDriver driver, String parentWindow) throws Exception {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);
		
		StudentAssignmentPage sap = new StudentAssignmentPage();
		sap.keepUsingTrialAccess();

		driver.switchTo().frame("swfb_iframe");
		
	}

	@Step("Get Student Assignment Window,  Method: {method} ")
	public void switchToStudentAssignmentWindow(WebDriver driver, String parentWindow) throws Exception {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);

		driver.switchTo().frame("swfb_iframe");
	}

	@Step("Get Instructor ClassActivityReport Window,  Method: {method} ")
	public String getInstructorClassActivityReportWindow(WebDriver driver) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}
		
		driver.switchTo().frame("swfb_iframe");
		try {
			WebElement missingParameterWindow = driver.findElement(By.xpath("/html/body/pre[contains(text(),'Missing required parameter.')]"));
			boolean carMissingParameter = missingParameterWindow.isDisplayed();
			if (carMissingParameter == true) {
				driver.navigate().refresh();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		Thread.sleep(10000);

		return childWindow;
	}
	
	@Step("Get eBook and Periodic Table Window,  Method: {method} ")
	public void geteBookWindow(WebDriver driver) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			if (winHandle != childWindow) {
				childWindow = winHandle;
			}
			
			driver.switchTo().window(childWindow);
		}
		
	}

	public String getStudentUserNameInUppercase(String studentUserName) throws Exception {
		if (studentUserName == null || studentUserName.isEmpty()) {
			throw new Exception("studentUserName is not valid.");
		}
		String retVal = "";
		String name = studentUserName.substring(0, studentUserName.indexOf('@')).toUpperCase();

		String firstName = name.substring(0, studentUserName.indexOf('_'));
		String lastName = name.substring(studentUserName.indexOf('_') + 1);
		retVal = lastName + ", " + firstName;
		return retVal;
	}

	@Step("Publish assignmnet CH1HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH1HW() throws Exception {
		InstructorPublishPremadeAssignmentCH1HW assignment = new InstructorPublishPremadeAssignmentCH1HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH1HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH1HW) != 0)
			throw new Exception("CH1HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH2HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH2HW() throws Exception {
		InstructorPublishPremadeAssignmentCH2HW assignment = new InstructorPublishPremadeAssignmentCH2HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH2HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH2HW) != 0)
			throw new Exception("CH2HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH3HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH3HW() throws Exception {
		InstructorPublishPremadeAssignmentCH3HW assignment = new InstructorPublishPremadeAssignmentCH3HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH3HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH3HW) != 0)
			throw new Exception("CH3HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH4HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH4HW() throws Exception {
		InstructorPublishPremadeAssignmentCH4HW assignment = new InstructorPublishPremadeAssignmentCH4HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH4HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH4HW) != 0)
			throw new Exception("CH4HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH5HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH5HW() throws Exception {
		InstructorPublishPremadeAssignmentCH5HW assignment = new InstructorPublishPremadeAssignmentCH5HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH5HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH5HW) != 0)
			throw new Exception("CH5HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH6HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH6HW() throws Exception {
		InstructorPublishPremadeAssignmentCH6HW assignment = new InstructorPublishPremadeAssignmentCH6HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH6HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH6HW) != 0)
			throw new Exception("CH6HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH7HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH7HW() throws Exception {
		InstructorPublishPremadeAssignmentCH7HW assignment = new InstructorPublishPremadeAssignmentCH7HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH7HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH7HW) != 0)
			throw new Exception("CH7HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH8HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH8HW() throws Exception {
		InstructorPublishPremadeAssignmentCH8HW assignment = new InstructorPublishPremadeAssignmentCH8HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH8HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH8HW) != 0)
			throw new Exception("CH8HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH9HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH9HW() throws Exception {
		InstructorPublishPremadeAssignmentCH9HW assignment = new InstructorPublishPremadeAssignmentCH9HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH9HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH9HW) != 0)
			throw new Exception("CH9HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH11HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH11HW() throws Exception {
		InstructorPublishPremadeAssignmentCH11HW assignment = new InstructorPublishPremadeAssignmentCH11HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH11HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH11HW) != 0)
			throw new Exception("CH12HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH12HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH12HW() throws Exception {
		InstructorPublishPremadeAssignmentCH12HW assignment = new InstructorPublishPremadeAssignmentCH12HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH12HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH12HW) != 0)
			throw new Exception("CH12HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}

	@Step("Publish assignmnet CH13HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH13HW() throws Exception {
		InstructorPublishPremadeAssignmentCH13HW assignment = new InstructorPublishPremadeAssignmentCH13HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH13HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH13HW) != 0)
			throw new Exception("CH13HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}
	
	@Step("Publish assignmnet CH14HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH14HW() throws Exception {
		InstructorPublishPremadeAssignmentCH14HW assignment = new InstructorPublishPremadeAssignmentCH14HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH14HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH14HW) != 0)
			throw new Exception("CH14HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}
	
	@Step("Publish assignmnet CH15HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH15HW() throws Exception {
		InstructorPublishPremadeAssignmentCH15HW assignment = new InstructorPublishPremadeAssignmentCH15HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH15HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH15HW) != 0)
			throw new Exception("CH15HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}
	
	
	@Step("Publish assignmnet CH16HW,  Method: {method} ")
	private String getPublishedAssignmentTitleCH16HW() throws Exception {
		InstructorPublishPremadeAssignmentCH16HW assignment = new InstructorPublishPremadeAssignmentCH16HW();
		assignment.updateSettingsAndPublish();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH16HW);
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (assignmnetTitle.compareTo(SW5Constants.ASSIGNMENT_CH16HW) != 0)

			throw new Exception("CH16HW assignment is not published in studentSetID => " + studentSetID);

		return assignmnetTitle;
	}
	
}
