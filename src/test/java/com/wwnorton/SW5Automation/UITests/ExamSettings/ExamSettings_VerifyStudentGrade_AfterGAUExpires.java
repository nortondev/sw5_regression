package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifyStudentGrade_AfterGAUExpires extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	AssignmentPage assignment;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}
	
		@Severity(SeverityLevel.NORMAL)
		@Description("Student Login - Verify Student Grades – Once GAU or GAU and late penalty expires.")
		@Stories("AS-354 Student Login - Verify Student Grades – Once GAU or GAU and late penalty expires.")
		@Test
		
		public void VerifyStudentGrade_AfterGAUExpires() throws Exception {

			driver = getDriver();
			
			SeleniumTestsContext gContext = SeleniumTestsContextManager
					.getGlobalContext();
			
			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
			String assignmentTitle = (String) gContext.getAttribute(SW5Constants.EXAMSettings_CUSTOMASSIGNMENT_1);
			String GAUTime = (String) gContext.getAttribute(SW5Constants.GAUTime_EXAMSettings_1);
			
			//String studentSetId = "331453";
			//String assignmentTitle = "Custom Assignment qytub";
			//String GAUTime = "03:00 PM";
			
			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
			
			// Login as Student 1 and submit the assignment.

			String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
			
			//String studentUserName1 = "john_mercey-gu75mb5tcz@mailinator.com";
			

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String studentWindow = driver.getWindowHandle();
			testHelper.studentAssignmentWindow(driver, studentWindow);
			sap = new StudentAssignmentPage();
			
			// Assert Assignment GAU Description on Student Assignment page.
			String GAUDescriptionText = "Exam was accepted until " + GetDate.getFormattedDate(0) 
				+ ", at " + GAUTime + " (Eastern Time).";
			
			String strGAUDescriptionAlert = sap.ExamSettings_getAssignmentDescription_SAP_AfterGAU();
			Assert.assertTrue(GAUDescriptionText.trim().contains(strGAUDescriptionAlert.trim()),
					"Exam Settings Assignment Description is displayed?");
			
			Thread.sleep(2000);
			Assert.assertTrue(sap.isReviewExamButtonDisplayed());

			Thread.sleep(2000);
			Assert.assertTrue(sap.getHeaderScore().contains("33%"),"Student Garde is displayed?");
			
			List<String> questionPointsList = sap.getAllQuestionPointsList();
			Assert.assertTrue(questionPointsList.get(0).equals("1 / 1"));
			Assert.assertTrue(questionPointsList.get(1).equals("2 / 3"));
			Assert.assertTrue(questionPointsList.get(2).equals("0 / 2"));
			Assert.assertTrue(questionPointsList.get(3).equals("0 / 1"));
			Assert.assertTrue(questionPointsList.get(4).equals("0 / 2"));
			
			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(studentWindow);
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.logoutSmartwork5();

		}

		// End of Test cases
		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
	}
