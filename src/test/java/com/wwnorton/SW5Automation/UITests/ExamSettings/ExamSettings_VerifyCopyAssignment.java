package com.wwnorton.SW5Automation.UITests.ExamSettings;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewSS;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet6;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifyCopyAssignment extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	CustomAssignmentQuestionSet6 custassignment;
	CreateNewSS createStudentSet;
	TestHelper testHelper = new TestHelper();
	
	//public String assignmentTitle;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Exam Settings: - Instructor Login – Copy Assignment")
	@Stories("AS-400 Exam Settings: - Instructor Login – Copy Assignment")
	@Test
	public void verifyCopyAssignment() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();

		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		String assignmentTitle = assignmentTitleSuffix + 
				GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();

		assignment.editAssignmentbutton();

		assignment.enterAssignmentName(assignmentTitle);
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");

		if (assignment.examModeButton_Off_Enabled() == true) {

			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);

		}
		
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		if(assignment.randomizeQuestionButton_On_Enabled() == false) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.RandomizeQuestionOff);
			
		}	

		assignment.selectshowstudentscore("Points");

		String questionsSetName = SW5Constants.QUESTION_SET_6;

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);

		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		Thread.sleep(2000);
		int pointsPerQuestion = 2;
		if (pointsPerQuestion > 0) {
			ReusableMethods.scrollToBottom(driver);
			assignment.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			assignment.ApplyToAllButton.click();
			Thread.sleep(2000);
		}

		Thread.sleep(2000);
		assignment.saveButton();

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		Thread.sleep(3000);
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		Thread.sleep(8000);
		
		SW5DLPpage.clickCopyButton(assignmentTitle);
		
		String winHandleBefore2 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String strcopiedAssignmentName = "Copy of "+ assignmentTitle;
		String copiedAssignmentName = assignment.examMode_AssignmentName();
		
		Thread.sleep(2000);
		Assert.assertEquals(copiedAssignmentName, strcopiedAssignmentName);
		Assert.assertEquals(assignment.getHeaderScore(), "0 out of 10");
		
		assignment.editAssignmentbutton();
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		Thread.sleep(2000);
		Assert.assertEquals(assignment.examModeButton_Off_Enabled(), true);
		
		Thread.sleep(3000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		Assert.assertTrue(assignment.getShowStudentScoreValue().equals("Points"));
		Assert.assertEquals(assignment.randomizeQuestionButton_On_Enabled(), true);

		ReusableMethods.scrollToBottom(driver);
		List<String> questionPointsList = assignment.examMode_PointsPerQuestion();
		Assert.assertTrue(questionPointsList.get(0).equals("2"));
		Assert.assertTrue(questionPointsList.get(1).equals("2"));
		Assert.assertTrue(questionPointsList.get(2).equals("2"));
		Assert.assertTrue(questionPointsList.get(3).equals("2"));
		Assert.assertTrue(questionPointsList.get(4).equals("2"));
		
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, assignment.ExamModeOff);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);

		Thread.sleep(2000);
		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		
		TimeUnit.SECONDS.sleep(5);
		
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.ClickAssignmentlink(strcopiedAssignmentName);
		
		Thread.sleep(3000);
		String winHandleBefore3 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment.editAssignmentbutton();
		
		Thread.sleep(2000);
		Assert.assertEquals(assignment.examModeButton_Off_Enabled(), false);
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		Assert.assertTrue(assignment.getShowStudentScoreValue().equals("Points"));
		
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
        
		SW5DLPpage.logoutSW5();
		
	
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
