package com.wwnorton.SW5Automation.UITests.ExamSettings;


import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewSS;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet6;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifyCopyStudentSet extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	CustomAssignmentQuestionSet6 custassignment;
	CreateNewSS createStudentSet;
	TestHelper testHelper = new TestHelper();
	
	//public String assignmentTitle;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Exam Settings: - Instructor Login – Copy Student Set")
	@Stories("AS-399 Exam Settings: - Instructor Login – Copy Student Set")
	@Test
	public void verifyCopyStudentSet() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();
		
		// Click the Manage Student Set Link and Create new Student
		createStudentset = new CreateNewStudentSet();
		createStudentset.SignIn_Register();
		createStudentset.ManageStudentSetlink();
		createStudentset.CreateStudentSetButton();
		createStudentset.createNewStudentset();
		createStudentset.createStudentset_information();
		String sourceStudentID = createStudentset.createStudentset_ID();
		System.out.println(sourceStudentID);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(sourceStudentID);
		Thread.sleep(8000);
		
		
		// Create Exam Mode assignment and Publish
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "ExamMode Assignment ";
		int suffixCharCount = 5;
		String assignmentTitle_ExamMode = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(assignmentTitle_ExamMode);
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		if(assignment.examModeButton_Off_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);
			
		}
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		
		Thread.sleep(2000);
		if(assignment.showEbookLinkButton_On_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ShowEbookLinkON);
			
		}
		
		Thread.sleep(2000);
		if(assignment.showPeriodicTableButton_On_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ShowPeriodicTableON);
			
		}	
		
		Thread.sleep(2000);
		assignment.selectshowstudentscore("Points");
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		Thread.sleep(2000);
		int pointsPerQuestion = 2;
		if (pointsPerQuestion > 0) {
			ReusableMethods.scrollToBottom(driver);
			assignment.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			assignment.ApplyToAllButton.click();
			Thread.sleep(2000);
		}
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		Thread.sleep(5000);
		SW5DLPpage.selectByPartOfVisibleText(sourceStudentID);
		Thread.sleep(8000);
		
		//Verify Exam mode Assignment is displayed for Source SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String sourceAssignmentTitle_ExamMode = SW5DLPpage.getAssignmentName(assignmentTitle_ExamMode);
		Assert.assertTrue(assignmentTitle_ExamMode.equalsIgnoreCase(sourceAssignmentTitle_ExamMode));

		boolean publishCheckBox_ExamMode_IsChecked_Active = SW5DLPpage.checkPublishCheckbox(assignmentTitle_ExamMode);
		Assert.assertEquals(true, publishCheckBox_ExamMode_IsChecked_Active);
		
		
		// Create Regular assignment and Publish
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore2 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		String assignmentTitleSuffix_Regular = "Regular Assignment ";
		String assignmentTitle_Regular = assignmentTitleSuffix_Regular
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();

		assignment.editAssignmentbutton();

		assignment.enterAssignmentName(assignmentTitle_Regular);
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);

		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		Thread.sleep(2000);
		assignment.saveButton();

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		
		Thread.sleep(3000);
		SW5DLPpage.selectByPartOfVisibleText(sourceStudentID);
		Thread.sleep(8000);
		
		//Verify Regular Assignment is displayed for Source SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String sourceAssignmentTitle_Regular = SW5DLPpage.getAssignmentName(assignmentTitle_Regular);
		Assert.assertTrue(assignmentTitle_Regular.equalsIgnoreCase(sourceAssignmentTitle_Regular));

		boolean publishCheckBox_Regular_IsChecked_Active = SW5DLPpage.checkPublishCheckbox(assignmentTitle_Regular);
		Assert.assertEquals(true, publishCheckBox_Regular_IsChecked_Active);
		
		// Create Full Adaptive assignment and Save
		Thread.sleep(3000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.clickFullAdaptiveButton();
		
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		
		driver.switchTo().window(ChildWindow);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		String AssignmentName_FullAdaptive = "FullAdaptive ";
		String assignmentTitle_FullAdaptive = AssignmentName_FullAdaptive
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.enterAssignmentName(assignmentTitle_FullAdaptive);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(assignment.addOBJECTIVES));
		assignment.addObejctives();

		assignment.saveButton();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		driver.close();
		driver.switchTo().window(ParentWindow);

		Thread.sleep(3000);
		SW5DLPpage.selectByPartOfVisibleText(sourceStudentID);
		Thread.sleep(8000);

		// Verify Full Adaptive Assignment is displayed for Source SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String sourceAssignmentTitle_FullAdaptive = SW5DLPpage.getAssignmentName(assignmentTitle_FullAdaptive);
		Assert.assertTrue(assignmentTitle_FullAdaptive.equalsIgnoreCase(sourceAssignmentTitle_FullAdaptive));

		boolean publishCheckBox_FullAdaptive_IsChecked_Active = SW5DLPpage.checkPublishCheckbox(assignmentTitle_FullAdaptive);
		Assert.assertEquals(false, publishCheckBox_FullAdaptive_IsChecked_Active);
		
		
		// Create Partial Adaptive assignment and Publish
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore3 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		String assignmentTitleSuffix_Partial = "PartialAdaptive ";
		String assignmentTitle_PartialAdaptive = assignmentTitleSuffix_Partial
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();

		assignment.editAssignmentbutton();
		assignment.enterAssignmentName(assignmentTitle_PartialAdaptive);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		Thread.sleep(2000);
		
		assignment.adaptiveOn();
		assignment.warmUp();

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);

		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(assignment.SELECTOBJECTIVES));
		assignment.selectObejctive();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		assignment.saveButton();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		
		Thread.sleep(3000);
		SW5DLPpage.selectByPartOfVisibleText(sourceStudentID);
		Thread.sleep(8000);
		
		//Verify Partial Adaptive Assignment is displayed for Source SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String sourceAssignmentTitle_PartialAdaptive = SW5DLPpage.getAssignmentName(assignmentTitle_PartialAdaptive);
		Assert.assertTrue(assignmentTitle_PartialAdaptive.equalsIgnoreCase(sourceAssignmentTitle_PartialAdaptive));

		boolean publishCheckBox_PartialAdaptive_IsChecked_Active = SW5DLPpage.checkPublishCheckbox(assignmentTitle_PartialAdaptive);
		Assert.assertEquals(false, publishCheckBox_PartialAdaptive_IsChecked_Active);
		
		createStudentSet = new CreateNewSS(driver);
		String StudentSetTitlePrefix = "TargetStudentSet_";
		String StudentSetTitle = StudentSetTitlePrefix
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		String SchoolName = "Alabama State University";
		String SSStartDate = GetDate.getCurrentDate();
		String SSEndDate = GetDate.getNextmonthDate();
		
		createStudentset = new CreateNewStudentSet();
		createStudentSet.manageStudentSet();
		createStudentset.CreateStudentSetButton();
		createStudentSet.createNewStudentSetWithCopySameInstructor(sourceStudentID);
		createStudentSet.studentSetInformationCopy(StudentSetTitle, 
				SchoolName, SSStartDate, SSEndDate);
		String targetStudentID = createStudentset.createStudentset_ID();
		System.out.println(targetStudentID);
		
		// Close the Manage Student Set page and navigate to Smartwork5 page
		managestudentsetpage = new ManageStudentSetsPage();
		managestudentsetpage.clickcloselink();
		DLPpage = new LoginPage();
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(targetStudentID);
		Thread.sleep(8000);
		
		//Verify Exam mode Assignment is displayed for Target SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String targetAssignmentTitle_ExamMode = SW5DLPpage.getAssignmentName(assignmentTitle_ExamMode);
		Assert.assertTrue(assignmentTitle_ExamMode.equalsIgnoreCase(targetAssignmentTitle_ExamMode));

		boolean publishCheckBox_ExamMode_IsChecked_Target = SW5DLPpage.checkPublishCheckbox(assignmentTitle_ExamMode);
		Assert.assertEquals(false, publishCheckBox_ExamMode_IsChecked_Target);
		
		//Verify Regular Assignment is displayed for Target SS in SW5 DLP page.
		String targetAssignmentTitle_Regular = SW5DLPpage.getAssignmentName(assignmentTitle_Regular);
		Assert.assertTrue(assignmentTitle_Regular.equalsIgnoreCase(targetAssignmentTitle_Regular));

		boolean publishCheckBox_Regular_IsChecked_Target = SW5DLPpage.checkPublishCheckbox(assignmentTitle_Regular);
		Assert.assertEquals(false, publishCheckBox_Regular_IsChecked_Target);
		
		// Verify Full Adaptive Assignment is displayed for Target SS in SW5 DLP page.
		String targetAssignmentTitle_FullAdaptive = SW5DLPpage.getAssignmentName(assignmentTitle_FullAdaptive);
		Assert.assertTrue(assignmentTitle_FullAdaptive.equalsIgnoreCase(targetAssignmentTitle_FullAdaptive));

		boolean publishCheckBox_FullAdaptive_IsChecked_Target = SW5DLPpage
				.checkPublishCheckbox(assignmentTitle_FullAdaptive);
		Assert.assertEquals(false, publishCheckBox_FullAdaptive_IsChecked_Target);
		
		//Verify Partial Adaptive Assignment is displayed for Target SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		String targetAssignmentTitle_PartialAdaptive = SW5DLPpage.getAssignmentName(assignmentTitle_PartialAdaptive);
		Assert.assertTrue(assignmentTitle_PartialAdaptive.equalsIgnoreCase(targetAssignmentTitle_PartialAdaptive));

		boolean publishCheckBox_PartialAdaptive_IsChecked_Target = SW5DLPpage
				.checkPublishCheckbox(assignmentTitle_PartialAdaptive);
		Assert.assertEquals(false, publishCheckBox_PartialAdaptive_IsChecked_Target);
		
		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle_ExamMode);
		
		Thread.sleep(3000);
		String winHandleBefore4 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		Thread.sleep(2000);
		Assert.assertEquals(assignment.examModeButton_Off_Enabled(), true);
		
		
		Thread.sleep(3000);
		assignment.ShowAdditionalSettingsButton();
		Thread.sleep(2000);
		Assert.assertTrue(assignment.getShowStudentScoreValue().equals("Points"));
		Assert.assertEquals(assignment.showEbookLinkButton_On_Enabled(), false);
		Assert.assertEquals(assignment.showPeriodicTableButton_On_Enabled(), false);
		ReusableMethods.scrollToBottom(driver);
		List<String> questionPointsList = assignment.examMode_PointsPerQuestion();
		Assert.assertTrue(questionPointsList.get(0).equals("2"));
		Assert.assertTrue(questionPointsList.get(1).equals("2"));
		Assert.assertTrue(questionPointsList.get(2).equals("2"));
		Assert.assertTrue(questionPointsList.get(3).equals("2"));
		Assert.assertTrue(questionPointsList.get(4).equals("2"));
		
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, assignment.ExamModeOff);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);

		Thread.sleep(2000);
		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		
		Thread.sleep(5000);
		SW5DLPpage.selectByPartOfVisibleText(targetStudentID);
		Thread.sleep(8000);
		
		//Verify Exam mode Assignment is Published for Target SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		boolean publishCheckBox_ExamMode_IsPublished_Target = SW5DLPpage.checkPublishCheckbox(assignmentTitle_ExamMode);
		Assert.assertEquals(true, publishCheckBox_ExamMode_IsPublished_Target);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
        
		SW5DLPpage.logoutSW5();
		
	
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
