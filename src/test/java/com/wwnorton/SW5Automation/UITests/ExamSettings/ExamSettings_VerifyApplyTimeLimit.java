package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet6;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifyApplyTimeLimit extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;

	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	CustomAssignmentQuestionSet6 custassignment;
	InstructorPreviewAssignmentPage InstructorAssignment;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	TestHelper testHelper = new TestHelper();
	
	public String assignmentTitle;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Exam Settings: - Instructor Login – CAR – Student Detail overlay – Apply Time limit.")
	@Stories("AS-359 - Instructor Login – CAR – Student Detail overlay – Apply Time limit.")
	@Test
	public void verifyApplyTimeLimit() throws Exception {
		
		driver = getDriver();
		// Login as Instructor
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);
				
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		assignmentTitle = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(assignmentTitle);
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		if(assignment.examModeButton_Off_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);
			
		}
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		assignment.timeLimitOn();

		// Set the time limit 15 mins
		assignment.setTimeLimit("15");
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		String studentName1 = testHelper.getStudentUserNameInUppercase(studentUserName1);
		
		Thread.sleep(2000);
		InstructorAssignment = new InstructorPreviewAssignmentPage();
		InstructorAssignment.clickCARButton();
		
		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);
		classActivityReport = new ClassActivityReportPage();
		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(studentName1);
		
		studentDetailOverlay = new StudentDetailOverlayPage();
		Thread.sleep(5000);
		studentDetailOverlay.clickEditButton();
		Thread.sleep(2000);
		studentDetailOverlay.timeLimitInput.clear();
		studentDetailOverlay.timeLimitInput.sendKeys("1450");
		Assert.assertEquals(studentDetailOverlay.examSettingsMode_InvalidTimeLimitPopUp(), true);
		Assert.assertTrue(studentDetailOverlay.getExamSettings_InvalidTimeLimitMsg().
				equalsIgnoreCase("This time limit exceeds the Grades Accepted Until (GAU) "
						+ "date. In order to set this time limit you need to extend the GAU "
						+ "date from the Edit Assignment Screen."));
		
		studentDetailOverlay.InvalidTimeLimitPopUp_Close.click();
		Thread.sleep(2000);
		int updatedTimeLimit = 60;
		studentDetailOverlay.timeLimitInput.clear();
		studentDetailOverlay.timeLimitInput.sendKeys(String.valueOf(updatedTimeLimit));
		Thread.sleep(2000);
		studentDetailOverlay.clickSaveButton();
		Thread.sleep(2000);
		studentDetailOverlay.clickEditButton();
		Thread.sleep(2000);
		Assert.assertEquals(studentDetailOverlay.examSettingsMode_UpdatedInvalidTimeLimit(
				updatedTimeLimit), true);
		Thread.sleep(2000);
		studentDetailOverlay.clickSaveButton();
		Thread.sleep(2000);
		studentDetailOverlay.clickCloseStudentDetailOverlay();
		
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
