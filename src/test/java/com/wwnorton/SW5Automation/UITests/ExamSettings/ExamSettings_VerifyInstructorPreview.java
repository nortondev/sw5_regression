package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet6;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifyInstructorPreview extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;

	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	CustomAssignmentQuestionSet6 custassignment;
	AssignmentPage instructorAssignmentPage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentQuestionSet6 custAssignmentAnswers;
	StudentQuestionPlayer sqp;
	TestHelper testHelper = new TestHelper();
	
	public String assignmentTitle;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Exam Settings: - Instructor Login – Preview.")
	@Stories("AS-397 - Exam Settings: - Instructor Login – Preview.")
	@Test
	public void verifyInstructorPreview() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
				
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		instructorAssignmentPage = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		assignmentTitle = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		instructorAssignmentPage.editAssignmentbutton();
		
		instructorAssignmentPage.enterAssignmentName(assignmentTitle);
		Thread.sleep(2000);
		instructorAssignmentPage.enterGAUDate(GetDate.getCurrentDate());
		instructorAssignmentPage.selecttimeZone("(GMT-05:00) Eastern Time");
		
		if(instructorAssignmentPage.examModeButton_Off_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					instructorAssignmentPage.ExamModeOff);
			
		}
		
		Thread.sleep(2000);
		instructorAssignmentPage.ShowAdditionalSettingsButton();
		
		// Set the time limit 15 mins
		instructorAssignmentPage.timeLimitOn();		
		instructorAssignmentPage.setTimeLimit("15");
		
		Thread.sleep(2000);
		if(instructorAssignmentPage.showEbookLinkButton_On_Enabled() == false) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					instructorAssignmentPage.ShowEbookLinkOff);
			
		}
		
		Thread.sleep(2000);
		if(instructorAssignmentPage.showPeriodicTableButton_On_Enabled() == false) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					instructorAssignmentPage.ShowPeriodicTableOff);
			
		}
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		Thread.sleep(2000);
		instructorAssignmentPage.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		instructorAssignmentPage.publishButton();
		Assert.assertTrue(instructorAssignmentPage.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		Assert.assertTrue(instructorAssignmentPage.isPreviewButtonDisplayed(), 
				"Preview Logo is displayed?");

		String parentWindow1 = driver.getWindowHandle();
		instructorAssignmentPage.clickPreviewButton();

		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver, winHandleBefore1, parentWindow1);

		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		// # Use case: Verify student assignment page - first time
		Thread.sleep(3000);
		Assert.assertTrue(instPreviewAssignmentPage.isBeginExamButtonDisplayed());
		Assert.assertTrue(instPreviewAssignmentPage.isPreviewIconDisplayed(), 
				"Preview Logo is displayed?");
		Assert.assertTrue(instPreviewAssignmentPage.getExamText().contains("EXAM"),
				"Exam Text is displayed?");
		
		// Assert Assignment GAU Info on Instructor Assignment page.
		String assignmentInfoBoxText = "Exam is accepted until" + GetDate.getFormattedDate(0) + 
				", at 11:59 PM (Eastern Time)" + "This exam has a time limit of 15 min.";
		
		Assert.assertEquals(instPreviewAssignmentPage.getAssignmentInfoBox().trim(), 
				assignmentInfoBoxText.trim());
		
		// # Use case: Verify Instructor Assignment page - first time
		Assert.assertTrue(instPreviewAssignmentPage.isBeginExamButtonDisplayed());
		Assert.assertTrue(instPreviewAssignmentPage.verifyInitialAllQuestionPoints());
		Assert.assertTrue(instPreviewAssignmentPage.verifyInitialAllQuestionStatus());
		Assert.assertTrue(instPreviewAssignmentPage.isTimerIconDisplayed());
		Assert.assertEquals(instPreviewAssignmentPage.getTimerText(), "00:15:00", 
				"Initial timer value is 15 minutes?");

		instPreviewAssignmentPage.beginExamClick();
		Thread.sleep(5000);

		Assert.assertTrue(instPreviewAssignmentPage.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		instPreviewAssignmentPage.clickNoIamNotReadyToBeginButton();

		Assert.assertTrue(instPreviewAssignmentPage.isBeginExamButtonDisplayed());
		Assert.assertTrue(instPreviewAssignmentPage.isTimerIconDisplayed());
		Assert.assertEquals(instPreviewAssignmentPage.getTimerText(), "00:15:00", 
				"Initial timer value is 15 minutes?");

		instPreviewAssignmentPage.beginExamClick();
		Thread.sleep(5000);

		Assert.assertTrue(instPreviewAssignmentPage.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		instPreviewAssignmentPage.clickYesIamReadyToBeginButton();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();
		Thread.sleep(2000);

		String timerText1 = sqp.getTimerText();
		Assert.assertTrue(timerText1.compareTo("00:15:00") != 0, "Is timer started?");
		Thread.sleep(5000);
		Assert.assertTrue(sqp.getTimerText().compareTo(timerText1) != 0, 
				"Is timer coninues?");
		Assert.assertTrue(sqp.getExamSettings_HeaderStudentScore().contains("EXAM"),
				"Exam Text is displayed?");
		Assert.assertEquals(sqp.examSettings_GAUText.getText(), GetDate.getCurrentDate());
		

		// START - useCase: Submit assignment when time limit is "ON"
		// to make sure the submission went through.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertTrue(sqp.getExamSettings_HeaderStudentScore().contains("EXAM"),
				"Exam Text is displayed?");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"0 OF 5 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/05");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		//Check eBook link and Periodic Table link are displayed.
		Assert.assertEquals(sqp.iseBookLinkDisplayed(),true);
		if(sqp.iseBookLinkDisplayed() == true) {
			String parentWindow2 = driver.getWindowHandle();
			// eBook Window
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", sqp.eBookPageLink);
			Thread.sleep(2000);
			testHelper.geteBookWindow(driver);
			String eBookUrl = driver.getCurrentUrl();
			Assert.assertTrue(eBookUrl.contains("/83876?page=14"));
			driver.close();
			driver.switchTo().window(parentWindow2);
		}
		
		Thread.sleep(2000);
		Assert.assertEquals(sqp.isPeriodicTableDisplayed(), true);
		if(sqp.isPeriodicTableDisplayed() == true) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", sqp.periodicTableButton);
			// Periodic Table PopUp Window
			Thread.sleep(2000);
			Assert.assertTrue(sqp.periodicTablePopUp.isDisplayed());
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", sqp.periodicTablePopUp_Close);
		}
		
		// Question #1 - First Attempt Incorrect Answer
		custAssignmentAnswers = new CustomAssignmentQuestionSet6();
		custAssignmentAnswers.question1Answers();
		sqp.clickSubmitAnswer();
		
		TimeUnit.SECONDS.sleep(5);
		Assert.assertEquals(sqp.isFeedbackOverlayDisplayed(), false);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
		Assert.assertTrue(sqp.getExamSettings_HeaderStudentScore().contains("EXAM"),
				"Exam Text is displayed?");

		// Check Try Again and View Solution Buttons at the footer.
		Thread.sleep(2000);
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false, 
				"Submit Button is displayed in the footer?");
		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), false, 
				"Try Again Button is displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false,
				"View Solution Button is displayed in the footer?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");
		
		
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		if (instructorAssignmentPage.showAdditionalSettingsButton_Dispalyed() == true) {
			instructorAssignmentPage.ShowAdditionalSettingsButton();
		}
		
		if(instructorAssignmentPage.showEbookLinkButton_On_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					instructorAssignmentPage.ShowEbookLinkON);
			
		}
		
		Thread.sleep(2000);
		if(instructorAssignmentPage.showPeriodicTableButton_On_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					instructorAssignmentPage.ShowPeriodicTableON);
			
		}
	
		
		Thread.sleep(2000);
		instructorAssignmentPage.saveButton();
		
		String parentWindow2 = driver.getWindowHandle();
		instructorAssignmentPage.clickPreviewButton();

		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver, winHandleBefore1, parentWindow2);
		
		Thread.sleep(3000);
		Assert.assertTrue(instPreviewAssignmentPage.isPreviewIconDisplayed(), 
				"Preview Logo is displayed?");
		
		instPreviewAssignmentPage.beginExamClick();
		Thread.sleep(5000);

		Assert.assertTrue(instPreviewAssignmentPage.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		instPreviewAssignmentPage.clickYesIamReadyToBeginButton();
		Thread.sleep(5000);
		
		//Check eBook link and Periodic Table link are NOT displayed.
		Assert.assertEquals(sqp.iseBookLinkDisplayed(),false);
		Assert.assertEquals(sqp.isPeriodicTableDisplayed(), false);
		
		driver.close();
		driver.switchTo().window(parentWindow2);
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
