package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_VerifySaveToAll_PublishToAll extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	TestHelper testHelper = new TestHelper();
	String ActiveSSID;
	String ExpiredSSID;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}
	

	@Severity(SeverityLevel.NORMAL)
	@Description("Exam Settings: - Instructor Login – Save to all/Publish to all.")
	@Stories("AS-398 - Instructor Login – Save to all/Publish to all.")
	@Test
	public void verifySaveToAll_PublishToAll() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		Thread.sleep(5000);
		SW5DLPpage.ClickAssignment();
		
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		
		String winHandleBefore1 = driver.getWindowHandle();
		String ParentWindow = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow);
		
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow) {
			ChildWindow = winHandle;
		}
		
		driver.switchTo().window(ChildWindow);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(
						"//span[@class='instructor-intro-assignment-title']")));
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		String AssignmentName = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(AssignmentName);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		if(assignment.examModeButton_Off_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);
			
		}
		
		assignment.adaptiveOff();
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		assignment.checkApplyToAllStudentSets();
		assignment.saveButton();
		assignment.saveToAllStudentSetsButton();
		
		//wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		TimeUnit.SECONDS.sleep(5);
		ActiveSSID = jsonObj.getAsJsonObject("StudentSetInfo").get("StudentSetID_Active").getAsString();
		SW5DLPpage.selectByPartOfVisibleText(ActiveSSID);
		
		//Verify Exam mode Assignment is displayed for Active SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(25);
		String assignmentTitle_Active = SW5DLPpage.getAssignmentName(AssignmentName);
		Assert.assertTrue(AssignmentName.equalsIgnoreCase(assignmentTitle_Active));
		
		boolean publishCheckBox_IsChecked_Active = SW5DLPpage.checkPublishCheckbox(AssignmentName);
		Assert.assertEquals(false, publishCheckBox_IsChecked_Active);
		
		//Verify Exam mode toggle is ON in Edit Assignment page.
		SW5DLPpage.ClickAssignmentlink(assignmentTitle_Active);
		String winHandleBefore2 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		TimeUnit.SECONDS.sleep(10);
		Assert.assertEquals(true, assignment.isApplyToAllStudentSet_CheckboxEnabled());
		Assert.assertEquals(false, assignment.examModeButton_Off_Enabled());
		
		TimeUnit.SECONDS.sleep(5);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		
		//Verify Exam mode Assignment is Not displayed for Expired SS in SW5 DLP page.
		TimeUnit.SECONDS.sleep(5);
		ExpiredSSID = jsonObj.getAsJsonObject("StudentSetInfo").get("StudentSetID_Expired").getAsString();
		SW5DLPpage.selectByPartOfVisibleText(ExpiredSSID);
		
		TimeUnit.SECONDS.sleep(25);
		//String assignmentTitle_Exp = SW5DLPpage.getAssignmentName(AssignmentName);
		Assert.assertFalse(SW5DLPpage.checkAssignmentName(AssignmentName));
		
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		String winHandleBefore3 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(AssignmentName);
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment.editAssignmentbutton();
		
		assignment.checkApplyToAllStudentSets();
		
		TimeUnit.SECONDS.sleep(10);
		assignment.publishToAllButton();
		assignment.publishToAllStudentSetsButton();
		
		Assert.assertTrue(assignment.isUnpublishToAllButtonDisplayed());
		
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		
		TimeUnit.SECONDS.sleep(10);
		ActiveSSID = jsonObj.getAsJsonObject("StudentSetInfo").get("StudentSetID_Active").getAsString();
		SW5DLPpage.selectByPartOfVisibleText(ActiveSSID);
		
		TimeUnit.SECONDS.sleep(25);
		String assignmentTitle_Active_Published = SW5DLPpage.getAssignmentName(AssignmentName);
		Assert.assertTrue(AssignmentName.equalsIgnoreCase(assignmentTitle_Active_Published));
		
		boolean publishCheckBox_IsChecked_Active_Publish = SW5DLPpage.checkPublishCheckbox(
				AssignmentName);
		Assert.assertEquals(true, publishCheckBox_IsChecked_Active_Publish);
		
		//Verify Exam mode toggle is ON in Edit Assignment page.
		SW5DLPpage.ClickAssignmentlink(assignmentTitle_Active);
		String winHandleBefore4 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();

		TimeUnit.SECONDS.sleep(5);
		Assert.assertEquals(true, assignment.isApplyToAllStudentSet_CheckboxEnabled());
		Assert.assertEquals(false, assignment.examModeButton_Off_Enabled());

		TimeUnit.SECONDS.sleep(10);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		
		TimeUnit.SECONDS.sleep(10);
		ExpiredSSID = jsonObj.getAsJsonObject("StudentSetInfo").get("StudentSetID_Expired").getAsString();
		SW5DLPpage.selectByPartOfVisibleText(ExpiredSSID);
		
		TimeUnit.SECONDS.sleep(25);
		//String assignmentTitle_Exp_Published = SW5DLPpage.getAssignmentName(AssignmentName);
		Assert.assertFalse(SW5DLPpage.checkAssignmentName(AssignmentName));

		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();
		
		
		// Login as Student

		String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		new LoginAsStudent().LoginStudent(studentUserName1);
		
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.addStudentToStudentSet(ActiveSSID);
		
		SW5Login = new LoginPage();
		SW5Login.clickSW5Icon();
		
		Thread.sleep(2000);
		SW5DLPstudent.selectByPartOfVisibleText(ActiveSSID);
		
		Thread.sleep(2000);
		SW5DLPstudent.SelectAssignment(AssignmentName);

		// Assignment Window
		String parentWindow3 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow3);

		sap = new StudentAssignmentPage();
		Thread.sleep(2000);
		sap.beginExamClick();
		sqp = new StudentQuestionPlayer();

		driver.close();
		driver.switchTo().window(parentWindow3);
		SW5DLPstudent.logoutSmartwork5();
		
				
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
