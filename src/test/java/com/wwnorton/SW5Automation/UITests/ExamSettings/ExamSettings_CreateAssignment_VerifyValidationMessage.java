package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ExamSettings_CreateAssignment_VerifyValidationMessage extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;

	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify validation message in Edit Assignment page in Exam Settings Mode.")
	@Stories("AS-352 Create Exam mode assignment - Save/Publish – Verify validation message.")
	@Test
	public void CreateAssignment_VerifyValidationMessage() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		String assignmentTitle = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(assignmentTitle);
		Thread.sleep(2000);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		if(assignment.examModeButton_Off_Enabled() == true) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.ExamModeOff);
			
		}
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, (By.xpath("//input[@class='ant-calendar-picker-input ant-input']")));
		assignment.calendarDate.click();
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
				assignment.calendarInput_ClearButton);
		
		
		Thread.sleep(2000);
		assignment.saveButton();
		String unableToSaveMsg_SAVE = assignment.examMode_UnableToSaveValidation();
		Assert.assertTrue(unableToSaveMsg_SAVE.
				equalsIgnoreCase("You must set a Grades Accepted Until (GAU) date in order to save this Exam."));
		
		Thread.sleep(2000);
		assignment.PublishButton.click();
		String unableToSaveMsg_PUBLISH = assignment.examMode_UnableToSaveValidation();
		Assert.assertTrue(unableToSaveMsg_PUBLISH.
				equalsIgnoreCase("You must set a Grades Accepted Until (GAU) date in order to save this Exam."));
		
		
		Thread.sleep(2000);
		ReusableMethods.scrollToElement(driver, (By.xpath("//input[@class='ant-calendar-picker-input ant-input']")));
		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);

		SW5DLPpage.logoutSW5();
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
