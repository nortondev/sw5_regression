package com.wwnorton.SW5Automation.UITests.ExamSettings;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class ExamSettings_VerifyStudentQuestionPlayer_AfterGAUExpires extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}
	
		@Severity(SeverityLevel.NORMAL)
		@Description("Student Login - Verify Student Question player and grades – Once GAU or GAU and late penalty expires.")
		@Stories("AS-356 - Student login and Student Question player and grades after GAU or GAU and late penalty expires.")
		@Test
		
		public void VerifyStudentQuestionPlayer_AfterGAUExpires() throws Exception {

			driver = getDriver();
			
			SeleniumTestsContext gContext = SeleniumTestsContextManager
					.getGlobalContext();
			
			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
			String assignmentTitle = (String) gContext.getAttribute(SW5Constants.EXAMSettings_CUSTOMASSIGNMENT_2);
			
			//String studentSetId = "331453";
			//String assignmentTitle = "Custom Assignment ua1gt";
			
			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
			
			// Login as Student 1 and submit the assignment.

			String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
			
			//String studentUserName1 = "john_mercey-fbj9jaelgi@mailinator.com";
			

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String studentWindow = driver.getWindowHandle();
			testHelper.studentAssignmentWindow(driver, studentWindow);
			sap = new StudentAssignmentPage();
			
			Thread.sleep(2000);
			Assert.assertTrue(sap.isReviewExamButtonDisplayed());

			Thread.sleep(2000);
			Assert.assertTrue(sap.getHeaderScore().contains("33%"),"Student Garde is displayed?");
			
			List<String> questionPointsList = sap.getAllQuestionPointsList();
			Assert.assertTrue(questionPointsList.get(0).equals("1 / 1"));
			Assert.assertTrue(questionPointsList.get(1).equals("2 / 3"));
			Assert.assertTrue(questionPointsList.get(2).equals("0 / 2"));
			Assert.assertTrue(questionPointsList.get(3).equals("0 / 1"));
			Assert.assertTrue(questionPointsList.get(4).equals("0 / 2"));
			
			sap.reviewExamClick();
			
			sqp = new StudentQuestionPlayer();
			Assert.assertEquals(sqp.getHeaderScore(), "33%");
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true);
			Assert.assertEquals(sqp.isCorrectAnswerIconDisplayed(), true);
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			sqp.clickNextQuestion();
			Assert.assertEquals(sqp.getHeaderScore(), "33%");
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true);
			Assert.assertEquals(sqp.isPartialAnswerIconDisplayed(), true);
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			sqp.clickNextQuestion();
			Assert.assertEquals(sqp.getHeaderScore(), "33%");
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true);
			Assert.assertEquals(sqp.isIncorrectAnswerIconDisplayed(), true);
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			sqp.clickNextQuestion();
			Assert.assertEquals(sqp.getHeaderScore(), "33%");
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true);
			Assert.assertEquals(sqp.isCorrectAnswerIconDisplayed(), false);
			Assert.assertEquals(sqp.isPartialAnswerIconDisplayed(), false);
			Assert.assertEquals(sqp.isIncorrectAnswerIconDisplayed(), false);
			
			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(studentWindow);
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.logoutSmartwork5();

		}

		// End of Test cases
		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
	}
