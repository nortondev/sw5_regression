
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH8HW;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentShowSolutionAfterCorrectAnswerOrAttemptExhausted extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH8HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("CH8HW (Show solution: Only After a correct answer...)")
	@Stories("AS-51 CH8HW (Show solution: Only After a correct answer...)")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH8HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();
		Thread.sleep(5000);
		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements();

		assignment = new AssignmentCH8HW();

		// Question #1 - First Attempt correct submit
		assignment.enterQuestion1_Correct();
		Thread.sleep(3000);
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(5000);

		// # Use case: Solution Should be displayed
		Assert.assertTrue(sqp.isViewSolutionDisplayed());

		sqp.clickNextQuestion();

		// Question #2 - 1st Attempt incorrect submit
		assignment.Question2_Incorrect();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// # Use case: Solution Should NOT be displayed
		Assert.assertFalse(sqp.isViewSolutionDisplayed());

		// Question #2 - 2nd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// # Use case: Solution Should NOT be displayed
		Assert.assertFalse(sqp.isViewSolutionDisplayed());

		// Question #2 - 3rd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(5000);

		// # Use case: Solution Should be displayed
		Assert.assertTrue(sqp.isViewSolutionDisplayed());

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}