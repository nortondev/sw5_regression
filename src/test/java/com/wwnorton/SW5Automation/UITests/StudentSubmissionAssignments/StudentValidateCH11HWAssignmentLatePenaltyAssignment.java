package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH11HW;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
@Listeners({ TestListener.class })
public class StudentValidateCH11HWAssignmentLatePenaltyAssignment extends
		PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	final String ASSIGNMENT_TITLE = "CH11HW";

	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	StudentAssignmentPage studentassignmentpage;
	Questions_Page questionspage;
	AssignmentCH11HW assignmentCH11HWpage;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH11HW (Late Penalty Assignment))")
	@Stories("AS-54 As an instructor set CH11HW as a late penalty assignment,GAU to be expired in next 30 mins,late work days to 1 with 10% penalty")
	@Test
	public void StudentValidateCH11HWAssignment() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		String assignmentTitle = testHelper
				.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH11HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window

		// Assignment Window
				String parentWindow = driver.getWindowHandle();
				testHelper.getStudentAssignmentWindow(driver, parentWindow);

		Thread.sleep(8000);
		sap = new StudentAssignmentPage();
		sap.keepUsingTrialAccess();

		driver.switchTo().frame("swfb_iframe");
		wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.or(
				ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));
		String Text = driver.findElement(By.className("assignment-info-box-r"))
				.getText();
		LogUtil.log("Assignment Due Message  " + Text);
		questionspage = new Questions_Page();
		sqp = new StudentQuestionPlayer();
		assignmentCH11HWpage = new AssignmentCH11HW(driver);
		sap.clickQuestion(9);
		assignmentCH11HWpage.Q10textbox.sendKeys("-15.3");
		sqp.clickSubmitAnswer();
		driver.findElement(By.className("close")).click();
		Thread.sleep(5000);
		String Scoreq10 = driver.findElement(By.xpath("//div[@class='head-score head-dash-list']")).getText();
		LogUtil.log(Scoreq10);
		LogUtil.log("Assignment Score before GAU Expire  " + Scoreq10);
		ReusableMethods.scrollToElement(driver, By.linkText(ASSIGNMENT_TITLE));
		driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();
		
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));

		studentassignmentpage = new StudentAssignmentPage();
		Thread.sleep(5000);
		Assert.assertEquals(studentassignmentpage.getQuestionPoints(9), "1 / 1");
		String points09 =studentassignmentpage.getQuestionPoints(9);
		LogUtil.log("Assignment Points before GAU expire" + points09 );
		sap.clickQuestion(11);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		assignmentCH11HWpage.solventAnswer1();
		sqp.clickSubmitAnswer();
		assignmentCH11HWpage.YesButton.click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("close")));
		driver.findElement(By.className("close")).click();
		Thread.sleep(5000);
		String Score = driver.findElement(By.xpath("//div[@class='head-score head-dash-list']")).getText();
		LogUtil.log("Assignment Score before GAU Expire  " + Score);
		System.out.println(Score);
		driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));
		studentassignmentpage = new StudentAssignmentPage();
		String points =studentassignmentpage.getQuestionPoints(11);
		LogUtil.log("Assignment Points before GAU expire  " + points);
		Thread.sleep(5000);
		Assert.assertEquals(studentassignmentpage.getQuestionPoints(11), "1 / 3");
		wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));

		questionspage.lateWorkAlertPopup();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("question-text")));
		sap.clickQuestion(11);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='tryAgain']")));
		//sqp.clickTryAgain();
		driver.findElement(By.xpath("//button[@id='tryAgain']")).click();
		Thread.sleep(5000);
		ReusableMethods.scrollToBottom(driver);
		assignmentCH11HWpage.solventAnswer2();
		assignmentCH11HWpage.solventAnswer3();
		sqp.clickSubmitAnswer();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("close")));
		driver.findElement(By.className("close")).click();
		String Score1 = driver.findElement(By.xpath("//div[@class='head-score head-dash-list']")).getText();
		LogUtil.log(Score1);
		driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();
		Thread.sleep(5000);
		String points1 =studentassignmentpage.getQuestionPoints(11);
		LogUtil.log(points1);
		Assert.assertEquals(studentassignmentpage.getQuestionPoints(11), "2.8 / 3");
		sap.clickQuestion(4);
		assignmentCH11HWpage.submitAnswerdragdropCH11HW();
		sqp.clickSubmitAnswer();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("close")));
		driver.findElement(By.className("close")).click();
		String q4Score = driver.findElement(By.xpath("//div[@class='head-score head-dash-list']")).getText();
		LogUtil.log(q4Score);
		Assert.assertEquals(studentassignmentpage.getQuestionPoints(4), "0.9 / 1");
		
		// assignmentpage.AssignmentActivityDetails();
		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}


}
