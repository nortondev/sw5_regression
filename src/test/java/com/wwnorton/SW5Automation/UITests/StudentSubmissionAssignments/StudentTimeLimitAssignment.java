
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH3HW;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentTimeLimitAssignment extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH3HW ch3HW;
	TestHelper testHelper = new TestHelper();
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-46 CH3HW (Time Limit Assignment)")
	@Test(priority = 0)
	public void studentTimeLimitAssignment(ITestContext context) throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH3HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.verifyInitialAllQuestionPoints());
		Assert.assertTrue(sap.verifyInitialAllQuestionAttempts());
		Assert.assertTrue(sap.verifyInitialAllQuestionStatus());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:05:00", "Initial timer value is 5 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickNoIamNotReadyToBeginButton();

		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:05:00", "Initial timer value is 5 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickYesIamReadyToBeginButton();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();
		Thread.sleep(2000);

		String timerText = sqp.getTimerText();

		Assert.assertTrue(timerText.compareTo("00:05:00") != 0, "Is timer started?");
		Thread.sleep(5000);

		Assert.assertTrue(sqp.getTimerText().compareTo(timerText) != 0, "Is timer coninues?");

		ch3HW = new AssignmentCH3HW(driver);

		// START - useCase: Submit assignment when time limit is on to make sure the
		// submission went through.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "0 OF 16 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/16");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #1 - First Attempt
		ch3HW.question1InCorrectAnswer();

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackTryAgain();

		// Question #1 - Second Attempt

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Hold submission and allow the time limit to expire to validate the time limit
		// expire message validation comes up and system should not allow you to submit
		// answer.
		int sleepTimeInMiliseconds = 10000;
		while (true) {
			Thread.sleep(10000);
			sleepTimeInMiliseconds += 10000;
			// Wait till timer expired or 5 mins
			if (sqp.getTimerText().compareTo("00:00:00") == 0 || sleepTimeInMiliseconds >= 300000)
				break;
		}
		Thread.sleep(10000);

		Assert.assertEquals(sqp.getAlertMessageText().trim(), "Time has expired and no further attempts are allowed!",
				"Alert popup with 'Time has expired and no further attempts are allowed!' message is displayed?");

		sqp.clickTimerExpiredAlertClose();

		Assert.assertTrue(sap.isReviewAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");

		sap.reviewAssignmentClick();
		Thread.sleep(10000);

		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertTrue(sqp.isViewSolutionDisplayed(), "There should be a solution on the page?");

		sqp.clickNextQuestion();

		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
		// END
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
