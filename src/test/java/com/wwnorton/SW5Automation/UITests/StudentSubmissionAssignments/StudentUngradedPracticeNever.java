
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH6HW;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentUngradedPracticeNever extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH6HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student - CH6HW (Ungraded Practice: Never)")
	@Stories("AS-43 As a student - CH6HW (Ungraded Practice: Never)")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH6HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements();

		assignment = new AssignmentCH6HW();

		// Question #1 - First Attempt correct submit
		assignment.enterQuestion1_Correct();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(5000);

		sqp.clickNextQuestion();

		// Question #2 - 1st Attempt incorrect submit
		assignment.enterQuestion2_Incorrect();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #2 - 2nd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #2 - 3rd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(5000);

		sqp.clickNextQuestion();

		// Question #3 - 1st Attempt incorrect submit
		assignment.enterQuestion3_Incorrect();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #3 - 2nd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #3 - 3rd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(5000);

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		sap.clickDeadlineReachedAlertClose();

		// # Use case: Practice Button should not be displayed
		sap.clickQuestion(1);
		Assert.assertFalse(sqp.isCheckPracticeButtonDisplayed());
		sqp.clickNextQuestion(); // Navigate to Question #2
		Assert.assertFalse(sqp.isCheckPracticeButtonDisplayed());
		sqp.clickNextQuestion(); // Navigate to Question #3
		Assert.assertFalse(sqp.isCheckPracticeButtonDisplayed());
		sqp.clickNextQuestion(); // Navigate to Question #4
		Assert.assertFalse(sqp.isCheckPracticeButtonDisplayed());

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}