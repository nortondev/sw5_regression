package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH2HW;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class StudentValidateCH2HWGAUExpirationwithlateworkaccepted extends
		PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	
	final String ASSIGNMENT_TITLE = "CH02HW";
	
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	
	Questions_Page questionspage;
	AssignmentCH2HW assignment;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("CH2HW (GAU Expiration with late work accepted)")
	@Stories("AS-45 As an instructor -  Set the GAU to +30 mins of current date & time with 2 days late work accepted and 10% penalty per day")
	@Test
	public void StudentValidateCH2HWAssignment() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		String assignmentTitle = testHelper
				.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH2HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window

		String winHandleBefore = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(8000);
		sap = new StudentAssignmentPage();
		sap.keepUsingTrialAccess();

		driver.switchTo().frame("swfb_iframe");

		wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.or(
				ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));
		// assignmentpage.Assignmentinfo();

		String Text = driver.findElement(By.className("assignment-info-box-r"))
				.getText();
		LogUtil.log("Assignment Info Text", Text);
		questionspage = new Questions_Page();
		assignment = new AssignmentCH2HW(driver);
		// questionspage.BeginAssignment();
		sap.clickQuestion(3);
		Thread.sleep(2000);
		assignment.Question3Answer_Correct();
		sqp = new StudentQuestionPlayer();
		sqp.clickSubmitAnswer();
		Thread.sleep(3000);
		if(sqp.isSubmitYourAnswerDisplayed() == true) {	
			sqp.clickYesFinishButton();
		}
		Thread.sleep(2000);
		//driver.findElement(By.className("close")).click();
		WebElement closeIcon = driver.findElement(By.xpath("//button[@class='close']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeIcon);
		Thread.sleep(2000);
		//driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();
		sqp.clickHeaderTitle(sap);
		wait.until(ExpectedConditions.or(
				ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
				ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));
		ReusableMethods.scrollToBottom(driver);
		Thread.sleep(1000);
		sap.clickQuestion(11);
		Thread.sleep(2000);
		assignment.Question11Answer_Correct();
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		Thread.sleep(2000);
		//driver.findElement(By.className("close")).click();
		driver.findElement(By.xpath("//button[@class='close']")).click();
		Thread.sleep(2000);
		String Score = driver.findElement(
				By.xpath("//div[@class='head-score head-dash-list']")).getText();
		LogUtil.log("Student Score is ", Score);
		//driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();
		sqp.clickHeaderTitle(sap);
		// String winHandleAfter = driver.getWindowHandle();
		/*driver.close();
		driver.switchTo().window(winHandleBefore);*/
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		testHelper.logoutSmartwork5(driver, winHandleBefore, SW5DLPstudent);
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
