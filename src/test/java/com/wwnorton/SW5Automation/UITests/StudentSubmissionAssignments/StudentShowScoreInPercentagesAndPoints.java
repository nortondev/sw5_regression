
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH13HW;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH13HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentShowScoreInPercentagesAndPoints extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH13HW ch13HW;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	AssignmentPage instructorAssignmentPage;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-53 CH13HW (Show Student Score In: Percentages) (Show Student Score In: Points)")
	@Test(priority = 0)
	public void studentShowScoreInPercentagesAndPoints() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		String studentName = testHelper.getStudentUserNameInUppercase(studentUsername);

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH13HW);

		// As a student submit some questions and assert following
		// STUDENT
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(10000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		ch13HW = new AssignmentCH13HW();

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		// Navigate to Question #1
		sap.clickQuestion(1);
		//sap.clickQuestion(9);
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		ch13HW.question1DragAndDropCorrectAnswer();

		sqp.clickSubmitAnswer();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		sqp.clickNextQuestion();
		sqp.clickNextQuestion();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		ch13HW.question3EnterCorrectAnswer();

		sqp.clickSubmitAnswer();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		// Navigate to Assignment Page
		//sqp.clickHeaderTitle(sap);

		// Navigate to Question #4
		sqp.clickNextQuestion();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		ch13HW.question4DragAndDropCorrectAnswer();

		sqp.clickSubmitAnswer();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		// Assert score on question player should be in %
		String sqpHeaderScore = sqp.getHeaderScore();
		Thread.sleep(5000);
		Assert.assertTrue(sqpHeaderScore.endsWith("%"), 
				"Student question player displays score in %?");
		Assert.assertEquals(sqpHeaderScore, "12%");
		Thread.sleep(5000);
		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		// Assert score on student assignment should be in %
		String sapHeaderScore = sap.getHeaderScore();
		Assert.assertTrue(sapHeaderScore.endsWith("%"), 
				"Student assignment page displays score in %?");
		Assert.assertEquals(sapHeaderScore, "12%");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();

		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);

		testHelper.getInstructorAssignmentWindow(driver);

		instructorAssignmentPage = new AssignmentPage();
		Thread.sleep(5000);

		// Assert the instructor assignment page should show class average in %
		String classAverage = instructorAssignmentPage.getHeaderScore();
		Assert.assertTrue(classAverage.endsWith("%"), 
				"Instructor assignment page displays class average in %?");
		Assert.assertEquals(classAverage, "12%", 
				"Average score for assignment CH13HW is 12%?");

		driver.close();
		driver.switchTo().window(winHandleBefore);

		// SW5DLPpage = new SW5DLPPage(driver);

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();

		// Assert CAR student tab score column should display %
		String carStudentTabScore = classActivityReport.getCARStudentsTabStudentScore(studentName);
		Assert.assertTrue(carStudentTabScore.endsWith("%"),
				"Instructor CAR student tab score column should display %?");
		Assert.assertEquals(carStudentTabScore, "12%", 
				"Instructor CAR student tab score is 12%?");

		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(studentName);

		studentDetailOverlay = new StudentDetailOverlayPage();
		Thread.sleep(5000);

		// Assert CAR - student detail overlay Overall score should displays %
		String overallScore = studentDetailOverlay.getOverallScore();
		Assert.assertTrue(overallScore.endsWith("%"),
				"Instructor CAR student detail overlay Overall score should displays %?");
		Assert.assertEquals(overallScore, "12%", 
				"Instructor CAR student detail overlay Overall score is 12%?");

		studentDetailOverlay.clickCloseStudentDetailOverlay();

		driver.close();
		driver.switchTo().window(winHandleBefore);

		// As an instructor open the assignment in edit mode and change the “Show
		// Student Score” to points.
		InstructorPublishPremadeAssignmentCH13HW assignment = new InstructorPublishPremadeAssignmentCH13HW();
		assignment.updateSettingsShowStudentScoreInPoints(SW5DLPpage);

		//instructorAssignmentPage = new AssignmentPage(driver);

		//instructorAssignmentPage.clickHeaderTitle(instructorAssignmentPage);
		Thread.sleep(5000);
		
		String winHandleBefore1 = driver.getWindowHandle();

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getInstructorAssignmentWindow(driver);

		// Assert the instructor assignment page should show class average in Points
		classAverage = instructorAssignmentPage.getHeaderScore();
		Assert.assertTrue(classAverage.endsWith("%") == false,
				"Instructor assignment page displays class average in points?");
		Assert.assertEquals(classAverage, "3 out of 25",
				"Instructor average score for assignment CH13HW is \"3 out of 25\"?");

		driver.close();
		driver.switchTo().window(winHandleBefore1);
		driver.navigate().refresh();
		Thread.sleep(10000);

		SW5Login = new LoginPage();

		// Navigate to Smartwork5 page
		SW5Login.clickSW5Icon();
		Thread.sleep(2000);

		// SW5DLPpage = new SW5DLPPage(driver);

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Thread.sleep(5000);
		// Assert CAR student tab score column should display points
		carStudentTabScore = classActivityReport.getCARStudentsTabStudentScore(studentName);
		Assert.assertTrue(carStudentTabScore.endsWith("%") == false,
				"Instructor CAR student tab score column displays in points?");
		Assert.assertEquals(carStudentTabScore, "3", "Instructor CAR student tab score is 3?");

		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(studentName);

		studentDetailOverlay = new StudentDetailOverlayPage();
		Thread.sleep(5000);

		// Assert CAR - student detail overlay Overall score should displays points
		overallScore = studentDetailOverlay.getOverallScore();
		Assert.assertTrue(overallScore.endsWith("%") == false,
				"Instructor CAR student detail overlay Overall score displays in points?");
		Assert.assertEquals(overallScore, "3", "Instructor CAR student detail overlay Overall score is 3?");

		studentDetailOverlay.clickCloseStudentDetailOverlay();

		driver.close();
		driver.switchTo().window(winHandleBefore1);

		SW5DLPpage.logoutSW5();

		// STUDENT
		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow2 = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);

		sap = new StudentAssignmentPage();
		Thread.sleep(10000);

		// Assert score on student assignment should be in points
		sapHeaderScore = sap.getHeaderScore();
		Assert.assertTrue(sapHeaderScore.endsWith("%") == false, 
				"Student assignment page score displays in points?");
		Assert.assertEquals(sapHeaderScore, "3 out of 25",
				"Student average score on assignment page for CH13HW is \"3 out of 25\"?");

		// Navigate to Question #1
		sap.clickQuestion(1);

		sqp = new StudentQuestionPlayer();

		// Assert score on question player should be in points
		sqpHeaderScore = sqp.getHeaderScore();
		Assert.assertTrue(sqpHeaderScore.endsWith("%") == false,
				"Student question player page score displays in points?");
		Assert.assertEquals(sqpHeaderScore, "3 out of 25",
				"Student average score on question player page for CH13HW is \"3 out of 25\"?");

		testHelper.logoutSmartwork5(driver, parentWindow2, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
