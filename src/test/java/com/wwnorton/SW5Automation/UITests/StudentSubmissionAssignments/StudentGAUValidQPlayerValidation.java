
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH1HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentGAUValidQPlayerValidation extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH1HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - GAU valid - student assignment & question player validation scenarios")
	@Stories("AS-43 As a student work with an assignment - GAU valid - student assignment & question player validation scenarios - CH1HW")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH1HW);

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentUsername = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String assignmentDueDate = (String) gContext.getAttribute(SW5Constants.GAUDATE_CH1HW);
		String assignmentInfoBoxText = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_INFOBOX_TEXT_CH1HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		SW5DLPstudent = new SW5DLPStudent();

		// Assert Assignment Visibility And GAU
		Assert.assertTrue(SW5DLPstudent.IsAssignmentVisible(assignmentTitle));
		Assert.assertTrue(SW5DLPstudent.IsAssignmentHaveGAU(assignmentTitle));

		// Select mentioned Assignment
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.elementToBeClickable(sap.BeginAssignment));

		// # Use case: Verify student assignment page - first time
		Assert.assertEquals(sap.getAssignmentInfoBox().trim(), assignmentInfoBoxText.trim());
		Thread.sleep(3000);
		//Assert.assertTrue(sap.getAssignmentInfoBox().trim().contains(assignmentInfoBoxText.trim()));
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.verifyInitialAllQuestionPoints());
		Assert.assertTrue(sap.verifyInitialAllQuestionAttempts());
		Assert.assertTrue(sap.verifyInitialAllQuestionStatus());

		// Click Begin Assignment
		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements(); // Waiting for question player elements to load

		Thread.sleep(3000);

		// # Use case: Verify begin assignment - first time
		Assert.assertEquals(sqp.getHeaderDueDate(), assignmentDueDate);
		Assert.assertEquals(sqp.QuestionTitle.getText(), "01Question(1 point)"); // Verifying, The first question should
																					// load on screen
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt")); // Verifying, The first question should load on screen
		Assert.assertTrue(sqp.isSubmitAnswerButtonDisplayed());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.QuestionFooterLeftText.getText(), "0 OF 20 QUESTIONS COMPLETED");

		// # Use case: Verify submit answer - first attempt
		assignment = new AssignmentCH1HW(driver);
		sqp.clickNextQuestion();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.QuestionInfoText.getAttribute("aria-label"),
				"This is a Ranking question / It is worth 2 points / You haveunlimited attempts / There is no attempt penalty.");
		Assert.assertTrue(sqp.isQuestionPartDisplayed("Part 1   (1 point)"));
		Assert.assertTrue(sqp.isQuestionPartDisplayed("Part 2   (1 point)"));
		
		assignment.Question2Answer_Correct();
		sqp.clickSubmitAnswer();
		Assert.assertTrue(sqp.isFeedbackOverlayDisplayed());
		sqp.clickFeedbackClose();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.HeaderScore.getText(), "6%");
		Assert.assertTrue(sqp.isViewSolutionDisplayed());
		Assert.assertFalse(sqp.isPracticeButtonDisplayed());
	
	    Assert.assertEquals(sqp.QuestionFooterLeftText.getText(), "1 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "02/20");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		// # Use case: Verify student assignment - post submission
		sqp.HeaderTitle.click();
		Thread.sleep(8000);
		Assert.assertEquals(sap.HeaderScore.getText(), "6%");
		Assert.assertEquals(sap.getQuestionPoints(2), "2 / 2");
		Assert.assertEquals(sap.getQuestionAttempts(2), "1 / ∞");
		Assert.assertEquals(sap.getQuestionStatus(2), "Completed");
		Assert.assertTrue(sap.isResumeAssignmentDisplayed());

		// # Use case: Verify resume assignment
		sap.resumeAssignmentClick();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.QuestionFooterLeftText.getText(), "1 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "02/20");
		Assert.assertFalse(sqp.isPracticeButtonDisplayed());
		Assert.assertEquals(sqp.QuestionInfoText.getAttribute("aria-label"),
				"This is a Ranking question / It is worth 2 points / You haveunlimited attempts / There is no attempt penalty.");
		Assert.assertTrue(sqp.isViewSolutionDisplayed());
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		// # Use case: verify direct question access by clicking on question
		sqp.HeaderTitle.click();
		Thread.sleep(3000);
		sap.clickQuestion(4);
		Thread.sleep(3000);
		Assert.assertEquals(sqp.AssignmentTitle.getText(), "CH01HW");
		Assert.assertEquals(sqp.HeaderScore.getText(), "6%");

		driver.switchTo().window(parentWindow); // Switching to Parent Window (SW5 Main)
		Assert.assertEquals(sqp.HeaderEmail.getText(), studentUsername);
		testHelper.switchToStudentAssignmentWindow(driver, parentWindow); // Switching to Student Assignment Window

		Assert.assertEquals(sqp.QuestionInfoText.getAttribute("aria-label"),
				"This is a Sorting question / It is worth 1 point / You haveunlimited attempts / There is no attempt penalty.");
		Assert.assertEquals(sqp.QuestionTitle.getText(), "04Question(1 point)");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		Assert.assertEquals(sqp.QuestionFooterLeftText.getText(), "1 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/20");
		Assert.assertTrue(sqp.isSubmitAnswerButtonDisplayed());

		// # Use case : verify submission of partial correct answer
		assignment.Question4Answer_Correct();
		sqp.clickSubmitAnswer();
		Thread.sleep(3000);
		Assert.assertTrue(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed());
		sqp.clickFeedbackTryAgain();
		Thread.sleep(5000);
		Assert.assertEquals(sqp.HeaderScore.getText(), "6%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertTrue(sqp.isViewSolutionAndSubmitAnswerDisplayed());

		// # Use case : verify pop-up during leaving question with out completing the
		// question
		sqp.HeaderTitle.click();
		Thread.sleep(3000);
		Assert.assertTrue(sqp.isLeaveQuestionDisplayed());
		sqp.clickLeaveQuestionYesLeave();
		Thread.sleep(10000);
		Assert.assertEquals(sap.getQuestionStatus(4), "In Progress");

		// # Use Case: Check the ">" button Question Player
		sap.clickQuestion(4);
		Thread.sleep(5000);
		Assert.assertEquals(sqp.QuestionTitle.getText(), "04Question(1 point)"); // This will confirm (It will load the
																					// question player again)
		sqp.clickNextQuestion();
		Thread.sleep(3000);
		Assert.assertTrue(sqp.isLeaveQuestionDisplayed());
		sqp.clickLeaveQuestionYesLeave();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.QuestionTitle.getText(), "05Question(3 points)");
		Assert.assertTrue(sqp.isSubmitAnswerButtonDisplayed());

		// # Use Case : Check the "<" button Question Player
		sqp.clickPreviousQuestion();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.QuestionTitle.getText(), "04Question(1 point)"); // This will confirm (It should load
																					// Question number #2)
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/20");
		Assert.assertTrue(sqp.isViewSolutionAndSubmitAnswerDisplayed());
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt")); // This will confirm attempts on screen
		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt")); // This will confirm attempts on screen
		Assert.assertEquals(sqp.HeaderScore.getText(), "6%");
		Assert.assertEquals(sqp.QuestionFooterLeftText.getText(), "1 OF 20 QUESTIONS COMPLETED");

		// # Use Case : Check No Attempts
		sqp.HeaderTitle.click();
		Thread.sleep(2000);
		sqp.clickLeaveQuestionYesLeave();
		Thread.sleep(3000);
		Assert.assertEquals(sap.getQuestionStatus(5), "No Attempts");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
