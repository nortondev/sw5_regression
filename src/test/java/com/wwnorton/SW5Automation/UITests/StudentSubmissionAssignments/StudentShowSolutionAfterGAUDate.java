
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH9HW;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;

import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class StudentShowSolutionAfterGAUDate extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH9HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("CH09HW (Show solution: Only after the GAU date...)")
	@Stories("AS-52 CH9HW (Show solution: Only after the GAU date...)")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH9HW);

		// Login as Student
		String studentUserName = (String) gContext
							.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		//String studentUserName = "";
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();

		// # Use case: Solution should not be visible prior to GAU expiration

		// Question #1
		//Assert.assertFalse(sqp.isViewSolutionDisplayed());
		
		assignment = new AssignmentCH9HW();
		assignment.submitCorrectAnswer_Question01_Part1();
		Thread.sleep(5000);
		assignment.submitCorrectAnswer_Question01_Part2();
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		
		sqp.clickFeedbackClose();

		// Navigate to Question #2
		sqp.clickNextQuestion();

		// Question #2
		Assert.assertFalse(sqp.isViewSolutionDisplayed());

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		sap.clickDeadlineReachedAlertClose();

		// # Use case: Solutions displayed post GAU expiration

		// Question #1
		sap.clickQuestion(1);
		Assert.assertTrue(sqp.isViewSolutionDisplayed());

		// Navigate to Question #2
		sqp.clickNextQuestion();

		// Question #2
		Assert.assertTrue(sqp.isViewSolutionDisplayed());

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}