package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentTutorialQuestion;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.TutorialLesson;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class StudentTutorialQuestionType extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentTutorialQuestion assignment;
	TutorialLesson tl;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("Custom Assignment - Tutorial Question Type")
	@Stories("AS-64 Custom Assignment - Tutorial Question Type")
	@Test
	public void submitAssignmentWithTutorialQuestionType() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish custom assignment with Tutorial Question
		InstructorPublishCustomAssignment publishAssignment = new InstructorPublishCustomAssignment();
		String assignmentTitle = publishAssignment.updateSettingsAndPublishCustomAssignmentTutorialQuestion();

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements();

		assignment = new CustomAssignmentTutorialQuestion();

		Assert.assertEquals("-- %", sqp.getHeaderScore());

		// Launch TutorialLesson
		sqp.clickLaunchTutorialLessonButton();

		tl = new TutorialLesson(driver);
		tl.clickStartTutorialButton();

		// Tutorial Question #1 - Submit
		assignment.enterTutorialQuestion1_Correct();
		tl.clickSubmitAnswerButton();
		tl.clickFeedbackClose();

		// Tutorial Question #2 - Submit
		tl.clickTutorialQuestion2();
		assignment.enterTutorialQuestion2_Correct();
		tl.clickSubmitAnswerButton();
		tl.clickFeedbackClose();

		// Tutorial Question #3 - Submit
		tl.clickTutorialQuestion3();
		assignment.enterTutorialQuestion3_Correct();
		tl.clickSubmitAnswerButton();
		tl.clickFeedbackClose();

		// Close TutorialLesson
		tl.clickCloseTutorialButton();
		
		sqp.waitForLoadPageElements();

		Assert.assertEquals(sqp.getHeaderScore(), "0%" );

		// Question #1 - Correct submit
		assignment.enterQuestion1_Correct();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getHeaderScore(),  "50%");

		sqp.clickNextQuestion();

		// Question #2 - Correct submit
		assignment.enterQuestion2_Correct();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getHeaderScore(), "100%" );

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
