package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH5HW;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class StudentValidateCH5HWUngradedPracticeAfterGAUDatePass extends
		PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	final String ASSIGNMENT_TITLE = "CH5HW";

	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH5HW assignment;
	Questions_Page questionspage;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH5HW (Ungraded Practice: after the GAU date pass)")
	@Stories("AS-48 As a student - CH5HW (Ungraded Practice: after the GAU date pass)")
	@Test
	public void StudentValidateCH5HWAssignment() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		String assignmentTitle = testHelper
				.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH5HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		// Assignment Window

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);
		
				Thread.sleep(8000);
				sap = new StudentAssignmentPage();
				sap.keepUsingTrialAccess();

				driver.switchTo().frame("swfb_iframe");
				wait = new WebDriverWait(driver, 120);
				wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
						ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));
				// assignmentpage.Assignmentinfo();

				String Text = driver.findElement(By.className("assignment-info-box-r")).getText();
				System.out.println(Text);
				questionspage = new Questions_Page();
				assignment = new AssignmentCH5HW();
				sap.clickQuestion(14);
				assignment.submitAnswerCH5HWAssignment();
				sqp = new StudentQuestionPlayer();
				sqp.clickSubmitAnswer();
				driver.findElement(By.className("close")).click();
				Thread.sleep(2000);
				String Score = driver.findElement(By.xpath("//div[@class='head-score head-dash-list']")).getText();
				System.out.println(Score);
				driver.findElement(By.linkText(ASSIGNMENT_TITLE)).click();

				wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(sap.BeginAssignment),
						ExpectedConditions.elementToBeClickable(sap.ResumeAssignment)));

				questionspage.alertPopup();
				WebDriverWait waitReviewAssignmentButton = new WebDriverWait(driver, 50);
				waitReviewAssignmentButton.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='question-btn']/a/span[contains(text(),'REVIEW ASSIGNMENT')]")));
				Thread.sleep(5000);
				questionspage.ReviewAssignmentButton();
				//WebDriverWait wait2 = new WebDriverWait(driver, 50);
				//wait2.until(ExpectedConditions.visibilityOfElementLocated(By.className("question-btn")));
				Thread.sleep(5000);
				boolean isPracticeButton = sqp.isPracticeButtonDisplayed();
				Assert.assertTrue(isPracticeButton);
				LogUtil.log("The Practice Button is displayed");
				
				// assignmentpage.AssignmentActivityDetails();
				testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
