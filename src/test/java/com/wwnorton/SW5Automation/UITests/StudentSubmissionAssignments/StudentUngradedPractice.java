
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH4HW;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentUngradedPractice extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH4HW ch4HW;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-44 As a student test ungraded practice after attempt exhaust / answer is correct / student viewed solution - CH4HW")
	@Test()
	public void StudentUngradedPracticeAfterAttemptExhaustOrCorrectAnswerOrViewedSolution(ITestContext context)
			throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH4HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();

		ch4HW = new AssignmentCH4HW();
		
		
		// START - useCase: verify practice after answer is correct

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Sorting question / It is worth 2 points / You have 3 of 3 attempts remaining / There is no attempt penalty");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionTitle(), "01 Question(2 points)");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/16");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #1 - First Attempt
		ch4HW.question1DragAndDropCorrectAnswer();

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Practice button is displayed in the feedback overlay popup?");
		boolean isFeedbackOverlayPartsCorrectAnswerIconsDisplayed = false;

		if (sqp.FeedbackPartCorrectAnswerIconsList.size() == 2) {
			isFeedbackOverlayPartsCorrectAnswerIconsDisplayed = true;
		}
		Assert.assertEquals(isFeedbackOverlayPartsCorrectAnswerIconsDisplayed, true,
				"A feedback overlay is displayed with correct answer icon?");

		sqp.clickFeedbackPractice();

		Assert.assertEquals(sqp.getHeaderScore(), "5%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "1 OF 16 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/16");
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		ch4HW.question1DragAndDropInCorrectAnswer();

		sqp.clickCheckPractice();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Practice button is displayed in the feedback overlay popup?");
		boolean isFeedbackOverlayPartsInCorrectAnswerIconsDisplayed = false;
		if (sqp.FeedbackPartInCorrectAnswerIconsList.size() == 2) {
			isFeedbackOverlayPartsInCorrectAnswerIconsDisplayed = true;
		}
		Assert.assertEquals(isFeedbackOverlayPartsInCorrectAnswerIconsDisplayed, true,
				"A feedback overlay is displayed with incorrect answer icon?");

		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getHeaderScore(), "5%");

		// END - useCase: verify practice after answer is correct
		

		// START - useCase: verify practice after attempt exhaust
				
		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);
		
		// Navigate to Question #7
		sap.clickQuestion(7);

		// Question #7
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "5%");
		Assert.assertTrue(sqp.getQuestionTitle().compareTo("07 Question(3 points)") == 0,
				"The assignment should load the question #7?");
		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Numeric Entry question / It is worth 3 points / You have 3 of 3 attempts remaining / There is no attempt penalty");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionTitle(), "07 Question(3 points)");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "1 OF 16 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "07/16");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #7 - First Attempt

		ch4HW.question7InCorrectAnswer();

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackTryAgain();

		// Question #7 - Second Attempt

		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Numeric Entry question / It is worth 3 points / You have 2 of 3 attempts remaining / There is no attempt penalty");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "5%");

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackTryAgain();

		// Question #7 - Third Attempt

		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Numeric Entry question / It is worth 3 points / You have 1 of 3 attempts remaining / There is no attempt penalty");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("3rd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "5%");

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Practice\" button?");

		sqp.clickFeedbackClose();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Numeric Entry question / It is worth 3 points / You have no attempts remaining / There is no attempt penalty");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "2 OF 16 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");

		sqp.clickPractice();

		// Question #7 - Practice Attempt

		Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.getHeaderScore(), "5%");

		// Enter Correct Answer
		ch4HW.question7CorrectAnswer();

		sqp.clickCheckPractice();

		isFeedbackOverlayPartsCorrectAnswerIconsDisplayed = false;
		if (sqp.FeedbackPartCorrectAnswerIconsList.size() == 3) {
			isFeedbackOverlayPartsCorrectAnswerIconsDisplayed = true;
		}

		Assert.assertEquals(isFeedbackOverlayPartsCorrectAnswerIconsDisplayed, true,
				"Feedback is displayed with correct icons for each part?");
		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Practice button is displayed in the feedback overlay popup?");
		Assert.assertEquals(sqp.getHeaderScore(), "5%");

		sqp.clickFeedbackClose();

		sqp.clickNextQuestion();

		sqp.clickPreviousQuestion();

		Assert.assertEquals(sqp.getQuestionTitle(), "07 Question(3 points)");

		boolean isThreeAttemptsTabDisplayedOnPage = false;
		if (sqp.isAttemptViewDisplayed("1st attempt") && sqp.isAttemptViewDisplayed("2nd attempt")
				&& sqp.isAttemptViewDisplayed("3rd attempt")) {
			isThreeAttemptsTabDisplayedOnPage = true;
		}

		Assert.assertEquals(isThreeAttemptsTabDisplayedOnPage, true, "There should be 3 attempts tab on the page?");
		Assert.assertTrue(sqp.isViewSolutionDisplayed(), "There should be a solution on the page?");
		//Assert.assertFalse(sqp.isAttemptViewDisplayed("Practice Attempt"),
				//"There should not be any Practice Attempt section on the page?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "There should be a button labeled \"Practice\"?");

		// END - useCase: verify practice after attempt exhaust


		// START - useCase: verify practice after student viewed solution

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);
				
		// Navigate to Question #4
		sap.clickQuestion(4);

		// Question #4
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getQuestionInfoText(),
				"This is a Numeric Entry question / It is worth 1 point / You have 3 of 3 attempts remaining / There is no attempt penalty");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/16");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #4 - First Attempt

		ch4HW.question4InCorrectAnswer();

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackViewSolution();

		sqp.clickConfirmPopupViewSolutionButton();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}
		Thread.sleep(7000);
		Assert.assertTrue(sqp.isViewSolutionDisplayed(), "There should be a solution on the page?");
		Assert.assertEquals(sqp.getHeaderScore(), "5%");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "3 OF 16 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/16");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
		// END - useCase: verify practice after student viewed solution
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
