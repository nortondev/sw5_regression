package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class InstructorExtendsGAU_ExpiredAssignmentCH17HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	
	// Run this scripts after Student submits CH17HW assignment - StudentSubmitsCH17HWAssignmentWithGAU.class file with GAU after 30 mins.
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-38 - Verify that Instructor extend the GAU for submitted expired assignment - Part2.")
	@Stories(" CH17HW - Verify that Additional settings remains same for expired submitted assignment when Instructor extends GAU.")
	@Test()
	public void verifyAdditionalSettings_SubmittedExpiredAssignmentCH17HW() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH17HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			//String studentSetId = "153071";
					
			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH17HW);
			Thread.sleep(3000);

			String winHandleBefore1 = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			assignmentpage.getSetGAUTime();
			assignmentpage.saveButton();
			
			Thread.sleep(2000);
			assignmentpage.returnToAssignmentList.click();
			
			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(winHandleBefore1);
			
			String winHandleBefore2 = driver.getWindowHandle();
			
			Thread.sleep(2000);
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);

			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH17HW);
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage.editAssignmentbutton();
			
			Thread.sleep(2000);
			assignmentpage.ShowAdditionalSettingsButton();
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkTimeLimitSettings(), false);
			
			assignmentpage
					.selectUngradedPractice("Never");
			
			assignmentpage
					.selectshowsolution("Any time (students may 'give up' and view the solution)");
			
			assignmentpage.selectShowFeedback("After every attempt");
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkShowEbookLinkSettings(), true);
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkShowPeriodicTableSettings(), true);
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkRandomizeQuestionSettings(), false);
			
			Thread.sleep(2000);
			assignmentpage.selectshowstudentscore("Percentages");
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.isQuestionSettingsDisabled(), true);
			
			
			driver.close();
			driver.switchTo().window(winHandleBefore2);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
