package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class StudentSubmitsCustomAssignment extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	TestHelper testHelper = new TestHelper();
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;

	SW5DLPPage SW5DLPpage;
	ClassActivityReportPage classActivityReport;
	ManageStudentSetsPage managestudentsetpage;
	UpdateStudentSet updatestudentset;
	CreateNewStudentSet createStudentset;
	
	String student2SubmittedGradescore;
	String student3SubmittedGradescore;
	String student1SubmittedGradescore;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Custom Assignments - Remove Question - Edit Assignment Page")
	@Stories("AS-59 As a student submit the questions correct and validate the Score")
	@Test
	public void studentValidateCustomAssignment() throws Exception {
		
		driver = getDriver();
		
		/*String StudentName11111 = testHelper
				.getStudentUserNameInUppercase("john_mercey-8xbszxwdjq@mailinator.com")*/;
		
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(3);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		instCustAssignment = new InstructorPublishCustomAssignment();

		String assignmentTitle = instCustAssignment
				.updateSettingsAndPublishRemoveQuestion();
		// String assignmentTitle = "STG AUTOMATION DO NOT UPDATE QUESTIONS";

		// Login as Student

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String studentUserName2 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		String studentUserName3 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_3);
		/*String studentUserName4 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_4);
		String studentUserName5 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_5);
		String studentUserName6 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_6);*/
		

		ArrayList<String> students = new ArrayList<String>();
		students.add(studentUserName1);
		students.add(studentUserName2);
		students.add(studentUserName3);
		/*students.add(studentUserName4);
		students.add(studentUserName5);
		students.add(studentUserName6);*/

		String StudentName1 = testHelper
				.getStudentUserNameInUppercase(studentUserName1).trim();
		System.out.println(StudentName1);
		String StudentName2 = testHelper
				.getStudentUserNameInUppercase(studentUserName2).trim();
		System.out.println(StudentName2);
		String StudentName3 = testHelper
				.getStudentUserNameInUppercase(studentUserName3).trim();
		System.out.println(StudentName3);
		/*String StudentName4 = testHelper
				.getStudentUserNameInUppercase(studentUserName4).trim();
		String StudentName5 = testHelper
				.getStudentUserNameInUppercase(studentUserName5).trim();
		String StudentName6 = testHelper
				.getStudentUserNameInUppercase(studentUserName6).trim();*/
				
		for (int i = 0; i < students.size(); i++) {

			new LoginAsStudent().LoginStudent(students.get(i));

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			//
			// # Use case: Verify student assignment page - first time
			// Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
			// Thread.sleep(2000);
			// sap.waitForLoadPageElements();
			sap.beginAssignmentClick();
			if (i == 0) {
				// student1
				customQuestionsAnswerAssignmentStudent1();
				student1SubmittedGradescore = sqp.getHeaderScore();
				LogUtil.log("The " + StudentName1 +" grade score", student1SubmittedGradescore);
			}
			if (i == 1) {
				// student2
				customQuestionsAnswerAssignmentStudent2();
				student2SubmittedGradescore = sqp.getHeaderScore();
				LogUtil.log("The " + StudentName2 +" grade score", student2SubmittedGradescore);
			}
			if (i == 2) {
				// student3
				customQuestionsAnswerAssignmentStudent3();
				student3SubmittedGradescore = sqp.getHeaderScore();
				LogUtil.log("The " + StudentName3 +" grade score", student3SubmittedGradescore);
			}
			
			
            Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//div[@class='head-title fl']/a")));
			driver.findElement(By.xpath("//div[@class='head-title fl']/a"))
					.click();
			driver.close();
			driver.switchTo().window(parentWindow);
			SW5DLPstudent.logoutSmartwork5();
			/*
			 * wait = new WebDriverWait(driver, 5); TimeUnit.SECONDS.sleep(5);
			 * // assignmentpage.AssignmentActivityDetails();
			 * testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
			 */
		}

		// INSTRUCTOR

		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		new LoginAsInstructor().loginInstructor();

		
		SW5DLPpage = new SW5DLPPage();
		//String parentWindow = driver.getWindowHandle();
		String winHandleBefore = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		// Assert As an instructor check the class average and Submitted Grades
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("3", submittedGrades);
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("94%", averageGrade);
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);
		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("94%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());
		
		//Student1 Grade details 
		getClassActivityStudent(StudentName1, "100%");
		
		//Student2 Grade details 
		getClassActivityStudent(StudentName2, "89%");	
		
		//Student3 Grade details 
		getClassActivityStudent(StudentName3, "93%");
			
		
		driver.close();
		driver.switchTo().window(winHandleBefore);

		// /As an instructor go and delete question #8 from edit assignment
		// page.
		String winHandleBefore1 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		assignment.removeQuestion(8);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		assignment.clickRemovebutton();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		/*String winHandleonAssignmentPage = driver.getWindowHandle();
		Thread.sleep(8000);
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage(driver);
		Assert.assertEquals("97%",assignment.getHeaderScore());
		driver.close();
		driver.switchTo().window(winHandleonAssignmentPage);*/
		Thread.sleep(5000);
		
		// Assert class average should be 97% on DLP & CAR.
		String averageGradeafterremovequestion = SW5DLPpage
				.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("97%", averageGradeafterremovequestion);
		//SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		//testHelper.getInstructorAssignmentWindow(driver);
		//
		Thread.sleep(5000);
		String winHandleBefore2 = driver.getWindowHandle();
		Thread.sleep(5000);
		SW5DLPpage.clickReportsButton(assignmentTitle);
		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);
		Thread.sleep(5000);
		Assert.assertEquals("97%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//Studunet1 grade after Remove Question 
		getClassActivityReportStudentRemoveQuestion(StudentName1, "100%");
    //Studunet2 grade after Remove Question 	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		getClassActivityReportStudentRemoveQuestion(StudentName2, "100%");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   //Studunet3 grade after Remove Question 	
		getClassActivityReportStudentRemoveQuestion(StudentName3, "92%");				

		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBefore2);
		String winHandleBefore3 = driver.getWindowHandle();
		assignment.Assignmentlinks(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment.editAssignmentbutton();
		Thread.sleep(1000);
		assignment.ShowAdditionalSettingsButton();
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//select[@data-reactid='.0.1.7.0.0.1.0.1.3.1.1:$input']"));
		String ShowStudentScore = "Points";
		assignment.selectshowstudentscore(ShowStudentScore);
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		Thread.sleep(6000);
		//driver.close();
		// assignmentpage.AssignmentActivityDetails();
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		Thread.sleep(6000);
		// Assert % should be converted to points at places and assert
		// individual student points and class average in points. This should be
		// asserted in DLP & CAR.
		String winHandleBefore4 = driver.getWindowHandle();
		SW5DLPpage.clickReportsButton(assignmentTitle);
		Thread.sleep(6000);
		testHelper.getInstructorClassActivityReportWindow(driver);
		Thread.sleep(6000);
		Assert.assertEquals("97%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		// Student 1 grade in Points 
		Thread.sleep(5000);
		getClassActivityStudent(StudentName1, "25");	
		//Student 2 grade in Points 
		getClassActivityStudent(StudentName2, "25");
		//Student 3 grade in Points 
		getClassActivityStudent(StudentName3, "23");
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		SW5DLPpage.logoutSW5();

		for (int j = 0; j < students.size(); j++) {

			new LoginAsStudent().LoginStudent(students.get(j));

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			//String winHandleBefore5 = driver.getWindowHandle();
			String parentWindow = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow);
			if (j == 0) {
				// student1
				Thread.sleep(2000);
				student1SubmittedGradescore = sqp.getHeaderScoreinPoints();
				LogUtil.log(student1SubmittedGradescore);
				driver.close();
				driver.switchTo().window(parentWindow);
				SW5DLPstudent.logoutSmartwork5();
			}
			if (j == 1) {
				// student2
				Thread.sleep(2000);
				student2SubmittedGradescore = sqp.getHeaderScoreinPoints();
				LogUtil.log(student2SubmittedGradescore);
				driver.close();
				driver.switchTo().window(parentWindow);
				SW5DLPstudent.logoutSmartwork5();
			}
			if (j == 2) {
				// student3
				Thread.sleep(2000);
				student3SubmittedGradescore = sqp.getHeaderScoreinPoints();
				LogUtil.log(student3SubmittedGradescore);
				driver.close();
				driver.switchTo().window(parentWindow);
				SW5DLPstudent.logoutSmartwork5();
			}

			/*wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//div[@class='head-title fl']/a")));
			driver.findElement(By.xpath("//div[@class='head-title fl']/a"))
					.click();
			driver.close();
			driver.switchTo().window(parentWindow);
			SW5DLPstudent.logoutSmartwork5();*/

		}
	}
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

	public void customQuestionsAnswerAssignmentStudent1() throws Exception {
		custassignment.question1Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 12 QUESTIONS COMPLETED", "14%" );
		// Navigate to Next Question
		sqp.clickNextQuestion();
		custassignment.question2Answer();
		
		submitCustomAssignmentAnswerAndAssert("2 OF 12 QUESTIONS COMPLETED", "18%" );
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		
		submitCustomAssignmentAnswerAndAssert("3 OF 12 QUESTIONS COMPLETED", "29%" );
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		
		submitCustomAssignmentAnswerAndAssert("4 OF 12 QUESTIONS COMPLETED", "39%" );
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		
		submitCustomAssignmentAnswerAndAssert("5 OF 12 QUESTIONS COMPLETED", "46%" );
		sqp.clickNextQuestion();
		
		custassignment.question6Answers();
		submitCustomAssignmentAnswerAndAssert("6 OF 12 QUESTIONS COMPLETED", "54%" );
		sqp.clickNextQuestion();
		custassignment.question7Answers();
		
		submitCustomAssignmentAnswerAndAssert("7 OF 12 QUESTIONS COMPLETED", "57%" );
		sqp.clickNextQuestion();
		
		custassignment.question8Answers();
		submitCustomAssignmentAnswerAndAssert("8 OF 12 QUESTIONS COMPLETED", "68%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question9Answers();
		submitCustomAssignmentAnswerAndAssert("9 OF 12 QUESTIONS COMPLETED", "75%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question10Answers();
		submitCustomAssignmentAnswerAndAssert("10 OF 12 QUESTIONS COMPLETED", "82%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question11Answers();
		submitCustomAssignmentAnswerAndAssert("11 OF 12 QUESTIONS COMPLETED", "86%" );
		sqp.clickNextQuestion();
		
		custassignment.question12Answers();
		submitCustomAssignmentAnswerAndAssert("12 OF 12 QUESTIONS COMPLETED", "100%" );
	}

	public void customQuestionsAnswerAssignmentStudent2() throws Exception {
		custassignment.question1Answers();
		
		submitCustomAssignmentAnswerAndAssert("1 OF 12 QUESTIONS COMPLETED", "14%" );
		// Navigate to Next Question
		sqp.clickNextQuestion();
		
		custassignment.question2Answer();
		submitCustomAssignmentAnswerAndAssert("2 OF 12 QUESTIONS COMPLETED", "18%" );
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		submitCustomAssignmentAnswerAndAssert("3 OF 12 QUESTIONS COMPLETED", "29%" );
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		submitCustomAssignmentAnswerAndAssert("4 OF 12 QUESTIONS COMPLETED", "39%" );
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		submitCustomAssignmentAnswerAndAssert("5 OF 12 QUESTIONS COMPLETED", "46%" );
		sqp.clickNextQuestion();
		
		custassignment.question6Answers();
		submitCustomAssignmentAnswerAndAssert("6 OF 12 QUESTIONS COMPLETED", "54%" );
		sqp.clickNextQuestion();
		
		custassignment.question7Answers();
		
		submitCustomAssignmentAnswerAndAssert("7 OF 12 QUESTIONS COMPLETED", "57%" );
		//Do not answer Question 8 which is having 3 Points
		sqp.clickNextQuestion();
		sqp.clickNextQuestion();
		
		custassignment.question9Answers();
		submitCustomAssignmentAnswerAndAssert("8 OF 12 QUESTIONS COMPLETED", "64%" );
		sqp.clickNextQuestion();
		
		custassignment.question10Answers();
		submitCustomAssignmentAnswerAndAssert("9 OF 12 QUESTIONS COMPLETED", "71%" );
		sqp.clickNextQuestion();
		
		custassignment.question11Answers();
		submitCustomAssignmentAnswerAndAssert("10 OF 12 QUESTIONS COMPLETED", "75%" );
		sqp.clickNextQuestion();
		
		custassignment.question12Answers();
		submitCustomAssignmentAnswerAndAssert("11 OF 12 QUESTIONS COMPLETED", "89%" );
		
	}

	public void customQuestionsAnswerAssignmentStudent3() throws Exception {
		custassignment.question1Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 12 QUESTIONS COMPLETED", "14%" );
		// Navigate to Next Question
		sqp.clickNextQuestion();
		
		custassignment.question2Answer();
		submitCustomAssignmentAnswerAndAssert("2 OF 12 QUESTIONS COMPLETED", "18%" );
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		submitCustomAssignmentAnswerAndAssert("3 OF 12 QUESTIONS COMPLETED", "29%" );
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		submitCustomAssignmentAnswerAndAssert("4 OF 12 QUESTIONS COMPLETED", "39%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		submitCustomAssignmentAnswerAndAssert("5 OF 12 QUESTIONS COMPLETED", "46%" );
		
		sqp.clickNextQuestion();
		custassignment.question6Answers();
		submitCustomAssignmentAnswerAndAssert("6 OF 12 QUESTIONS COMPLETED", "54%" );
		sqp.clickNextQuestion();
		
		custassignment.question7Answers();
	
		submitCustomAssignmentAnswerAndAssert("7 OF 12 QUESTIONS COMPLETED", "57%" );
		sqp.clickNextQuestion();
		
		custassignment.question8Answers();
		
		submitCustomAssignmentAnswerAndAssert("8 OF 12 QUESTIONS COMPLETED", "68%" );
		
		sqp.clickNextQuestion();
		sqp.clickNextQuestion();
		
		custassignment.question10Answers();
		
		submitCustomAssignmentAnswerAndAssert("9 OF 12 QUESTIONS COMPLETED", "75%" );
		sqp.clickNextQuestion();
		
		custassignment.question11Answers();
		submitCustomAssignmentAnswerAndAssert("10 OF 12 QUESTIONS COMPLETED", "79%" );
		sqp.clickNextQuestion();
		
		custassignment.question12Answers();
		submitCustomAssignmentAnswerAndAssert("11 OF 12 QUESTIONS COMPLETED", "93%" );
		
	}
	
	private void getClassActivityStudent(String studentName, String studentscore) throws Exception{
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		classActivityReport.clickStudentNamelink(studentName);
		Thread.sleep(1000);
		classActivityReport.getStudentOverallScore();
		Assert.assertEquals(studentscore,
				classActivityReport.getStudentOverallScore());
		classActivityReport.closeStudentDetailPage();
	}
	
	private void getClassActivityReportStudentRemoveQuestion(String studentName, String value) throws Exception{
	classActivityReport.clickStudentSearchIcon();
	classActivityReport.enterStudentName(studentName);
	classActivityReport.clickStudentNamelink(studentName);
	classActivityReport.getStudentOverallScore();
	Assert.assertEquals(value,
			classActivityReport.getStudentOverallScore());
	classActivityReport.closeStudentDetailPage();
	}
	
	private void submitCustomAssignmentAnswerAndAssert(String questionfooterText, String Score) throws Exception{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.clickSubmitAnswer();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		ReusableMethods.questionCloseLink(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.getQuestionFooterLeftText();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionfooterText);
		sqp.getHeaderScore();
		Assert.assertEquals(sqp.getHeaderScore(), Score);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}
//"1 OF 12 QUESTIONS COMPLETED"
	//"14%"
	
	// Closing driver and test.

		
}
