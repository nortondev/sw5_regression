package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.TutorialGraphingAssignment;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class StudentSubmits_TutorialGraphingAssignment extends PropertiesFile {

	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	TutorialGraphingAssignment assignment;
	
	String ASSIGNMENT_TITLE = "Tutorial: Graphing";
	
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
		driver = getDriver();
		driver.get("https://digital.wwnorton.com/prineco3");
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("CH09HW (Show solution: Only after the GAU date...)")
	@Stories("AS-52 CH9HW (Show solution: Only after the GAU date...)")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		//testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		//String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_TUTORIALGRAPHING);

		// Login as Student
		//String studentUserName = (String) gContext
		//					.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		String studentUserName = "lsimpsonazjfy@evergreener.edu";
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(ASSIGNMENT_TITLE);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();
		//sap.beginAssignmentClick();
		sap.resumeAssignmentClick();
		sqp = new StudentQuestionPlayer();

		// # Use case: Submit Graphical Question 

		// Question #1
		assignment = new TutorialGraphingAssignment();
		assignment.submitAnswer_Question01();
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		
		Assert.assertEquals(sqp.getHeaderScore(), "13%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				"1 OF 8 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
		
		Thread.sleep(3000);
		sqp.clickHeaderTitle(sap);
		driver.close();
		driver.switchTo().window(parentWindow);
		SW5DLPstudent.logoutSmartwork5();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
