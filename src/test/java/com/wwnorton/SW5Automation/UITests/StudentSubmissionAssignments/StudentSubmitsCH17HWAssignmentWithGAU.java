package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class StudentSubmitsCH17HWAssignmentWithGAU extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	
	// Run this scripts before Instructor Extends GAU _ ExpiredAssignmentCH17HW.class file with GAU before 30 mins.
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-38 - Student submits CH17HW assignment with GAU - Part2.")
	@Stories(" CH17HW - Verify that Additional settings remains same for expired submitted assignment when Instructor extends GAU.")
	@Test()
	public void studentSubmitsCH17HWAssignment() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH17HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH17HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());
			calendarInput.sendKeys(Keys.TAB);

			assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");

			assignmentpage.saveButton();
			
			Thread.sleep(5000);
			assignmentpage.publishButton();
			
			Thread.sleep(5000);
			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
			
			// Login as Student Account 1
			
			String studentUserName1 = (String) gContext
					.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
			
			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(SW5Constants.ASSIGNMENT_CH17HW);

			// Assignment Window
			String studentWindow = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, studentWindow);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();

			sap.beginAssignmentClick();				
			sqp.waitForLoadPageElements();

			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(studentWindow);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPstudent.logoutSmartwork5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
