package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet3;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.QuestionDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentDetailandQuestionDetailOverlay extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	String assignmentTitle = "";
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	StudentDetailOverlayPage sdo;
	ClassActivityReportPage classActivityReport;
	CustomAssignmentQuestionSet3 customAssignment;
	AssignmentPage instructorAssignmentPage;
	QuestionDetailOverlayPage qdo;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As an instructor I set up a custom assignment with 5 questions all default settings with a GAU & a late work period,  each question of 3 points, unlimited attempt.")
	@Stories("AS-62 Custom Assignment - Student & Question Detail Overlay")
	@Test
	public void studentDetailandQuestionDetailOverlayClassActivityReportView()
			throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet,
		// StudentUsernames)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(3);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		String studentSetID = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String studentUsername2 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		String studentUsername3 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_3);

		String StudentName1 = testHelper.getStudentUserNameInUppercase(
				studentUsername1).trim();
		LogUtil.log("The Student Name 1", StudentName1);
		String StudentName2 = testHelper.getStudentUserNameInUppercase(
				studentUsername2).trim();
		LogUtil.log("The Student Name 2", StudentName2);
		String StudentName3 = testHelper.getStudentUserNameInUppercase(
				studentUsername3).trim();
		LogUtil.log("The Student Name 3", StudentName3);

		// Create new custom assignment & Publish
		InstructorPublishCustomAssignment instructorPublishCustomAssignment = new InstructorPublishCustomAssignment();
		assignmentTitle = instructorPublishCustomAssignment
				.updateSettingsAndPublishCustomAssignmentStudentAndQuestiondetailOverlay();

		if (assignmentTitle == null || assignmentTitle.isEmpty())
			throw new Exception(
					"CAR Custom assignment is not published in studentSetID => "
							+ studentSetID);

		// assignmentTitle = "CustomAssignment CAR cw2rh";
		String parentWindow = "";

		// STUDENT 1
		new LoginAsStudent().LoginStudent(studentUsername1);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();
		sdo = new StudentDetailOverlayPage();
		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		customAssignment = new CustomAssignmentQuestionSet3();

		student1SubmitAnswers();

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);

		// STUDENT 2
		new LoginAsStudent().LoginStudent(studentUsername2);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(5000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		customAssignment = new CustomAssignmentQuestionSet3();

		student2SubmitAnswers();

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);

		// STUDENT 3
		// As a student joined the class but did not started the assignment.

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();

		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);

		testHelper.getInstructorAssignmentWindow(driver);

		instructorAssignmentPage = new AssignmentPage();
		Thread.sleep(5000);

		// Assert the instructor assignment page should show class average in %
		String classAverage = instructorAssignmentPage.getHeaderScore();
		Assert.assertTrue(classAverage.endsWith("%"),
				"Instructor assignment page displays class average in %?");
		Assert.assertEquals(classAverage, "80%",
				"Average score for custom assignment " + assignmentTitle
						+ " is 80%?");

		// Assert Student Detail Overlay For Student 01 / 02 & 03:
		driver.close();
		driver.switchTo().window(winHandleBefore);
		SW5DLPpage.clickReportsButton(assignmentTitle);
		// Navigate to CAR page

		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("80%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());

		// Student1 Grade details
		getClassActivityStudent(StudentName1);
		getStudentDetailScore("100%", "5/5");
		List<WebElement> stud1GrapgPercentile = classActivityReport
				.getPieChartSlices("100%");
		Assert.assertEquals(stud1GrapgPercentile.size(), 1,
				"The completed graph should show 100% as completed");
		Assert.assertEquals(sdo.getStudentDetailTotalQuestionsCount(), 5,
				"Student details Question Tab display the 5 questions?");
		// Assert the questions average score is 3/3pts for all 5 questions
		List<String> studentquestionsScores1 = sdo.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(studentquestionsScores1.get(i), "3/3pts",
					"Questions average score is \"3/3pts\" for all 5 questions?");
		}

		List<String> student1Attempts = sdo.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student1Attempts.get(i), (i + 1) + "/Unlimited");

		}
		List<String> student1submissionDate = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(student1submissionDate.get(i) != "00:00:00");
		}
		List<String> student1TimeSpent = sdo.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpent = student1TimeSpent.get(i);
			Assert.assertTrue(timeSpent.compareTo("00:00:00") != 0);
			Assert.assertTrue(timeSpent.compareTo("-") != 0);
			Assert.assertTrue(timeSpent.compareTo("") != 0);
		}

		// As instructor perform Edit operation on Student 01
		sdo.clickEditButton();
		Assert.assertTrue(sdo.isPublishONorOFF());
		Assert.assertEquals(GetDate.getCurrentDate(), sdo.getGauDate());
		Assert.assertEquals("11:59 PM", sdo.getGauTime());
		Assert.assertEquals(
				"Late work is accepted 1 day after the GAU at 10% penalty per day",
				sdo.getStudentDetailLateWorkText());
		Assert.assertEquals("Never", sdo.getUngradedPracticeText());
		Assert.assertTrue(sdo.getTimeLimitOnOFFStatus());
		// Assert the questions average score is 3/3pts for all 5 questions on
		// edit mode
		List<String> studentquestionsScoresEditMode = sdo
				.getStudentDetailEditScoreData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(studentquestionsScoresEditMode.get(i),
					"3/3pts",
					"Questions average score is \"3/3pts\" for all 5 questions?");
		}

		List<String> studentAttemptslistEditMode = sdo
				.getStudentDetailEditAttemptsData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(studentAttemptslistEditMode.get(i), (i + 1)
					+ "/Unlimited");
		}

		List<String> student1submissionDateInEditMode = sdo
				.getStudentDetailStudentSubmissionsTimeListEdit();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(student1submissionDateInEditMode.get(i) != "00:00:00");
		}

		List<String> student1TimeSpentEditMode = sdo
				.getStudentDetailTimeSpentDataOnEdit();
		for (int i = 0; i < 5; i++) {
			String timeSpentEditMode = student1TimeSpentEditMode.get(i);
			Assert.assertTrue(timeSpentEditMode.compareTo("00:00:00") != 0);
			Assert.assertTrue(timeSpentEditMode.compareTo("-") != 0);
			Assert.assertTrue(timeSpentEditMode.compareTo("") != 0);
		}
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();

		// Student2 Grade details

		getClassActivityStudent(StudentName2);
		getStudentDetailScore("60%", "3/5");
		List<WebElement> stud2GrapgPercentile = classActivityReport
				.getPieChartSlices("60%");
		Assert.assertEquals(stud2GrapgPercentile.size(), 1,
				"The completed graph should show 60% as completed");
		List<WebElement> stud2GrapgPercentilePart2 = classActivityReport
				.getPieChartSlices("40%");
		Assert.assertEquals(stud2GrapgPercentilePart2.size(), 1,
				"The completed graph should show 40% as Not Started");
		Assert.assertEquals(sdo.getStudentDetailTotalQuestionsCount(), 5,
				"Student details Question Tab display the 5 questions?");

		List<String> studentquestionsScores2 = sdo.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertEquals(studentquestionsScores2.get(i), "0/3pts",
						"Questions average score is \"0/3pts\" for 4 question");
				// sdo.getStudentDetailCircleArrow(i);

			} else {
				Assert.assertEquals(studentquestionsScores2.get(i), "3/3pts",
						"Questions average score is \"3/3pts\" for all 3 questions?");
			}
		}

		List<String> student2Attempts = sdo.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertEquals(student2Attempts.get(i), "-" + "/Unlimited");
			} else {
				Assert.assertEquals(student2Attempts.get(i), (i + 1)
						+ "/Unlimited");
			}
		}
		List<String> Student2submissionDate = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertTrue(Student2submissionDate.get(i)
						.equalsIgnoreCase("-"));
			} else {
				Assert.assertTrue(Student2submissionDate.get(i) != "00:00:00");
			}
		}

		List<String> student2TimeSpent = sdo.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpent = student2TimeSpent.get(i);
			if ((i == 3) || (i == 4)) {
				Assert.assertTrue(timeSpent.equalsIgnoreCase("-"));
			} else {
				Assert.assertTrue(timeSpent.compareTo("00:00:00") != 0);
			}

		}
		// As instructor perform Edit operation on Student 02
		sdo.clickEditButton();
		Assert.assertTrue(sdo.isPublishONorOFF());
		Assert.assertEquals(GetDate.getCurrentDate(), sdo.getGauDate());
		Assert.assertEquals("11:59 PM", sdo.getGauTime());
		Assert.assertEquals(
				"Late work is accepted 1 day after the GAU at 10% penalty per day",
				sdo.getStudentDetailLateWorkText());
		Assert.assertEquals("Never", sdo.getUngradedPracticeText());
		Assert.assertTrue(sdo.getTimeLimitOnOFFStatus());
		// Assert the questions average score is 3/3pts for all 5 questions on
		// edit mode
		List<String> student2questionsScoresEditMode = sdo
				.getStudentDetailEditScoreData();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertEquals(student2questionsScoresEditMode.get(i),
						"0/3pts",
						"Questions average score is \"0/3pts\" for 4 & 5  questions?");
			} else {
				Assert.assertEquals(student2questionsScoresEditMode.get(i),
						"3/3pts",
						"Questions average score is \"3/3pts\" for first 3 questions?");
			}
		}

		List<String> student2AttemptslistEditMode = sdo
				.getStudentDetailEditAttemptsData();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertEquals(student2AttemptslistEditMode.get(i), "-"
						+ "/Unlimited");
			} else {
				Assert.assertEquals(student2AttemptslistEditMode.get(i),
						(i + 1) + "/Unlimited");
			}
		}
		List<String> student2submissionDateInEditMode = sdo
				.getStudentDetailStudentSubmissionsTimeListEdit();
		for (int i = 0; i < 5; i++) {
			if ((i == 3) || (i == 4)) {
				Assert.assertTrue(student2submissionDateInEditMode.get(i)
						.equalsIgnoreCase("-"));
			} else {
				Assert.assertTrue(student2submissionDateInEditMode.get(i) != "00:00:00");
			}

		}

		List<String> student2TimeSpentEditMode = sdo
				.getStudentDetailTimeSpentDataOnEdit();
		for (int i = 0; i < 5; i++) {
			String timeSpentEditMode = student2TimeSpentEditMode.get(i);
			if ((i == 3) || (i == 4)) {
				Assert.assertTrue(timeSpentEditMode.equalsIgnoreCase("-"));
			} else {
				Assert.assertTrue(timeSpentEditMode.compareTo("00:00:00") != 0);
				Assert.assertTrue(timeSpentEditMode.compareTo("-") != 0);
				Assert.assertTrue(timeSpentEditMode.compareTo("") != 0);
			}
		}
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();

		// Student 3 Detail Tab

		getClassActivityStudent(StudentName3);
		getStudentDetailScore("-", "-");
		List<WebElement> stud3GrapgPercentile = classActivityReport
				.getPieChartSlices("100%");
		Assert.assertEquals(stud3GrapgPercentile.size(), 1,
				"The completed graph should show 100% as Not Started");
		/*
		 * classActivityReport.enterStudentName(StudentsName2);
		 * classActivityReport.enterStudentName(StudentsName3);
		 */
		Assert.assertEquals(sdo.getStudentDetailTotalQuestionsCount(), 5,
				"Student details Question Tab display the 5 questions?");
		List<String> studentquestionsScores3 = sdo.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(studentquestionsScores3.get(i), "0/3pts",
					"Questions average score is \"0/3pts\" for all 3 questions?");
		}
		List<String> student3Attempts = sdo.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student3Attempts.get(i), "-" + "/Unlimited");
		}
		List<String> Student3submissionDate = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(Student3submissionDate.get(i).equalsIgnoreCase(
					"-"));
		}
		List<String> student3TimeSpent = sdo.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpent = student3TimeSpent.get(i);
			Assert.assertTrue(timeSpent.compareTo("00:00:00") != 0);
			Assert.assertTrue(timeSpent.equalsIgnoreCase("-"));
		}

		// As instructor perform Edit operation on Student 03
		sdo.clickEditButton();
		Assert.assertTrue(sdo.isPublishONorOFF());
		Assert.assertEquals(GetDate.getCurrentDate(), sdo.getGauDate());
		Assert.assertEquals("11:59 PM", sdo.getGauTime());
		Assert.assertEquals(
				"Late work is accepted 1 day after the GAU at 10% penalty per day",
				sdo.getStudentDetailLateWorkText());
		Assert.assertEquals("Never", sdo.getUngradedPracticeText());
		Assert.assertTrue(sdo.getTimeLimitOnOFFStatus());
		// Assert the questions average score is 0/3pts for all 5 questions on
		// edit mode
		List<String> student3questionsScoresEditMode = sdo
				.getStudentDetailEditScoreData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student3questionsScoresEditMode.get(i),
					"0/3pts",
					"Questions average score is \"0/3pts\" for all 5  questions?");

		}

		List<String> student3AttemptslistEditMode = sdo
				.getStudentDetailEditAttemptsData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student3AttemptslistEditMode.get(i), "-"
					+ "/Unlimited");

		}
		List<String> student3submissionDateInEditMode = sdo
				.getStudentDetailStudentSubmissionsTimeListEdit();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(student3submissionDateInEditMode.get(i)
					.equalsIgnoreCase("-"));

		}

		List<String> student3TimeSpentEditMode = sdo
				.getStudentDetailTimeSpentDataOnEdit();
		for (int i = 0; i < 5; i++) {
			String timeSpentEditMode = student3TimeSpentEditMode.get(i);
			Assert.assertTrue(timeSpentEditMode.equalsIgnoreCase("-"));

		}
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		// Reset question #2 assert score should be set to zero, attempt should
		// be set to -, submission date should be “-”, time spent should be “-”
		searchStudent(StudentName1);
		Thread.sleep(3000);
		sdo.clickEditButton();
		sdo.getResetQuestiononEditMode(1);
		sdo.clickResetButton();
		sdo.clickSaveButton();
		// getClassActivityStudent(studentName1);
		getStudentDetailScore("80%", "4/5");
		List<WebElement> stud1GrapgPercentileAfterReset = classActivityReport
				.getPieChartSlices("80%");
		Assert.assertEquals(stud1GrapgPercentileAfterReset.size(), 1,
				"The completed graph should show 80% as completed");
		List<WebElement> stud1GrapgPercentileAfterReset2 = classActivityReport
				.getPieChartSlices("20%");
		Assert.assertEquals(stud1GrapgPercentileAfterReset2.size(), 1,
				"The  graph should show 20% as Not Started");
		// Assert the questions average score is 3/3pts for all 5 questions
		List<String> studentquestionsScoresafterReset = sdo
				.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			if (i == 1) {
				Assert.assertEquals(studentquestionsScoresafterReset.get(i),
						"0/3pts",
						"Questions average score is \"0/3pts\" for 4 question");
				// sdo.getStudentDetailCircleArrow(i);

			} else {
				Assert.assertEquals(studentquestionsScoresafterReset.get(i),
						"3/3pts",
						"Questions average score is \"3/3pts\" for all 4 questions?");
			}
		}

		List<String> student1AttemptsAfterReset = sdo
				.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			if (i == 1) {
				Assert.assertEquals(student1AttemptsAfterReset.get(i), "-"
						+ "/Unlimited");
			} else {
				Assert.assertEquals(student1AttemptsAfterReset.get(i), (i + 1)
						+ "/Unlimited");
			}
		}
		List<String> Student1submissionDateAfterReset = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			if (i == 1) {
				Assert.assertTrue(Student1submissionDateAfterReset.get(i)
						.equalsIgnoreCase("-"));
			} else {
				Assert.assertTrue(Student1submissionDateAfterReset.get(i) != "00:00:00");
			}
		}
		sdo.getStudentDetailCircleArrow(1);
		// Student1 Grade details After Reset question 2
		// getClassActivityStudent(studentName1);
		getStudentDetailScore("80%", "4/5");
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		// close student detail overlay and then assert following,Average score
		// on assignment graph
		// student tab - score,student tab - questions answered,student tab -
		// incorrect attempt
		// /question tab - for #2 - average score, student submission, average
		// attempt
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='container-box chapter-charts']/div[@class='chart-left']")));
		String averageScore = classActivityReport
				.getCARAverageScoreOnAssignment();
		System.out.println(averageScore);

		Assert.assertEquals(averageScore, "70%", "The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());

		Assert.assertEquals(
				classActivityReport.getCARStudentsTabStudentScore(StudentName1),
				"80%", "First student should have score 80%?");
		Assert.assertEquals(classActivityReport
				.getCARStudentsTabQuestionsAnswered(StudentName1), "4/5",
				"First student should have 4/5 question answered?");
		Assert.assertEquals(classActivityReport
				.getCARStudentsTabIncorrectAttempts(StudentName1), "15",
				"First student should have 13 incorrect attempt?");

		// CAR QUESTIONS TAB for Student 1

		classActivityReport.clickQuestionsTab();
		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions Tab should show the 5 questions?");
		List<String> questionsTabAverageScores = classActivityReport
				.getCARQuestionsTabAverageScoresList();
		List<String> questionsTabStudentSubmissions = classActivityReport
				.getCARQuestionsTabStudentSubmissionsList();
		List<String> questionsTabAverageAttempts = classActivityReport
				.getCARQuestionsTabAverageAttemptsList();
		List<String> questionsTabAverageTimeSpentList = classActivityReport
				.getCARQuestionsTabAverageTimeSpentList();
		if (questionsTabAverageScores.size() != 5
				|| questionsTabStudentSubmissions.size() != 5
				|| questionsTabAverageAttempts.size() != 5
				|| questionsTabAverageTimeSpentList.size() != 5) {
			throw new Exception(
					"CAR Questions Tab is not having details for 5 questions.");
		}
		// Assert the questions average score is 3/3pts for all 5 questions
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(questionsTabAverageScores.get(i), "3/3pts",
					"Questions average score is \"3/3pts\" for all 5 questions?");
		}
		// Assert the questions student submission is #1 for 2 questions

		Assert.assertEquals(questionsTabStudentSubmissions.get(1), "1",
				"Questions student submission is 2 for first 3 questions?");
		// Assert the questions average attempts is 2/Unlimited for #2 questions

		Assert.assertEquals(questionsTabAverageAttempts.get(1),
				2 + "/Unlimited");

		classActivityReport.clickStudentsTab();

		// As instructor open student detail overlay for Student 02 and hit
		// reset all results and assert
		searchStudent(StudentName2);
		Thread.sleep(3000);
		sdo.clickEditButton();
		Thread.sleep(3000);
		sdo.clickResetAllResultsButton();
		Thread.sleep(3000);
		sdo.clickResetAllResultsButtonOnPopUp();
		Thread.sleep(3000);
		sdo.clickSaveButton();
		// getClassActivityStudent(studentName2);
		getStudentDetailScore("-", "-");
		List<WebElement> Student2GrapgPercentileAfterResetAll = classActivityReport
				.getPieChartSlices("100%");
		Assert.assertEquals(Student2GrapgPercentileAfterResetAll.size(), 1,
				"The  graph should show 100% as Not Started");
		// Assert the questions average score is 0/3pts for all 5 questions
		List<String> student2questionsScoresafterResetAll = sdo
				.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student2questionsScoresafterResetAll.get(i),
					"0/3pts",
					"Questions average score is \"0/3pts\" for 5 question");
			// sdo.getStudentDetailCircleArrow(i);

		}

		List<String> student2AttemptsAfterResetAll = sdo
				.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student2AttemptsAfterResetAll.get(i), "-"
					+ "/Unlimited");

		}
		List<String> Student2submissionDateAfterResetAll = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(Student2submissionDateAfterResetAll.get(i)
					.equalsIgnoreCase("-"));

		}

		List<String> student2TimeSpentAfterResetAll = sdo
				.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpentAfterResetAll = student2TimeSpentAfterResetAll
					.get(i);
			Assert.assertTrue(timeSpentAfterResetAll.equalsIgnoreCase("-"));

		}
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(5000);
		
		WebDriverWait wait1 = new WebDriverWait(driver, 50);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='container-box chapter-charts']/div[@class='chart-left']")));
		
		String averageScore1 = classActivityReport
				.getCARAverageScoreOnAssignment();
		LogUtil.log("The average Score", averageScore1);
		Thread.sleep(3000);
		Assert.assertEquals("80%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());

		// CAR Question tab for Student 2
		classActivityReport.clickQuestionsTab();
		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions Tab should show the 5 questions?");
		List<String> Student2questionsTabAverageScores = classActivityReport
				.getCARQuestionsTabAverageScoresList();
		List<String> Student2questionsTabStudentSubmissions = classActivityReport
				.getCARQuestionsTabStudentSubmissionsList();
		List<String> Student2questionsTabAverageAttempts = classActivityReport
				.getCARQuestionsTabAverageAttemptsList();
		List<String> Student2questionsTabAverageTimeSpentList = classActivityReport
				.getCARQuestionsTabAverageTimeSpentList();
		if (Student2questionsTabAverageScores.size() != 5
				|| Student2questionsTabStudentSubmissions.size() != 5
				|| Student2questionsTabAverageAttempts.size() != 5
				|| Student2questionsTabAverageTimeSpentList.size() != 5) {
			throw new Exception(
					"CAR Questions Tab is not having details for 5 questions.");
		}
		// Assert the questions average score is 3/3pts for all 5 questions
		for (int i = 0; i < 5; i++) {
			if (i == 1) {
				Assert.assertEquals(Student2questionsTabAverageScores.get(i),
						"-/3pts",
						"Questions average score is \"-/3pts\" for 2 questions?");
			} else {
				Assert.assertEquals(Student2questionsTabAverageScores.get(i),
						"3/3pts",
						"Questions average score is \"3/3pts\" for all 4 questions?");
			}
		}
		// Assert the questions student submission is #0 for 2 questions

		Assert.assertEquals(Student2questionsTabStudentSubmissions.get(1), "-",
				"Questions student submission is - for first 2 question?");
		// Assert the questions average attempts is 2/Unlimited for #2 questions

		Assert.assertEquals(Student2questionsTabAverageAttempts.get(1), "-"
				+ "/Unlimited");

		classActivityReport.clickStudentsTab();

		Thread.sleep(5000);
		// classActivityReport.closeStudentDetailPage();

		// As instructor open student detail overlay for Student 03 and override
		// full score for question #1 , then assert 1.1.
		searchStudent(StudentName3);
		Thread.sleep(5000);
		sdo.clickEditButton();
		sdo.updateStudentScoreOnEditMode(1, "3");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		sdo.clickSaveButton();
		// getClassActivityStudent(studentName3);
		getStudentDetailScore("20%", "-");
		List<WebElement> Student3GrapgPercentileAfterFullScore = classActivityReport
				.getPieChartSlices("100%");
		Assert.assertEquals(Student3GrapgPercentileAfterFullScore.size(), 1,
				"The  graph should show 100% as Not Started");

		// Assert the questions average score is 0/3pts for all 4 questions and
		// 1st Question is 3/3 pts
		List<String> student3questionsOverideFullScore = sdo
				.getStudentDetailScoreData();
		for (int i = 0; i < 5; i++) {
			if (i == 1) {
				Assert.assertEquals(student3questionsOverideFullScore.get(i),
						"3/3pts",
						"Questions average score is \"3/3pts\" for 1st question");
			} else {

				Assert.assertEquals(student3questionsOverideFullScore.get(i),
						"0/3pts",
						"Questions average score is \"0/3pts\" for 4 question");
				// sdo.getStudentDetailCircleArrow(i);
			}
		}

		List<String> student3AttemptsAfterOverideFullScore = sdo
				.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student3AttemptsAfterOverideFullScore.get(i),
					"-" + "/Unlimited");

		}
		List<String> Student3submissionDateAfterOverideFullScore = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(Student3submissionDateAfterOverideFullScore
					.get(i).equalsIgnoreCase("-"));

		}

		List<String> student3TimeSpentAfterOverideFullScore = sdo
				.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpentAfterResetAll = student3TimeSpentAfterOverideFullScore
					.get(i);
			Assert.assertTrue(timeSpentAfterResetAll.equalsIgnoreCase("-"));
		}
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(5000);
		// As instructor open student detail overlay for Student 03 and override
		// half of the total point for question #3
		searchStudent(StudentName3);
		Thread.sleep(2000);
		sdo.clickEditButton();
		sdo.updateStudentScoreOnEditMode(3, "1.5");
		sdo.clickSaveButton();
		//getClassActivityStudent(StudentName3);
		getStudentDetailScore("30%", "-");
		List<WebElement> Student3GrapgPercentileAfterHalfScore = classActivityReport
				.getPieChartSlices("100%");
		Assert.assertEquals(Student3GrapgPercentileAfterHalfScore.size(), 1,
				"The  graph should show 100% as Not Started");

		// Assert the questions average score is 0/3pts for all 4 questions and
		// 1st Question is 3/3 pts
		List<String> student3questionsOverideHalflScore = sdo
				.getStudentDetailScoreData();
		for (int i = 3; i < 4; i++) {
			if (i == 3) {
				Assert.assertEquals(student3questionsOverideHalflScore.get(i),
						"1.5/3pts",
						"Questions average score is \"1.5/3pts\" for 3 question");
			}
		}
		List<String> student3AttemptsAfterOverideHalfcore = sdo
				.getStudentDetailAttempts();
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(student3AttemptsAfterOverideHalfcore.get(i),
					"-" + "/Unlimited");

		}
		List<String> Student3submissionDateAfterOverideHalfScore = sdo
				.getStudentDetailStudentSubmissionsList();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(Student3submissionDateAfterOverideHalfScore
					.get(i).equalsIgnoreCase("-"));

		}

		List<String> student3TimeSpentAfterOverideHalfScore = sdo
				.getStudentDetailTimeSpentData();
		for (int i = 0; i < 5; i++) {
			String timeSpentAfterResetAll = student3TimeSpentAfterOverideHalfScore
					.get(i);
			Assert.assertTrue(timeSpentAfterResetAll.equalsIgnoreCase("-"));
		}
		Thread.sleep(1000);
		classActivityReport.closeStudentDetailPage();
		Thread.sleep(5000);
	    questionDetailOverlay();
		//driver.close();
		//driver.switchTo().window(winHandleBefore);

	}
	
	// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}

	private void student1SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "20%");

		sqp.clickNextQuestion();

		// Q2 - Correct in the second attempt
		// First Incorrect Attempt
		customAssignment.question2IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Correct Attempt
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "40%");

		sqp.clickNextQuestion();

		// Q3 - Correct in the third attempt
		// First Incorrect Attempt
		customAssignment.question3IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Correct Attempt
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "60%");

		sqp.clickNextQuestion();

		// Q4 - Correct in the fourth attempt
		// First Incorrect Attempt
		customAssignment.question4IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fourth Correct Attempt
		customAssignment.question4Answers();
		submitCorrectAnswerAndAssert("4 OF 5 QUESTIONS COMPLETED", "80%");

		sqp.clickNextQuestion();

		// Q5 - Correct in the fifth attempt
		// First Incorrect Attempt
		customAssignment.question5IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fourth Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fifth Correct Attempt
		customAssignment.question5Answers();
		submitCorrectAnswerAndAssert("5 OF 5 QUESTIONS COMPLETED", "100%");
	}

	private void student2SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "20%");

		sqp.clickNextQuestion();
        Thread.sleep(1000);
		// Q2 - Correct in the second attempt
		// First Incorrect Attempt
		customAssignment.question2IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Correct Attempt
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "40%");

		sqp.clickNextQuestion();

		// Q3 - Correct in the third attempt
		// First Incorrect Attempt
		customAssignment.question3IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Correct Attempt
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "60%");

		// Q4 - No Submission

		// Q5 - No Submission

	}

	private void submitIncorrectAnswerAndClickTryAgain() throws Exception {
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackTryAgain();

		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");
	}

	private void submitCorrectAnswerAndAssert(String questionFooterLeftText,
			String score) throws Exception {
		sqp.clickSubmitAnswer();
		Thread.sleep(1000);
		questionCloseLink();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionFooterLeftText);
		Assert.assertEquals(sqp.getHeaderScore(), score);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}

	private void questionCloseLink() throws Exception {
		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}
	}

	private void getClassActivityStudent(String studentName) throws Exception {
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		Thread.sleep(1000);
		classActivityReport.clickStudentNamelink(studentName);

		Thread.sleep(1000);
		// classActivityReport.closeStudentDetailPage();
	}

	// 1.5 Question detail overlay:
	public void questionDetailOverlay() throws Exception {
		qdo = new QuestionDetailOverlayPage();
		Thread.sleep(2000);
		
		classActivityReport.clickQuestionsTab();
		sdo.clickQuestionNumberonQuestionTab(1);
		Thread.sleep(3000);

		Assert.assertEquals("3pts", sdo.getQuestiondetailAverageScore());
		Assert.assertEquals("-", sdo.getQuestiondetailAverageTimeSpent());
		Assert.assertEquals("1", sdo.getQuestiondetailStudentSubmission());
		String ItemID = sdo.getQuestiondetailItemID();
		LogUtil.log("the Item ID is ", ItemID);
		Assert.assertTrue(sdo.getQuestiondetailItemID() != null);
		List<String> studentList = sdo.getQuestiondetailListOfStudent();
		for (int i = 0; i < studentList.size(); i++) {
			Assert.assertNotNull(studentList.get(i), "The StudentList Name");
		}
		Thread.sleep(3000);
		sdo.clickGiveCreditAllStudentLink();
		Thread.sleep(3000);
		sdo.clickGiveCreditButton();
		Thread.sleep(8000);
		Assert.assertEquals("3pts", sdo.getQuestiondetailAverageScore());
		Assert.assertEquals("-", sdo.getQuestiondetailAverageTimeSpent());
		Assert.assertEquals("3", sdo.getQuestiondetailStudentSubmission());
		String ItemID1 = sdo.getQuestiondetailItemID();
		LogUtil.log("the Item ID is ", ItemID1);
		Assert.assertTrue(sdo.getQuestiondetailItemID() != null);
		Thread.sleep(5000);
		qdo.clickCloseQuestionDetailOverlay();
		Thread.sleep(8000);
		classActivityReport.clickQuestionsTab();
		Thread.sleep(5000);
		List<String> questionsTabAverageScoresQuestion2 = classActivityReport
				.getCARQuestionsTabAverageScoresList();
		List<String> questionsTabStudentSubmissionsQuestion2 = classActivityReport
				.getCARQuestionsTabStudentSubmissionsList();
		List<String> questionsTabAverageAttemptsQuestion2 = classActivityReport
				.getCARQuestionsTabAverageAttemptsList();

		// Assert the questions average score is 3/3pts for 2nd questions
		for (int i = 1; i < 2; i++) {
			Assert.assertEquals(questionsTabAverageScoresQuestion2.get(i),
					"3/3pts",
					"Questions average score is \"3/3pts\" for 2nd questions?");
		}
		// Assert the questions student submission is 3/3 for 2nd questions
		for (int i = 1; i < 2; i++) {
			Assert.assertEquals(questionsTabStudentSubmissionsQuestion2.get(i),
					"3", "Questions student submission is 3/3 2nd Question");
		}
		// Assert the questions average attempts is 1/Unlimited to 5/Unlimited
		// for all 5
		// questions
		for (int i = 1; i < 2; i++) {
			Assert.assertEquals(questionsTabAverageAttemptsQuestion2.get(i),
					"-" + "/Unlimited");
		}
	}

	public void searchStudent(String studentName) throws Exception {
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		Thread.sleep(1000);
		classActivityReport.clickStudentNamelink(studentName);
	}

	private void getStudentDetailScore(String OverallScore,
			String questioncompleted) throws Exception {
		classActivityReport.getStudentOverallScore();
		// Assert.assertEquals(value,
		// classActivityReport.getStudentOverallScore());
		Assert.assertEquals(OverallScore, sdo.getOverallScore());

		//Assert.assertTrue((sdo.getTimeSpent()!=0)||(sdo.TimeSpentData.getText().equalsIgnoreCase("-")));

		Assert.assertEquals(questioncompleted,
				sdo.getAssignedQuestionCompleted());

		System.out.println(sdo.getGradeText());
		System.out.println(sdo.getlateworkText());
		System.out.println(sdo.getStudentDetailTimeSpentData());
		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions column should show the 5 questions?");
	}

	
}
