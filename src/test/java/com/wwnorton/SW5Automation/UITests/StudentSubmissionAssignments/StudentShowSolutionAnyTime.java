
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH7HW;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentShowSolutionAnyTime extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH7HW ch7HW;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-50 CH7HW (Show solution: Any time) - As a student")
	@Test(priority = 0)
	public void studentShowSolutionAnyTime() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH7HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue((sap.isBeginAssignmentButtonDisplayed()) || ( sap.isResumeAssignmentDisplayed()));

		sap.beginAssignmentClick();
		
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		ch7HW = new AssignmentCH7HW();

//		START - useCase:
//		As a student submit an incorrect answer and assert you should have presented with a "View Solution" option.
//		As a student submit multiple attempts and assert every attempt should have "View Solution" option. (try to set attempt limit for questions as instructor)
//		As a student submit assignment within the GAU and assert the "View Solution" option presented.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "0 OF 17 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/17");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");
		Assert.assertTrue(sqp.isPreviousQuestionDisabledButtonDisplayed(), "Previous question button is disabled?");
		Assert.assertTrue(sqp.isNextQuestionEnabledButtonDisplayed(), "Next question button is enabled?");

		// Question #1 - First Attempt
		ch7HW.question1InCorrectAnswer();
        Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		Thread.sleep(5000);
		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackTryAgain();

		// Question #1 - Second Attempt

		Assert.assertEquals(sqp.getHeaderScore(), "0%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), true,
				"View Solution button is displayed in the footer?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackTryAgain();

		// Question #1 - Third Attempt

		Assert.assertEquals(sqp.getHeaderScore(), "0%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("3rd attempt"));
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), true,
				"View Solution button is displayed in the footer?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayDisplayed(), true, "Feedback overlay is displayed ?");

		sqp.clickFeedbackClose();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		boolean isThreeAttemptsTabDisplayedOnPage = false;
		if (sqp.isAttemptViewDisplayed("1st attempt") && sqp.isAttemptViewDisplayed("2nd attempt")
				&& sqp.isAttemptViewDisplayed("3rd attempt")) {
			isThreeAttemptsTabDisplayedOnPage = true;
		}

		Assert.assertEquals(isThreeAttemptsTabDisplayedOnPage, true, "There should be 3 attempts tab on the page?");
		Assert.assertTrue(sqp.isViewSolutionDisplayed(), "There should be a solution on the page?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false,
				"View Solution button is not displayed in the footer?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		// Navigate to Question #4
		sap.clickQuestion(4);

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionTitle(), "04 Question(1 point)");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/17");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #4 - First Attempt
		ch7HW.question4InCorrectAnswer();

		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");

		sqp.clickFeedbackViewSolution();

		Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
				"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
		Assert.assertEquals(sqp.getConfirmViewSolutionHeaderText().trim(),
				"Are you sure you want to view the solution?",
				"Alert popup with 'Are you sure you want to view the solution?' header is displayed?");
		Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
				"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
				"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");

		sqp.clickConfirmPopupCancelButton();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		sqp.clickViewSolution();

		Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
				"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
		Assert.assertEquals(sqp.getConfirmViewSolutionHeaderText().trim(),
				"Are you sure you want to view the solution?",
				"Alert popup with 'Are you sure you want to view the solution?' header is displayed?");
		Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
				"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
				"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");

		sqp.clickConfirmPopupViewSolutionButton();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertTrue(sqp.isViewSolutionDisplayed(), "There should be a solution on the page?");
		Assert.assertEquals(sqp.getHeaderScore(), "0%");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "2 OF 17 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "04/17");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false,
				"View Solution button is not displayed in the footer?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
		// END
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
