
package com.wwnorton.SW5Automation.UITests.StudentSubmissionAssignments;


import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH12HW;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class StudentLimitedAttemptAndAttemptPaneltyScenario extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH12HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("CH12HW - Limited Attempt & Attempt Penalty Scenario")
	@Stories("AS-55 CH12HW - Limited Attempt & Attempt Penalty Scenario")
	@Test
	public void PartiallySubmitAssignmentWithValidGAU() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH12HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);
		
		assignment = new AssignmentCH12HW();
		sap = new StudentAssignmentPage();

		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements();

		// Question #1 - First Attempt correct submit
		assignment.question1CorrectAnswer();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(2000);

		Assert.assertEquals(sqp.getHeaderScore(), "1 out of 27",
				"Student average score on assignment page for CH12HW is \"1 out of 27\"?");

		sqp.clickNextQuestion();

		// Question #1 - 1st Attempt incorrect submit
		assignment.enterQuestion2_InCorrect();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #1 - 2nd Attempt correct submit
		Thread.sleep(2000);
		assignment.enterQuestion2_Correct();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(2000);

		Assert.assertEquals(sqp.getHeaderScore(), "2.9 out of 27",
				"Student average score on assignment page for CH12HW is \"2.9 out of 27\"?");

		Assert.assertEquals(sqp.getHeaderRemainingAttempt(), "1",
				"Student remaining attempt on assignment page for CH12HW is \"1\"?");

		sqp.clickNextQuestion();

		// Question #3 - 1st Attempt incorrect submit
		assignment.question3SelectInCorrectAnswer();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #3 - 2nd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #3 - 3rd Attempt correct submit
		
		assignment.question3SelectCorrectAnswer();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		Thread.sleep(2000);

		Assert.assertEquals(sqp.getHeaderScore(), "4.7 out of 27",
				"Student average score on assignment page for CH12HW is \"4.7 out of 27\"?");

		Assert.assertEquals(sqp.getHeaderRemainingAttempt(), "no attempts",
				"Student remaining attempt on assignment page for CH12HW is \"no attempts\"?");

		sqp.clickNextQuestion();

		// Question #4 - 1st Attempt incorrect submit
		assignment.Question4_InCorrectAnswer();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #4 - 2nd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #4 - 3rd Attempt incorrect submit
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();
		
		Thread.sleep(3000);
		Assert.assertEquals(sqp.getHeaderScore(), "5.7 out of 27",
				"Student average score on assignment page for CH12HW is \"5.7 out of 27\"?");

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "4 OF 21 QUESTIONS COMPLETED",
				"Student completed questions on assignment page for CH12HW is \"4 OF 21 QUESTIONS COMPLETED\"");

		Assert.assertFalse(sqp.isSubmitAnswerButtonDisplayed(),
				"Student should not be able to submit answer on assignment page for CH12HW ?");

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		sap.resumeAssignmentClick();

		Thread.sleep(2000);
		
		Assert.assertEquals(sqp.getHeaderScore(), "5.7 out of 27",
				"Student average score on assignment page for CH12HW is \"5.7 out of 27\"?");

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "4 OF 21 QUESTIONS COMPLETED",
				"Student completed questions on assignment page for CH12HW is \"4 OF 21 QUESTIONS COMPLETED\"");

		Assert.assertFalse(sqp.isSubmitAnswerButtonDisplayed(),
				"Student should not be able to submit answer on assignment page for CH12HW for question number 4");

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		sap.clickQuestion(21);

		// Question #21 - 1st Attempt, Enter Part 1 Correct and rest incorrect submit
		assignment.enterQuestion21(1);
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		// Question #21 - 2nd Attempt, Enter Part 1 and Part 2 Correct and Part 3
		// incorrect submit
		assignment.enterQuestion21(2);
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}
       
		// Question #21 - 3rd Attempt correct submit
		assignment.enterQuestion21();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getHeaderScore(), "8.4 out of 27",
				"Student average score on assignment page for CH12HW is \"8.4 out of 27\"");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}