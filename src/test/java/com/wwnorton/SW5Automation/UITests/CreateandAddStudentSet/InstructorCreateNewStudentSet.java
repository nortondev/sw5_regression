package com.wwnorton.SW5Automation.UITests.CreateandAddStudentSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Step;

@Listeners({ TestListener.class })
public class InstructorCreateNewStudentSet extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Test
	public void createNewStudentSetTest() throws Exception {
		createNewStudentSet();
	}

	@Step("Create New Student set,  Method: {method}")
	public String createNewStudentSet() throws Exception {
		driver = getDriver();
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetID == null) {
			LoginAsInstructor logininst = new LoginAsInstructor();
			logininst.loginInstructor();
			CreateNewStudentSet CSS = new CreateNewStudentSet();
			Thread.sleep(5000);
			CSS.SignIn_Register();
			CSS.ManageStudentSetlink();
			CSS.CreateStudentSetButton();
			CSS.createNewStudentset();
			CSS.createStudentset_information();
			studentSetID = CSS.createStudentset_ID();
            LogUtil.log("The StudentSetID", studentSetID);
			ManageStudentSetsPage managestudentsetpage = new ManageStudentSetsPage();
			managestudentsetpage.clickcloselink();
			CSS.logoutSmartwork5();
		}

		return studentSetID;
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
