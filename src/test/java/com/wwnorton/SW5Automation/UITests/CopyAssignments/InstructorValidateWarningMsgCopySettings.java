package com.wwnorton.SW5Automation.UITests.CopyAssignments;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.CreateNewSS;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

import junit.framework.Assert;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class InstructorValidateWarningMsgCopySettings extends PropertiesFile {
	ManageStudentSetsPage manageStudentSetpage;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	/*
	 * Do setup operations, get JSON response from the API and put it into JsonPath
	 * object Then we will do query and manipulations with JsonPath class’s methods.
	 */

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	// Allure Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Validate Warning Message when Copying Settings from Source and students have already started working on an assignment in Target")
	@Stories("Copy Settings And Assignments")
	@Test

	public void ValidateWarningMsgCopySettings() throws Exception {

		String JsonTargetSS = jsonObj.getAsJsonObject("CopySettings_ValidateWarningMsg").get("targetStudentSetTitle")
				.getAsString();
		String JsonSelectSourceSS = jsonObj.getAsJsonObject("CopySettings_ValidateWarningMsg")
				.get("sourceStudentSetTitle").getAsString();
		String JsonWarningMsg = jsonObj.getAsJsonObject("CopySettings_ValidateWarningMsg").get("warningmsg")
				.getAsString().trim();

		String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		String instructorPassword = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();

		try {

			LoginPage login = PageFactory.initElements(driver, LoginPage.class);
			login.loginSW5(instructorUserName, instructorPassword);

			CreateNewSS createSS = PageFactory.initElements(driver, CreateNewSS.class);
			createSS.manageStudentSet();
            Thread.sleep(5000);
			UpdateStudentSet updateSS = PageFactory.initElements(driver, UpdateStudentSet.class);
			 Thread.sleep(5000);
			updateSS.editStudentSet(JsonTargetSS);
			 Thread.sleep(5000);
			updateSS.copyAssignmentnSettings(JsonSelectSourceSS);
			 Thread.sleep(5000);

			String warningMessage = updateSS.ValidateWarningMsg().trim();
			 Thread.sleep(5000);
			Assert.assertEquals(JsonWarningMsg, warningMessage);
			driver.findElement(By.xpath("//span[@class='ui-button-text'][contains(.,'Cancel')]")).click();
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loading_message")));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("$('a[class^=\"btn-close\"]').click()");
			driver.findElement(By.xpath("//span[@class='ui-button-text'][contains(.,'Continue')]")).click();
			Thread.sleep(3000);
			login.logoutSW5();

		}

		catch (InvocationTargetException ex) {
			System.err.println("An InvocationTargetException was caught!");
			Throwable cause = ex.getCause();
			System.out.format("Invocation of %s failed because of: %s%n", cause.getMessage());
		}

		catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
			System.err.println("The following exception was thrown:");
			ex.printStackTrace();
		}

	}

	// Closing driver and test.

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
