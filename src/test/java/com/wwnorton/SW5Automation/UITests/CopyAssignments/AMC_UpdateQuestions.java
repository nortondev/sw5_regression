package com.wwnorton.SW5Automation.UITests.CopyAssignments;

import java.io.FileReader;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class AMC_UpdateQuestions {

	public WebDriver driver = BaseDriver.getDriver();
	public String UserDir = System.getProperty("user.dir");
	
	@Test
	public void UpdateQuestions() throws InterruptedException {
		driver = BaseDriver.getDriver();
		//String baseUrl = "https://sw5-sw5-stg.wwnorton.net/problemsets/";
		//String loginEmail = "ybhosale@wwnorton.com";
		String baseUrl = "http://sw5.wwnorton.com/problemsets/";
		String loginEmail = "Gbalgazina@wwnorton.com";
		String password = "Smartwork5";
	   // String questionIDSet = null;
	    
	    JsonObject jsonobject = readAddquestionJson();

		// declaration and instantiation of objects/variables
    	System.setProperty("webdriver.chrome.driver", UserDir +
				"/Drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

        // launch Fire fox and direct it to the Base URL
		driver.manage().window().maximize();
		driver.get(baseUrl);
        
        WebDriverWait wait = new WebDriverWait(driver, 15);
		TimeUnit.SECONDS.sleep(15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginuser']")));
		WebElement signIn_Email = driver.findElement(By.xpath("//input[@id='loginuser']"));
		WebElement signIn_Pswd = driver.findElement(By.xpath("//input[@id='loginpsw']"));
		WebElement signInButton = driver.findElement(By.xpath("//input[@id='loginBtn']"));
		signIn_Email.clear();
		signIn_Email.sendKeys(loginEmail);
		signIn_Pswd.sendKeys(password);
		signInButton.click();
		WebDriverWait wait1 = new WebDriverWait(driver, 100);
		wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='loading']")));
		Thread.sleep(2000);
		String questionsSetName = SW5Constants.QUESTION_DATA;
		JsonElement element = jsonobject.get(questionsSetName);
		JsonObject obj = element.getAsJsonObject();
		Set<String> questionsKeys = obj.keySet();
		Iterator<String> it = questionsKeys.iterator();
		int counter = 0;
		while (it.hasNext()) {
			String key = it.next();
			String value = obj.get(key).getAsString();
			WebElement SearchQuestionTextbox = driver.findElement(
					By.xpath("//input[@id='quesSearchInput']"));
			WebElement Searchbutton = driver.findElement(By.xpath("//a[@id='quesSearchBut']"));
			SearchQuestionTextbox.clear();
			ReusableMethods.waitForPageLoad(driver);
			SearchQuestionTextbox.sendKeys(value);
			Thread.sleep(2000);
			Searchbutton.click();
			wait1.until(ExpectedConditions.invisibilityOfElementLocated(
					By.xpath("//div[@id='questionTable_processing']")));
			List<WebElement> selectQuestion = driver.findElements(
					By.xpath("//input[@class='que-table-chx']"));
			selectQuestion.get(0).click();
			Thread.sleep(1000);
			WebElement editButton = driver.findElement(
					By.xpath("//button[@id='editQuestionBut']/span"));
			editButton.click();
			Thread.sleep(5000);
			List<WebElement> gotITButton;
			gotITButton = driver.findElements(
					By.xpath("//button[text()='Got it']"));
			if(gotITButton.size() > 0) {
				gotITButton.get(0).click();
			}
			String ChildWindow1 = null;
			Set<String> Windowhandles = driver.getWindowHandles();
			String ParentWindow = driver.getWindowHandle();
			Windowhandles.remove(ParentWindow);
			String winHandle = Windowhandles.iterator().next();
			if (winHandle != ParentWindow) {
				ChildWindow1 = winHandle;
			}
			driver.switchTo().window(ChildWindow1);
			Thread.sleep(10000);
			
			wait1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.xpath("//iframe[contains(@title,'Rich Text Editor, psEditor_')]")));
			//wait1.until(ExpectedConditions.invisibilityOfElementLocated(
			//		By.xpath("//div[@class='ajax-loader']")));
			int numberOfiframesEditor = driver.findElements(
					By.xpath("//iframe[contains(@title,'Rich Text Editor, psEditor_')]")).size();
				if (numberOfiframesEditor > 0) {
					List <WebElement> iframesEditor = driver.findElements(
							By.xpath("//iframe[contains(@title,'Rich Text Editor, psEditor_')]"));
					Actions actions = new Actions(driver);
					for (int j=0; j<numberOfiframesEditor; j++) {
						ReusableMethods.scrollIntoView(driver, iframesEditor.get(j));
						driver.switchTo().frame(iframesEditor.get(j));
						
						try {
								WebElement molecularDrawingObj = driver.findElement(
										By.xpath("//div[@class='wafer_ps_chemi_class cke_widget_element']/span/img"));
								if (molecularDrawingObj.isDisplayed() == true) {
									actions.doubleClick(molecularDrawingObj).perform();
									molecularDrawingObj.click();
									driver.switchTo().defaultContent();
									String ChildWindow2 = null;
									Windowhandles = driver.getWindowHandles();
									ChildWindow1 = driver.getWindowHandle();
									Windowhandles.remove(ParentWindow);
									Windowhandles.remove(ChildWindow1);
									winHandle = Windowhandles.iterator().next();
									if (winHandle != ParentWindow && winHandle != ChildWindow1) {
									ChildWindow2 = winHandle;
								}
								
									driver.switchTo().window(ChildWindow2);
									Thread.sleep(15000);
									wait1.until(ExpectedConditions.invisibilityOfElementLocated(
											By.xpath("//div[@class='ajax-loader']")));
									Thread.sleep(3000);
									WebElement saveAndExitButton = driver.findElement(
											By.xpath("//button[@id='saveAndExit']"));
									saveAndExitButton.click();
									//Thread.sleep(15000);
									wait1.until(ExpectedConditions.invisibilityOfElementLocated(
											By.xpath("//span[@id='savedBanner' and text()='Question Saved.']")));
									Thread.sleep(5000);
									driver.switchTo().window(ChildWindow1);
								}

						} catch (Exception e) {
							e.getMessage();
							System.out.println("Molecular Drawing object does not exist");
							driver.switchTo().window(ChildWindow1);
							driver.switchTo().defaultContent();
						}
						
					}
				}
				
				Thread.sleep(2000);
				wait1.until(ExpectedConditions.elementToBeClickable(
						By.xpath("//li/a[@id='toSave']")));
				driver.findElement(By.xpath("//li/a[@id='toSave']")).click();
				Thread.sleep(5000);
				List<WebElement> editDialogBox;
						editDialogBox = driver.findElements(
								By.xpath("//div[@id='liveEditDialogDiv']"));
				if(editDialogBox.size() > 0) {
					WebElement commentsText = driver.findElement(
							By.xpath("//textarea[@id='actionEditorNote']"));
					commentsText.clear();
					commentsText.sendKeys("Test - SW5-4845");
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@id='okBtn']")).click();
				}
			
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='saveBusy saveDone']/i/b")));
			wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//span[@class='saveBusy saveDone']/i/b")));
			Thread.sleep(5000);
			driver.close();
			driver.switchTo().window(ParentWindow);
			Thread.sleep(2000);
			SearchQuestionTextbox.clear();
			System.out.println(counter);
			System.out.println(value);
			counter++;
			
		}
		
		
       
        driver.close();

	}
	
	public boolean checkWarningSign() {
		
		WebElement warningSign;
		
		try {
				driver = BaseDriver.getDriver();
				warningSign = driver.findElement(
						By.xpath("//span[@class='glyphicons glyphicons-warning-sign']"));
				warningSign.isDisplayed();
				return true;
		} catch (Exception e) { return false;}
	}
	
	
	public boolean checkQuestionDialoge() {
		driver = BaseDriver.getDriver();
		List<WebElement> editDialogBox;
		
		try {
				WebDriverWait wait = new WebDriverWait(driver, 100);
				wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@id='liveEditDialogDiv']")));
				editDialogBox = driver.findElements(
						By.xpath("//div[@id='liveEditDialogDiv']"));
				if (editDialogBox.size() != 0) {
					return true;
				}
		} catch (Exception e) { return false;}
		return true;
	}
	
	
	public JsonObject readAddquestionJson() {

		JsonObject jsonobject = null;

		String questionSetPath = System.getProperty("user.dir") + "/src/test/resources/QuestionsTestData.json";
		try {
				JsonParser jsonParser = new JsonParser();
				JsonReader reader = new JsonReader(new FileReader(questionSetPath));
				reader.setLenient(true);
				JsonElement rootElement = jsonParser.parse(reader);
				jsonobject = rootElement.getAsJsonObject();

			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject;

	}

}

