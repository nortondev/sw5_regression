package com.wwnorton.SW5Automation.UITests.CopyAssignments;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.wwnorton.SW5Automation.objectFactory.CreateNewSS;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class SW5_LoadTestData {
	
	LoginPage login;
	CreateNewSS createSS;
	SW5DLPPage SW5DLPpage;
	
	public WebDriver driver = BaseDriver.getDriver();
	public String UserDir = System.getProperty("user.dir");


	// TestNG Annotations
	@Parameters("Browser")
	@BeforeTest

	// Test Setup
	public void setUp() throws Exception {
		driver = BaseDriver.getDriver();
		System.setProperty("webdriver.chrome.driver", UserDir + "/Drivers/" + "chromedriver.exe");
		ChromeOptions chromeOptions = new ChromeOptions();
		driver = new ChromeDriver(chromeOptions);
		driver.manage().window().maximize();
		driver.get("https://digital.wwnorton.com/test6");

	}

	// Allure Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Create New Student Set copying Student set from Other Instructor")
	@Stories("Create Student Set_Copy From Other Instructor")
	@Test

	public void createNewStudentSetCopyFromOtherInstructor() throws Exception {
		
		driver = BaseDriver.getDriver();
		String loadTesturl = "https://digital.wwnorton.com/test6";
		JsonObject jsonObj = null;

		
		try {

				JsonParser parser = new JsonParser();
					if(loadTesturl.equalsIgnoreCase("https://digital.wwnorton.com/test6")) {
						JsonReader jReader = new JsonReader(new FileReader(UserDir
								+ "/src/test/resources/LoadTest_TestData.json"));
						jReader.setLenient(true);
						JsonElement rootElement = parser.parse(jReader);
						jsonObj = rootElement.getAsJsonObject();
					}

			} catch (Exception e)
				{ e.printStackTrace();}
		
		//String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		
		
		String JsonSSOption = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("studentSetOption").getAsString();
		String JsonSchoolState = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("schoolState").getAsString();
		String JsonSchoolName = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("schoolName").getAsString();
		String JsonSSStartDate = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("startDate").getAsString(); 
		String JsonSSEndDate = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor").get("endDate").getAsString();
		
		
		//Create an object of File class to open CSV file
		String filePath = UserDir + "/src/test/resources/InstructorLoginData.csv";
		
	    String loginEmail;
	    String password = "Tabletop";
        
    	try {
    			
    			// Create an object of file reader 
            	// CSV file path as a parameter
    			FileReader filereader = new FileReader(filePath); 
    			
    			// create testData object for CSVReader class and skip first Line
    	        CSVReader testData = new CSVReaderBuilder(filereader).withSkipLines(1).build(); 
    	        
    	     // Read CSV test data line by line
    	        String[] csvCell; 
    	        
    	while ((csvCell = testData.readNext()) != null) 
    	 	{
    	        	
    	       	for(int i=0; i<csvCell.length; i++) 
    	        {
    	        	loginEmail = csvCell[0];
	    
					login = new LoginPage();
					login.loginSW5(loginEmail, password);

					for (int j = 0; j <= 2; j++) {

						String SSIDPrefix = "LoadTest_SSID_";
						int suffixCharCount = 5;

						String studentSetTitle = SSIDPrefix
								+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();

						JsonObject jsonResObj = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor");
						JsonArray jsonArrayObj = jsonResObj.getAsJsonArray("sourceStudentSet");
						JsonObject JsonSSIDObj = new JsonObject();
						JsonSSIDObj = jsonArrayObj.get(j).getAsJsonObject();

						String JsonSSID = JsonSSIDObj.get("SSID").getAsString();

						createSS = new CreateNewSS(driver);
						createSS.manageStudentSet();

						WebDriverWait wait = new WebDriverWait(driver, 10);
						wait.until(ExpectedConditions.visibilityOfElementLocated(
								By.xpath("//span[contains(text(),'Create New Student Set')]")));

						createSS.clickCreateNewStudentSet();

						Thread.sleep(3000);
						createSS.createNewStudentSetWithCopyOtherInstructor(JsonSSOption, JsonSSID);
						createSS.studentSetInformationCopy(studentSetTitle, JsonSchoolName,
								JsonSSStartDate, JsonSSEndDate);

						Thread.sleep(3000);
						String studentsetID = null;
						WebElement Student_alert_Text;
						Student_alert_Text = driver.findElement(By.xpath(
								"//div[@class='alert_dialog_class ui-dialog-content ui-widget-content' and contains(text(),"
										+ "'Student Set successfully created. The new Student Set ID is:')]"));
						// wait.until(ExpectedConditions.visibilityOf(Student_alert_Text));
						String str = Student_alert_Text.getText();
						Matcher m = Pattern.compile("\\d+").matcher(str);
						while (m.find()) {
							studentsetID = m.group(0);
						}

						String NewStudentSetConfirmation = createSS.confirmNewStudentSet();
						Assert.assertEquals(NewStudentSetConfirmation, studentSetTitle);
						createSS.closeManageStudentSetPopUp();

						login.clickSW5Icon();

						Thread.sleep(5000);
						SW5DLPpage = new SW5DLPPage();
						SW5DLPpage.selectByPartOfVisibleText(studentsetID);

						int assignmentCount = SW5DLPpage.assignmentRowCount();

						ReusableMethods.checkPageIsReady(driver);

						// Validating Number of Assignments with JSON Assignment Count.
						String JSONCount = jsonObj.getAsJsonObject("CreateStudentSet_CopyFromOtherInstructor")
								.get("numberOfAssignment").getAsString();
						Assert.assertEquals(String.valueOf(assignmentCount), JSONCount);

					}

						login.logoutSW5();

					}

    	        } 
    		
    		}
    	        
    	
    	catch (FileNotFoundException fe) 
    	
    	{
            fe.printStackTrace();
        } 
    	
    	
    	catch (IOException e) 
    	
    	{
            e.printStackTrace();
        }
    	        	
    	        	
	}


	// Closing driver and test.

	@AfterTest
	public void closeTest() throws Exception {
		driver = BaseDriver.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		driver.close();
		driver.quit();

	}

}