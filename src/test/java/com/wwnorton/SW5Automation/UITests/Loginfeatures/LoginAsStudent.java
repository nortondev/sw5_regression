package com.wwnorton.SW5Automation.UITests.Loginfeatures;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class LoginAsStudent extends PropertiesFile {

	LoginPage SW5Login;
	LoginPage DLPpage;
	LoginAsStudent loginStd;

	// TestNG Annotations
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	// Read data from Json File
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();

	@Severity(SeverityLevel.NORMAL)
	@Description("Login to Smartwork5 application as a Student")
	@Stories("AS-4 Login in as Student")
	@Test(priority = 0)
	public void LoginStudent() throws Exception {
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		if (studentUserName == null) {
			throw new Exception("Please Add Test Case SetupTestData as first test cases of current suit.");
		}
		String studentPassword = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password").getAsString();
		_login(studentUserName, studentPassword);
	}

	@Step("Login to Smartwork5 application as a Student with given username,  Method: {method} ")
	public void LoginStudent(String studentUserName) throws Exception {
		if (studentUserName == null || studentUserName.isEmpty()) {
			throw new Exception("studentUserName is not valid. Please provide valid student user name.");
		}
		String studentPassword = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password").getAsString();
		_login(studentUserName, studentPassword);
	}

	private void _login(String studentUserName, String studentPassword) throws Exception, InterruptedException {
		driver = getDriver();
		SW5Login = new LoginPage();
		SW5Login.loginSW5(studentUserName, studentPassword);
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.invisibilityOf(SW5Login.Overlay));
		wait.until(ExpectedConditions.visibilityOf(SW5Login.gear_button_username));

		Assert.assertEquals(studentUserName, SW5Login.gear_button_username.getText());

		Thread.sleep(2000);

		// Navigate to Smartwork5 page
		SW5Login.clickSW5Icon();
		Thread.sleep(2000);
	}

	@Test(priority = 1)
	public void logoutSW5() throws Exception {
		SW5Login.logoutSW5();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
