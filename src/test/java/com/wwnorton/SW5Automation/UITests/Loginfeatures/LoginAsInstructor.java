package com.wwnorton.SW5Automation.UITests.Loginfeatures;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class LoginAsInstructor extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	String userName;
	String Password;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	// Read data from Json File
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();

	// Allure Annotations

	@Severity(SeverityLevel.NORMAL)
	@Description("Login as a Instructor")
	@Stories("Login to application as a Instructor ")
	@Test(priority = 0)
	public void loginInstructor() throws Exception {
		
		driver = getDriver();
		
		SW5Login = new LoginPage();
		userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
		SW5Login.loginSW5(userName, Password);
		
		wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.invisibilityOf(SW5Login.Overlay));
		wait.until(ExpectedConditions.visibilityOf(SW5Login.gear_button_username));
		String LoginMail = SW5Login.gear_button_username.getText().toLowerCase();
		SW5Login.verifySignin(userName, LoginMail);

		Thread.sleep(2000);
		
		// Navigate to Smartwork5 page
		SW5Login.clickSW5Icon();
		Thread.sleep(2000);
	}

	@Test(priority = 1)
	public void logoutInstructor() throws Exception {
		SW5Login.logoutSW5();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
