package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class InstructorPublishPremadeAssignmentCH22HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	AssignmentPage assignment;

	SW5DLPPage SW5DLPpage;
	
	final String ASSIGNMENT_TITLE = "CH22HW";
	public String GAUTime;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment Page - Verify that Instructor can edit GAU, Late Penalty and TimeZone post Student submissions - Limited Assignment.")
	@Stories("AS-20 - Edit Assignment: Update the assignment GAU, late penalty and Time Zone post Student submissions - Limited Assignment.")
	@Test
	public void editAssignment_InstructorPublishPremadeAssignmentCH22HW() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		//String studentSetId = "153638";
		//String assignmentTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH22HW);
		String assignmentTitle = SW5Constants.ASSIGNMENT_CH22HW;

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		LogUtil.log("Student ID is " + studentSetId);
		Thread.sleep(3000);
		
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		Thread.sleep(3000);

		String winHandleBefore = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
		Thread.sleep(2000);
		GAUTime = assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		assignment.LateworkCheckbox("1", "10");

		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		assignment.timeLimitOn();

		// Set the time limit 15 mins
		assignment.setTimeLimit("15");

		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");

		assignment.selectshowstudentscore("Percentages");

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		gContext = SeleniumTestsContextManager.getGlobalContext();
		gContext.setAttribute(SW5Constants.ASSIGNMENT_CH22HW, ASSIGNMENT_TITLE);
		gContext.setAttribute(SW5Constants.GAUTime_CH22HW, GAUTime);
		SeleniumTestsContextManager.setGlobalContext(gContext);
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.scrollToAssignmentTitle(assignmentTitle);
		SW5DLPpage.checkExtraCreditCheckBox(assignmentTitle);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		
		String extraCreditTitleSuffix_InstructorDLP_1 = SW5DLPpage.getExtraCreditOnInstructorDLP(assignmentTitle);
		Assert.assertEquals(extraCreditTitleSuffix_InstructorDLP_1, "(Extra Credit)");
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();
		
	}
		
		
	//End of Test cases
		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

	}
}
