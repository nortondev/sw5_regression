package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class InstructorPublishCustomAssignment_AllTypesOfQuestions extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Create and add All types of questions to the Custom Assignment")
	@Stories("AS-67 Create and add All types of questions to the Custom Assignment")
	@Test
	public void createAllTypesQuestions_CustomAssignment() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(1);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null) throw new Exception(
				"Please Add Test Case 'SetupTestData' as first test cases of current suit.");


		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		Thread.sleep(3000);
		String customAssignmentPrefix = "AllTypesOfQuestions_";
		
		int suffixCharCount = 4;
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());
		
		Thread.sleep(3000);
		assignment.adaptiveOff();
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		createAssignment.clickCreateNewQuestion();
		
		String FeedbackAnswer = "Correct Answer";
		String DefaultFeedbackAnswer = "Please Try Again";
		String metaData_DropDownTitle = "Determine Protien riched value";
		String dropDownQuestionTitle = "Please select the correct value";
		int answerOption_DropDownList = 1;
		
		String questionTitle_DropDownList = createAssignment.createNewCustomQuestion_MetaData(metaData_DropDownTitle);
		createAssignment.createNewCustomQuestion_DropDownList(dropDownQuestionTitle, FeedbackAnswer, DefaultFeedbackAnswer, answerOption_DropDownList);
		createAssignment.addNewCustomQuestion(questionTitle_DropDownList);
		createAssignment.yourCurrentAssignment();
		
		
		questionpage.addQuestions();
		createAssignment.clickCreateNewQuestion();
		
		String metaData_MultiChoiceTitle = "Add Two Numbers";
		String multiChoiceQuestionTitle = "Please select the correct option for addition of 10 and 20";
		int answerOption_MultiChoice = 3;
		
		String questionTitle_MultiChoice = createAssignment.createNewCustomQuestion_MetaData(metaData_MultiChoiceTitle);
		createAssignment.createNewCustomQuestion_MultiChoice(multiChoiceQuestionTitle, FeedbackAnswer, DefaultFeedbackAnswer, answerOption_MultiChoice);
		createAssignment.addNewCustomQuestion(questionTitle_MultiChoice);
		createAssignment.yourCurrentAssignment();
		
		
		questionpage.addQuestions();
		createAssignment.clickCreateNewQuestion();
		
		String metaData_MultiSelectTitle = "Determine Prime Numbers";
		String multiSelectQuestionTitle = "Please select Prime numbers from the following numbers";
		int answerOption_MultiSelect = 2;
		
		String questionTitle_MultiSelect = createAssignment.createNewCustomQuestion_MetaData(metaData_MultiSelectTitle);
		createAssignment.createNewCustomQuestion_MultiSelect(multiSelectQuestionTitle, FeedbackAnswer, DefaultFeedbackAnswer, answerOption_MultiSelect);
		createAssignment.addNewCustomQuestion(questionTitle_MultiSelect);
		createAssignment.yourCurrentAssignment();
		
		
		questionpage.addQuestions();
		createAssignment.clickCreateNewQuestion();
		
		String metaData_NumericEntryTitle = "Numbers Multiplication";
		String numericEntryQuestionTitle = "Enter the Answer for Multiplication";
		
		String questionTitle_NumericEntry = createAssignment.createNewCustomQuestion_MetaData(metaData_NumericEntryTitle);
		createAssignment.createNewCustomQuestion_NumericEntry(numericEntryQuestionTitle, FeedbackAnswer, DefaultFeedbackAnswer);
		createAssignment.addNewCustomQuestion(questionTitle_NumericEntry);
		createAssignment.yourCurrentAssignment();
		
		
		questionpage.addQuestions();
		createAssignment.clickCreateNewQuestion();
		
		String metaData_ShortAnswerTitle = "Determine  Short Answer";
		String shortAnswerQuestionTitle = "Enter the Short Answer for the following";
		
		String questionTitle_ShortAnswer = createAssignment.createNewCustomQuestion_MetaData(metaData_ShortAnswerTitle);
		createAssignment.createNewCustomQuestion_ShortAnswer(shortAnswerQuestionTitle, FeedbackAnswer, DefaultFeedbackAnswer);
		createAssignment.addNewCustomQuestion(questionTitle_ShortAnswer);
		createAssignment.yourCurrentAssignment();
		

		Thread.sleep(3000);
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());

		driver.close();
		driver.switchTo().window(winHandleBefore);

		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}
