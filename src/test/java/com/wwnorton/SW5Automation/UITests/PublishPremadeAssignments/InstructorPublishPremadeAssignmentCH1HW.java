package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH1HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	final String ASSIGNMENT_TITLE = "CH01HW";
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH1HW - As a student work with an assignment - GAU valid - student assignment & question player validation scenarios")
	@Stories("AS-43 As an instructor -  As an instructor use pre-made assignment with different settings combination")
	@Test
	public void updateSettingsAndPublish() throws Exception {
		
		driver = getDriver();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH1HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {

			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);			
			
			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(5000);
			SW5DLPpage.ClickAssignmentlink(ASSIGNMENT_TITLE);
			Thread.sleep(5000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();

			WebElement currentdate = driver.findElement(By.xpath(
					"//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();

			WebElement calendarInput = driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());

			//assignmentpage.setGAUTime("11:59 PM");
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();

			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());

			String assignmentInfoBoxText = "Grades are accepted until" + GetDate.getFormattedDate(0) + ", at 11:59 PM (Eastern Time)";

			SeleniumTestsContext gcontext = SeleniumTestsContextManager.getGlobalContext();
			gcontext.setAttribute(SW5Constants.ASSIGNMENT_CH1HW, ASSIGNMENT_TITLE);
			gcontext.setAttribute(SW5Constants.GAUDATE_CH1HW, GetDate.getCurrentDate2());
			gcontext.setAttribute(SW5Constants.ASSIGNMENT_INFOBOX_TEXT_CH1HW, assignmentInfoBoxText);
			SeleniumTestsContextManager.setGlobalContext(gcontext);

			// String winHandleAfter = driver.getWindowHandle();

			driver.close();
			driver.switchTo().window(winHandleBefore);
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
