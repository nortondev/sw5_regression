package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH15HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH15HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionPage;
	CreateCustomAssignment createCustomAssignment;
	TestHelper testHelper = new TestHelper();
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	AssignmentCH15HW assignmentCH15HW;
	InstructorPreviewQuestionPlayer instPreviewQuestionPlayer;
	StudentQuestionPlayer sqp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify the \"Show Feedback\" settings for practice questions.")
	@Stories(" CH15HW - Show Feedback: After Every attempt or After Attempt are exhausted , answer is correct or student has viewed the solution.")
	@Test()
	public void updateSettingsAndPublish() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH15HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH15HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());
			calendarInput.sendKeys(Keys.TAB);

			assignmentpage.setGAUTime("11:59 PM");
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.ShowAdditionalSettingsButton();
			assignmentpage.selectshowstudentscore("Percentages");
			
			assignmentpage
					.selectUngradedPractice("After attempts are exhausted, answer is correct, or student has viewed the solution");
			
			assignmentpage.selectShowFeedback("After every attempt");
			
			assignmentpage
					.selectshowsolution("Any time (students may 'give up' and view the solution)");
			
			Thread.sleep(3000);
			assignmentpage.selectAttempts("2");
			assignmentpage.selectGradePanelties("10%");
			assignmentpage.ApplyToAllButton.click();

			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			
			String parentWindow = driver.getWindowHandle();
			
			assignmentpage.clickPreviewButton();
			Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
					"Preview Logo is displayed?");
			
			
			// Instructor Preview Assignment Window
			testHelper.getInstructorPreviewAssignmentWindow(driver,
					winHandleBefore, parentWindow);
			
			instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
			
			sqp = new StudentQuestionPlayer();
			assignmentCH15HW = new AssignmentCH15HW();
			
			instPreviewAssignmentPage.beginAssignmentClick();

			// Provide Correct Answer and Assert Feedback and Practice button.
			assignmentCH15HW
			.question1MultipleParts_CorrectAnswer();
	
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			// Check Feedback and Practice Button
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
		
			sqp.clickFeedbackPractice();
			
			Thread.sleep(2000);
			Assert.assertEquals(sqp.getHeaderScore(), "12%");
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			
			assignmentCH15HW
			.question1MultipleParts_CorrectAnswer();
			
			sqp.clickCheckPractice();
			
			Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			
			sqp.clickFeedbackClose();
		
			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"1 OF 20 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			Assert.assertEquals(sqp.getHeaderScore(), "12%");
			
			// Provide Incorrect Answer - 1st attempt and Assert Feedback overlay.
			sqp.clickNextQuestion();
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			
			assignmentCH15HW
					.question6DragAndDrop_IncorrectAnswer();
			
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			// Check Feedback overlay with View Solution and Try Again Buttons.
			Thread.sleep(2000);
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(
					sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
					"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");
			
			Thread.sleep(2000);
			sqp.clickFeedbackViewSolution();
		
			Assert.assertEquals(sqp
					.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
					"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
			
			Assert.assertEquals(
					sqp.getConfirmViewSolutionMessageText().trim(),
					"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
					"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
		
			sqp.clickConfirmPopupViewSolutionButton();
			
			Thread.sleep(5000);
			Assert.assertEquals(sqp.getHeaderScore(), "12%");
			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"2 OF 20 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			
			sqp.clickPractice();
			
			Assert.assertEquals(sqp.getHeaderScore(), "12%");
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
		
			Thread.sleep(5000);
			
			// Do not enter answer and Click Check Practice.
			sqp.clickCheckPractice();
			
			Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
								"You have not answered all parts of this question. "
								+ "Are you sure you want to submit your answer?" );
						
			sqp.clickNoLetMeFinishButton();
			
			// Click Check Practice and Provide InCorrect Answer
			
			assignmentCH15HW.question6DragAndDrop_IncorrectAnswer();
			
			sqp.clickCheckPractice();
			
			Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			
			sqp.clickFeedbackPractice();
			Assert.assertEquals(sqp.getHeaderScore(), "12%");
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"2 OF 20 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			
			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH15HW,
					SW5Constants.ASSIGNMENT_CH15HW);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			driver.close();
			driver.switchTo().window(parentWindow);
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}