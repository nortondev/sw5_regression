package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH14HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH14HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	//final String ASSIGNMENT_TITLE = "CH14HW";

	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionPage;
	CreateCustomAssignment createCustomAssignment;
	TestHelper testHelper = new TestHelper();
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	AssignmentCH14HW assignmentCH14HW;
	InstructorPreviewQuestionPlayer instPreviewQuestionPlayer;
	StudentQuestionPlayer sqp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Ungraded Practice: After Attempts Are exhausted Show Solution: Any Time Show Feedback: Every time")
	@Stories(" CH14HW - Ungraded Practice: After Attempts Are exhausted Show Solution: Any Time Show Feedback: Every time")
	@Test()
	public void updateSettingsAndPublish() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		/*
		 * gContext.setAttribute(SW5Constants.ASSIGNMENT_CH14HW, "CH14HW");
		 * gContext.setAttribute(SW5Constants.STUDENT_SET_ID, "152373");
		 * gContext.setAttribute(SW5Constants.STUDENT_USER_NAME_1,
		 * "john_mercey-x7y7xncc1g@mailinator.com");
		 * SeleniumTestsContextManager.setGlobalContext(gContext);
		 */

		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH14HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH14HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());

			//assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.ShowAdditionalSettingsButton();
			assignmentpage.selectshowstudentscore("Percentages");
			assignmentpage
					.selectUngradedPractice("After attempts are exhausted, answer is correct, or student has viewed the solution");
			assignmentpage.selectShowFeedback("After every attempt");
			assignmentpage
					.selectshowsolution("Any time (students may 'give up' and view the solution)");

			questionPage = new Questions_Page();
			questionPage.addQuestions();
			questionPage.searchQuestions();

			createCustomAssignment = new CreateCustomAssignment();
			// createCustomAssignment.addCustomQuestions(SW5Constants.QUESTION_SET_2);
			createCustomAssignment.yourCurrentAssignment();
			Thread.sleep(5000);

			assignmentpage.selectAttempts("2");
			assignmentpage.selectGradePanelties("10%");
			assignmentpage.ApplyToAllButton.click();

			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			String parentWindow = driver.getWindowHandle();
			assignmentpage.clickPreviewButton();
			Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
					"Preview Logo is displayed?");
			// Instructor Preview Assignment Window
			testHelper.getInstructorPreviewAssignmentWindow(driver,
					winHandleBefore, parentWindow);
			instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
			assignmentCH14HW = new AssignmentCH14HW();
			instPreviewAssignmentPage.beginAssignmentClick();
			/* instPreviewAssignmentPage.waitForLoadPageElements(); */
			
			// Question #1 - 1st Attempt incorrect submit
			sqp = new StudentQuestionPlayer();
			Thread.sleep(2000);
			sqp.clickNextQuestion();
			
			Thread.sleep(2000);
			sqp.clickNextQuestion();
			assignmentCH14HW.selectQuestion3();

			// Check Feedback and Practice Button
			sqp.clickSubmitAnswer();
			sqp.clickFeedbackTryAgain();
			if (sqp.isSubmitAnswerButtonDisplayed() == false) {
				sqp.clickTryAgain();
			}

			Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "0%");
			
			
			// Question #1 - 2nd Attempt incorrect submit
			//assignmentCH14HW.selectQuestion3();
			sqp.clickSubmitAnswer();

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");

			sqp.clickFeedbackClose();

			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"1 OF 19 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");

			sqp.clickPractice();
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.getHeaderScore(), "0%");
			Thread.sleep(5000);
			// Click Check Practice and Provide Incorrect Answer
			sqp.clickCheckPractice();
			Assert.assertEquals(
					sqp.getAreYouSureSubmitAnswergetText().trim(),
					"You have not answered all parts of this question. Are you sure you want to submit your answer?");
			sqp.clickNoLetMeFinishButton();
			ReusableMethods.scrollIntoView(driver, sqp.PracticeAttemptsection);
			
			assignmentCH14HW.selectQuestion3();
			sqp.clickCheckPractice();
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			sqp.clickFeedbackClose();
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			sqp.clickPractice();
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Thread.sleep(5000);
			sqp.clickCheckPractice();
			Thread.sleep(2000);
			Assert.assertEquals(
					sqp.getAreYouSureSubmitAnswergetText().trim(),
					"You have not answered all parts of this question. Are you sure you want to submit your answer?");
			sqp.clickNoLetMeFinishButton();
			assignmentCH14HW.selectQuestion3();
			sqp.clickCheckPractice();
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");

			sqp.clickFeedbackClose();
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			
			
			// Provide Incorrect Answer and Assert View Solution
			sqp.clickNextQuestion();
			Thread.sleep(2000);
			sqp.clickNextQuestion();
			
			assignmentCH14HW.enterQuestion5();
			sqp.clickSubmitAnswer();
			
			Thread.sleep(2000);
			Assert.assertEquals(
					sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(),
					true,
					"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");
			sqp.clickFeedbackViewSolution();

			Assert.assertEquals(sqp
					.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(),
					true,
					"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
			Assert.assertEquals(
					sqp.getConfirmViewSolutionMessageText().trim(),
					"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
					"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");

			sqp.clickConfirmPopupViewSolutionButton();
			Thread.sleep(5000);
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");

			sqp.clickPractice();
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");

			sqp.clickNextQuestion();
			Thread.sleep(2000);
			sqp.clickNextQuestion();
			Thread.sleep(2000);
			assignmentCH14HW.enterQuestion7();
			sqp.clickSubmitAnswer();

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");

			sqp.clickFeedbackClose();
			sqp.clickPractice();
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");

			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH14HW,
					SW5Constants.ASSIGNMENT_CH14HW);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			driver.close();
			driver.switchTo().window(parentWindow);
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
