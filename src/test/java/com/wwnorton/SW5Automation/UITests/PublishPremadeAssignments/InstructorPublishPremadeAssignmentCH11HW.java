package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorPublishPremadeAssignmentCH11HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	final String ASSIGNMENT_TITLE = "CH11HW";

	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH11HW (Late Penalty Assignment)")
	@Stories("AS-54 As an instructor -  As an instructor use pre-made assignment with CH11HW (Late Penalty Assignment)")
	@Test
	public void updateSettingsAndPublish() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH11HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(ASSIGNMENT_TITLE);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			assignmentpage.enterGAUDate(GetDate.getCurrentDate());
			
			/*WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());*/
             //Set up the 30 Mins Time.
			assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			Thread.sleep(2000);
			assignmentpage.LateworkCheckbox("1", "10");
			assignmentpage.ShowAdditionalSettingsButton();
			String ShowStudentScore = "Points";
			assignmentpage.selectshowstudentscore(ShowStudentScore);
			// WebElement Attempts =
			// driver.findElement(By.xpath("//span[@class='ant-select-selection__rendered']"));
			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());

			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH11HW,
					ASSIGNMENT_TITLE);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			// String winHandleAfter = driver.getWindowHandle();

			driver.close();
			driver.switchTo().window(winHandleBefore);
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
