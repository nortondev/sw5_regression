package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH18HW_Exp extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	
	// Run this scripts before Instructor_VerifyExpiredAssignmentCH18HW.class file with GAU before 30 mins.
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-39 - As an Instructor publish CH18HW Assignment.")
	@Stories(" CH18HW - As an Instructor Verify the Other settings & Preview mode.")
	@Test()
	public void updateSettingsAndPublish_CH18HW() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH18HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH18HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());
			calendarInput.sendKeys(Keys.TAB);

			assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");

			assignmentpage.saveButton();
			
			Thread.sleep(5000);
			assignmentpage.publishButton();
			
			Thread.sleep(5000);
			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
