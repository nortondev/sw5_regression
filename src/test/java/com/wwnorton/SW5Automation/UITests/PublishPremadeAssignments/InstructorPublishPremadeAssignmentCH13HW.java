package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH13HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class InstructorPublishPremadeAssignmentCH13HW extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH13HW ch13HW;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH13HW (Show Student Score In: Percentages)")
	@Stories("AS-53 As an instructor -  As an instructor use pre-made assignment with CH13HW (Show Student Score In: Percentages) (Show Student Score In: Points")
	@Test
	public void updateSettingsAndPublish() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH13HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();

			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);

			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH13HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);

			assignmentpage.enterGAUDate(GetDate.getCurrentDate());
			Thread.sleep(3000);
			//assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.ShowAdditionalSettingsButton();

			assignmentpage.selectshowstudentscore("Percentages");

			//		assignmentpage.saveButton();
			//		Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());

			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH13HW, SW5Constants.ASSIGNMENT_CH13HW);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			String parentWindow = driver.getWindowHandle();
			assignmentpage.clickPreviewButton();
			Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
					"Preview Logo is displayed?");
			
			
			// Instructor Preview Assignment Window
			testHelper.getInstructorPreviewAssignmentWindow(driver,
					winHandleBefore, parentWindow);
			instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
			
			ch13HW = new AssignmentCH13HW();
			instPreviewAssignmentPage.beginAssignmentClick();
			
			Thread.sleep(5000);

			sqp = new StudentQuestionPlayer();

			ch13HW = new AssignmentCH13HW();
			
			sap = new StudentAssignmentPage();

			// Question #1
			if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
				Thread.sleep(2000);
			}

			Assert.assertEquals(sqp.getHeaderScore(), "-- %");

			// Navigate to Assignment Page
			sqp.clickHeaderTitle(sap);

			// Navigate to Question #1
			Thread.sleep(3000);
			sap.clickQuestion(1);
			if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
				Thread.sleep(2000);
			}

			ch13HW.question1DragAndDropCorrectAnswer();

			sqp.clickSubmitAnswer();

			if (sqp.isFeedbackOverlayDisplayed()) {
				sqp.clickFeedbackClose();
			}

			sqp.clickNextQuestion();
			sqp.clickNextQuestion();

			if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
				Thread.sleep(2000);
			}

			ch13HW.question3EnterCorrectAnswer();

			sqp.clickSubmitAnswer();

			if (sqp.isFeedbackOverlayDisplayed()) {
				sqp.clickFeedbackClose();
			}

			// Assert score on question player should be in %
			String sqpHeaderScore = sqp.getHeaderScore();
			Thread.sleep(5000);
			Assert.assertTrue(sqpHeaderScore.endsWith("%"), 
					"Instructor Preview player displays score in %?");
			Assert.assertEquals(sqpHeaderScore, "8%");
			Thread.sleep(5000);
			
			// Navigate to Assignment Page
			sqp.clickHeaderTitle(sap);

			// Assert score on student assignment should be in %
			String sapHeaderScore = sap.getHeaderScore();
			Assert.assertTrue(sapHeaderScore.endsWith("%"), 
					"Assignment page displays score in %?");
			Assert.assertEquals(sapHeaderScore, "8%");
			

			driver.close();
			driver.switchTo().window(parentWindow);
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
		}
	}

	@Step("CH13HW (Show Student Score In: Points,  Method: {method} ")
	public void updateSettingsShowStudentScoreInPoints(SW5DLPPage SW5DLPpage) throws Exception {
		
		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH13HW);
		
		String winHandleBefore = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		assignmentpage = new AssignmentPage();
		assignmentpage.editAssignmentbutton();

		assignmentpage.ShowAdditionalSettingsButton();

		ReusableMethods.scrollByY(driver, 200);

		assignmentpage.selectshowstudentscore("Points");

		assignmentpage.saveButton();
		Thread.sleep(10000);

		Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
		
		
		String parentWindow = driver.getWindowHandle();
		assignmentpage.clickPreviewButton();
		Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
				"Preview Logo is displayed?");
		
		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver,
				winHandleBefore, parentWindow);
		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		
		
		ch13HW = new AssignmentCH13HW();
		instPreviewAssignmentPage.beginAssignmentClick();
		
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		ch13HW = new AssignmentCH13HW();
		
		sap = new StudentAssignmentPage();

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "0 out of 25");

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		// Navigate to Question #10
		sap.clickQuestion(1);
		//sap.clickQuestion(9);
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		ch13HW.question1DragAndDropCorrectAnswer();

		sqp.clickSubmitAnswer();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		sqp.clickNextQuestion();
		sqp.clickNextQuestion();

		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		ch13HW.question3EnterCorrectAnswer();

		sqp.clickSubmitAnswer();

		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}

		// Assert score on Instructor Preview player should be in Points
		String sqpHeaderScore = sqp.getHeaderScore();
		Thread.sleep(5000);
		Assert.assertTrue(sqpHeaderScore.endsWith("%") == false, 
				"Instructor Preview player displays score in Points?");
		Assert.assertEquals(sqpHeaderScore, "2 out of 25");
		Thread.sleep(5000);
		
		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);

		// Assert score on Student Assignment should be in Points
		String sapHeaderScore = sap.getHeaderScore();
		Assert.assertTrue(sapHeaderScore.endsWith("%") == false, 
				"Assignment page displays score in Points?");
		Assert.assertEquals(sapHeaderScore, "2 out of 25");
		

		driver.close();
		driver.switchTo().window(parentWindow);
		
		driver.close();
		driver.switchTo().window(winHandleBefore);

	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
