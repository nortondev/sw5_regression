package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Listeners({ TestListener.class })
public class InstructorPublishPremadeAssignmentCH2HW extends PropertiesFile {

	final String ASSIGNMENT_TITLE = "CH02HW";
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH2HW (GAU Expiration with late work accepted)")
	@Stories("AS-45 As an instructor -  Set the GAU to +30 mins of current date & time with 2 days late work accepted and 10% penalty per day")
	@Test()
	public void updateSettingsAndPublish() throws Exception {
		
		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH2HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			
				String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
				if (studentSetID == null) {
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
			} 
		 
			
			//String studentSetID = "150168";
			
			new LoginAsInstructor().loginInstructor();
			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetID);
			LogUtil.log("Student ID is " + studentSetID);
			Thread.sleep(5000);
			SW5DLPpage.ClickAssignmentlink(ASSIGNMENT_TITLE);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();

			WebElement currentdate = driver.findElement(By.xpath(
					"//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			WebElement calendarInput = driver.findElement(By.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());
			assignmentpage.getSetGAUTime();
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			// assignmentpage.selecttimeZone("(GMT+11:00) Melbourne, Sydney Time");
			driver.findElement(By.xpath("//input[@data-passref='description']")).click();
			assignmentpage.LateworkCheckbox("2", "10");
			
			Thread.sleep(2000);
			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);
			// String winHandleAfter = driver.getWindowHandle();
			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH2HW, ASSIGNMENT_TITLE);
			SeleniumTestsContextManager.setGlobalContext(gContext);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);
			SW5DLPpage.logoutSW5();
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
