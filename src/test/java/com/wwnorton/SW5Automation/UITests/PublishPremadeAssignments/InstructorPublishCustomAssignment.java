package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorPublishCustomAssignment extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	AssignmentPage assignmentpage;
	Questions_Page questionpage;
	LoginAsInstructor logininst;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Instructor Publishes the Custom Assignment")
	@Stories("AS-59 As an instructor -  As an instructor publish the Custom Assignment")
	@Test
	public void updateSettingsAndPublishRemoveQuestionTest() throws Exception {
		updateSettingsAndPublishRemoveQuestion();
	}

	@Step("AS-59 As an instructor -  As an instructor publish the Custom Assignment")
	public String updateSettingsAndPublishRemoveQuestion() throws Exception {
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 3;
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_1, customAssignmentPrefix, suffixCharCount, 0, false);
	}

	@Step("AS-61 As an instructor publish the Custom Assignment to test Class Activity Report Default View,  Method: {method}")
	public String updateSettingsAndPublishCustomAssignmentClassActivityReportView() throws Exception {
		int suffixCharCount = 5;
		String customAssignmentPrefix = "CustomAssignment CAR ";
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_3, customAssignmentPrefix, suffixCharCount, 3, false);
	}

	@Step("AS-62 Custom Assignment - Student & Question Detail Overlay,  Method: {method}")
	public String updateSettingsAndPublishCustomAssignmentStudentAndQuestiondetailOverlay() throws Exception {
		int suffixCharCount = 5;
		String customAssignmentPrefix = "CustomAssignment CAR ";
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_3, customAssignmentPrefix, suffixCharCount, 3, true);
	}

	@Step("AS-64 Custom Assignment - Tutorial Question Type,  Method: {method}")
	public String updateSettingsAndPublishCustomAssignmentTutorialQuestion() throws Exception {
		int suffixCharCount = 5;
		String customAssignmentPrefix = "CustomAssignment TutorialQuestion ";
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_4, customAssignmentPrefix, suffixCharCount, 0, false);
	}

	@Step("AS-65 As an instructor publish the Custom Assignment to test Class Activity Report Default View,  Method: {method}")
	public String updateSettingsAndPublishOpenResponseQuestion() throws Exception {
		int suffixCharCount = 5;
		String customAssignmentPrefix = "Custom OpenResponse ";
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_5, customAssignmentPrefix, suffixCharCount, 3, false);
	}
	
	@Step("AS-70 As an instructor publish the Custom Assignment to test Instructor Preview,  Method: {method}")
	public String updateSettingsAndPublishCustomAssignmentInstructorPreview() throws Exception {
		int suffixCharCount = 5;
		String customAssignmentPrefix = "CustomAssignment InstPreview ";
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_3, customAssignmentPrefix, suffixCharCount, 3, false);
	}
	
	@Step("SW5-2897 As an instructor -  publish the Custom Assignment and GAU Time Zone")
	public String updateSettingsAndPublishGAUTimeZone() throws Exception {
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 3;
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_2, customAssignmentPrefix, suffixCharCount, 0, false);
	}
	
	@Step("SW5-3844  As an instructor publish the Custom Assignment and Delete the Questions")
	public String publishCustomAssignmentandRemoveQuestion() throws Exception {
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 3;
		return updateSettingsAndPublish(SW5Constants.QUESTION_SET_1, customAssignmentPrefix, suffixCharCount, 0, false);
	}

	private String updateSettingsAndPublish(String questionsSetName, String customAssignmentPrefix, int suffixCharCount,
			int pointsPerQuestion, boolean lateWorkCheck) throws Exception {
		driver = getDriver();
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		// LogUtil.log("Student ID is " + studentSetId);
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		String winHandleBefore = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		assignmentpage = new AssignmentPage();
		assignmentpage.editAssignmentbutton();
		assignmentpage.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignmentpage.adaptiveOff();

		assignmentpage.enterGAUDate(GetDate.getCurrentDate());
        assignmentpage.gettimeZone();
		assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
		
		if (lateWorkCheck == true) {
			assignmentpage.LateworkCheckbox("1", "10");
		}

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		Thread.sleep(5000);
		
		//String xpathvariable =("//div[@role='toolbar']/span")+"[3]";
		WebElement ele = driver.findElement(By.xpath("//button[@class='btn-submit-s btn-primary lato-regular-14'][span[contains(text(),'ADD QUESTIONS')]]"));
		ReusableMethods.scrollIntoView(driver, ele);
		Thread.sleep(5000);
		
		if (pointsPerQuestion > 0) {
			assignmentpage.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']/span[contains(text(),'APPLY TO ALL')]"));
			assignmentpage.ApplyToAllButton.click();

			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		assignmentpage.ShowAdditionalSettingsButton();
		Thread.sleep(2000);
		assignmentpage.selectshowstudentscore("Percentages");

		createAssignment.getTotalPoints();
		String assignmentTitle = createAssignment.getAssignmentTitle();
		System.out.println(assignmentTitle);

		assignmentpage.publishButton();
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();
		return assignmentTitle;
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
