package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH16HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH16HW extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	Questions_Page questionPage;
	CreateCustomAssignment createCustomAssignment;
	TestHelper testHelper = new TestHelper();
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	AssignmentCH16HW assignmentCH16HW;
	InstructorPreviewQuestionPlayer instPreviewQuestionPlayer;
	StudentQuestionPlayer sqp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify the \"Show Solution\" settings for practice questions.")
	@Stories(" CH16HW - Show Solution: Any time or Only after a correct answer or all attempts are exhausted.")
	@Test()
	public void updateSettingsAndPublish() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH16HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH16HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);
			WebElement currentdate = driver
					.findElement(By
							.xpath("//div[@class='timeZoneSpan']/span[@class='ant-calendar-picker']/span/input[@placeholder='mm/dd/yyyy']"));
			currentdate.click();
			Thread.sleep(2000);
			WebElement calendarInput = driver.findElement(By
					.xpath("//input[@class='ant-calendar-input  ']"));
			calendarInput.sendKeys(GetDate.getCurrentDate());
			calendarInput.sendKeys(Keys.TAB);

			assignmentpage.setGAUTime("11:59 PM");
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.ShowAdditionalSettingsButton();
			assignmentpage.selectshowstudentscore("Percentages");
			
			assignmentpage
					.selectUngradedPractice("After attempts are exhausted, answer is correct, or student has viewed the solution");
			
			assignmentpage.selectShowFeedback("After attempts are exhausted, answer is correct, or student has viewed the solution");
			
			assignmentpage
					.selectshowsolution("Any time (students may 'give up' and view the solution)");
			
			Thread.sleep(3000);
			assignmentpage.selectAttempts("2");
			assignmentpage.selectGradePanelties("10%");
			assignmentpage.ApplyToAllButton.click();

			assignmentpage.saveButton();
			Thread.sleep(5000);
			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());
			String parentWindow = driver.getWindowHandle();
			assignmentpage.clickPreviewButton();
			Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
					"Preview Logo is displayed?");
			
			
			// Instructor Preview Assignment Window
			testHelper.getInstructorPreviewAssignmentWindow(driver,
					winHandleBefore, parentWindow);
			instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
			
			assignmentCH16HW = new AssignmentCH16HW();
			instPreviewAssignmentPage.beginAssignmentClick();
			
			sqp = new StudentQuestionPlayer();
			
			// Move to Question# 2
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			
			// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
			assignmentCH16HW.question2_EnterIncorrectAnswer("5");

			// Check View Solution and Try Again Buttons
			
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			Thread.sleep(1000);
			Assert.assertEquals(
					sqp.isViewSolutionButtonDisplayed(),
					true,
					"View Solution Button is displayed");
			
			Assert.assertEquals(
					sqp.isTryAgainButtonDisplayed(),
					true,
					"Try Again Button is displayed");
			
			Thread.sleep(1000);
			sqp.clickViewSolution();
			
			// After View Solution, Check Practice Button.
			Thread.sleep(2000);
			Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
					"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
			Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
					"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
					"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
			
			sqp.clickConfirmPopupViewSolutionButton();
			Thread.sleep(5000);
			
			Assert.assertEquals(sqp.getHeaderScore(), "0%");
			
			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"1 OF 19 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			
			
			// Click on Practice button and validate Check Practice button.
			sqp.clickPractice();
			
			Thread.sleep(2000);
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			Assert.assertEquals(sqp.getHeaderScore(), "0%");
			Thread.sleep(5000);
			
			
			// Do not enter answer and Click Check Practice.
			sqp.clickCheckPractice();
			
			Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
								"You have not answered all parts of this question. Are you sure you want to submit your answer?" );
						
			sqp.clickNoLetMeFinishButton();
						
			ReusableMethods.scrollIntoView(driver, sqp.Solutionsection);
			//WebElement solutionEle =driver.findElement(By.xpath("//a[@class='collapseA']/div[@class='judge-title'][contains(text(),'Solution ')]"));		
			//solutionEle.click();
			
			WebElement txtSolution = driver.findElement(By.id("wafer_ps_number37921620550cd9d0f1ccb4b38dfecf49186b4546db05151__solution"));
			String solutionAns = txtSolution.getAttribute("value");
			
			
			// Click Check Practice and Provide Correct Answer
			
			assignmentCH16HW.question2_EnterIncorrectAnswer(solutionAns);
			
			sqp.clickCheckPractice();
			
			Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			
			sqp.clickFeedbackClose();
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");
			
			sqp.clickPractice();
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");

			
			// Move to Question# 1
			sqp.clickPreviousQuestion();
			
			
			// Provide Correct Answer - 1st attempt - Assert Feedback overlay.
			assignmentCH16HW
					.question1DragAndDrop_CorrectAnswer();
			
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			// Check Feedback overlay and Practice Button.
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "3%");

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			
			sqp.clickFeedbackPractice();
			
			Thread.sleep(2000);
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, "Solution View is displayed");
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			
			assignmentCH16HW
			.question1DragAndDrop_CorrectAnswer();
			
			sqp.clickCheckPractice();
			
			Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Practice\" button?");
			
			sqp.clickFeedbackClose();
			
			Assert.assertEquals(sqp.getQuestionFooterLeftText(),
					"2 OF 19 QUESTIONS COMPLETED");

			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
					"Practice button is displayed in the footer?");

			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			Assert.assertEquals(sqp.getHeaderScore(), "3%");
			
			sqp.clickPractice();
			Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
			Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
					"CHECK PRACTICE button is displayed in the footer?");
			
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
					"QUESTION COMPLETED");
			
			Thread.sleep(5000);
			
			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH16HW,
					SW5Constants.ASSIGNMENT_CH16HW);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			driver.close();
			driver.switchTo().window(parentWindow);
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}