package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class InstructorPublishPremadeAssignmentCH3HW extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	CreateNewStudentSet createStudentset;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignmentpage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("CH3HW (Assignment with GAU & Set the GAU to +60 mins of current date & time)")
	@Stories("AS-46 As an instructor -  As an instructor use pre-made assignment with Time Limit Assignment")
	@Test(priority = 0)
	public void updateSettingsAndPublish() throws Exception {
		
		driver = getDriver();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String assignmnetTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH3HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student Set ID is " + studentSetId);
			Thread.sleep(5000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH3HW);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();

			assignmentpage.editAssignmentbutton();
			Thread.sleep(3000);

			assignmentpage.enterGAUDate(GetDate.getCurrentDate());

			String time2 = null;
			assignmentpage.selectGAUTime(time2);
			assignmentpage.selecttimeZone("(GMT-05:00) Eastern Time");
			assignmentpage.ShowAdditionalSettingsButton();

			assignmentpage.timeLimitOn();

			// Set the time limit 05 mins
			assignmentpage.setTimeLimit("5");

			assignmentpage.selectshowstudentscore("Percentages");

//		assignmentpage.saveButton();
//		Thread.sleep(5000);

			assignmentpage.publishButton();
			Thread.sleep(5000);

			Assert.assertTrue(assignmentpage.isUnpublishButtonDisplayed());

			gContext = SeleniumTestsContextManager.getGlobalContext();
			gContext.setAttribute(SW5Constants.ASSIGNMENT_CH3HW, SW5Constants.ASSIGNMENT_CH3HW);
			SeleniumTestsContextManager.setGlobalContext(gContext);

			// String winHandleAfter = driver.getWindowHandle();

			driver.close();
			driver.switchTo().window(winHandleBefore);
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPpage.logoutSW5();
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
