package com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorUpdateGAUTimeZone extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	AssignmentPage assignment;
	Questions_Page questionpage;
	LoginAsInstructor logininst;
	InstructorPublishCustomAssignment instCustAssignment;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Instructor updates the Time Zone for Custom Assignments")
	@Stories("SW5-2897 As an instructor - updates the TimeZone and Verify the Settings")
	@Test
	public void updateGAUTimeZoneByInstrcutorTest() throws Exception {
		
		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		instCustAssignment = new InstructorPublishCustomAssignment();
		

		String assignmentTitle = instCustAssignment
				.updateSettingsAndPublishGAUTimeZone();
		
		//String timeZone =  instCustAssignment.assignmentpage.gettimeZone();
		// INSTRUCTOR
	
				String studentSetId = (String) gContext
						.getAttribute(SW5Constants.STUDENT_SET_ID);

				if (studentSetId == null)
					throw new Exception(
							"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
				new LoginAsInstructor().loginInstructor();

				
				SW5DLPpage = new SW5DLPPage();
				//String parentWindow = driver.getWindowHandle();
				String winHandleBefore = driver.getWindowHandle();
				SW5DLPpage.selectByPartOfVisibleText(studentSetId);
				SW5DLPpage.ClickAssignmentlink(assignmentTitle);
				testHelper.getInstructorAssignmentWindow(driver);
				assignment = new AssignmentPage();
				assignment.editAssignmentbutton();
				WebDriverWait wait = new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
				Thread.sleep(5000);
				Assert.assertEquals(assignment.gettimeZone(),"(GMT-04:00) Eastern Time");
				assignment.selecttimeZone("(GMT-06:00) Central Time");
				assignment.saveButton();
				Thread.sleep(5000);
				driver.close();
				driver.switchTo().window(winHandleBefore);
				createAssignment = new CreateCustomAssignment();
				createAssignment.createAssignment();
				String winHandleBefore1 = driver.getWindowHandle();
				testHelper.getInstructorAssignmentWindow(driver);
				assignment.editAssignmentbutton();
				assignment.AssignmentName
						.sendKeys("customAssignment" + GetRandomId.randomAlphaNumeric(2).toLowerCase());

				Thread.sleep(3000);
				assignment.adaptiveOff();

				assignment.enterGAUDate(GetDate.getCurrentDate());
				Thread.sleep(3000);
				Assert.assertEquals(assignment.gettimeZone(),"(GMT-06:00) Central Time");
				driver.close();
				driver.switchTo().window(winHandleBefore1);
				createAssignment.createFullyAdaptiveAssignment();
				String winHandleBefore2 = driver.getWindowHandle();
				testHelper.getInstructorAssignmentWindow(driver);
				assignment.editAssignmentbutton();
				Thread.sleep(3000);
				Assert.assertEquals(assignment.gettimeZone(),"(GMT-06:00) Central Time");
				driver.close();
				driver.switchTo().window(winHandleBefore2);
				CreateNewStudentSet CSS = new CreateNewStudentSet();
				Thread.sleep(5000);
				CSS.SignIn_Register();
				CSS.ManageStudentSetlink();
				CSS.CreateStudentSetButton();
				CSS.clickNextButton_CreateNewStudentset();
				CSS.createStudentset_information();
				String studentSetID = CSS.createStudentset_ID();
	            LogUtil.log("The StudentSetID", studentSetID);
				ManageStudentSetsPage managestudentsetpage = new ManageStudentSetsPage();
				managestudentsetpage.clickcloselink();
				SW5DLPpage.selectByPartOfVisibleText(studentSetID);
				createAssignment.createAssignment();
				String winHandleBefore3 = driver.getWindowHandle();
				testHelper.getInstructorAssignmentWindow(driver);
				assignment.editAssignmentbutton();
				Thread.sleep(3000);
				Assert.assertEquals(assignment.gettimeZone(),"(GMT-06:00) Central Time");
				driver.close();
				driver.switchTo().window(winHandleBefore3);
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}


}
