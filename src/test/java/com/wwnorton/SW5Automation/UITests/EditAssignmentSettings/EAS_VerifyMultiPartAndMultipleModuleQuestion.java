package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyMultiPartAndMultipleModuleQuestion extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit assignment: add multi-part and multiple module question to the assignment")
	@Stories("AS-223 - Edit assignment: add multi-part and multiple module question to the assignment")
	@Test
	public void editAssignment_VerifyMultiPartAndMultipleModuleQuestion() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(1);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String questionsID = "DoNotUseAutomation_MultiPart_MultiStep";
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;
			
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
        assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");
		
		assignment.selectshowstudentscore("Percentages");
	
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addQuestionsWithQuestionID(questionsID);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		
		
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		String parentWindow1 = driver.getWindowHandle();
		assignment.clickPreviewButton();
		Assert.assertTrue(assignment.isPreviewButtonDisplayed(),
				"Preview Logo is displayed?");
		
		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver,
				winHandleBefore1, parentWindow1);
		
		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		
		custassignment = new CustomAssignmentAnswers();
		instPreviewAssignmentPage.beginAssignmentClick();
		
		sqp = new StudentQuestionPlayer();
		
		Assert.assertEquals(sqp.multipleQuestions_SQP("Part 1"), true);
		Assert.assertEquals(sqp.multipleQuestions_SQP("Part 2"), true);
		Assert.assertEquals(sqp.multipleQuestions_SQP("Part 3"), true);
		

		driver.close();
		driver.switchTo().window(parentWindow1);
		
		Thread.sleep(3000);
		driver.close();
		
		driver.switchTo().window(winHandleBefore1);
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();
		

		// Login as Student 1

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow2 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow2);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			
			Thread.sleep(2000);
			sap.beginAssignmentClick();
			
			
			custassignment.CustomMultiParts_MultiModuleQuestionAnswers();
			sqp.clickSubmitAnswer();
			//sqp.clickYesFinishButton();
			sqp.clickFeedbackClose();
			Assert.assertEquals(sqp.getHeaderScore(), "50%");
			
            Thread.sleep(2000);
			/*wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//div[@class='head-title fl']/a")));*/
			driver.findElement(By.xpath("//div[@class='head-title fl']/a"))
					.click();
			
			//Assert Header Score on Student Assignment page.
			Thread.sleep(2000);
			String studentHeaderGradeScore = sap.getHeaderScore();
			Assert.assertEquals("50%", studentHeaderGradeScore, 
					"The Header Score on Student Assignment Page.");
			
			driver.close();
			driver.switchTo().window(parentWindow2);
			
			//Assert Student Grade in Student DLP.
			Thread.sleep(2000);
			SW5DLPpage = new SW5DLPPage();
			String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student01_Grade, "50%", 
					"Student average score on Student DLP \"50%\"?");
			
			SW5DLPstudent.logoutSmartwork5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}
