package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH16HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class EAS_ShowSolutionSettings_PracticeQuestions extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH16HW assignment;
	Questions_Page questionPage;
	CreateCustomAssignment createCustomAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit Assignment: Verify the \"Show Solution\" settings for practice questions.")
	@Stories("AS-30 Show Solution: Any time or Only after a correct answer or all attempts are exhausted.")
	@Test
	public void editAssignmentShowSolutionSettings_PracticeQuestions() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
				testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);

				// Publish assignment if assignmentTitle not found in global context
				String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH16HW);

				// Login as Student Account 1
				SeleniumTestsContext gContext = SeleniumTestsContextManager
						.getGlobalContext();
				
				String studentUserName1 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
				
				new LoginAsStudent().LoginStudent(studentUserName1);

				// Select mentioned Assignment
				SW5DLPstudent = new SW5DLPStudent();
				SW5DLPstudent.SelectAssignment(assignmentTitle);

				// Assignment Window
				String studentWindow1 = driver.getWindowHandle();
				testHelper.getStudentAssignmentWindow(driver, studentWindow1);

				sap = new StudentAssignmentPage();

				sap.beginAssignmentClick();
				

				sqp = new StudentQuestionPlayer();
				sqp.waitForLoadPageElements();
				assignment = new AssignmentCH16HW();
				
				// Move to Question# 2
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				
				// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
				
				assignment
						.question2_EnterIncorrectAnswer("5");

				sqp = new StudentQuestionPlayer();
				
				Thread.sleep(2000);
				sqp.clickSubmitAnswer();
				
				// Check View Solution and Try Again Buttons
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(),
						true,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(),
						true,
						"Try Again Button is displayed");
				
				Thread.sleep(1000);
				sqp.clickViewSolution();
				
				// After View Solution, Check Practice Button.
				Thread.sleep(2000);
				Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
						"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
				Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
						"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
						"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
				
				sqp.clickConfirmPopupViewSolutionButton();
				Thread.sleep(5000);
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 19 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				// Click on Practice button and validate Check Practice button.
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				Thread.sleep(5000);
				
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
							
				ReusableMethods.scrollIntoView(driver, sqp.Solutionsection);
				
				WebElement txtSolution1 = driver.findElement(By.id("wafer_ps_number37921620550cd9d0f1ccb4b38dfecf49186b4546db05151__solution"));
				String solutionAns1 = txtSolution1.getAttribute("value");
				
				
				// Click Check Practice and Provide Correct Answer
				
				assignment.question2_EnterIncorrectAnswer(solutionAns1);
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");

				
				// Move to Question# 1
				sqp.clickPreviousQuestion();
				
				// Provide Correct Answer - 1st attempt - Assert Feedback overlay.
				assignment
						.question1DragAndDrop_CorrectAnswer();
				
				Thread.sleep(2000);
				sqp.clickSubmitAnswer();
				
				// Check Feedback overlay and Practice Button.
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.getHeaderScore(), "3%");

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackPractice();
				
				Thread.sleep(2000);
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1DragAndDrop_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 19 QUESTIONS COMPLETED");

				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");

				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				Assert.assertEquals(sqp.getHeaderScore(), "3%");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				driver.close();
				driver.switchTo().window(studentWindow1);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPstudent.logoutSmartwork5();
				
				
				//Login as an Instructor and change the Feedback settings to Option 2
				
				new LoginAsInstructor().loginInstructor();
				
				String studentSetId = (String) gContext
						.getAttribute(SW5Constants.STUDENT_SET_ID);

				SW5DLPpage = new SW5DLPPage();
				SW5DLPpage.selectByPartOfVisibleText(studentSetId);
				LogUtil.log("Student ID is " + studentSetId);
				
				Thread.sleep(3000);
				SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH16HW);
				
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				testHelper.getInstructorAssignmentWindow(driver);

				assignmentpage = new AssignmentPage();
				assignmentpage.editAssignmentbutton();
				
				Thread.sleep(3000);
				assignmentpage.ShowAdditionalSettingsButton();
				assignmentpage.selectshowsolution("Only after a correct answer, or all attempts are exhausted");
				
				Thread.sleep(2000);
				assignmentpage.saveButton();
				
				String parentWindow = driver.getWindowHandle();
				assignmentpage.clickPreviewButton();
				Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
						"Preview Logo is displayed?");
				
				
				// Instructor Preview Assignment Window
				testHelper.getInstructorPreviewAssignmentWindow(driver,
						winHandleBefore, parentWindow);
				instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
				
				assignment = new AssignmentCH16HW();
				instPreviewAssignmentPage.beginAssignmentClick();
				
				// Move to Question# 2
				Thread.sleep(1000);
				sqp.clickNextQuestion(); 
				
				// Provide Incorrect Answer - 1st attempt and Assert Try Again button.
				assignment.question2_EnterIncorrectAnswer("5");

				sqp = new StudentQuestionPlayer();
				sqp.clickSubmitAnswer();
				
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(),
						false,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(),
						true,
						"Try Again Button is displayed");
				
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						false,
						"Solution View is displayed");
				
				
				Thread.sleep(1000);
				sqp.clickTryAgain();
				
				Thread.sleep(1000);
				sqp.clickSubmitAnswer();
				
				// Attempts Exhausted - Check Show Feedback and Practice Button
				Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				// Assert View Solution and Practice button is displayed.
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						true,
						"Solution View is displayed");
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 19 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				// Click on Practice button and validate Check Practice button.
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				Thread.sleep(5000);
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
							
				ReusableMethods.scrollIntoView(driver, sqp.Solutionsection);
				//WebElement solutionView1 =driver.findElement(By.xpath("//a[@class='collapseA']/div[@class='judge-title'][contains(text(),'Solution ')]"));		
				//solutionView1.click();
				
				WebElement txtViewSolution2 = driver.findElement(By.id("wafer_ps_number37921620550cd9d0f1ccb4b38dfecf49186b4546db05151__solution"));
				String viewSolutionAns2 = txtViewSolution2.getAttribute("value");
				
				
				// Click Check Practice and Provide Correct Answer
				
				assignment.question2_EnterIncorrectAnswer(viewSolutionAns2);
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				

				// Move to Question# 1
				sqp.clickPreviousQuestion();
				
				
				// Provide correct Answer - 1st attempt and Assert Feedback Overlay and Practice button.
				assignment
						.question1DragAndDrop_CorrectAnswer();

				sqp.clickSubmitAnswer();
				
				// 1st attempt - Correct, Check Feedback Overlay and Practice Button.
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.getHeaderScore(), "3%");

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				// Assert View Solution and Practice button is displayed.
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						true,
						"Solution View is displayed");

				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 19 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Thread.sleep(2000);
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1DragAndDrop_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getHeaderScore(), "3%");
				
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 19 QUESTIONS COMPLETED");

				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");

				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				
				driver.close();
				driver.switchTo().window(parentWindow);
				
				driver.close();
				driver.switchTo().window(winHandleBefore);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPpage.logoutSW5();
				
				
				// Login as Student Account 2
				
				String studentUserName2 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
				
				new LoginAsStudent().LoginStudent(studentUserName2);

				// Select mentioned Assignment
				SW5DLPstudent = new SW5DLPStudent();
				SW5DLPstudent.SelectAssignment(assignmentTitle);

				// Assignment Window
				String studentWindow2 = driver.getWindowHandle();
				testHelper.getStudentAssignmentWindow(driver, studentWindow2);

				sap = new StudentAssignmentPage();

				sap.beginAssignmentClick();
				

				sqp = new StudentQuestionPlayer();
				sqp.waitForLoadPageElements();
				assignment = new AssignmentCH16HW();
				
				// Move to Question# 2
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				
				// Provide Incorrect Answer - 1st attempt and Assert Try Again button.
				assignment.question2_EnterIncorrectAnswer("5");
				
				sqp = new StudentQuestionPlayer();
				sqp.clickSubmitAnswer();
				
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(),
						false,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(),
						true,
						"Try Again Button is displayed");
				
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						false,
						"Solution View is displayed");
				
				
				Thread.sleep(1000);
				sqp.clickTryAgain();
				
				Thread.sleep(1000);
				sqp.clickSubmitAnswer();
				
				// Check Show Feedback and Practice Button
				Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				// Assert View Solution and Practice button is displayed.
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						true,
						"Solution View is displayed");
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 19 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				// Click on Practice button and validate Check Practice button.
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				Assert.assertEquals(sqp.getHeaderScore(), "0%");
				Thread.sleep(5000);
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
							
				ReusableMethods.scrollIntoView(driver, sqp.Solutionsection);
				//WebElement solutionView2 =driver.findElement(By.xpath("//a[@class='collapseA']/div[@class='judge-title'][contains(text(),'Solution ')]"));		
				//solutionView2.click();
				
				WebElement txtViewSolution3 = driver.findElement(By.id("wafer_ps_number37921620550cd9d0f1ccb4b38dfecf49186b4546db05151__solution"));
				String viewSolutionAns3 = txtViewSolution3.getAttribute("value");

				// Click Check Practice and Provide Correct Answer
				
				assignment.question2_EnterIncorrectAnswer(viewSolutionAns3);
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				
				// Move to Question# 1
				sqp.clickPreviousQuestion();
				
				
				// Provide correct Answer - 1st attempt and Assert Feedback Overlay and Practice button.
				assignment
						.question1DragAndDrop_CorrectAnswer();

				sqp.clickSubmitAnswer();
				
				// 1st attempt - Correct, Check Show Feedback Overlay and Practice Button.
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.getHeaderScore(), "3%");

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				// Assert View Solution and Practice button is displayed.
				Assert.assertEquals(
						sqp.isViewSolutionDisplayed(),
						true,
						"Solution View is displayed");

				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 19 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Thread.sleep(2000);
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1DragAndDrop_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));

				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getHeaderScore(), "3%");
				
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 19 QUESTIONS COMPLETED");

				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");

				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				
				driver.close();
				driver.switchTo().window(studentWindow2);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPstudent.logoutSmartwork5();
				
				
	}
	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
