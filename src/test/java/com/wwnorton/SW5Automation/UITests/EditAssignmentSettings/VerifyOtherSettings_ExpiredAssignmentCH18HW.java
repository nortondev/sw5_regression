package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class VerifyOtherSettings_ExpiredAssignmentCH18HW extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	
	// Run this scripts after InstructorPublishPremadeAssignmentCH18HW.class file with GAU after 30 mins.
	
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-39 - As an Instructor Verify the Other settings & Preview mode.")
	@Stories(" CH18HW - As an Instructor Verify the Other settings & Preview mode.")
	@Test()
	public void verifyOtherSettings_ExpiredAssignmentCH18HW() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		 
		String assignmnetTitle = (String) gContext
				.getAttribute(SW5Constants.ASSIGNMENT_CH18HW);
		if (assignmnetTitle == null || assignmnetTitle.isEmpty()) {
			
			String studentSetId = (String) gContext
					.getAttribute(SW5Constants.STUDENT_SET_ID);

			//String studentSetId = "153071";
					
			if (studentSetId == null)
				throw new Exception(
						"Please Add Test Case 'SetupTestData' as first test cases of current suit.");

			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);
			SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH18HW);
			Thread.sleep(3000);

			String winHandleBefore = driver.getWindowHandle();
			testHelper.getInstructorAssignmentWindow(driver);

			assignmentpage = new AssignmentPage();
			assignmentpage.editAssignmentbutton();
			
			Thread.sleep(2000);
			assignmentpage.ShowAdditionalSettingsButton();
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkTimeLimitSettings(), false);
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkShowEbookLinkSettings(), true);
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkShowPeriodicTableSettings(), true);
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.checkRandomizeQuestionSettings(), false);
			
			Thread.sleep(2000);
			Assert.assertEquals(assignmentpage.isQuestionSettingsEnabled(), true);
			
			
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);

			SW5DLPpage.logoutSW5();
			
			// Login as Student Account 1
			
			String studentUserName1 = (String) gContext.getAttribute(
					SW5Constants.STUDENT_USER_NAME_1);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(SW5Constants.ASSIGNMENT_CH18HW);

			// Assignment Window
			String studentWindow = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, studentWindow);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();

			Assert.assertEquals(sap.isReviewAssignmentButtonDisplayed(), true);

			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(studentWindow);

			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);

			SW5DLPstudent.logoutSmartwork5();
			
		}
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
