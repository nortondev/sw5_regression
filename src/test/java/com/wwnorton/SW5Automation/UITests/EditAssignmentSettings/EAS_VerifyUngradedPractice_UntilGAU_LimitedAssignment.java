package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;


import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH20HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyUngradedPractice_UntilGAU_LimitedAssignment extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	AssignmentCH20HW assignmentCH20HW;

	SW5DLPPage SW5DLPpage;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As a Student Verify the \"Ungraded Practice\" for time limited assignment - Until GAU.")
	@Stories("AS-21 Edit Assignment: \"Ungraded Practice\"  (Time Limit Assignment) - Until GAU.")
	@Test(priority = 0)
	
	public void ungradedPractice_TimeLimitAssignment_UntilGAU(ITestContext context) throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String assignmentTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH20HW);
		//String assignmentTitle =SW5Constants.ASSIGNMENT_CH20HW;

		// String assignmentTitle = SW5Constants.ASSIGNMENT_CH20HW;
		
		 if (studentSetId == null)
			 throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
	
		
		
		// Login as Student

		String studentUserName1 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		 //String studentUserName1 = "john_mercey-g510knt6yf@mailinator.com";
				
		new LoginAsStudent().LoginStudent(studentUserName1);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.verifyInitialAllQuestionPoints());
		Assert.assertTrue(sap.verifyInitialAllQuestionStatus());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:10:00", "Initial timer value is 10 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(3000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickNoIamNotReadyToBeginButton();

		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:10:00", "Initial timer value is 10 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(3000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickYesIamReadyToBeginButton();
		Thread.sleep(3000);

		sqp = new StudentQuestionPlayer();
		//Thread.sleep(2000);

		String timerText = sqp.getTimerText();

		Assert.assertTrue(timerText.compareTo("00:10:00") != 0, "Is timer started?");
		Thread.sleep(5000);

		Assert.assertTrue(sqp.getTimerText().compareTo(timerText) != 0, "Is timer coninues?");

		assignmentCH20HW = new AssignmentCH20HW();

		// START - useCase: Submit assignment when time limit is "ON"
		// to make sure the submission went through.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "0 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/20");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #1 - First Attempt Incorrect Answer
		assignmentCH20HW.question1_DragNDrop_IncorrectAnswers();

		sqp.clickSubmitAnswer();

		// Check Try Again Button at the footer.
		Thread.sleep(1000);
		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
				"Try Again Button is displayed in the footer?");

		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), true, 
				"View Solution Button is displayed in the footer?");

		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		// Question #1 - Second Attempt InCorrect Answer

		Thread.sleep(1000);
		sqp.clickTryAgain();

		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true, 
				"Submit Answer button is displayed in the footer?");

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		// Check Feedback Overlay and Practice button is displayed.
		Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is displayed in Feedback overlay");
		
		Thread.sleep(2000);
		sqp.clickFeedbackClose();
		

		//Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "0%");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "1 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");


		// Navigate to Next Question

		sqp.clickNextQuestion();
		Thread.sleep(1000);
		sqp.clickNextQuestion();

		// Question #3 - First Attempt Correct Answer

		assignmentCH20HW.question3_CorrectAnswer();

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		// Check Feedback Overlay with Practice button is displayed.
		Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is displayed in Feedback overlay");

		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "2 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");
		
		
		// Navigate to Next Question

		sqp.clickNextQuestion();
		Thread.sleep(1000);
		sqp.clickNextQuestion();

		// Question #5 - Give up the question and View Solution. 

		sqp.clickSubmitAnswer();
		sqp.clickYesFinishButton();

		Thread.sleep(2000);
		// Check Try Again and View Solution Buttons at the Footer.
		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
				"Try Again Button is displayed");

		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), true, 
				"View Solution Button is displayed");

		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");
		
		sqp.clickViewSolution();
		
		Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
				"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
		Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
				"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
				"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
		
		sqp.clickConfirmPopupViewSolutionButton();
		
		Thread.sleep(2000);
		//Assert.assertEquals(sqp.getHeaderScore(), "3%");
		Assert.assertEquals(sqp.getHeaderScore(), "5%");
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "3 OF 20 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false,
				"Practice button is displayed in the footer?");
		
		

		// Hold submission and allow the time limit to expire to validate the time limit
		// expire message validation comes up and system should not allow you to submit
		// answer.
		int sleepTimeInMiliseconds2 = 25000;
		while (true) {
			Thread.sleep(15000);
			sleepTimeInMiliseconds2 += 25000;
			// Wait till timer expired or 10 mins
			if (sqp.getTimerText().compareTo("00:00:00") == 0 || sleepTimeInMiliseconds2 >= 50000)
				break;
		}

		Thread.sleep(15000);

		Assert.assertEquals(sqp.getAlertMessageText().trim(), 
				"Time has expired and no further attempts are allowed!",
				"Alert popup with 'Time has expired and no further attempts are allowed!' "
				+ "message is displayed?");

		sqp.clickTimerExpiredAlertClose();

		Thread.sleep(2000);
		Assert.assertTrue(sap.isReviewAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");

		sap.reviewAssignmentClick();
		Thread.sleep(10000);

		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		sqp.clickNextQuestion();
		sqp.clickNextQuestion();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		sqp.clickNextQuestion();
		sqp.clickNextQuestion();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		sqp.clickNextQuestion();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", "Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		Thread.sleep(2000);
		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
		// END

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}