package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH14HW;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class EAS_UPracticeAfterAttemptsexhaust_ShowSolAnyTime_FeedbackEverytime extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH14HW assignment;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("Ungraded Practice: After Attempts Are exhausted Show Solution: Any Time Show Feedback: Every time")
	@Stories("Ungraded Practice: After Attempts Are exhausted Show Solution: Any Time Show Feedback: Every time")
	@Test
	public void editAssignmentUPracticeAfterAttemptsexhaust_ShowSolAnyTime_FeedbackEverytime() throws Exception {
		
		driver = getDriver();
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
				
		testHelper.checkPrerequisiteInGlobalContext();

		// Publish assignment if assignmentTitle not found in global context
		String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH14HW);

		// Login as Student
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();
		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		sqp.waitForLoadPageElements();
		assignment = new AssignmentCH14HW();

		// Question #1 - 1st Attempt incorrect submit
		Thread.sleep(2000);
		sqp.clickNextQuestion();

		Thread.sleep(2000);
		sqp.clickNextQuestion();
		assignment.selectQuestion3();

		// Check Feedback and Practice Button
		sqp = new StudentQuestionPlayer();
		sqp.clickSubmitAnswer();
		sqp.clickFeedbackTryAgain();
		if (sqp.isSubmitAnswerButtonDisplayed() == false) {
			sqp.clickTryAgain();
		}

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "0%");

		// Question #1 - 2nd Attempt incorrect submit
		// assignment.selectQuestion3();
		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Practice\" button?");

		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "1 OF 19 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");

		sqp.clickPractice();

		Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.getHeaderScore(), "0%");

		sqp.clickCheckPractice();
		Thread.sleep(5000);
		Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
				"You have not answered all parts of this question. Are you sure you want to submit your answer?");
		sqp.clickNoLetMeFinishButton();

		ReusableMethods.scrollIntoView(driver, sqp.PracticeAttemptsection);
		assignment.selectQuestion3();
		sqp.clickCheckPractice();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Practice\" button?");
		sqp.clickFeedbackClose();
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");
		sqp.clickPractice();
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		Thread.sleep(5000);
		sqp.clickCheckPractice();
		Thread.sleep(2000);
		Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
				"You have not answered all parts of this question. Are you sure you want to submit your answer?");
		sqp.clickNoLetMeFinishButton();

		Thread.sleep(5000);
		assignment.selectQuestion3();
		sqp.clickCheckPractice();
		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Practice\" button?");

		sqp.clickFeedbackClose();
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");

		// Provide Incorrect Answer and Assert View Solution
		sqp.clickNextQuestion();
		Thread.sleep(2000);
		sqp.clickNextQuestion();

		assignment.enterQuestion5();
		sqp.clickSubmitAnswer();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
				"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");
		sqp.clickFeedbackViewSolution();

		Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
				"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
		Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
				"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
				"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");

		sqp.clickConfirmPopupViewSolutionButton();
		Thread.sleep(5000);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true, "Practice button is displayed in the footer?");

		sqp.clickPractice();
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		sqp.clickNextQuestion();
		Thread.sleep(2000);
		sqp.clickNextQuestion();
		Thread.sleep(2000);
		assignment.enterQuestion7();
		sqp.clickSubmitAnswer();

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Practice\" button?");

		sqp.clickFeedbackClose();
		sqp.clickPractice();
		Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
				"CHECK PRACTICE button is displayed in the footer?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
	}
	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
