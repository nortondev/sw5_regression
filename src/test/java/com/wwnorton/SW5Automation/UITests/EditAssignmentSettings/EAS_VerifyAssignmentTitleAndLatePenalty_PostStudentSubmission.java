package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH19HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyAssignmentTitleAndLatePenalty_PostStudentSubmission extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH19HW assignmentCH19HW;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;


	SW5DLPPage SW5DLPpage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	ManageStudentSetsPage managestudentsetpage;
	UpdateStudentSet updatestudentset;
	CreateNewStudentSet createStudentset;
	
	String studentHeaderGradeScore;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment Page - Verify that Instructor can edit assignment post Student submissions.")
	@Stories("AS-17 - Edit Assignment: Verify the updated assignment title, late penalty post Student submissions.")
	@Test
	public void editAssignmentTitleAndLatePenalty_PostStudentSubmission() throws Exception {
		
		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String assignmentTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH19HW);
		String GAUTime = (String) gContext.getAttribute(SW5Constants.GAUTime_CH19HW);
		
		//String studentSetId = "154194";
		//String assignmentTitle = SW5Constants.ASSIGNMENT_CH19HW;
		//String GAUTime = "01:30 PM";
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
	


		// Login as Student

		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String StudentName = testHelper.getStudentUserNameInUppercase(studentUserName).trim();

		//String studentUserName = "john_mercey-5szpggbrsy@mailinator.com";
		//String StudentName = "MERCEY-5SZPGGBRSY, JOHN";
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
				
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);
			
		sap = new StudentAssignmentPage();
		
		// Assert Assignment GAU Info on Student Assignment page.
		Thread.sleep(2000);
		String LatePenaltyText = "Your work is overdue! Late penalties now apply (10% per day) "
				+ "and you have 1 day left to complete the assignment";
		String strLatePenaltyAlert = sap.getLatePenaltyAlert();
		Assert.assertTrue(LatePenaltyText.trim().contains(strLatePenaltyAlert.trim()),
				"Assignment OverDue message is displayed correctly with Late Penalty information?");

		
		sqp = new StudentQuestionPlayer();
		assignmentCH19HW = new AssignmentCH19HW();
		
		sap.beginAssignmentClick();
		
		// Assert that Late Penalty Information is displayed for Student.
		Thread.sleep(5000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), true);

		// Provide Correct Answer and Assert Feedback and Practice button.
		assignmentCH19HW.question1_CorrectAnswer();

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackClose();
		
		Thread.sleep(1000);
		sqp.clickHeaderTitle(sap);

		// Assert Student score on Student Assignment page.
		String student01_sapHeaderScore = sap.getHeaderScore();
		Assert.assertEquals(student01_sapHeaderScore, "24%",
					"Student average score on Custome assignment page \"24%\"?");
		
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		SW5DLPpage = new SW5DLPPage();
		String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student01_Grade, "24%", 
				"Student average score on Student DLP \"24%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		Thread.sleep(2000);	
		String gauDate_Student = SW5DLPpage.getSystemGAUDate_Student(0);

		String strGAUDate_Student = SW5DLPpage.getGAUDate_Student(assignmentTitle);
		Assert.assertEquals(strGAUDate_Student, gauDate_Student);
		
		SW5DLPstudent.logoutSmartwork5();

		
			
		// Login as INSTRUCTOR to update Assignment Title and Late Penalty.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore3 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		//As an instructor check the Class Average and Submitted Grades on Instructor DLP page.
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", submittedGrades);
		
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("24%", averageGrade);
		
		// Assert GAU Date associated to Assignment - CH19HW on Instructor DLP.
		Thread.sleep(2000);
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, 0);

		DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yy");
		String date1 = dateFormat1.format(cal1.getTime());

		String gauDate_Instructor = date1 + " " + GAUTime;
		String strGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(assignmentTitle);
		Assert.assertEquals(strGAUDate_Instructor, gauDate_Instructor);
		
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);
		
		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("24%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());
		
		
		studentDetailOverlay = new StudentDetailOverlayPage();
		
		//Student Grade details 
		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);

		Thread.sleep(5000);
		
		//Assert Overall Score in Student Details Overlay.
		String strOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(strOverallScore, "24%");
		
		String strMain = GAUTime;
	    String[] arrSplit = strMain.split(" ");
	    String  updatedGAUTime = arrSplit[0]+arrSplit[1];
		
		// Assert GAU Text on Student Details Overlay.
		String assignmentInfoBoxText = "Grades were accepted until " + GetDate.getFormattedDate(0) + 
				" at " + updatedGAUTime + " (Eastern Time)";
		String GAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(GAUText.trim(), assignmentInfoBoxText.trim());
		
		studentDetailOverlay.clickCloseStudentDetailOverlay();
		
		// Assert GAU Text on CAR page.
		Thread.sleep(2000);
		String CurrentDate_CAR = GetDate.getFormattedDate(0);
		String GAUText_CAR = "Grades were accepted for this assignment until " 
		+ CurrentDate_CAR + ", at " + GAUTime + " (Eastern Time)"; 
		String strGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(strGAU.trim(), GAUText_CAR.trim());
		
		
		// Assert GAU Text on Instructor Assignment page.
		classActivityReport.clickSmartwork5LinkfromCarPage();
		
		// Assert Assignment GAU Info on Instructor Assignment page.
		Thread.sleep(2000);
		String CurrentDate_Instructor = GetDate.getFormattedDate(0);
		String LatePenaltyText_Instructor = "Grades were accepted until " + CurrentDate_Instructor 
				+ ", at " + GAUTime + " (Eastern Time). Late work is  accepted 1 day after the GAU at "
				+ "10% penalty per day";
		String strLatePenaltyAlert_Instructor = sap.getGAUUntilAlert_Instructor();
		Assert.assertTrue(LatePenaltyText_Instructor.trim().contains(strLatePenaltyAlert_Instructor.trim()), 
				"Assignment GAU message is displayed correctly with Late Penalty information?");
		
		assignment = new AssignmentPage();
		String headerScore = assignment.getHeaderScore();
		Assert.assertEquals("24%", headerScore, 
				"The Header Score on Instructor Assignment Page.");

		driver.close();
		driver.switchTo().window(winHandleBefore3);

		// /As an instructor log into and Edit Assignment Title and Penalty period.
		String winHandleBefore4 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		
		assignment.editAssignmentbutton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		
		assignment.AssignmentName.clear();
		
		String customAssignmentPrefix = "CH19HW ";
		int suffixCharCount = 5;
		
		Thread.sleep(2000);
		assignment.AssignmentName
		.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());
		
		Thread.sleep(2000);
		String ExtendedGAUTime = assignment.getSetGAUTime();
		String strMain_Extended = ExtendedGAUTime;
	    String[] arrSplit_Extended = strMain_Extended.split(" ");
	    String  updatedExtendedGAUTime = arrSplit_Extended[0]+arrSplit_Extended[1];
		
		Thread.sleep(2000);
		assignment.LateworkCheckbox("1", "50");
		
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		
		Thread.sleep(2000);
		createAssignment = new CreateCustomAssignment();
		String updatedAssignmentTitle = createAssignment.AssignmentName.getAttribute("value");
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		
		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();


		//Login as Student User
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String strStudent_Grade = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals(strStudent_Grade, "26%", 
				"Student average score on Student DLP \"26%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		String updatedGAUDate_Student = SW5DLPpage.getSystemGAUDate_Student(0);

		String updatedStrGAUDate_Student = SW5DLPpage.getGAUDate_Student(updatedAssignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Student, updatedGAUDate_Student);
		
		SW5DLPstudent.SelectAssignment(updatedAssignmentTitle);

		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);
		
		sap = new StudentAssignmentPage();

		//Assert Header Score on Student Assignment page.
		studentHeaderGradeScore = sap.getHeaderScore();
		Assert.assertEquals("26%", studentHeaderGradeScore, 
				"The Header Score on Student Assignment Page.");
		LogUtil.log(studentHeaderGradeScore);
		
		// Assert Assignment GAU Info on Student Assignment page.
		Thread.sleep(2000);
		String CurrentDate_Student01 = GetDate.getFormattedDate(0);
		
		String updatedLatePenaltyText = "Your assignment is due on " + CurrentDate_Student01
				+ ", at " + ExtendedGAUTime +" (Eastern Time). Late work is accepted 1 day  after the due date, "
				+ "at 50% penalty per day";
		
		String updatedStrLatePenaltyAlert = sap.getGAUUntilAlert_Student();
		Assert.assertTrue(updatedLatePenaltyText.trim().contains(updatedStrLatePenaltyAlert.trim()),
				"Assignment GAU message is displayed correctly with Late Penalty information?");

		sqp = new StudentQuestionPlayer();
		assignmentCH19HW = new AssignmentCH19HW();
		
		sap.clickQuestion(14);
		
		//Assert that Late Penalty Information is displayed for Student.
		Thread.sleep(3000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), false);
		
		// Provide Correct Answer and Assert Feedback and Practice button.
		assignmentCH19HW.question14_PartialCorrectAnswer();

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackClose();
		
		Thread.sleep(1000);
		sqp.clickHeaderTitle(sap);

		// Assert Student score on Student Assignment page.
		String student02_sapHeaderScore = sap.getHeaderScore();
		Assert.assertEquals(student02_sapHeaderScore, "29%",
					"Student average score on Custome assignment page \"29%\"?");

		driver.close();
		driver.switchTo().window(parentWindow2);
		
		Thread.sleep(2000);
		String updatedStudent_Grade = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals(updatedStudent_Grade, "29%", 
				"Student average score on Student DLP \"29%\"?");
		
		Thread.sleep(2000);
		SW5DLPstudent.logoutSmartwork5();
		
		
		// Login as INSTRUCTOR again to validate updated Assignment Title and Late Penalty.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore6 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		// As an instructor check the Class Average and Submitted Grades on Instructor
		// DLP page.
		String updatedSubmittedGrades = SW5DLPpage.getSubmittedGrades(updatedAssignmentTitle);
		Assert.assertEquals("1", updatedSubmittedGrades);

		String averageGradeafterAssignmentUpdate = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals("29%", averageGradeafterAssignmentUpdate);

		// Assert GAU Date associated to Assignment on Instructor DLP.
		Thread.sleep(2000);
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, 0);

		DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yy");
		String date2 = dateFormat2.format(cal2.getTime());
		
		String updatedGAUDate_Instructor = date2 + " " + ExtendedGAUTime;
		String updatedStrGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(updatedAssignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Instructor, updatedGAUDate_Instructor);

		Thread.sleep(5000);
		SW5DLPpage.clickReportsButton(updatedAssignmentTitle);

		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);
		classActivityReport = new ClassActivityReportPage();

		Thread.sleep(5000);
		Assert.assertEquals("29%", classActivityReport.getCARAverageScoreOnAssignment(), 
				"The Average Score");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		studentDetailOverlay = new StudentDetailOverlayPage();

		// Studunet1 grade after Assignment update.

		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);
		Thread.sleep(5000);

		// Assert Overall Score in Student Details Overlay.
		String updatedStrOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(updatedStrOverallScore, "29%");
	    
		// Assert GAU Text on Student Details Overlay.
		String updatedAssignmentInfoBoxText = "Grades are accepted until " + GetDate.getFormattedDate(0)
				+ " at " + updatedExtendedGAUTime + " (Eastern Time)";
		String updatedGAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(updatedGAUText.trim(), updatedAssignmentInfoBoxText.trim());

		studentDetailOverlay.clickCloseStudentDetailOverlay();

		// Assert GAU Text on CAR page.
		Thread.sleep(2000);
		String UpdatedGAUText_CAR = "Grades are accepted for this assignment until " 
		+ CurrentDate_CAR + ", at " + ExtendedGAUTime + " (Eastern Time)";
		String updatedStrGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(updatedStrGAU.trim(), UpdatedGAUText_CAR.trim());

		// Assert GAU Text on Instructor Assignment page.
		classActivityReport.clickSmartwork5LinkfromCarPage();

		// Assert Assignment GAU Info on Instructor Assignment page.
		Thread.sleep(2000);
		String updatedLatePenaltyText_Instructor = "Grades are accepted until " 
		+ CurrentDate_Instructor + ", at " + ExtendedGAUTime + " (Eastern Time). Late work is  accepted 1 day "
				+ "after the GAU at 50% penalty per day";
		String updatedStrLatePenaltyAlert_Instructor = sap.getGAUUntilAlert_Instructor();
		Assert.assertTrue(
				updatedLatePenaltyText_Instructor.trim().contains(updatedStrLatePenaltyAlert_Instructor.trim()),
				"Assignment GAU message is displayed correctly with Late Penalty information?");

		String headerScoreafterAssignmentUpdate = assignment.getHeaderScore();
		Assert.assertEquals("29%", headerScoreafterAssignmentUpdate, 
				"The Header Score on Instructor Assignment Page.");

		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore6);

		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}
