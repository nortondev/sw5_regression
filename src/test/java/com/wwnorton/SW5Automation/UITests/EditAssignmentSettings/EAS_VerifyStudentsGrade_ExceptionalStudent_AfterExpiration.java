package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet5;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyStudentsGrade_ExceptionalStudent_AfterExpiration extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentQuestionSet5 custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As an Exceptional Student, submit Custom Assignment after expiration.")
	@Stories("AS-24 - Edit Assignment: Verify Student Grades for Exceptional Student post students submission.")
	@Test
	public void editAssignment_ExceptionalStudentSubmit_CustomAssignment() throws Exception {
		
		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String assignmentTitle = (String) gContext.getAttribute(SW5Constants.CUSTOM_ASSIGNMENT_GAUException);
		
		//String studentSetId = "153763";
		//String assignmentTitle = "StudentGAU_CustomAssignment h0m9h";
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
	
		
		
		// Login as INSTRUCTOR again to validate average students grades.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		wait = new WebDriverWait(driver, 15);
		TimeUnit.SECONDS.sleep(15);

		// As an instructor check the Class Average and Submitted Grades on Instructor
		// DLP page.
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", submittedGrades);

		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(averageGrade, "56%", 
				"Student average score on Student DLP \"56%\"?");

		SW5DLPpage.logoutSW5();
		
		
		// Login as Student 2 and validate Student Grade as Hard Zero .
		
		String studentUserName2 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		//String studentUserName2 = "john_mercey-3pbqtgnzon@mailinator.com";
		new LoginAsStudent().LoginStudent(studentUserName2);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		// Assert updated Exception Student Grade in Student DLP.
		Thread.sleep(2000);
		String student02_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student02_Grade, "—", 
				"Student average score on Student DLP \"—\"?");

		SW5DLPstudent.logoutSmartwork5();
		
		
		
		// Login as Student 3, submit the assignment and validate Student Grade as Non Hard Zero .

		String studentUserName3 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_3);
		
		//String studentUserName3 = "john_mercey-utjahm5z38@mailinator.com";
				
		new LoginAsStudent().LoginStudent(studentUserName3);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentQuestionSet5();
		
		Thread.sleep(2000);
		sap.beginAssignmentClick();
		Thread.sleep(1000);
		sqp.clickNextQuestion();
		
		// Provide Correct Answer and Assert Feedback and Practice button.
		custassignment.question2Answers();
		
		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackClose();
		
		Thread.sleep(1000);
		sqp.clickHeaderTitle(sap);
		
		// Assert Student score on Student Assignment page.
		String student03_sapHeaderScore = sap.getHeaderScore();
		Assert.assertEquals(student03_sapHeaderScore, "33%", 
				"Student average score on Custome assignment page \"33%\"?");
		
        Thread.sleep(1000);
        driver.close();
		driver.switchTo().window(parentWindow1);
		
		Thread.sleep(2000);
		String student03_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student03_Grade, "33%", 
				"Student average score on Student DLP \"33%\"?");
		
		SW5DLPstudent.logoutSmartwork5();
		
		
		// Login as INSTRUCTOR again to validate average students grades.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		wait = new WebDriverWait(driver, 15);
		TimeUnit.SECONDS.sleep(15);

		// As an instructor check the Class Average and Submitted Grades on Instructor
		// DLP page.
		String updatedSubmittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("2", updatedSubmittedGrades);

		String updatedAverageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(updatedAverageGrade, "45%", 
				"Student average score on Student DLP \"45%\"?");

		SW5DLPpage.logoutSW5();
		

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
