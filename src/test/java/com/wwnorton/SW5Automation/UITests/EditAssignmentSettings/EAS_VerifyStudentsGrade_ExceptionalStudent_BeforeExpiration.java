package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet5;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyStudentsGrade_ExceptionalStudent_BeforeExpiration extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentQuestionSet5 custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As an Instructor, Apply GAU+Late Penalty exception with Custom Assignment.")
	@Stories("AS-24 - Edit Assignment: Verify Student Grades for Exceptional Student post students submission.")
	@Test
	public void editAssignment_InstructorPublishCustomAssignment_GAUStudentException() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(3);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		String customAssignmentPrefix = "StudentGAU_CustomAssignment ";
		int suffixCharCount = 5;
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		assignment.LateworkCheckbox("1", "50");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");
		
		assignment.selectshowstudentscore("Percentages");
	
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		gContext = SeleniumTestsContextManager.getGlobalContext();
		gContext.setAttribute(SW5Constants.CUSTOM_ASSIGNMENT_GAUException,
				assignmentTitle);
		SeleniumTestsContextManager.setGlobalContext(gContext);
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();
		

		
		// Login as Student 1 and submit the assignment.

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		String studentUserName3 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_3);
		
		String studentName3 = testHelper.getStudentUserNameInUppercase(studentUserName3);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow1 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow1);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentQuestionSet5();
			
			Thread.sleep(2000);
			sap.beginAssignmentClick();
			Thread.sleep(1000);
			sqp.clickNextQuestion();
			
			// Provide Correct Answer and Assert Feedback and Practice button.
			custassignment.question2Answers();
			
			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();

			// Navigate to Next Question
			
			sqp.clickNextQuestion();
			
			custassignment.question3Answers();
			
			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();
			
			Thread.sleep(1000);
			sqp.clickHeaderTitle(sap);
			
			// Assert Student score on Student Assignment page.
			String student01_sapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student01_sapHeaderScore, "56%", 
					"Student average score on Custome assignment page \"56%\"?");
			
            Thread.sleep(1000);
            driver.close();
			driver.switchTo().window(parentWindow1);
			
			Thread.sleep(2000);
			String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student01_Grade, "56%", 
					"Student average score on Student DLP \"56%\"?");
			
			SW5DLPstudent.logoutSmartwork5();
			
			
			// INSTRUCTOR
			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();

			SW5DLPpage.clickReportsButton(assignmentTitle);
			String winHandleBefore2 = driver.getWindowHandle();

			// Instructor Assignment Window
			testHelper.getInstructorClassActivityReportWindow(driver);
			classActivityReport = new ClassActivityReportPage();
			
			Assert.assertEquals("56%",
					classActivityReport.getCARAverageScoreOnAssignment(),
					"The Average Score");
			
			classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(studentName3);

			studentDetailOverlay = new StudentDetailOverlayPage();
			Thread.sleep(5000);
			
			//Assert Overall Score in Student Details Overlay before Extension.
			String strOverallScore = studentDetailOverlay.getOverallScore();
			Assert.assertEquals(strOverallScore, "-");
			
			studentDetailOverlay.clickEditButton();
			
			Thread.sleep(2000);
			studentDetailOverlay.setGAUTime("11:59 PM");

			studentDetailOverlay.clickSaveButton();
			Thread.sleep(2000);
			studentDetailOverlay.clickCloseStudentDetailOverlay();
			
			Thread.sleep(2000);
			Assert.assertEquals("56%",
					classActivityReport.getCARAverageScoreOnAssignment(),
					"The Average Score");
			
			// Assert GAU Text on Instructor Assignment page.
			classActivityReport.clickSmartwork5LinkfromCarPage();

			driver.close();
			driver.switchTo().window(winHandleBefore2);
			
			Thread.sleep(2000);
			String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(averageGrade, "56%", 
					"Student average score on Student DLP \"56%\"?");

			SW5DLPpage.logoutSW5();
			
		}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}