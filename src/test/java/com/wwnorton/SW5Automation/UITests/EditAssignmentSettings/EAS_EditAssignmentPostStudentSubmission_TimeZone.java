package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_EditAssignmentPostStudentSubmission_TimeZone extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	ManageStudentSetsPage managestudentsetpage;
	UpdateStudentSet updatestudentset;
	CreateNewStudentSet createStudentset;
	
	String studentHeaderGradeScore;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment Page - Verify that Instructor can edit Assignment - Time Zone post Student submissions.")
	@Stories("AS-19 - Edit Assignment: Update the assignment Time Zone post Student submissions.")
	@Test
	public void editAssignmentTimeZone_PostStudentSubmission() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		String questionsSetName = SW5Constants.QUESTION_SET_3;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");

		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();

		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");

		assignment.selectshowstudentscore("Percentages");

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		String assignmentTitle = createAssignment.getAssignmentTitle();
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();


		// Login as Student

		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String StudentName = testHelper.getStudentUserNameInUppercase(studentUserName).trim();

		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
				
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
			
		// Assert Assignment Time Zone Info on Student Assignment page.
		String assignmentTimeZoneInfo_SAP = "Grades are accepted until " + GetDate.getFormattedDate(0) + 
				", at 11:59 PM (Eastern Time)";

		Thread.sleep(5000);
		String strAssignmentInfoBox_SAP = sap.getAssignmentTimeZoneInfo_SAP();
		
		Assert.assertEquals(strAssignmentInfoBox_SAP.trim(), assignmentTimeZoneInfo_SAP.trim(),
				"Assignment Time Zone information is displayed correctly?");

		sap.beginAssignmentClick();

		// Assert that Time Zone is NOT displayed in SQP for Student.
		Thread.sleep(5000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), false);

		customQuestionsAnswerAssignmentStudent();

		Thread.sleep(2000);
		//wait.until(ExpectedConditions.elementToBeClickable(
		//		By.xpath("//div[@class='head-title fl']/a")));
		driver.findElement(By.xpath("//div[@class='head-title fl']/a")).click();
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student01_Grade, "80%", "Student average score on Student DLP \"80%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		Thread.sleep(2000);	
		String gauDate_Student = SW5DLPpage.getSystemGAUDate_Student(0);

		String strGAUDate_Student = SW5DLPpage.getGAUDate_Student(assignmentTitle);
		Assert.assertEquals(strGAUDate_Student, gauDate_Student);
		
		SW5DLPstudent.logoutSmartwork5();

		
			
		// Login as INSTRUCTOR to update Assignment Title and Late Penalty.
		
		new LoginAsInstructor().loginInstructor();

		
		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore2 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		//As an instructor check the Class Average and Submitted Grades on Instructor DLP page.
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", submittedGrades);
		
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("80%", averageGrade);
		
		// Assert GAU Date associated to Custom Assignment on Instructor DLP.
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, 0);

		DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yy");
		String date1 = dateFormat1.format(cal1.getTime());

		String gauDate_Instructor = date1 + " 11:59 PM";
		Thread.sleep(3000);
		String strGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(assignmentTitle);
		Assert.assertEquals(strGAUDate_Instructor, gauDate_Instructor);
		
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);
		
		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("80%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());
		
		
		studentDetailOverlay = new StudentDetailOverlayPage();
		
		//Student Grade details 
		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);

		Thread.sleep(5000);
		
		//Assert Overall Score in Student Details Overlay.
		String strOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(strOverallScore, "80%");
		
		// Assert GAU Text on Student Details Overlay.
		String assignmentInfoBoxText = "Grades are accepted until " + GetDate.getFormattedDate(0) + 
				" at 11:59PM (Eastern Time)";
		Thread.sleep(2000);
		String GAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(GAUText.trim(), assignmentInfoBoxText.trim());
		
		studentDetailOverlay.clickCloseStudentDetailOverlay();
		
		// Assert GAU Text on CAR page.
		String CurrentDate_CAR = GetDate.getFormattedDate(0);
		String GAUText_CAR = "Grades are accepted for this assignment until " 
		+ CurrentDate_CAR + ", at 11:59 PM (Eastern Time)"; 
		Thread.sleep(2000);
		String strGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(strGAU.trim(), GAUText_CAR.trim());
		
		// Assert GAU Text on Instructor Assignment page.
		
		assignment.returnToAssignmentList.click();
		
		// Assert Assignment Time Zone Information on Instructor Assignment page.
		String CurrentDate_Instructor = GetDate.getFormattedDate(0);
		String assignmentTimeZoneText_Instructor = "Grades are accepted until " + CurrentDate_Instructor 
				+ ", at 11:59 PM (Eastern Time)";
		Thread.sleep(2000);
		String strTimeZoneInfo_Instructor = sap.getAssignmentTimeZoneInfo_Instructor();
		
		Assert.assertTrue(assignmentTimeZoneText_Instructor.trim().contains(strTimeZoneInfo_Instructor.trim()), 
				"Assignment Time Zone information is displayed correctly?");
		
		String headerScore = assignment.getHeaderScore();
		Assert.assertEquals("80%", headerScore, 
				"The Header Score on Instructor Assignment Page.");

		driver.close();
		driver.switchTo().window(winHandleBefore2);

		// /As an instructor log into and Edit Assignment Time Zone.
		String winHandleBefore3 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		
		assignment.editAssignmentbutton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		
		Thread.sleep(3000);
		assignment.enterGAUDate(GetDate.addDays(0));

		Assert.assertEquals(assignment.gettimeZone(),"(GMT-04:00) Eastern Time");
		assignment.selecttimeZone("(GMT-06:00) Central Time");
		
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		
		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();


		//Login as Student User
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String strStudent_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(strStudent_Grade, "80%", 
				"Student average score on Student DLP \"80%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		String updatedGAUDate_Student = SW5DLPpage.getSystemGAUDate_Student(0);

		String updatedStrGAUDate_Student = SW5DLPpage.getGAUDate_Student(assignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Student, updatedGAUDate_Student);
		
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);
		
		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();

		//Assert Header Score on Student Assignment page.
		studentHeaderGradeScore = sap.getHeaderScore();
		Assert.assertEquals("80%", studentHeaderGradeScore, 
				"The Header Score on Student Assignment Page.");
		LogUtil.log(studentHeaderGradeScore);
		
		// Assert Assignment Updated Time Zone Info on Student Assignment page.
				String updatedAssignmentTimeZoneInfo_SAP = "Grades are accepted until " + GetDate.getFormattedDate(0) + 
						", at 11:59 PM (Central Time)";
				
				Thread.sleep(2000);
				String strUpdatedAssignmentInfoBox_SAP = sap.getAssignmentTimeZoneInfo_SAP();
				
				Assert.assertEquals(strUpdatedAssignmentInfoBox_SAP.trim(), updatedAssignmentTimeZoneInfo_SAP.trim(),
						"Assignment Time Zone information is displayed correctly?");
				
				//Assert.assertTrue(updatedAssignmentTimeZoneInfo_SAP.trim().contains(strUpdatedAssignmentInfoBox_SAP.trim()),
				//		"Assignment Updated Time Zone information is displayed correctly?");

		sap.resumeAssignmentClick();
		
		//Assert that Late Penalty Information is displayed for Student.
		Thread.sleep(3000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), false);

		sqp.clickNextQuestion();

		custassignment.question9Answers();
		submitCustomAssignmentAnswerAndAssert("5 OF 5 QUESTIONS COMPLETED", "100%");

		driver.close();
		driver.switchTo().window(parentWindow2);
		
		Thread.sleep(2000);
		String updatedStudent_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(updatedStudent_Grade, "100%", 
				"Student average score on Student DLP \"100%\"?");
		
		Thread.sleep(2000);
		SW5DLPstudent.logoutSmartwork5();
		
		
		// Login as INSTRUCTOR again to validate updated Assignment Time Zone.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore4 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		// As an instructor check the Class Average and Submitted Grades on Instructor
		// DLP page.
		String updatedSubmittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", updatedSubmittedGrades);

		String averageGradeafterAssignmentUpdate = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("100%", averageGradeafterAssignmentUpdate);

		// Assert GAU Date associated to Custom Assignment on Instructor DLP.
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, 0);

		DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yy");
		String date2 = dateFormat2.format(cal2.getTime());

		String updatedGAUDate_Instructor = date2 + " 11:59 PM";
		Thread.sleep(2000);
		String updatedStrGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(assignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Instructor, updatedGAUDate_Instructor);

		Thread.sleep(5000);
		SW5DLPpage.clickReportsButton(assignmentTitle);

		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);

		Thread.sleep(5000);
		Assert.assertEquals("100%", classActivityReport.getCARAverageScoreOnAssignment(), 
				"The Average Score");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		studentDetailOverlay = new StudentDetailOverlayPage();

		// Studunet1 grade after Assignment update.

		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);
		Thread.sleep(5000);

		// Assert Overall Score in Student Details Overlay.
		String updatedStrOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(updatedStrOverallScore, "100%");

		// Assert GAU Text on Student Details Overlay.
		String updatedAssignmentInfoBoxText = "Grades are accepted until " + GetDate.getFormattedDate(0)
				+ " at 11:59PM (Central Time)";
		Thread.sleep(2000);
		String updatedGAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(updatedGAUText.trim(), updatedAssignmentInfoBoxText.trim());

		studentDetailOverlay.clickCloseStudentDetailOverlay();

		// Assert GAU Text on CAR page.
		Thread.sleep(2000);
		String UpdatedGAUText_CAR = "Grades are accepted for this assignment until " 
		+ CurrentDate_CAR + ", at 11:59 PM (Central Time)";
		Thread.sleep(2000);
		String updatedStrGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(updatedStrGAU.trim(), UpdatedGAUText_CAR.trim());

		// Assert GAU Text on Instructor Assignment page.

		assignment.returnToAssignmentList.click();
		
		// Assert Assignment Updated Time Zone Information on Instructor Assignment page.

		Thread.sleep(2000);
		String updatedAssignmentTimeZoneText_Instructor = "Grades are accepted until " + CurrentDate_Instructor 
				+ ", at 11:59 PM (Central Time)";
		Thread.sleep(2000);
		String strUpdatedTimeZoneInfo_Instructor = sap.getAssignmentTimeZoneInfo_Instructor();
		
		Assert.assertTrue(updatedAssignmentTimeZoneText_Instructor.trim().contains(strUpdatedTimeZoneInfo_Instructor.trim()), 
				"Assignment Time Zone information is displayed correctly?");

		String headerScoreafterAssignmentUpdate = assignment.getHeaderScore();
		Assert.assertEquals("100%", headerScoreafterAssignmentUpdate, 
				"The Header Score on Instructor Assignment Page.");

		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);

		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

	public void customQuestionsAnswerAssignmentStudent() throws Exception {
		custassignment.question4Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "30%" );
		
		// Navigate to Next Question
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		submitCustomAssignmentAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "50%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question6Answers();
		submitCustomAssignmentAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "70%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question7Answers();
		submitCustomAssignmentAnswerAndAssert("4 OF 5 QUESTIONS COMPLETED", "80%" );

		
	}

	
	private void submitCustomAssignmentAnswerAndAssert(String questionfooterText, String Score) throws Exception{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.clickSubmitAnswer();
		
		WebElement yesButton;
		boolean LetMeFinishButtonExist;
		
		try {
			
				LetMeFinishButtonExist = driver.findElement(By.xpath("//body/div/div/div/div/div/div[3]/button[2]/span[1]")).isDisplayed();
				if(LetMeFinishButtonExist == true) {
				
					yesButton = driver.findElement(By.xpath("//button[contains(text(),'YES')]"));
					yesButton.click();
				
					sqp.clickFeedbackViewSolution();
					Thread.sleep(2000);
					sqp.clickConfirmPopupViewSolutionButton();
				
			}
			
		} catch (Exception e) {
			
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ReusableMethods.questionCloseLink(driver);
			}
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.getQuestionFooterLeftText();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionfooterText);
		sqp.getHeaderScore();
		Assert.assertEquals(sqp.getHeaderScore(), Score);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}
		
}
