package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class EAS_VerifyShowSolution_FeedbackSettings_Part2 extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: Verify the \"Show Solution\" settings for feedback settings - PART2.")
	@Stories("AS-29 - Edit Assignment: Part2 - Verify the \"Show Solution\" settings for feedback settings.")
	@Test
	public void editAssignment_VerifyShowSolutions_Part2() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String questionsSetName = SW5Constants.QUESTION_SET_1;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;
		int pointsPerQuestion = 2;
		
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
        assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Only after a correct answer, or all attempts are exhausted");
		
		assignment.selectshowstudentscore("Percentages");
	
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		if (pointsPerQuestion > 0) {
			assignment.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			assignment.selectAttempts("2");
			assignment.selectGradePanelties("10%");
			assignment.ApplyToAllButton.click();

			Thread.sleep(2000);
		}
		
		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		
		
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		String parentWindow1 = driver.getWindowHandle();
		assignment.clickPreviewButton();
		Assert.assertTrue(assignment.isPreviewButtonDisplayed(),
				"Preview Logo is displayed?");
		
		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver,
				winHandleBefore1, parentWindow1);
		
		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		
		custassignment = new CustomAssignmentAnswers();
		instPreviewAssignmentPage.beginAssignmentClick();
		
		sqp = new StudentQuestionPlayer();
		
		custassignment.question1Answers();
		sqp.clickSubmitAnswer();
		
		// Check Feedback Overlay and Practice button is NOT displayed.
		Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "8%");

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is NOT displayed in Feedback overlay");

		sqp.clickFeedbackClose();

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"1 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is NOT displayed in the footer?");

		// Navigate to Next Question

		sqp.clickNextQuestion();
		
		custassignment.question2_IncorrectAnswer();
		
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		
		// Check Feedback overlay with Try Again button only.
		Thread.sleep(2000);
		Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "8%");
		
		Assert.assertEquals(sqp.isFeedbackOverlayWithTryAgainButtonDisplayed(), true,
				"Feedback overlay is displayed with \"Try Again\" option?");
		
		Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionButtonDisplayed(), false,
				"Feedback overlay is displayed with \"View Solution\" option?");
		
		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is NOT displayed in Feedback overlay");

		Thread.sleep(2000);
		sqp.clickFeedbackTryAgain();

		Thread.sleep(3000);
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is NOT displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		
		// Check Feedback Overlay and Practice button is NOT displayed.
		Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "8%");
		
		Assert.assertEquals(
				sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), false,
				"Feedback overlay is displayed WITHOUT \"View Solution\" & \"Try Again\" option?");

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is NOT displayed in Feedback overlay");

		sqp.clickFeedbackClose();
		
		Thread.sleep(2000);
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "2 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
				"\"Practice\" button is NOT displayed in Feedback overlay");
		
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();
				
				
		// Login as Student 1

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow2 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow2);

			sap = new StudentAssignmentPage();
			
			Thread.sleep(2000);
			sap.beginAssignmentClick();
					
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			
			custassignment.question1Answers();
			sqp.clickSubmitAnswer();
			
			// Check Feedback Overlay and Practice button is NOT displayed.
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "8%");

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");

			sqp.clickFeedbackClose();

			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, "Solution View is displayed");

			Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

			Assert.assertEquals(sqp.getQuestionFooterLeftText(), "1 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, "Practice button is NOT displayed in the footer?");

			// Navigate to Next Question

			sqp.clickNextQuestion();
			
			custassignment.question2_IncorrectAnswer();
			
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			// Check Feedback overlay with Try Again button only.
			Thread.sleep(2000);
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "8%");
			
			Assert.assertEquals(sqp.isFeedbackOverlayWithTryAgainButtonDisplayed(), true,
					"Feedback overlay is displayed with \"Try Again\" option?");
			
			Assert.assertEquals(sqp.isFeedbackOverlayWithViewSolutionButtonDisplayed(), false,
					"Feedback overlay is displayed with \"View Solution\" option?");

			Thread.sleep(2000);
			sqp.clickFeedbackTryAgain();

			Thread.sleep(3000);
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
					"Solution View is NOT displayed");

			Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
			
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
			
			// Check Feedback Overlay and Practice button is NOT displayed.
			Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "8%");
			
			Assert.assertEquals(
					sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), false,
					"Feedback overlay is displayed WITHOUT \"View Solution\" & \"Try Again\" option?");

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");

			sqp.clickFeedbackClose();
			
			Thread.sleep(2000);
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");

			Assert.assertEquals(sqp.getQuestionFooterLeftText(), "2 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");
							
			Thread.sleep(3000);
			sqp.clickHeaderTitle(sap);
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='head-title fl']/a")));
			//driver.findElement(By.xpath("//div[@class='head-title fl']/a")).click();
			driver.close();
			driver.switchTo().window(parentWindow2);
			SW5DLPstudent.logoutSmartwork5();
				
				
			// INSTRUCTOR
				
			new LoginAsInstructor().loginInstructor();
	
			SW5DLPpage = new SW5DLPPage();
	
			String winHandleBefore2 = driver.getWindowHandle();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
	
			SW5DLPpage.ClickAssignmentlink(assignmentTitle);
			testHelper.getInstructorAssignmentWindow(driver);
	
			assignment = new AssignmentPage();
			assignment.editAssignmentbutton();
	
			Thread.sleep(1000);
			assignment.ShowAdditionalSettingsButton();
	
			Thread.sleep(2000);
			assignment.selectShowFeedback("After attempts are exhausted, answer is correct, or student has viewed the solution");
			assignment.selectshowsolution("Only after a correct answer, or all attempts are exhausted");
	
			ReusableMethods.scrollToBottom(driver);
			assignment.saveButton();
	
			Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
			String parentWindow3 = driver.getWindowHandle();
			assignment.clickPreviewButton();
			Assert.assertTrue(assignment.isPreviewButtonDisplayed(), "Preview Logo is displayed?");
	
			// Instructor Preview Assignment Window
			testHelper.getInstructorPreviewAssignmentWindow(driver, winHandleBefore2, parentWindow3);
	
			instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
	
			custassignment = new CustomAssignmentAnswers();
			instPreviewAssignmentPage.beginAssignmentClick();
			
			sqp = new StudentQuestionPlayer();
	
			custassignment.question1Answers();
			sqp.clickSubmitAnswer();
			
			// Check Feedback Overlay and Practice button is NOT displayed.
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "8%");

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");

			sqp.clickFeedbackClose();

			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");

			Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

			Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
					"1 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
					"Practice button is NOT displayed in the footer?");

			// Navigate to Next Question

			sqp.clickNextQuestion();
			
			
			custassignment.question2_IncorrectAnswer();

			Thread.sleep(2000);
			sqp.clickSubmitAnswer();

			// Check only Try Again button is displayed at the footer.
			Thread.sleep(1000);
			
			Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
					"Try Again Button is displayed");
			
			Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false, 
					"View Solution Button is displayed");

			Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
					"Solution View is displayed");

			Thread.sleep(1000);
			sqp.clickTryAgain();
			
			Thread.sleep(1000);
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
					"Solution View is displayed");

			sqp.clickSubmitAnswer();

			// Attempts Exhausted - Assert Show Feedback Overlay.
			Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));

			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");

			sqp.clickFeedbackClose();

			// Assert View Solution section is displayed.
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");
			
			Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));

			Assert.assertEquals(sqp.getHeaderScore(), "8%");
			Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
					"2 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
					"Practice button is NOT displayed in the footer?");

			driver.close();
			driver.switchTo().window(parentWindow3);
	
			Thread.sleep(5000);
			driver.close();
			driver.switchTo().window(winHandleBefore2);
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);
			SW5DLPpage.logoutSW5();
				
				
			// Login as Student 2
	
			String studentUserName2 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
	
			new LoginAsStudent().LoginStudent(studentUserName2);
	
			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);
	
			// Assignment Window
			String parentWindow4 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow4);
	
			sap = new StudentAssignmentPage();
	
			Thread.sleep(2000);
			sap.beginAssignmentClick();
	
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			
			custassignment.question1Answers();
			sqp.clickSubmitAnswer();
	
			// Check Feedback Overlay and Practice button is NOT displayed.
			Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
			Assert.assertEquals(sqp.getHeaderScore(), "8%");
	
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");
	
			sqp.clickFeedbackClose();
	
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");
	
			Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
	
			Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
					"1 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
					"Practice button is NOT displayed in the footer?");
	
			// Navigate to Next Question
	
			sqp.clickNextQuestion();
	
			custassignment.question2_IncorrectAnswer();
	
			Thread.sleep(2000);
			sqp.clickSubmitAnswer();
	
			// Check only Try Again button is displayed at the footer.
			Thread.sleep(1000);
	
			Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
					"Try Again Button is displayed");
	
			Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), 
					false, "View Solution Button is displayed");
	
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
					"Solution View is NOT displayed");
	
			Thread.sleep(1000);
			sqp.clickTryAgain();
	
			Thread.sleep(1000);
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
					"Solution View is NOT displayed");
	
			sqp.clickSubmitAnswer();
	
			// Attempts Exhausted - Assert Show Feedback Overlay.
			Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
	
			Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), false,
					"\"Practice\" button is NOT displayed in Feedback overlay");
	
			sqp.clickFeedbackClose();
	
			// Assert View Solution section is displayed.
			Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
					"Solution View is displayed");
	
			Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
	
			Assert.assertEquals(sqp.getHeaderScore(), "8%");
			Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
					"2 OF 12 QUESTIONS COMPLETED");
			Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
					"QUESTION COMPLETED");
			Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
					"Practice button is NOT displayed in the footer?");
	
			Thread.sleep(3000);
			sqp.clickHeaderTitle(sap);
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='head-title fl']/a")));
			//driver.findElement(By.xpath("//div[@class='head-title fl']/a")).click();
			driver.close();
			driver.switchTo().window(parentWindow4);
			SW5DLPstudent.logoutSmartwork5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}