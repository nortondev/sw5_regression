package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_RemoveMultipleQuestions_PostStudentSubmission extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;

	SW5DLPPage SW5DLPpage;
	ClassActivityReportPage classActivityReport;
	ManageStudentSetsPage managestudentsetpage;
	UpdateStudentSet updatestudentset;
	CreateNewStudentSet createStudentset;
	
	String student1SubmittedGradescore;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment Page - Verify that Instructor can remove multiple questions post Student submission.")
	@Stories("AS-218 - As an Instructor - Remove multiple questions post Student submission.")
	@Test
	public void editAssignment_RemoveMultipleQuestions_PostStudentSubmission() throws Exception {
		
		driver = getDriver();
		
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		instCustAssignment = new InstructorPublishCustomAssignment();

		String assignmentTitle = instCustAssignment
				.updateSettingsAndPublishRemoveQuestion();
		

		// Login as Student

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		String StudentName1 = testHelper
				.getStudentUserNameInUppercase(studentUserName1).trim();

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();

			sap.beginAssignmentClick();

			customQuestionsAnswerAssignmentStudent1();
			student1SubmittedGradescore = sqp.getHeaderScore();
			LogUtil.log("The " + StudentName1 +" grade score", student1SubmittedGradescore);

            Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//div[@class='head-title fl']/a")));
			driver.findElement(By.xpath("//div[@class='head-title fl']/a"))
					.click();
			driver.close();
			driver.switchTo().window(parentWindow);
			SW5DLPstudent.logoutSmartwork5();

		
			
		// INSTRUCTOR

		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		new LoginAsInstructor().loginInstructor();

		
		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		// Assert As an instructor check the class average and Submitted Grades
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", submittedGrades);
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("100%", averageGrade);
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);
		
		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("100%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());
		
		//Student1 Grade details 
		getClassActivityStudent(StudentName1, "100%");

		
		driver.close();
		driver.switchTo().window(winHandleBefore);

		// /As an instructor go and delete question #8 from edit assignment page.
		String winHandleBefore1 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		
		for (int i=7; i<=9; i++) {
			assignment.removeQuestion(i);
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			assignment.clickRemovebutton();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			ReusableMethods.scrollToBottom(driver);
			assignment.saveButton();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		}
		
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		Thread.sleep(5000);
		
		// Assert class average should be 100% on DLP & CAR.
		String averageGradeafterremovequestion = SW5DLPpage
				.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("100%", averageGradeafterremovequestion);

		Thread.sleep(5000);
		String winHandleBefore2 = driver.getWindowHandle();
		
		Thread.sleep(5000);
		SW5DLPpage.clickReportsButton(assignmentTitle);
		
		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);
		
		Thread.sleep(8000);
		Assert.assertEquals("100%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		//Studunet1 grade after Remove Question 
		getClassActivityReportStudentRemoveQuestion(StudentName1, "100%");				

		driver.close();
		
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBefore2);
		
		String winHandleBefore3 = driver.getWindowHandle();
		assignment.Assignmentlinks(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment.editAssignmentbutton();
		
		Thread.sleep(1000);
		assignment.ShowAdditionalSettingsButton();

		String ShowSolution = "Only after a correct answer, or all attempts are exhausted";
		assignment.selectshowsolution(ShowSolution);
		
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//select[@data-reactid='.0.1.7.0.0.1.0.1.3.1.1:$input']"));
		String ShowStudentScore = "Points";
		
		assignment.selectshowstudentscore(ShowStudentScore);
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		
		Thread.sleep(6000);
		driver.close();
		driver.switchTo().window(winHandleBefore3);
		Thread.sleep(6000);
		
		// Assert % should be converted to points at places and assert
		// individual student points and class average in points. This should be
		// asserted in DLP & CAR.
		String winHandleBefore4 = driver.getWindowHandle();
		SW5DLPpage.clickReportsButton(assignmentTitle);
		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);
		Thread.sleep(5000);
		Assert.assertEquals("100%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		
		// Student 1 grade in Points 
		Thread.sleep(5000);
		getClassActivityStudent(StudentName1, "21");	

		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		SW5DLPpage.logoutSW5();


			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow1 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow1);

				Thread.sleep(2000);
				student1SubmittedGradescore = sqp.getHeaderScoreinPoints();
				Assert.assertEquals("21", student1SubmittedGradescore);
				LogUtil.log(student1SubmittedGradescore);
				
				sap = new StudentAssignmentPage();
				sap.reviewAssignmentClick();
				
				// # Use case: Solutions displayed after the correct answer.

				// Question #1
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());

				// Navigate to Question #2
				sqp.clickNextQuestion();

				// Question #2
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #3
				sqp.clickNextQuestion();

				// Question #3
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #4
				sqp.clickNextQuestion();

				// Question #4
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #5
				sqp.clickNextQuestion();

				// Question #5
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #6
				sqp.clickNextQuestion();

				// Question #6
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #7
				sqp.clickNextQuestion();

				// Question #7
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #8
				sqp.clickNextQuestion();

				// Question #8
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				// Navigate to Question #9
				sqp.clickNextQuestion();

				// Question #9
				Thread.sleep(1000);
				Assert.assertTrue(sqp.isViewSolutionDisplayed());
				
				driver.close();
				driver.switchTo().window(parentWindow1);
				
				SW5DLPstudent.logoutSmartwork5();


	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

	public void customQuestionsAnswerAssignmentStudent1() throws Exception {
		custassignment.question1Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 12 QUESTIONS COMPLETED", "14%" );
		
		// Navigate to Next Question
		sqp.clickNextQuestion();
		custassignment.question2Answer();
		
		submitCustomAssignmentAnswerAndAssert("2 OF 12 QUESTIONS COMPLETED", "18%" );
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		
		submitCustomAssignmentAnswerAndAssert("3 OF 12 QUESTIONS COMPLETED", "29%" );
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		
		submitCustomAssignmentAnswerAndAssert("4 OF 12 QUESTIONS COMPLETED", "39%" );
		sqp.clickNextQuestion();
		
		custassignment.question5Answers();
		
		submitCustomAssignmentAnswerAndAssert("5 OF 12 QUESTIONS COMPLETED", "46%" );
		sqp.clickNextQuestion();
		
		custassignment.question6Answers();
		
		submitCustomAssignmentAnswerAndAssert("6 OF 12 QUESTIONS COMPLETED", "54%" );
		sqp.clickNextQuestion();
		
		custassignment.question7Answers();
		
		submitCustomAssignmentAnswerAndAssert("7 OF 12 QUESTIONS COMPLETED", "57%" );
		sqp.clickNextQuestion();
		
		custassignment.question8Answers();
		
		submitCustomAssignmentAnswerAndAssert("8 OF 12 QUESTIONS COMPLETED", "68%" );
		sqp.clickNextQuestion();
		
		custassignment.question9Answers();
		
		submitCustomAssignmentAnswerAndAssert("9 OF 12 QUESTIONS COMPLETED", "75%" );
		sqp.clickNextQuestion();
		
		custassignment.question10Answers();
		
		submitCustomAssignmentAnswerAndAssert("10 OF 12 QUESTIONS COMPLETED", "82%" );
		sqp.clickNextQuestion();
		
		custassignment.question11Answers();
		
		submitCustomAssignmentAnswerAndAssert("11 OF 12 QUESTIONS COMPLETED", "86%" );
		sqp.clickNextQuestion();
		
		custassignment.question12Answers();
		
		submitCustomAssignmentAnswerAndAssert("12 OF 12 QUESTIONS COMPLETED", "100%" );
	}

	
	private void getClassActivityStudent(String studentName, String studentscore) throws Exception{
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		classActivityReport.clickStudentNamelink(studentName);
		Thread.sleep(1000);
		classActivityReport.getStudentOverallScore();
		Assert.assertEquals(studentscore,
				classActivityReport.getStudentOverallScore());
		classActivityReport.closeStudentDetailPage();
	}
	
	private void getClassActivityReportStudentRemoveQuestion(String studentName, String value) throws Exception{
	classActivityReport.clickStudentSearchIcon();
	classActivityReport.enterStudentName(studentName);
	classActivityReport.clickStudentNamelink(studentName);
	classActivityReport.getStudentOverallScore();
	Assert.assertEquals(value,
			classActivityReport.getStudentOverallScore());
	classActivityReport.closeStudentDetailPage();
	}
	
	private void submitCustomAssignmentAnswerAndAssert(String questionfooterText, String Score) throws Exception{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.clickSubmitAnswer();
		
		WebElement yesButton;
		boolean LetMeFinishButtonExist;
		
		try {
			
				LetMeFinishButtonExist = driver.findElement(By.xpath("//body/div/div/div/div/div/div[3]/button[2]/span[1]")).isDisplayed();
				if(LetMeFinishButtonExist == true) {
				
					yesButton = driver.findElement(By.xpath("//button[contains(text(),'YES')]"));
					yesButton.click();
				
					sqp.clickFeedbackViewSolution();
					Thread.sleep(2000);
					sqp.clickConfirmPopupViewSolutionButton();
				
			}
			
		} catch (Exception e) {
			
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ReusableMethods.questionCloseLink(driver);
			}
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.getQuestionFooterLeftText();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionfooterText);
		sqp.getHeaderScore();
		Assert.assertEquals(sqp.getHeaderScore(), Score);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}
		
}