package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyAllFiltersInQuestionLibrary extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;

	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify All filters in Questions library page.")
	@Stories("AS-68 As an Instructor - Verify All filters in Questions library page.")
	@Test
	public void VerifyAllFiltersInQuestionLibrary() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		String assignmentTitle = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(assignmentTitle);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		
		//String strTotalQuestionCount = "4552";
		//int intTotalQuestionCount = Integer.valueOf(strTotalQuestionCount);
		
		int initialQuestionCount = questionpage.questionsCount();
		//Assert.assertEquals(initialQuestionCount == intTotalQuestionCount, true, 
		//		"Initial Total Question Count is matched");
		
		questionpage.applyChapterFilter();
		Thread.sleep(3000);
		int questionCountAfterChapterFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterChapterFilter < initialQuestionCount, true, 
				"Question Count after Chapter Filter is less than Initial Total Question Count");

		questionpage.applyObjectiveFilter();
		Thread.sleep(3000);
		int questionCountAfterObjectiveFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterObjectiveFilter < questionCountAfterChapterFilter, true, 
				"Question Count after Objective Filter is less than Question Count after Chapter Filter");
		
		questionpage.applySeriesFilter();
		Thread.sleep(3000);
		int questionCountAfterSeriesFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterSeriesFilter < questionCountAfterObjectiveFilter, true, 
				"Question Count after Series Filter is less than Question Count after Objective Filter");
		
		questionpage.applyTypeFilter();
		Thread.sleep(3000);
		int questionCountAfterTypeFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterTypeFilter <= questionCountAfterSeriesFilter, true, 
				"Question Count after Type Filter is less than Question Count after Series Filter");
		
		questionpage.applyDifficultyFilter();
		Thread.sleep(3000);
		int questionCountAfterDifficultyFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterDifficultyFilter < questionCountAfterTypeFilter, true, 
				"Question Count after Difficulty Filter is less than Question Count after Type Filter");
		
		questionpage.applyAuthorFilter();
		Thread.sleep(3000);
		int questionCountAfterAuthorFilter = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterAuthorFilter < questionCountAfterDifficultyFilter, true, 
				"Question Count after Author Filter is less than Question Count after Difficulty Filter");
		
		questionpage.clearAllFilters();
		Thread.sleep(3000);
		int questionCountAfterClearAllFilters = questionpage.questionsCount();
		Assert.assertEquals(questionCountAfterClearAllFilters == initialQuestionCount, true, 
				"Question Count after Clearing All Filters matches with Initial Total Question Count.");
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);

		SW5DLPpage.logoutSW5();
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}