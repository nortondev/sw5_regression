package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyRandomizeQuestions_On extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment - Verify Randomize Questions - On functionality in Edit assignment and in Question Player.")
	@Stories("AS-33 - Verify Randomize Questions - On functionality in Edit assignment and in Question Player.")
	@Test
	public void VerifyRandomizeQuestions_On() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		Thread.sleep(5000);
		SW5DLPpage.ClickAssignment();
		
		String ChildWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		
		String winHandleBefore1 = driver.getWindowHandle();
		String ParentWindow1 = driver.getWindowHandle();
		Windowhandles.remove(ParentWindow1);
		
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != ParentWindow1) {
			ChildWindow = winHandle;
		}
		
		driver.switchTo().window(ChildWindow);
		
		Thread.sleep(5000);
		driver.switchTo().frame("swfb_iframe");
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//span[@class='instructor-intro-assignment-title']")));
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Randomize Assignment ";
		int suffixCharCount = 5;
		String AssignmentName = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(AssignmentName);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		//assignment.adaptiveOff();
		assignment.adaptiveOn();
		assignment.followUp();
		assignment.selectAdaptivePoints("0");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		if(assignment.randomizeQuestionButton_On_Enabled() == false) {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", 
					assignment.RandomizeQuestionOff);
			
		}
		
		String questionsSetName = SW5Constants.QUESTION_SET_6;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestionsWithQuestionID(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);
		wait.until(ExpectedConditions.visibilityOf(assignment.SELECTOBJECTIVES));
		assignment.selectObejctive();

		assignment.saveButton();
		
		wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);
		
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		
		String parentWindow2 = driver.getWindowHandle();
		
		assignment.clickPreviewButton();
		Assert.assertTrue(assignment.isPreviewButtonDisplayed());
		
		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver,
				winHandleBefore1, parentWindow2);
		
		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		
		List<String> questionList_RandomizeOn_Before = instPreviewAssignmentPage.getAllQuestionList();
		
		driver.navigate().refresh();
		
		ReusableMethods.checkPageIsReady(driver);
		List<String> questionList_RandomizeOn_After = instPreviewAssignmentPage.getAllQuestionList();
		
		Assert.assertFalse(questionList_RandomizeOn_Before.equals(questionList_RandomizeOn_After));
		
		driver.close();
		driver.switchTo().window(parentWindow2);
		
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		SW5DLPpage.logoutSW5();
		
		
		// Login as Student

		String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		new LoginAsStudent().LoginStudent(studentUserName1);
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(AssignmentName);

		// Assignment Window
		String parentWindow3 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow3);
		sap = new StudentAssignmentPage();
		
		List<String> questionList_RandomizeOn_Std_Before = sap.getAllQuestionList();

		driver.navigate().refresh();
		
		StudentAssignmentPage sap = new StudentAssignmentPage();
		sap.keepUsingTrialAccess();

		driver.switchTo().frame("swfb_iframe");

		sap.waitForLoadPageElements();
		List<String> questionList_RandomizeOn_Std_After = sap.getAllQuestionList();
		
		Assert.assertFalse(questionList_RandomizeOn_Std_Before.equals(questionList_RandomizeOn_Std_After));
		
		Thread.sleep(2000);
		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
		sqp.returnToAssignmentPage);
		
		Thread.sleep(2000);
		List<String> questionList_RandomizeOn_Std_AfterSub = sap.getAllQuestionList();
		
		Assert.assertTrue(questionList_RandomizeOn_Std_After.equals(questionList_RandomizeOn_Std_AfterSub));
		
		driver.close();
		driver.switchTo().window(parentWindow3);
		SW5DLPstudent.logoutSmartwork5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
