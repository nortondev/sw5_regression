package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyAdditionalSettings_BeforeAndAfterStudentSubmission extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify the Additional settings before and after student submission.")
	@Stories("AS-38 As an Instructor - Verify the Additional settings before and after student submission.")
	@Test
	public void VerifyAdditionalSettings_BeforeAndAfterStudentSubmission() throws Exception {
		
		driver = getDriver();
		
		// Login as Instructor
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception(
					"Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
		logininst = new LoginAsInstructor();
		logininst.loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		
		String assignmentTitleSuffix = "Custom Assignment ";
		int suffixCharCount = 5;
		String assignmentTitle = assignmentTitleSuffix 
				+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
		
		assignment.editAssignmentbutton();
		
		assignment.enterAssignmentName(assignmentTitle);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.getSetGAUTime();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		String questionsSetName = SW5Constants.QUESTION_SET_3;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();

		createAssignment = new CreateCustomAssignment();
		createAssignment.addCustomQuestions(questionsSetName);
		
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.PublishButton.click();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkTimeLimitSettings(), true);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkShowEbookLinkSettings(), true);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkShowPeriodicTableSettings(), true);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkRandomizeQuestionSettings(), true);
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);

		SW5DLPpage.logoutSW5();
		
		
		// Login as Student

		String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		new LoginAsStudent().LoginStudent(studentUserName1);
		SW5DLPstudent = new SW5DLPStudent();
		
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
		
		sap.beginAssignmentClick();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(parentWindow);
		SW5DLPstudent.logoutSmartwork5();
		
		
		// Login as Instructor and verify Question settings post student submission.
		
		new LoginAsInstructor().loginInstructor();

		
		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore2 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		WebDriverWait wait1 = new WebDriverWait(driver, 50);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkTimeLimitSettings(), false);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkShowEbookLinkSettings(), true);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkShowPeriodicTableSettings(), true);
		Thread.sleep(2000);
		Assert.assertEquals(assignment.checkRandomizeQuestionSettings(), false);
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		SW5DLPpage.logoutSW5();
		
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
