package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class EAS_VerifyShowSolution_LimitedAssignment_Part2 extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As a Student verify the \"Show Solution\" for time limited assignment - PART2.")
	@Stories("AS-22 Edit Assignment: \"Show Solution\"  (Time Limit Assignment)")
	@Test(priority = 0)
	public void showSolution_TimeLimitAssignment_Part2(ITestContext context) throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String questionsSetName = SW5Constants.QUESTION_SET_1;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;
		int pointsPerQuestion = 2;
		
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
        assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		assignment.timeLimitOn();

		// Set the time limit 9 mins
		assignment.setTimeLimit("12");
		
		Thread.sleep(2000);
		assignment.selectUngradedPractice("After the \"Grades Accepted Until\" date passes");
		assignment.selectShowFeedback("After the \"Grades Accepted Until\" date passes");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");
		
		assignment.selectshowstudentscore("Percentages");
	
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		if (pointsPerQuestion > 0) {
			assignment.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			assignment.selectAttemptsforQuestion("2");
			Thread.sleep(2000);
		}

		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();
		
		
		
		// Login as Student

		String studentUserName1 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
				
		new LoginAsStudent().LoginStudent(studentUserName1);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.verifyInitialAllQuestionPoints());
		//Assert.assertTrue(sap.verifyInitialAllQuestionAttempts());
		Assert.assertTrue(sap.verifyInitialAllQuestionStatus());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:12:00", "Initial timer value is 12 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickNoIamNotReadyToBeginButton();

		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:12:00", 
				"Initial timer value is 12 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickYesIamReadyToBeginButton();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();
		Thread.sleep(2000);

		String timerText1 = sqp.getTimerText();

		Assert.assertTrue(timerText1.compareTo("00:12:00") != 0, "Is timer started?");
		Thread.sleep(5000);

		Assert.assertTrue(sqp.getTimerText().compareTo(timerText1) != 0, "Is timer coninues?");

		custassignment = new CustomAssignmentAnswers();

		// START - useCase: Submit assignment when time limit is "ON" 
		// to make sure the submission went through.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		//Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "0 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/12");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #1 - First Attempt Correct Answer
		custassignment.question1Answers();

		sqp.clickSubmitAnswer();
		
		// Check Feedback Overlay with Practice button is NOT displayed.
		Assert.assertEquals(sqp.getHeaderScore(), "14%");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"1 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		// Navigate to Next Question

		sqp.clickNextQuestion();
		
		// Question #2 - First Attempt InCorrect Answer

		custassignment.question2_IncorrectAnswer();
		
		Thread.sleep(2000);
		sqp.clickSubmitAnswer();
		
		// Check View Solution and Try Again Buttons at the footer.
		Thread.sleep(1000);
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), true, 
				"View Solution Button is displayed");

		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
				"Try Again Button is displayed");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is displayed");
		
		
		// Question #2 - Second Attempt Correct Answer
		
		Thread.sleep(1000);
		sqp.clickTryAgain();
		
		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");
		
		custassignment.question2Answer();
		
		Thread.sleep(1000);
		sqp.clickSubmitAnswer();
		
		// Check Feedback Overlay with Practice button is NOT displayed.
		Assert.assertEquals(sqp.getHeaderScore(), "18%");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"2 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");


		// Hold submission and allow the time limit to expire to validate the time limit
		// expire message validation comes up and system should not allow you to submit
		// answer.
		int sleepTimeInMiliseconds1 = 50000;
		while (true) {
			Thread.sleep(15000);
			sleepTimeInMiliseconds1 += 50000;
			// Wait till timer expired or 7 mins
			if (sqp.getTimerText().compareTo("00:00:00") == 0 || sleepTimeInMiliseconds1 >= 100000)
				break;
		}
		
		Thread.sleep(15000);

		Assert.assertEquals(sqp.getAlertMessageText().trim(), 
				"Time has expired and no further attempts are allowed!",
				"Alert popup with 'Time has expired and no further attempts are allowed!' message is displayed?");

		sqp.clickTimerExpiredAlertClose();
		
		Thread.sleep(2000);
		Assert.assertTrue(sap.isReviewAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");

		sap.reviewAssignmentClick();
		Thread.sleep(10000);

		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");
		
		sqp.clickNextQuestion();
		
		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");
		
		sqp.clickNextQuestion();
		
		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");
		
		Thread.sleep(2000);
		testHelper.logoutSmartwork5(driver, parentWindow1, SW5DLPstudent);
		// END
		
		
		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore2 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();

		Thread.sleep(1000);
		assignment.ShowAdditionalSettingsButton();

		Thread.sleep(2000);
		assignment.selectshowsolution(
				"Only after a correct answer, or all attempts are exhausted");

		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();

		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();
		
		
		// Login as Student 2
		
		String studentUserName2 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);

		new LoginAsStudent().LoginStudent(studentUserName2);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.verifyInitialAllQuestionPoints());
		// Assert.assertTrue(sap.verifyInitialAllQuestionAttempts());
		Assert.assertTrue(sap.verifyInitialAllQuestionStatus());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:12:00", "Initial timer value is 12 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickNoIamNotReadyToBeginButton();

		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:12:00", "Initial timer value is 12 minutes?");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		Assert.assertTrue(sap.isConfirmOverlayWithYesNoToBeginAssignmentDisplayed(),
				"Confirm popup with Yes/No to begin assignment displayed?");

		sap.clickYesIamReadyToBeginButton();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();
		Thread.sleep(2000);

		String timerText2 = sqp.getTimerText();

		Assert.assertTrue(timerText2.compareTo("00:12:00") != 0, "Is timer started?");
		Thread.sleep(5000);

		Assert.assertTrue(sqp.getTimerText().compareTo(timerText2) != 0, "Is timer coninues?");

		custassignment = new CustomAssignmentAnswers();

		// START - useCase: Submit assignment when time limit is "ON"
		// to make sure the submission went through.

		// Question #1
		if (sqp.getQuestionFooterLeftText().compareTo("0 OF 0 QUESTIONS COMPLETED") == 0) {
			Thread.sleep(2000);
		}

		Assert.assertEquals(sqp.getHeaderScore(), "-- %");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		// Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"0 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionFooterMiddleText(), "01/12");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		// Question #1 - First Attempt Correct Answer
		custassignment.question1_IncorrectAnswers();

		sqp.clickSubmitAnswer();

		// Check View Solution and Try Again Buttons at the footer.
		Thread.sleep(1000);
		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 
				"Try Again Button is displayed");
		
		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false, 
				"View Solution Button is displayed");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is displayed");

		// Question #1 - Second Attempt InCorrect Answer

		Thread.sleep(1000);
		sqp.clickTryAgain();

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		// Check Feedback Overlay and Practice button is NOT displayed.
		Assert.assertEquals(sqp.getHeaderScore(), "7%");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("2nd attempt"));

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"1 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");

		// Navigate to Next Question

		sqp.clickNextQuestion();

		// Question #2 - First Attempt Correct Answer

		custassignment.question2Answer();

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		// Check Feedback Overlay with Practice button is NOT displayed.
		Assert.assertEquals(sqp.getHeaderScore(), "11%");

		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), 
				"2 OF 12 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");
		
		
		// Navigate to Next Question

		sqp.clickNextQuestion();

		// Question #3 - Incorrect Answer

		custassignment.question3_IncorrectAnswers();

		Thread.sleep(1000);
		sqp.clickSubmitAnswer();

		Thread.sleep(3000);
		Assert.assertEquals(sqp.getHeaderScore(), "14%");
		Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
		
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is displayed");

		// Check Try Again Button at the footer.
		Assert.assertEquals(sqp.isTryAgainButtonDisplayed(), true, 	
				"Try Again Button is displayed");

		Assert.assertEquals(sqp.isViewSolutionButtonDisplayed(), false, 
				"View Solution Button is displayed");

		Assert.assertEquals(sqp.isPracticeButtonDisplayed(), false, 
				"Practice button is displayed in the footer?");
		

		// Hold submission and allow the time limit to expire to validate the time limit
		// expire message validation comes up and system should not allow you to submit
		// answer.
		int sleepTimeInMiliseconds2 = 30000;
		while (true) {
			Thread.sleep(15000);
			sleepTimeInMiliseconds2 += 30000;
			// Wait till timer expired or 7 mins
			if (sqp.getTimerText().compareTo("00:00:00") == 0 || sleepTimeInMiliseconds2 >= 60000)
				break;
		}

		Thread.sleep(15000);

		Assert.assertEquals(sqp.getAlertMessageText().trim(), 
				"Time has expired and no further attempts are allowed!",
				"Alert popup with 'Time has expired and no further attempts are allowed!' message is displayed?");

		sqp.clickTimerExpiredAlertClose();

		Thread.sleep(2000);
		Assert.assertTrue(sap.isReviewAssignmentButtonDisplayed());
		Assert.assertTrue(sap.isTimerIconDisplayed());
		Assert.assertEquals(sap.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");

		sap.reviewAssignmentClick();
		Thread.sleep(10000);

		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		sqp.clickNextQuestion();
		
		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
				"Solution View is displayed");

		sqp.clickNextQuestion();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), 
				"QUESTION COMPLETED");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is displayed");
		
		sqp.clickNextQuestion();

		Thread.sleep(2000);
		Assert.assertEquals(sqp.getTimerText(), "00:00:00", 
				"Expired timer value is 0 minutes?");
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), false,
				"Submit Answer button is not displayed in the footer?");
		Assert.assertEquals(sqp.isViewSolutionDisplayed(), false, 
				"Solution View is displayed");
		
		Thread.sleep(2000);
		testHelper.logoutSmartwork5(driver, parentWindow2, SW5DLPstudent);
		// END

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
