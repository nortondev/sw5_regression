package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH15HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class EAS_ShowFeedbackSettings_PracticeQuestions extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	SW5DLPPage SW5DLPpage;
	LoginAsStudent loginStd;
	LoginPage DLPpage;
	SW5DLPStudent SW5DLPstudent;
	AssignmentPage assignmentpage;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH15HW assignment;
	Questions_Page questionPage;
	CreateCustomAssignment createCustomAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	TestHelper testHelper = new TestHelper();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit Assignment: Verify the \"Show Feedback\" settings for practice questions.")
	@Stories("AS-28 Show Feedback: After Every attempt or After Attempt are exhausted , answer is correct or student has viewed the solution.")
	@Test
	public void editAssignmentShowFeedbackSettings_PracticeQuestions() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
				testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);

				// Publish assignment if assignmentTitle not found in global context
				String assignmentTitle = testHelper.getPublishedAssignmnetTitle(SW5Constants.ASSIGNMENT_CH15HW);

				// Login as Student Account 1
				SeleniumTestsContext gContext = SeleniumTestsContextManager
						.getGlobalContext();
				
				String studentUserName1 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
				
				new LoginAsStudent().LoginStudent(studentUserName1);

				// Select mentioned Assignment
				SW5DLPstudent = new SW5DLPStudent();
				SW5DLPstudent.SelectAssignment(assignmentTitle);

				// Assignment Window
				String studentWindow1 = driver.getWindowHandle();
				testHelper.getStudentAssignmentWindow(driver, studentWindow1);

				sap = new StudentAssignmentPage();
				sqp = new StudentQuestionPlayer();
				assignment = new AssignmentCH15HW();

				sap.beginAssignmentClick();				
				sqp.waitForLoadPageElements();
				
				// Provide Correct Answer and Assert Feedback and Practice button.
				
				assignment
						.question1MultipleParts_CorrectAnswer();
				
				Thread.sleep(2000);
				sqp.clickSubmitAnswer();
				
				// Check Feedback and Practice Button
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackPractice();
				
				Thread.sleep(2000);
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1MultipleParts_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();

				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				
				// Provide Incorrect Answer - 1st attempt and Assert Feedback overlay.
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				
				assignment
						.question6DragAndDrop_IncorrectAnswer();
				
				Thread.sleep(2000);
				sqp.clickSubmitAnswer();
				
				// Check Feedback overlay with View Solution and Try Again Buttons.
				Thread.sleep(2000);
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(
						sqp.isFeedbackOverlayWithViewSolutionAndTryAgainButtonsDisplayed(), true,
						"Feedback overlay is displayed with \"View Solution\" & \"Try Again\" option?");
				
				Thread.sleep(2000);
				sqp.clickFeedbackViewSolution();

				Assert.assertEquals(sqp
						.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
						"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
				
				Assert.assertEquals(
						sqp.getConfirmViewSolutionMessageText().trim(),
						"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
						"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");

				sqp.clickConfirmPopupViewSolutionButton();
				
				Thread.sleep(5000);
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
			
				Thread.sleep(5000);
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. "
									+ "Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
				
				// Click Check Practice and Provide InCorrect Answer
				
				assignment.question6DragAndDrop_IncorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackPractice();
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				
				driver.close();
				driver.switchTo().window(studentWindow1);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPstudent.logoutSmartwork5();
				
				
				//Login as an Instructor and change the Feedback settings to Option 2
				
				new LoginAsInstructor().loginInstructor();
				
				String studentSetId = (String) gContext
						.getAttribute(SW5Constants.STUDENT_SET_ID);

				SW5DLPpage = new SW5DLPPage();
				SW5DLPpage.selectByPartOfVisibleText(studentSetId);
				LogUtil.log("Student ID is " + studentSetId);
				
				Thread.sleep(3000);
				SW5DLPpage.ClickAssignmentlink(SW5Constants.ASSIGNMENT_CH15HW);
				
				Thread.sleep(3000);
				String winHandleBefore = driver.getWindowHandle();
				testHelper.getInstructorAssignmentWindow(driver);

				assignmentpage = new AssignmentPage();
				assignmentpage.editAssignmentbutton();
				
				Thread.sleep(3000);
				assignmentpage.ShowAdditionalSettingsButton();
				assignmentpage.selectShowFeedback("After attempts are exhausted, answer is correct, or student has viewed the solution");
				
				Thread.sleep(2000);
				assignmentpage.saveButton();
				
				String parentWindow = driver.getWindowHandle();
				assignmentpage.clickPreviewButton();
				Assert.assertTrue(assignmentpage.isPreviewButtonDisplayed(),
						"Preview Logo is displayed?");
				
				
				// Instructor Preview Assignment Window
				testHelper.getInstructorPreviewAssignmentWindow(driver,
						winHandleBefore, parentWindow);
				instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
				
				sqp = new StudentQuestionPlayer();
				assignment = new AssignmentCH15HW();
				instPreviewAssignmentPage.beginAssignmentClick();
				
				// Provide Correct Answer - 1st attempt and Assert Feedback overlay and Practice button.
				assignment.question1MultipleParts_CorrectAnswer();

				sqp.clickSubmitAnswer();
				
				// Check Show Feedback and Practice Button
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackPractice();
				
				Thread.sleep(2000);
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1MultipleParts_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");

				
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				
				// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
				assignment
						.question6DragAndDrop_IncorrectAnswer();

				sqp.clickSubmitAnswer();
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(),
						true,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(),
						true,
						"Try Again Button is displayed");
				
				Thread.sleep(1000);
				sqp.clickTryAgain();
				
				Thread.sleep(1000);
				sqp.clickSubmitAnswer();
				
				// After attempt are exhausted - 2nd attempt, Check Show Feedback Overlay and Practice Button.
				Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				
				// Provide Incorrect Answer - practice attempt and Assert View Solution and Try Again buttons.
				assignment
						.question6DragAndDrop_IncorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				
				sqp.clickHeaderTitle(sap);
				
				Thread.sleep(2000);
				sap.clickQuestion(11);
				
				// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
				assignment.question11_IncorrectAnswer();

				sqp.clickSubmitAnswer();
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(), true,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(), true,
						"Try Again Button is displayed");
				
				Thread.sleep(1000);
				sqp.clickViewSolution();
				
				// After View Solution, Check Practice Button.
				Thread.sleep(2000);
				Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
						"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
				Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
						"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
						"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
				
				sqp.clickConfirmPopupViewSolutionButton();
				Thread.sleep(5000);
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"3 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Thread.sleep(5000);
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. "
									+ "Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
				
				// Click Check Practice and Provide InCorrect Answer
				
				assignment.question11_IncorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"3 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				driver.close();
				driver.switchTo().window(parentWindow);
				
				driver.close();
				driver.switchTo().window(winHandleBefore);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPpage.logoutSW5();
				
				
				// Login as Student Account 2
				
				String studentUserName2 = (String) gContext
						.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
				
				new LoginAsStudent().LoginStudent(studentUserName2);

				// Select mentioned Assignment
				SW5DLPstudent = new SW5DLPStudent();
				SW5DLPstudent.SelectAssignment(assignmentTitle);

				// Assignment Window
				String studentWindow2 = driver.getWindowHandle();
				testHelper.getStudentAssignmentWindow(driver, studentWindow2);

				sap = new StudentAssignmentPage();
				assignment = new AssignmentCH15HW();
				sqp = new StudentQuestionPlayer();

				sap.beginAssignmentClick();
				sqp.waitForLoadPageElements();

				
				// Provide Correct Answer - 1st attempt and Assert Feedback overlay and Practice button.
				assignment.question1MultipleParts_CorrectAnswer();

				sqp.clickSubmitAnswer();
				
				// Check Show Feedback and Practice Button
				Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackPractice();
				
				Thread.sleep(2000);
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				assignment
				.question1MultipleParts_CorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"1 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");

				
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				Thread.sleep(1000);
				sqp.clickNextQuestion();
				
				// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
				assignment
						.question6DragAndDrop_IncorrectAnswer();

				sqp.clickSubmitAnswer();
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(),
						true,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(),
						true,
						"Try Again Button is displayed");
				
				Thread.sleep(1000);
				sqp.clickTryAgain();
				
				Thread.sleep(1000);
				sqp.clickSubmitAnswer();
				
				// After attempt are exhausted - 2nd attempt, Check Show Feedback Overlay and Practice Button.
				Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");

				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				
				// Provide Incorrect Answer - practice attempt and Assert View Solution and Try Again buttons.
				assignment
						.question6DragAndDrop_IncorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				
				Assert.assertEquals(sqp.getHeaderScore(), "12%");
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"2 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				
				sqp.clickHeaderTitle(sap);
				
				Thread.sleep(2000);
				sap.clickQuestion(11);
				
				// Provide Incorrect Answer - 1st attempt and Assert View Solution and Try Again buttons.
				assignment.question11_IncorrectAnswer();

				sqp.clickSubmitAnswer();
				
				Thread.sleep(1000);
				Assert.assertEquals(
						sqp.isViewSolutionButtonDisplayed(), true,
						"View Solution Button is displayed");
				
				Assert.assertEquals(
						sqp.isTryAgainButtonDisplayed(), true,
						"Try Again Button is displayed");
				
				Thread.sleep(1000);
				sqp.clickViewSolution();
				
				// After View Solution, Check Practice Button.
				Thread.sleep(2000);
				Assert.assertEquals(sqp.isConfirmPopupWithViewSolutionAndCancelButtonsDisplayed(), true,
						"Confirm popup is displayed with \"View Solution\" & \"Cancel\" option?");
				Assert.assertEquals(sqp.getConfirmViewSolutionMessageText().trim(),
						"By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.",
						"Alert popup with 'By viewing the solution, you are giving up on this question. You will not be able to submit any further attempts or receive any more points.' message is displayed?");
				
				sqp.clickConfirmPopupViewSolutionButton();
				Thread.sleep(5000);
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("1st attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"3 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.isViewSolutionDisplayed(), true, 
						"Solution View is displayed");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				Thread.sleep(5000);
				
				// Do not enter answer and Click Check Practice.
				sqp.clickCheckPractice();
				
				Assert.assertEquals(sqp.getAreYouSureSubmitAnswergetText().trim(),
									"You have not answered all parts of this question. "
									+ "Are you sure you want to submit your answer?" );
							
				sqp.clickNoLetMeFinishButton();
				
				// Click Check Practice and Provide InCorrect Answer
				
				assignment.question11_IncorrectAnswer();
				
				sqp.clickCheckPractice();
				
				Assert.assertTrue(sqp.isAttemptDisplayed("practice attempt"));
				Assert.assertEquals(sqp.isFeedbackPracticeButtonDisplayed(), true,
						"Feedback overlay is displayed with \"Practice\" button?");
				
				sqp.clickFeedbackClose();
				Assert.assertEquals(sqp.isPracticeButtonDisplayed(), true,
						"Practice button is displayed in the footer?");
				
				sqp.clickPractice();
				Assert.assertTrue(sqp.isAttemptViewDisplayed("Practice Attempt"));
				Assert.assertEquals(sqp.getQuestionFooterLeftText(),
						"3 OF 20 QUESTIONS COMPLETED");
				Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
						"QUESTION COMPLETED");
				Assert.assertEquals(sqp.isCheckPracticeButtonDisplayed(), true,
						"CHECK PRACTICE button is displayed in the footer?");
				
				
				driver.close();
				driver.switchTo().window(studentWindow2);
				
				wait = new WebDriverWait(driver, 8);
				TimeUnit.SECONDS.sleep(8);

				SW5DLPstudent.logoutSmartwork5();
				
				
	}
	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
