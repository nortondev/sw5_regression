package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.QuestionDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyQuestionResetAllSettings_QDO_Part1 extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	CreateCustomAssignment createAssignment;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	ClassActivityReportPage classActivityReport;
	QuestionDetailOverlayPage questionDetailOverlay;

	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As an Instructor - Verify Question Reset All in Question Details Overlay - Part 1")
	@Stories("AS-217 - Edit Assignment: As an Instructor - Verify Question Reset All in Question Details Overlay - Part 1")
	@Test
	public void editAssignment_VerifyQuestionResetAllSettings_QDO_Part1() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(2);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		
		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		
		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		
		// INSTRUCTOR
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();
		
		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		String questionsSetName = SW5Constants.QUESTION_SET_3;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix 
						+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
        assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();
		
		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");
		
		assignment.selectshowstudentscore("Percentages");
	
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();
		

		
		// Login as Student 1 and submit the assignment.

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		
		//String studentName1 = testHelper.getStudentUserNameInUppercase(studentUserName1);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
			TimeUnit.SECONDS.sleep(5);
			
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow1 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow1);

			sap = new StudentAssignmentPage();
			
			Thread.sleep(2000);
			sap.beginAssignmentClick();
			
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			
			//Provide Correct Answer and Assert Feedback and Practice button.
			custassignment.question4Answers();
			
			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();

			// Navigate to Next Question
			
			sqp.clickNextQuestion();
			
			custassignment.question5Answers();
			
			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();
			
			Assert.assertEquals(sqp.getHeaderScore(), "50%");
			
			Thread.sleep(1000);
			assignment.returnToAssignmentList.click();
			
			// Assert Student score on Student Assignment page.
			Thread.sleep(2000);
			String student01_sapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student01_sapHeaderScore, "50%", 
					"Student average score on Custome assignment page \"50%\"?");
			
            Thread.sleep(1000);
            driver.close();
			driver.switchTo().window(parentWindow1);
			
			SW5DLPstudent.logoutSmartwork5();


			
		// Login as Student 2 and submit an assignment.
		
		String studentUserName2 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		
		//String studentName2 = testHelper.getStudentUserNameInUppercase(studentUserName2);
		
		new LoginAsStudent().LoginStudent(studentUserName2);
		
		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
		TimeUnit.SECONDS.sleep(5);
		
		SW5DLPstudent.SelectAssignment(assignmentTitle);
		
		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);

		sap = new StudentAssignmentPage();
	
		Thread.sleep(2000);
		sap.beginAssignmentClick();

		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
		
		//Provide Correct Answer and Assert Feedback and Practice button.
		custassignment.question4Answers();

		Thread.sleep(2000);
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackClose();

		// Navigate to Next Question

		sqp.clickNextQuestion();

		custassignment.question5Answers();

		Thread.sleep(2000);
		sqp.clickSubmitAnswer();

		sqp.clickFeedbackClose();
		
		Assert.assertEquals(sqp.getHeaderScore(), "50%");

		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();

		// Assert Student score on Student Assignment page.
		Thread.sleep(2000);
		String student02_sapHeaderScore = sap.getHeaderScore();
		Assert.assertEquals(student02_sapHeaderScore, "50%", 
				"Student average score on Custome assignment page \"50%\"?");

		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(parentWindow2);
		
		SW5DLPstudent.logoutSmartwork5();
		
		
		
		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		
		// Assert As an instructor check the class average and Submitted Grades
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("2", submittedGrades);
		
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("50%", averageGrade);
		
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor Assignment Window
		String winHandleBefore2 = driver.getWindowHandle();
		testHelper.getInstructorClassActivityReportWindow(driver);
		
		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("50%",
				classActivityReport.getCARAverageScoreOnAssignment(), "The Average Score");
		
		classActivityReport.clickQuestionsTab();

		classActivityReport.clickCARQuestionsTabOpenQuestionDetailOverlayButton(1);

		questionDetailOverlay = new QuestionDetailOverlayPage();
		
		//Assert Overall Score in Student Details Overlay before Extension.
		Thread.sleep(5000);
		String strOverallScore = questionDetailOverlay.getOverallScore();
		Assert.assertEquals(strOverallScore, "3pts");
		
		questionDetailOverlay.clickResetButton();
		
		questionDetailOverlay.clickResetButton_PopUp();
		
		//Assert updated Overall Score in Question Details Overlay after Reset.
		Thread.sleep(5000);
		String strOverallScore_updated = questionDetailOverlay.getOverallScore();
		Assert.assertEquals(strOverallScore_updated, "-");

		questionDetailOverlay.clickCloseQuestionDetailOverlay();
		
		//Assert updated Overall Score in CAR page after Question Reset.
		Thread.sleep(5000);
		Assert.assertEquals("20%",
				classActivityReport.getCARAverageScoreOnAssignment(), "The Average Score");
		
		// Return to Instructor Assignment page.
		assignment.returnToAssignmentList.click();
		
		// Assert Avg Grade Score on Instructor Assignment page.
		Thread.sleep(2000);
		String updatedGrade = assignment.getHeaderScore();
		Assert.assertEquals(updatedGrade, "20%", 
				"Student average score updated on Instructor Assignment page \"20%\"?");

		driver.close();
		driver.switchTo().window(winHandleBefore2);
		
		String updatedAvgGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("20%", updatedAvgGrade, 
				"Student Average Score updated on Instructor DLP \"20%\"?");
		
		SW5DLPpage.logoutSW5();
		
		
		// Login as Student 1 and verify updated grades.

		new LoginAsStudent().LoginStudent(studentUserName1);
		
		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		
		//Assert updated Exception Student Grade in Student DLP.
		Thread.sleep(2000);
		String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student01_Grade, "20%", 
				"Student average score on Student DLP \"20%\"?");
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
		TimeUnit.SECONDS.sleep(5);
		
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow3 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow3);

		sap = new StudentAssignmentPage();
		
		
		// Assert Student score on Student Assignment page.
		Thread.sleep(2000);
		String student01_UpdatedsapHeaderScore_AfterReset = sap.getHeaderScore();
		Assert.assertEquals(student01_UpdatedsapHeaderScore_AfterReset, "20%", 
				"Student average score updated on Custom assignment page \"20%\"?");
	
		Thread.sleep(2000);
		sap.resumeAssignmentClick();
		
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
		
		//Check updated Student Grade on Student Question player after Question Reset.
		Assert.assertEquals(sqp.getHeaderScore(), "20%");

		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();

		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(parentWindow3);
		SW5DLPstudent.logoutSmartwork5();
		
		
		
		// Login as Student 2 and verify updated grades.

		new LoginAsStudent().LoginStudent(studentUserName2);
		
		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		// Assert updated Exception Student Grade in Student DLP.
		Thread.sleep(2000);
		String student02_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student02_Grade, "20%", 
				"Student average score on Student DLP \"20%\"?");
			
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
		TimeUnit.SECONDS.sleep(5);
		
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow4 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow4);

		sap = new StudentAssignmentPage();

		// Assert Student score on Student Assignment page.
		Thread.sleep(2000);
		String student02_sapHeaderScore_AfterReset = sap.getHeaderScore();
		Assert.assertEquals(student02_sapHeaderScore_AfterReset, "20%",
				"Student average score updated on Custom assignment page \"20%\"?");

		Thread.sleep(2000);
		sap.resumeAssignmentClick();

		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
		
		//Check updated Student Grade on Student Question player after Question Reset.
		Assert.assertEquals(sqp.getHeaderScore(), "20%");

		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();

		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(parentWindow4);

		SW5DLPstudent.logoutSmartwork5();


	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}
