package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyLargeSizePoolQuestionsException extends PropertiesFile {
	
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginAsInstructor logininst;
	CreateNewStudentSet createStudentset;
	ManageStudentSetsPage managestudentsetpage;
	LoginPage DLPpage;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	CustomAssignmentAnswers custassignment;
	TestHelper testHelper = new TestHelper();

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("As an Instructor - Verify large size pool questions exception.")
	@Stories("AS-224 Edit Assignment - Verify large size pool questions exception.")
	@Test
	public void VerifyLargeSizePoolQuestionsException() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(1);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null) throw new Exception(
				"Please Add Test Case 'SetupTestData' as first test cases of current suit.");


		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		Thread.sleep(3000);
		String AssignmentName = "LargeSize_PoolQuestions";
		
		assignment.enterAssignmentName(AssignmentName);
		assignment.enterGAUDate(GetDate.getCurrentDate());
		
		Thread.sleep(2000);
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		String questionsSetName = SW5Constants.QUESTION_SET_8;
		
		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.clickCreateNewPoolQuestionsButton();
		
		String PoolTitleSuffix = "LargePoolQuestions ";
		int suffixCharCount = 5;
		
		String PoolQuestionTitle =  PoolTitleSuffix+GetRandomId.randomAlphaNumeric(
				suffixCharCount).toLowerCase();
		
		questionpage.createLargePoolQuestions(PoolQuestionTitle, questionsSetName, 20);
		
		Thread.sleep(3000);
		createAssignment.yourCurrentAssignment();
		
		questionpage.addQuestions();
		questionpage.addLargePoolQuestionsToAssignment(PoolQuestionTitle, 21);
		createAssignment.yourCurrentAssignment();
		
		questionpage.addQuestions();
		createAssignment.addQuestionsWithQuestionID(PoolQuestionTitle);
		
		Assert.assertTrue(createAssignment.isMaximumLargePoolQuestionsExceptionDisplayed());
		createAssignment.yourCurrentAssignment();

		Thread.sleep(3000);
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		String parentWindow1 = driver.getWindowHandle();
		assignment.clickPreviewButton();
		Assert.assertTrue(assignment.isPreviewButtonDisplayed());
		
		
		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver,
				winHandleBefore1, parentWindow1);
		
		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();
		
		custassignment = new CustomAssignmentAnswers();
		instPreviewAssignmentPage.beginAssignmentClick();
		
		sqp = new StudentQuestionPlayer();
		
		Thread.sleep(3000);
		Assert.assertEquals(sqp.QuestionNo.getText(), "01");

		driver.close();
		driver.switchTo().window(parentWindow1);
		
		Thread.sleep(3000);
		driver.close();
		
		driver.switchTo().window(winHandleBefore1);
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();
		

		// Login as Student 1

		String studentUserName1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			SW5DLPstudent.SelectAssignment(AssignmentName);

			// Assignment Window
			String parentWindow2 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow2);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			custassignment = new CustomAssignmentAnswers();
			
			Thread.sleep(2000);
			sap.beginAssignmentClick();
			
			
			Thread.sleep(3000);
			Assert.assertEquals(sqp.QuestionNo.getText(), "01");
			
            Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//div[@class='head-title fl']/a")));
			driver.findElement(By.xpath("//div[@class='head-title fl']/a"))
					.click();
			
			//Assert Header Score on Student Assignment page.
			Thread.sleep(3000);
			Assert.assertEquals(sap.getHeaderScore(), "-- %");
			
			driver.close();
			driver.switchTo().window(parentWindow2);
			
			//Assert Student Grade in Student DLP.
			Thread.sleep(3000);
			SW5DLPpage = new SW5DLPPage();
			String student01_Grade = SW5DLPpage.getAverageGradeText(AssignmentName);
			Assert.assertEquals(student01_Grade, "—");
			
			SW5DLPstudent.logoutSmartwork5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}

