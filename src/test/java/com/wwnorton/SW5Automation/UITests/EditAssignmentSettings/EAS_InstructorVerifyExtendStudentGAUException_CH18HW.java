	package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishPremadeAssignmentCH18HW;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH18HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_InstructorVerifyExtendStudentGAUException_CH18HW extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH18HW assignmentCH18HW;
	AssignmentPage assignment;
	InstructorPublishPremadeAssignmentCH18HW assignmentSettings;

	SW5DLPPage SW5DLPpage;
	LoginPage SW5Login;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}
	
		@Severity(SeverityLevel.NORMAL)
		@Description("Edit Assignment: As an Instructor extend the assignment GAU+late penalty for "
				+ "exceptional student in CAR-student Details post students submission.")
		@Stories("AS-23 - Edit Assignment: Verify Student GAU Exception post students submission.")
		@Test
		
		public void editAssignment_ExtendStudentGAUException_CH18HW() throws Exception {

			driver = getDriver();
			
			SeleniumTestsContext gContext = SeleniumTestsContextManager
					.getGlobalContext();
			
			String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
			String assignmentTitle = (String) gContext.getAttribute(SW5Constants.ASSIGNMENT_CH18HW);
			String GAUTime = (String) gContext.getAttribute(SW5Constants.GAUTime_CH18HW);
			
			//String studentSetId = "331453";
			//String assignmentTitle = SW5Constants.ASSIGNMENT_CH18HW;
			//String GAUTime = "12:00 PM";
			
			if (studentSetId == null)
				throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");
		
			
			// Login as Student 1 and submit the assignment.

			String studentUserName1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
			String studentName1 = testHelper.getStudentUserNameInUppercase(studentUserName1);
			
			//String studentUserName1 = "john_mercey-4nuwijlzhy@mailinator.com";
			//String studentName1 = "MERCEY-4NUWIJLZHY, JOHN";
			

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
			TimeUnit.SECONDS.sleep(5);
			
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow1 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow1);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			assignmentCH18HW = new AssignmentCH18HW();

			// Assert Assignment GAU Info on Student Assignment page.
			Thread.sleep(2000);
			String LatePenaltyText_Student01 = "Your work is overdue! Late penalties now apply (50% per day) "
					+ "and you have 1 day left to complete the assignment";
			String strLatePenaltyAlert_student01 = sap.getLatePenaltyAlert();

			Assert.assertTrue(LatePenaltyText_Student01.trim().contains(strLatePenaltyAlert_student01.trim()),
					"Assignment OverDue message is displayed correctly with Late Penalty information?");

			Thread.sleep(2000);
			sap.beginAssignmentClick();
			//sap.resumeAssignmentClick();
			
			// Assert that Late Penalty Information is displayed for Student 01.
			Thread.sleep(5000);
			Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), true);


			// Provide Correct Answer and Assert Feedback and Practice button.
			assignmentCH18HW.question1_CorrectAnswer();

			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();

			Thread.sleep(1000);
			sqp.clickHeaderTitle(sap);

			// Assert Student score on Student Assignment page.
			String student01_sapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student01_sapHeaderScore, "6%",
					"Student average score on Custome assignment page \"6%\"?");

			Thread.sleep(1000);
			driver.close();
			driver.switchTo().window(parentWindow1);

			SW5DLPstudent.logoutSmartwork5();

			
			
			// Login as Student 2 and submit an assignment.

			String studentUserName2 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
			//String studentUserName2 = "john_mercey-9lh3mktzb0@mailinator.com";
			
			new LoginAsStudent().LoginStudent(studentUserName2);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();
			
			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
			TimeUnit.SECONDS.sleep(5);
			
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow2 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow2);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			assignmentCH18HW = new AssignmentCH18HW();

			// Assert Assignment GAU Info on Student Assignment page.
			Thread.sleep(2000);
			String LatePenaltyText_Student02 = "Your work is overdue! Late penalties now apply (50% per day) "
					+ "and you have 1 day left to complete the assignment";
			String strLatePenaltyAlert_student02 = sap.getLatePenaltyAlert();

			Assert.assertTrue(LatePenaltyText_Student02.trim().contains(strLatePenaltyAlert_student02.trim()),
					"Assignment OverDue message is displayed correctly with Late Penalty information?");

			Thread.sleep(2000);
			sap.beginAssignmentClick();

			// Assert that Late Penalty Information is displayed for Student 01.
			Thread.sleep(5000);
			Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), true);

			// // Provide Correct Answer and Assert Feedback and Practice button.
			assignmentCH18HW.question1_CorrectAnswer();

			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();

			Thread.sleep(1000);
			sqp.clickHeaderTitle(sap);

			// Assert Student score on Student Assignment page.
			String student02_sapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student02_sapHeaderScore, "6%",
					"Student average score on Custome assignment page \"6%\"?");

			Thread.sleep(1000);
			driver.close();
			driver.switchTo().window(parentWindow2);

			SW5DLPstudent.logoutSmartwork5();
			
			
			// INSTRUCTOR
			new LoginAsInstructor().loginInstructor();

			SW5DLPpage = new SW5DLPPage();
			SW5DLPpage.selectByPartOfVisibleText(studentSetId);
			LogUtil.log("Student ID is " + studentSetId);
			Thread.sleep(3000);

			SW5DLPpage.clickReportsButton(assignmentTitle);

			String winHandleBefore = driver.getWindowHandle();
			
			// Instructor Assignment Window
			testHelper.getInstructorClassActivityReportWindow(driver);

			classActivityReport = new ClassActivityReportPage();
			classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(studentName1);

			studentDetailOverlay = new StudentDetailOverlayPage();
			Thread.sleep(5000);

			// Assert Overall Score in Student Details Overlay before Extension.
			String strOverallScore = studentDetailOverlay.getOverallScore();
			Assert.assertEquals(strOverallScore, "6%");

			studentDetailOverlay.clickEditButton();

			Thread.sleep(2000);
			String ExtendedGAUTime = studentDetailOverlay.getSetGAUTime();

			studentDetailOverlay.clickSaveButton();

			// Assert updated Overall Score in Student Details Overlay after Extension.
			Thread.sleep(5000);
			String strOverallScore_updated = studentDetailOverlay.getOverallScore();
			Assert.assertEquals(strOverallScore_updated, "11%");
			
			String strMain = ExtendedGAUTime;
		    String[] arrSplit = strMain.split(" ");
		    String  updatedExtendedGAUTime = arrSplit[0]+arrSplit[1];
		    

			// Assert Updated GAU Text on Student Details Overlay.
			String assignmentInfoBoxText = "Grades are accepted until " + GetDate.getFormattedDate(0)
					+ " at " + updatedExtendedGAUTime +" (Eastern Time)";
			String updatedGAUText = studentDetailOverlay.getGradeText();
			Assert.assertEquals(updatedGAUText.trim(), assignmentInfoBoxText.trim());

			String exceptionText = "*This student has exceptions applied.";
			String strEexceptionAApplied = studentDetailOverlay.getExceptionAppliedText();
			Assert.assertEquals(strEexceptionAApplied.trim(), exceptionText.trim());

			studentDetailOverlay.clickCloseStudentDetailOverlay();

			// Assert GAU Text on CAR page.
			Thread.sleep(2000);
			String GAUText_CAR = "Grades were accepted for this assignment until " + GetDate.getFormattedDate(0)
					+ ", at " + GAUTime + " (Eastern Time)";
			String strGAU = classActivityReport.getGAUText_CarPage();
			Assert.assertEquals(strGAU.trim(), GAUText_CAR.trim());

			// Assert GAU Text on Instructor Assignment page.
			classActivityReport.clickSmartwork5LinkfromCarPage();


			// Assert Assignment GAU Info on Instructor Assignment page.
			Thread.sleep(2000);
			String LatePenaltyText_Instructor = "Grades were accepted until " + GetDate.getFormattedDate(0)
					+ ", at "+ GAUTime +" (Eastern Time). Late work is  accepted 1 day after the GAU at "
					+ "50% penalty per day";
			String strLatePenaltyAlert_Instructor = sap.getGAUUntilAlert_Instructor();
			Assert.assertTrue(LatePenaltyText_Instructor.trim().contains(strLatePenaltyAlert_Instructor.trim()),
					"Assignment GAU message is displayed correctly with Late Penalty information?");

			driver.close();
			driver.switchTo().window(winHandleBefore);

			// Assert GAU Date associated to Custom Assignment on Instructor DLP.
			Thread.sleep(2000);
			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, 0);

			DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yy");
			String date1 = dateFormat1.format(cal1.getTime());

			String gauDate_Instructor = date1 + " " + GAUTime;
			String strGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(assignmentTitle);
			Assert.assertEquals(strGAUDate_Instructor, gauDate_Instructor);

			SW5DLPpage.logoutSW5();

			
			
			// Login as Student 1 and submit the assignment.

			new LoginAsStudent().LoginStudent(studentUserName1);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();

			// Assert updated Exception Student Grade in Student DLP.
			Thread.sleep(2000);
			String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student01_Grade, "11%", "Student average score on Student DLP \"11%\"?");

			// Assert GAU Date associated to Custom Assignment on Student DLP for
			// Exceptional Student.
			Thread.sleep(2000);
			String gauDate_Student01 = SW5DLPpage.getSystemGAUDate_Student(0);

			String strGAUDate_Student01 = SW5DLPpage.getGAUDate_Student(assignmentTitle);
			Assert.assertEquals(strGAUDate_Student01, gauDate_Student01);

			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
			TimeUnit.SECONDS.sleep(5);
			
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow3 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow3);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			assignmentCH18HW = new AssignmentCH18HW();

			// Assert Student score on Student Assignment page.
			Thread.sleep(2000);
			String student01_UpdatedsapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student01_UpdatedsapHeaderScore, "11%",
					"Student average score updated on Student assignment page \"11%\"?");

			// Assert Assignment GAU Info on Student Assignment page.
			Thread.sleep(2000);
			String CurrentDate_Student01 = GetDate.getFormattedDate(0);
			String updatedLatePenaltyText_Student01 = "Your assignment is due on " + CurrentDate_Student01
					+ ", at " + ExtendedGAUTime +" (Eastern Time). Late work is accepted 1 day  after the due date, "
					+ "at 50% penalty per day";
			String strUpdatedLatePenaltyAlert_student01 = sap.getGAUUntilAlert_Student();
			Assert.assertTrue(updatedLatePenaltyText_Student01.trim().contains(strUpdatedLatePenaltyAlert_student01.trim()),
					"Assignment GAU message is displayed correctly with Late Penalty information?");

			Thread.sleep(2000);
			sap.clickQuestion(9);

			assignmentCH18HW.question9Mixed_CorrectAnswer();

			Thread.sleep(1000);
			sqp.clickSubmitAnswer();

			sqp.clickFeedbackClose();

			Thread.sleep(1000);
			sqp.clickHeaderTitle(sap);

			// Assert Student score on Student Assignment page.
			Thread.sleep(2000);
			String student01_UpdatedsapHeaderScore_AfterSubmission = sap.getHeaderScore();
			Assert.assertEquals(student01_UpdatedsapHeaderScore_AfterSubmission, "22%",
					"Student average score updated on Custom assignment page \"22%\"?");

			Thread.sleep(1000);
			driver.close();
			driver.switchTo().window(parentWindow3);

			// Assert Student grade on Student DLP page.
			Thread.sleep(2000);
			String student01_UpdatedGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student01_UpdatedGrade, "22%", 
					"Student average score updated on Student DLP \"22%\"?");

			SW5DLPstudent.logoutSmartwork5();

			
			// Login as Student 2 and submit an assignment.

			new LoginAsStudent().LoginStudent(studentUserName2);

			// Select mentioned Assignment
			SW5DLPstudent = new SW5DLPStudent();

			// Assert updated Exception Student Grade in Student DLP.
			Thread.sleep(2000);
			String student02_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student02_Grade, "6%", "Student average score on Student DLP \"6%\"?");

			// Assert GAU Date associated to Custom Assignment on Student DLP for
			// Exceptional Student.
			Thread.sleep(2000);
			String gauDate_Student02 = SW5DLPpage.getSystemGAUDate_Student(0);

			String strGAUDate_Student02 = SW5DLPpage.getGAUDate_Student(assignmentTitle);
			Assert.assertEquals(strGAUDate_Student02, gauDate_Student02);

			wait = new WebDriverWait(driver, 5);
			TimeUnit.SECONDS.sleep(5);
			SW5DLPstudent.scrollToAssignmentTitle(assignmentTitle);
			TimeUnit.SECONDS.sleep(5);
			
			SW5DLPstudent.SelectAssignment(assignmentTitle);

			// Assignment Window
			String parentWindow4 = driver.getWindowHandle();
			testHelper.getStudentAssignmentWindow(driver, parentWindow4);

			sap = new StudentAssignmentPage();
			sqp = new StudentQuestionPlayer();
			assignmentCH18HW = new AssignmentCH18HW();

			// Assert Student score on Student Assignment page.
			Thread.sleep(2000);
			String student02_UpdatedsapHeaderScore = sap.getHeaderScore();
			Assert.assertEquals(student02_UpdatedsapHeaderScore, "6%",
					"Student average score updated on Custom assignment page \"6%\"?");

			// Assert Assignment GAU Info on Student Assignment page.
			Thread.sleep(2000);
			String updatedLatePenaltyText_Student02 = "Your work is overdue! Late penalties now apply (50% per day) "
					+ "and you have 1 day left to complete the assignment";
			String strUpdatedLatePenaltyAlert_student02 = sap.getLatePenaltyAlert();
			Assert.assertTrue(updatedLatePenaltyText_Student02.trim().contains(strUpdatedLatePenaltyAlert_student02.trim()),
					"Assignment OverDue message is displayed correctly with Late Penalty information?");

			Thread.sleep(2000);
			sap.clickQuestion(9);

			assignmentCH18HW.question9Mixed_CorrectAnswer();
	
			Thread.sleep(1000);
			sqp.clickSubmitAnswer();
	
			sqp.clickFeedbackClose();

			Thread.sleep(1000);
			sqp.clickHeaderTitle(sap);

			// Assert Student score on Student Assignment page.
			Thread.sleep(2000);
			String student02_UpdatedsapHeaderScore_AfterSubmission = sap.getHeaderScore();
			Assert.assertEquals(student02_UpdatedsapHeaderScore_AfterSubmission, "11%",
					"Student average score updated on Custom assignment page \"11%\"?");

			Thread.sleep(1000);
			driver.close();
			driver.switchTo().window(parentWindow4);

			// Assert Student grade on Student DLP page.
			Thread.sleep(2000);
			String student02_UpdatedGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
			Assert.assertEquals(student02_UpdatedGrade, "11%", 
					"Student average score updated on Student DLP \"11%\"?");

			SW5DLPstudent.logoutSmartwork5();

		}

		// End of Test cases
		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
	}
