package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_VerifyGradesForEditedQuestion extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;
	SW5DLPPage SW5DLPpage;
	
	String studentHeaderGradeScore;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment: As an Instructor edit the question in Instructor assignment settings.")
	@Stories("AS-25 - As an Instructor edit the question and verify grades on Student submission.")
	@Test
	public void editAssignment_VerifyGradesForEditedQuestion() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		String questionsSetName = SW5Constants.QUESTION_SET_3;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix 
						+ GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		assignment.LateworkCheckbox("1", "50");

		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();

		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");

		assignment.selectshowstudentscore("Percentages");

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();
		
			Thread.sleep(2000);
			assignment.selectAttempts("2");
			assignment.selectGradePanelties("10%");
			assignment.ApplyToAllButton.click();

			Thread.sleep(2000);
		
		String assignmentTitle = createAssignment.getAssignmentTitle();
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		String editQuestionTitle = questionpage.editQuestion();
		
		wait.until(ExpectedConditions.visibilityOf(questionpage.copyQuestionTitle));
		Assert.assertEquals(questionpage.copyQuestionTitle.getText().
				equalsIgnoreCase(editQuestionTitle), true);
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[@class='allQuestionBack']/td[4]/div/input[@type='text']")));
		ReusableMethods.scrollToElement(driver, By.xpath("//tr[@class='allQuestionBack']/td[4]/div/input[@type='text']"));
		assignment.selectPoints("2");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", assignment.ApplyToAllButton);
		//assignment.ApplyToAllButton.click();
		
		Thread.sleep(2000);
		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		SW5DLPpage.logoutSW5();


		// Login as Student

		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
				
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();

		sap.beginAssignmentClick();
		
		custassignment.question4_IncorrectAnswers();
		sqp.clickSubmitAnswer();
		
		// Check correct Grades with Incorrect attempt.
		Assert.assertTrue(sqp.isAttemptDisplayed("1st attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "0%");

		sqp.clickFeedbackClose();
		
		sqp.clickTryAgain();

		custassignment.question4Answers();
		sqp.clickSubmitAnswer();
		
		// Check correct Grades with Incorrect attempt.
		Assert.assertTrue(sqp.isAttemptDisplayed("2nd attempt"));
		Assert.assertEquals(sqp.getHeaderScore(), "18%");

		sqp.clickFeedbackClose();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[@class='head-title fl']/a")));
		driver.findElement(By.xpath("//div[@class='head-title fl']/a")).click();
		
		//Assert Header Score on Student Assignment page.
		Thread.sleep(2000);
		studentHeaderGradeScore = sap.getHeaderScore();
		Assert.assertEquals("18%", studentHeaderGradeScore, 
				"The Header Score on Student Assignment Page.");
		LogUtil.log(studentHeaderGradeScore);
		
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student01_Grade, "18%", 
				"Student average score on Student DLP \"18%\"?");
		
		SW5DLPstudent.logoutSmartwork5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
