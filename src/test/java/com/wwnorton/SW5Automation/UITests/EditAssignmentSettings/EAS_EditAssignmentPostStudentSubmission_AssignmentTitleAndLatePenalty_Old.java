package com.wwnorton.SW5Automation.UITests.EditAssignmentSettings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CreateCustomAssignment;
import com.wwnorton.SW5Automation.objectFactory.CreateNewStudentSet;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentAnswers;
import com.wwnorton.SW5Automation.objectFactory.ManageStudentSetsPage;
import com.wwnorton.SW5Automation.objectFactory.Questions_Page;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.UpdateStudentSet;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.GetDate;
import com.wwnorton.SW5Automation.utilities.GetRandomId;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class EAS_EditAssignmentPostStudentSubmission_AssignmentTitleAndLatePenalty_Old extends PropertiesFile {

	/*WebDriver driver = BaseDriver.getDriver();*/
	TestHelper testHelper = new TestHelper();
	InstructorPublishCustomAssignment instCustAssignment;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	CustomAssignmentAnswers custassignment;
	AssignmentPage assignment;
	CreateCustomAssignment createAssignment;
	Questions_Page questionpage;

	SW5DLPPage SW5DLPpage;
	ClassActivityReportPage classActivityReport;
	StudentDetailOverlayPage studentDetailOverlay;
	ManageStudentSetsPage managestudentsetpage;
	UpdateStudentSet updatestudentset;
	CreateNewStudentSet createStudentset;
	
	String studentHeaderGradeScore;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Edit Assignment Page - Verify that Instructor can edit assignment post Student submissions.")
	@Stories("AS-17 - Edit Assignment: Update the assignment title, late penalty post Student submissions.")
	@Test
	public void editAssignmentTitleAndLatePenalty_PostStudentSubmission() throws Exception {
		
		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();

		String studentSetId = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		if (studentSetId == null)
			throw new Exception("Please Add Test Case 'SetupTestData' as first test cases of current suit.");

		// INSTRUCTOR

		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		createAssignment = new CreateCustomAssignment();
		createAssignment.createAssignment();

		String winHandleBefore1 = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		String questionsSetName = SW5Constants.QUESTION_SET_1;
		String customAssignmentPrefix = "CustomAssignment ";
		int suffixCharCount = 5;
		int pointsPerQuestion = 2;

		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		assignment.AssignmentName
				.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());

		Thread.sleep(3000);
		assignment.adaptiveOff();

		assignment.enterGAUDate(GetDate.getCurrentDate());
		assignment.gettimeZone();
		assignment.selecttimeZone("(GMT-05:00) Eastern Time");
		
		assignment.LateworkCheckbox("1", "10");

		Thread.sleep(2000);
		assignment.ShowAdditionalSettingsButton();

		Thread.sleep(2000);
		assignment.selectUngradedPractice("Never");
		assignment.selectShowFeedback("After every attempt");
		assignment.selectshowsolution("Any time (students may 'give up' and view the solution)");

		assignment.selectshowstudentscore("Percentages");

		questionpage = new Questions_Page();
		questionpage.addQuestions();
		questionpage.searchQuestions();
		createAssignment.addCustomQuestions(questionsSetName);
		createAssignment.getQuestioncount();
		createAssignment.yourCurrentAssignment();

		if (pointsPerQuestion > 0) {
			assignment.selectPoints(String.valueOf(pointsPerQuestion));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']")));
			ReusableMethods.scrollToElement(driver, By.xpath("//button[@class='btn-submit-xs btn-info clear-margin']"));
			assignment.selectAttemptsforQuestion("2");
			Thread.sleep(2000);
		}

		String assignmentTitle = createAssignment.getAssignmentTitle();

		assignment.publishButton();
		Assert.assertTrue(assignment.isUnpublishButtonDisplayed());
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		String winHandleBefore2 = driver.getWindowHandle();
		
		Thread.sleep(2000);
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		//String currentGAUDateMinusOne = GetDate.addDays(-1);
		
		Thread.sleep(2000);
		//assignment.resetSW5GAUDate(currentGAUDateMinusOne);
		assignment.enterGAUDate(GetDate.addDays(-1));
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		SW5DLPpage.logoutSW5();


		// Login as Student

		String studentUserName = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);

		String StudentName = testHelper.getStudentUserNameInUppercase(studentUserName).trim();

		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
				
		SW5DLPstudent.SelectAssignment(assignmentTitle);

		// Assignment Window
		String parentWindow1 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow1);

		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();
			
		// Assert Assignment GAU Info on Student Assignment page.
		Thread.sleep(2000);
		String LatePenaltyText = "Your work is overdue! Late penalties now apply (10% per day) "
				+ "and you have 0 days left to complete the assignment";
		String strLatePenaltyAlert = sap.getLatePenaltyAlert();
		Assert.assertTrue(LatePenaltyText.trim().contains(strLatePenaltyAlert.trim()),
				"Assignment OverDue message is displayed correctly with Late Penalty information?");

		sap.beginAssignmentClick();

		// Assert that Late Penalty Information is displayed for Student.
		Thread.sleep(5000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), true);

		customQuestionsAnswerAssignmentStudent();

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[@class='head-title fl']/a")));
		driver.findElement(By.xpath("//div[@class='head-title fl']/a")).click();
		driver.close();
		driver.switchTo().window(parentWindow1);
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String student01_Grade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals(student01_Grade, "35%", "Student average score on Student DLP \"35%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		Thread.sleep(2000);	
		String gauDate_Student = SW5DLPpage.getSystemGAUDate_Student(-1);

		String strGAUDate_Student = SW5DLPpage.getGAUDate_Student(assignmentTitle);
		Assert.assertEquals(strGAUDate_Student, gauDate_Student);
		
		SW5DLPstudent.logoutSmartwork5();

		
			
		// Login as INSTRUCTOR to update Assignment Title and Late Penalty.
		
		new LoginAsInstructor().loginInstructor();

		
		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore3 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		//As an instructor check the Class Average and Submitted Grades on Instructor DLP page.
		String submittedGrades = SW5DLPpage.getSubmittedGrades(assignmentTitle);
		Assert.assertEquals("1", submittedGrades);
		
		String averageGrade = SW5DLPpage.getAverageGradeText(assignmentTitle);
		Assert.assertEquals("35%", averageGrade);
		
		// Assert GAU Date associated to Custom Assignment on Instructor DLP.
		Thread.sleep(2000);
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, -1);

		DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yy");
		String date1 = dateFormat1.format(cal1.getTime());

		String gauDate_Instructor = date1 + " 11:59 PM";
		String strGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(assignmentTitle);
		Assert.assertEquals(strGAUDate_Instructor, gauDate_Instructor);
		
		ReusableMethods.scrollToElement(driver, By.linkText(assignmentTitle));
		SW5DLPpage.clickReportsButton(assignmentTitle);
		
		// Instructor Assignment Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();
		Assert.assertEquals("35%",
				classActivityReport.getCARAverageScoreOnAssignment(),
				"The Average Score");
		LogUtil.log("The Average Score",
				classActivityReport.getCARAverageScoreOnAssignment());
		
		
		studentDetailOverlay = new StudentDetailOverlayPage();
		
		//Student Grade details 
		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);
		//classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton("ACCOUNT5, TEST");

		Thread.sleep(5000);
		
		//Assert Overall Score in Student Details Overlay.
		String strOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(strOverallScore, "35%");
		
		// Assert GAU Text on Student Details Overlay.
		String assignmentInfoBoxText = "Grades were accepted until " + GetDate.getFormattedDate(-1) + 
				" at 11:59PM (Eastern Time)";
		String GAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(GAUText.trim(), assignmentInfoBoxText.trim());
		
		studentDetailOverlay.clickCloseStudentDetailOverlay();
		
		// Assert GAU Text on CAR page.
		Thread.sleep(2000);
		String CurrentDateMinus1_CAR = GetDate.getFormattedDate(-1);
		String GAUText_CAR = "Grades were accepted for this assignment until " 
		+ CurrentDateMinus1_CAR + ", at 11:59 PM (Eastern Time)"; 
		String strGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(strGAU.trim(), GAUText_CAR.trim());
		
		// Assert GAU Text on Instructor Assignment page.
		
		assignment.returnToAssignmentList.click();
		
		// Assert Assignment GAU Info on Instructor Assignment page.
		Thread.sleep(2000);
		String CurrentDateMinus1_Instructor = GetDate.getFormattedDate(-1);
		String LatePenaltyText_Instructor = "Grades were accepted until " + CurrentDateMinus1_Instructor 
				+ ", at 11:59 PM (Eastern Time). Late work is  accepted 1 day after the GAU at "
				+ "10% penalty per day";
		String strLatePenaltyAlert_Instructor = sap.getGAUUntilAlert_Instructor();
		Assert.assertTrue(LatePenaltyText_Instructor.trim().contains(strLatePenaltyAlert_Instructor.trim()), 
				"Assignment GAU message is displayed correctly with Late Penalty information?");
		
		String headerScore = assignment.getHeaderScore();
		Assert.assertEquals("35%", headerScore, 
				"The Header Score on Instructor Assignment Page.");

		driver.close();
		driver.switchTo().window(winHandleBefore3);

		// /As an instructor log into and Edit Assignment Title and Penalty period.
		String winHandleBefore4 = driver.getWindowHandle();
		SW5DLPpage.ClickAssignmentlink(assignmentTitle);
		
		testHelper.getInstructorAssignmentWindow(driver);
		assignment = new AssignmentPage();
		
		assignment.editAssignmentbutton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
		
		assignment.AssignmentName.clear();
		
		Thread.sleep(2000);
		assignment.AssignmentName
		.sendKeys(customAssignmentPrefix + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase());
		
		Thread.sleep(2000);
		//assignment.resetSW5GAUDate(GetDate.getCurrentDate());
		assignment.enterGAUDate(GetDate.addDays(0));
		
		Thread.sleep(2000);
		assignment.LateworkCheckbox("1", "50");
		
		ReusableMethods.scrollToBottom(driver);
		assignment.saveButton();
		
		Thread.sleep(2000);
		//String updatedAssignmentTitle = createAssignment.getAssignmentTitle();
		String updatedAssignmentTitle = createAssignment.AssignmentName.getAttribute("value");
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore4);
		
		Thread.sleep(2000);
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);
		
		String winHandleBefore5 = driver.getWindowHandle();

		SW5DLPpage.ClickAssignmentlink(updatedAssignmentTitle);
		testHelper.getInstructorAssignmentWindow(driver);
		
		assignment = new AssignmentPage();
		assignment.editAssignmentbutton();
		
		Thread.sleep(2000);
		//assignment.resetSW5GAUDate(currentGAUDateMinusOne);
		assignment.enterGAUDate(GetDate.addDays(-1));
		
		Thread.sleep(2000);
		assignment.saveButton();
		
		Thread.sleep(2000);
		assignment.returnToAssignmentList.click();
		
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.close();
		driver.switchTo().window(winHandleBefore5);
		
		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();


		//Login as Student User
		
		new LoginAsStudent().LoginStudent(studentUserName);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();
		
		//Assert Student Grade in Student DLP.
		Thread.sleep(2000);
		String strStudent_Grade = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals(strStudent_Grade, "39%", 
				"Student average score on Student DLP \"39%\"?");

		// Assert GAU Date associated to Custom Assignment on Student DLP.
		String updatedGAUDate_Student = SW5DLPpage.getSystemGAUDate_Student(-1);

		String updatedStrGAUDate_Student = SW5DLPpage.getGAUDate_Student(updatedAssignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Student, updatedGAUDate_Student);
		
		SW5DLPstudent.SelectAssignment(updatedAssignmentTitle);

		// Assignment Window
		String parentWindow2 = driver.getWindowHandle();
		testHelper.getStudentAssignmentWindow(driver, parentWindow2);
		
		sap = new StudentAssignmentPage();
		sqp = new StudentQuestionPlayer();
		custassignment = new CustomAssignmentAnswers();

		//Assert Header Score on Student Assignment page.
		studentHeaderGradeScore = sap.getHeaderScore();
		Assert.assertEquals("39%", studentHeaderGradeScore, 
				"The Header Score on Student Assignment Page.");
		LogUtil.log(studentHeaderGradeScore);
		
		// Assert Assignment GAU Info on Student Assignment page.
		String updatedLatePenaltyText = "Your work is overdue! Late penalties now apply (50% per day) "
				+ "and you have 0 days left to complete the assignment";
		String updatedStrLatePenaltyAlert = sap.getLatePenaltyAlert();
		Assert.assertTrue(updatedLatePenaltyText.trim().contains(updatedStrLatePenaltyAlert.trim()),
				"Assignment OverDue message is displayed correctly with Late Penalty information?");

		sap.resumeAssignmentClick();
		
		//Assert that Late Penalty Information is displayed for Student.
		Thread.sleep(3000);
		Assert.assertEquals(sqp.isLatePenaltyInfoDisplayed(), true);

		sqp.clickNextQuestion();

		custassignment.question5Answers();
		submitCustomAssignmentAnswerAndAssert("5 OF 12 QUESTIONS COMPLETED", "43%");

		driver.close();
		driver.switchTo().window(parentWindow2);
		
		Thread.sleep(2000);
		String updatedStudent_Grade = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals(updatedStudent_Grade, "43%", 
				"Student average score on Student DLP \"43%\"?");
		
		Thread.sleep(2000);
		SW5DLPstudent.logoutSmartwork5();
		
		
		// Login as INSTRUCTOR again to validate updated Assignment Title and Late Penalty.
		
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore6 = driver.getWindowHandle();
		SW5DLPpage.selectByPartOfVisibleText(studentSetId);

		// As an instructor check the Class Average and Submitted Grades on Instructor
		// DLP page.
		String updatedSubmittedGrades = SW5DLPpage.getSubmittedGrades(updatedAssignmentTitle);
		Assert.assertEquals("1", updatedSubmittedGrades);

		String averageGradeafterAssignmentUpdate = SW5DLPpage.getAverageGradeText(updatedAssignmentTitle);
		Assert.assertEquals("43%", averageGradeafterAssignmentUpdate);

		// Assert GAU Date associated to Custom Assignment on Instructor DLP.
		Thread.sleep(2000);
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);

		DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yy");
		String date2 = dateFormat2.format(cal2.getTime());

		String updatedGAUDate_Instructor = date2 + " 11:59 PM";
		String updatedStrGAUDate_Instructor = SW5DLPpage.getGAUDate_Instructor(updatedAssignmentTitle);
		Assert.assertEquals(updatedStrGAUDate_Instructor, updatedGAUDate_Instructor);

		Thread.sleep(5000);
		SW5DLPpage.clickReportsButton(updatedAssignmentTitle);

		Thread.sleep(5000);
		testHelper.getInstructorClassActivityReportWindow(driver);

		Thread.sleep(5000);
		Assert.assertEquals("43%", classActivityReport.getCARAverageScoreOnAssignment(), 
				"The Average Score");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		studentDetailOverlay = new StudentDetailOverlayPage();

		// Studunet1 grade after Assignment update.

		classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton(StudentName);
		//classActivityReport.clickCARStudentsTabOpenStudentDetailOverlayButton("ACCOUNT5, TEST");
		Thread.sleep(5000);

		// Assert Overall Score in Student Details Overlay.
		String updatedStrOverallScore = studentDetailOverlay.getOverallScore();
		Assert.assertEquals(updatedStrOverallScore, "43%");

		// Assert GAU Text on Student Details Overlay.
		String updatedAssignmentInfoBoxText = "Grades were accepted until " + GetDate.getFormattedDate(-1)
				+ " at 11:59PM (Eastern Time)";
		String updatedGAUText = studentDetailOverlay.getGradeText();
		Assert.assertEquals(updatedGAUText.trim(), updatedAssignmentInfoBoxText.trim());

		studentDetailOverlay.clickCloseStudentDetailOverlay();

		// Assert GAU Text on CAR page.
		Thread.sleep(2000);
		String UpdatedGAUText_CAR = "Grades were accepted for this assignment until " 
		+ CurrentDateMinus1_CAR + ", at 11:59 PM (Eastern Time)";
		String updatedStrGAU = classActivityReport.getGAUText_CarPage();
		Assert.assertEquals(updatedStrGAU.trim(), UpdatedGAUText_CAR.trim());

		// Assert GAU Text on Instructor Assignment page.

		assignment.returnToAssignmentList.click();

		// Assert Assignment GAU Info on Instructor Assignment page.
		Thread.sleep(2000);
		String updatedLatePenaltyText_Instructor = "Grades were accepted until " 
		+ CurrentDateMinus1_Instructor + ", at 11:59 PM (Eastern Time). Late work is  accepted 1 day "
				+ "after the GAU at 50% penalty per day";
		String updatedStrLatePenaltyAlert_Instructor = sap.getGAUUntilAlert_Instructor();
		Assert.assertTrue(
				updatedLatePenaltyText_Instructor.trim().contains(updatedStrLatePenaltyAlert_Instructor.trim()),
				"Assignment GAU message is displayed correctly with Late Penalty information?");

		String headerScoreafterAssignmentUpdate = assignment.getHeaderScore();
		Assert.assertEquals("43%", headerScoreafterAssignmentUpdate, 
				"The Header Score on Instructor Assignment Page.");

		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore6);

		Thread.sleep(5000);
		SW5DLPpage.logoutSW5();

	}
	
	//End of Test cases
	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

	public void customQuestionsAnswerAssignmentStudent() throws Exception {
		custassignment.question1Answers();
		submitCustomAssignmentAnswerAndAssert("1 OF 12 QUESTIONS COMPLETED", "13%" );
		
		// Navigate to Next Question
		sqp.clickNextQuestion();
		
		custassignment.question2Answer();
		submitCustomAssignmentAnswerAndAssert("2 OF 12 QUESTIONS COMPLETED", "16%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question3Answers();
		submitCustomAssignmentAnswerAndAssert("3 OF 12 QUESTIONS COMPLETED", "26%" );
		
		sqp.clickNextQuestion();
		
		custassignment.question4Answers();
		submitCustomAssignmentAnswerAndAssert("4 OF 12 QUESTIONS COMPLETED", "35%" );

		
	}

	
	private void getClassActivityStudent(String studentName, String studentscore) throws Exception{
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		classActivityReport.clickStudentNamelink(studentName);
		Thread.sleep(1000);
		classActivityReport.getStudentOverallScore();
		Assert.assertEquals(studentscore,
				classActivityReport.getStudentOverallScore());
		classActivityReport.closeStudentDetailPage();
	}
	
	private void getClassActivityReportStudentAfterAssignmentUpdate(String studentName, String value) throws Exception{
	classActivityReport.clickStudentSearchIcon();
	classActivityReport.enterStudentName(studentName);
	classActivityReport.clickStudentNamelink(studentName);
	classActivityReport.getStudentOverallScore();
	Assert.assertEquals(value,
			classActivityReport.getStudentOverallScore());
	classActivityReport.closeStudentDetailPage();
	}
	
	private void submitCustomAssignmentAnswerAndAssert(String questionfooterText, String Score) throws Exception{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.clickSubmitAnswer();
		
		WebElement yesButton;
		boolean LetMeFinishButtonExist;
		
		try {
			
				LetMeFinishButtonExist = driver.findElement(By.xpath("//body/div/div/div/div/div/div[3]/button[2]/span[1]")).isDisplayed();
				if(LetMeFinishButtonExist == true) {
				
					yesButton = driver.findElement(By.xpath("//button[contains(text(),'YES')]"));
					yesButton.click();
				
					sqp.clickFeedbackViewSolution();
					Thread.sleep(2000);
					sqp.clickConfirmPopupViewSolutionButton();
				
			}
			
		} catch (Exception e) {
			
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ReusableMethods.questionCloseLink(driver);
			}
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sqp.getQuestionFooterLeftText();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionfooterText);
		sqp.getHeaderScore();
		Assert.assertEquals(sqp.getHeaderScore(), Score);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}
		
}