package com.wwnorton.SW5Automation.UITests.CAR;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH13HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet3;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.LogUtil;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorableDownloadCSVPDFfromCAR extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH13HW ch13HW;
	ClassActivityReportPage classActivityReport;
	CustomAssignmentQuestionSet3 customAssignment;
	AssignmentPage instructorAssignmentPage;
	TestHelper testHelper = new TestHelper();
	String assignmentTitle = "";
	StudentDetailOverlayPage sdo;

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a Instructor Assignment .csv Export")
	@Stories("SW5-3833 Instructor Assignment .csv Export Download ")
	@Test
	public void instructorDownloadCSV() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet,
		// StudentUsernames)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(1);

		SeleniumTestsContext gContext = SeleniumTestsContextManager
				.getGlobalContext();
		String studentSetID = (String) gContext
				.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername1 = (String) gContext
				.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		// String studentUsername2 = (String)
		// gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		// String studentUsername3 = (String)
		// gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_3);
		/*
		 * String studentUsername1 ="john_mercey-ewkbycbdcl@mailinator.com";
		 * String studentSetID = "152255";
		 */
		String studentName1 = testHelper.getStudentUserNameInUppercase(
				studentUsername1).trim();

		// String studentName2 =
		// testHelper.getStudentUserNameInUppercase(studentUsername2);
		// String studentName3 =
		// testHelper.getStudentUserNameInUppercase(studentUsername3);

		// Create new custom assignment & Publish
		InstructorPublishCustomAssignment instructorPublishCustomAssignment = new InstructorPublishCustomAssignment();
		assignmentTitle = instructorPublishCustomAssignment
				.updateSettingsAndPublishCustomAssignmentClassActivityReportView();

		if (assignmentTitle == null || assignmentTitle.isEmpty())
			throw new Exception(
					"CAR Custom assignment is not published in studentSetID => "
							+ studentSetID);

		// STUDENT 1
		studentSubmitAnswers(studentUsername1, "STUDENT 1");

		// STUDENT 2
		// studentSubmitAnswers(studentUsername2, "STUDENT 2");

		// STUDENT 3
		// As a student joined the class but did not started the assignment.

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();

		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		// Assert the instructor DLP page
		String averageTimeSpent = SW5DLPpage
				.getAverageTimeSpent(assignmentTitle);
		Assert.assertEquals(SW5DLPpage.getSubmittedGrades(assignmentTitle),
				"1", "Submitted grades for custom assignment "
						+ assignmentTitle + " is 1?");
		Assert.assertTrue(averageTimeSpent.compareTo("—") != 0);
		Assert.assertEquals(SW5DLPpage.getAverageGradeText(assignmentTitle),
				"100%", "Average grade for custom assignment "
						+ assignmentTitle + " is 100%?");

		// CAR STUDENTS TAB

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();

		Assert.assertTrue(classActivityReport.StudentsTab.getAttribute("class")
				.compareTo("tab-1 active") == 0,
				"Default student view is loaded in CAR");
		Assert.assertEquals(
				classActivityReport.getCARAverageScoreOnAssignment(), "100%",
				"Average Score Assignment Graph should show 100%?");
		Assert.assertEquals(
				classActivityReport.getCARStudentsCompletedTheAssignmentScore(),
				"33%", "Students completed the assignment percentage is 33%?");

		Assert.assertEquals(
				classActivityReport.getCARStudentsTabStudentScore(studentName1),
				"100%", "First student should have score 100%?");

		Assert.assertEquals(classActivityReport
				.getCARStudentsTabQuestionsAnswered(studentName1), "5/5",
				"First student should have 5/5 question answered?");

		// CSV File Validation
		classActivityReport.clickExportButton();
		classActivityReport.clickCSVorPDFButton(".CSV");
		classActivityReport.clickdownloadButton();
		String zipFilePath = System.getProperty("user.dir") + "/Downloads";
		// PDF File Download

		String destFilePath = System.getProperty("user.dir") + "/Downloads";
		ReusableMethods.processZipFile(zipFilePath, destFilePath);
		Assert.assertTrue(destFilePath.length() > 0);
		Assert.assertTrue(zipFilePath.length() > 0);
		ReusableMethods.deleteFile(zipFilePath);
		Thread.sleep(8000);
		driver.close();
		driver.switchTo().window(winHandleBefore);

		String winHandleBefore1 = driver.getWindowHandle();
		// CAR STUDENTS TAB

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport.clickExportButton();
		classActivityReport.clickCSVorPDFButton(".PDF");
		classActivityReport.clickdownloadButton();
		String PDFFilePath = System.getProperty("user.dir") + "/Downloads";
		Assert.assertTrue(PDFFilePath.length() > 0);
		// List the files on that folder
		ReusableMethods.deleteFile(PDFFilePath);
		Thread.sleep(8000);
		driver.close();
		driver.switchTo().window(winHandleBefore1);
		
		String winHandleStudentOverlay = driver.getWindowHandle();
		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);
		
		getClassActivityStudent(studentName1);
		getStudentDetailScore("100%", "5/5");
		List<WebElement> stud1GrapgPercentile = classActivityReport
				.getPieChartSlices("100%");
		LogUtil.log(stud1GrapgPercentile.toString());
		Thread.sleep(8000);
		sdo.studendetailExportbutton.click();
		classActivityReport.clickCSVorPDFButton(".PDF");
		classActivityReport.clickdownloadButton();
		String studentDetailOverlayzipPDFFilePath = System
				.getProperty("user.dir") + "\\Downloads";
		Assert.assertTrue(studentDetailOverlayzipPDFFilePath.length() > 0);
		classActivityReport.closeStudentDetailPage();
		ReusableMethods.deleteFile(studentDetailOverlayzipPDFFilePath);
		Thread.sleep(8000);
		driver.close();
		driver.switchTo().window(winHandleStudentOverlay);
		
		String winHandleStudentOverlay1 = driver.getWindowHandle();
		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);
		getClassActivityStudent(studentName1);
		sdo.studendetailExportbutton.click();
		classActivityReport.clickCSVorPDFButton(".CSV");
		classActivityReport.clickdownloadButton();
		String studentDetailOverlayzipFilePath = System.getProperty("user.dir")
				+ "\\Downloads";

		// List the files on that folder
		String studentDetailOverlaydestFilePath = System
				.getProperty("user.dir") + "/Downloads";
		ReusableMethods.processZipFile(studentDetailOverlayzipFilePath,
				studentDetailOverlaydestFilePath);
		Assert.assertTrue(studentDetailOverlaydestFilePath.length() > 0);
		Assert.assertTrue(studentDetailOverlayzipFilePath.length() > 0);
		ReusableMethods.deleteFile(studentDetailOverlayzipFilePath);
		Thread.sleep(8000);
		driver.close();		
		driver.switchTo().window(winHandleStudentOverlay1);
		
		//Export button on Question details Overlay
		String winHandlequestiondetailOverlay = driver.getWindowHandle();
		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);
		//sdo.CloseStudentDetailOverlay.click();
		classActivityReport.clickQuestionsTab();
		sdo.clickQuestionNumberonQuestionTab(0);
		Thread.sleep(8000);
		//classActivityReport.clickExportButton();
		driver.findElement(By.xpath("//button[@class='btn-primary btn-submit-s lato-regular-14 fr']/span[contains(text(),'EXPORT')]")).click();
		Thread.sleep(5000);
		classActivityReport.clickCSVorPDFButton(".CSV");
		classActivityReport.clickdownloadButton();
		String questionDetailOverlayzipFilePath = System.getProperty("user.dir")
				+ "\\Downloads";

		// List the files on that folder
		String questionDetailOverlaydestFilePath = System
				.getProperty("user.dir") + "\\Downloads";
		ReusableMethods.processZipFile(questionDetailOverlayzipFilePath,
				questionDetailOverlaydestFilePath);
		Assert.assertTrue(questionDetailOverlaydestFilePath.length() > 0);
		Assert.assertTrue(questionDetailOverlayzipFilePath.length() > 0);
		ReusableMethods.deleteFile(questionDetailOverlayzipFilePath);
		Thread.sleep(8000);
		
		driver.close();
		driver.switchTo().window(winHandlequestiondetailOverlay);
		
		Thread.sleep(1000);
		SW5DLPpage.logoutSW5();
	}

	private void studentSubmitAnswers(String studentUsername, String student)
			throws Exception {
		
		driver = getDriver();
		
		new LoginAsStudent().LoginStudent(studentUsername);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		customAssignment = new CustomAssignmentQuestionSet3();

		switch (student) {
		case "STUDENT 1":
			student1SubmitAnswers();
			break;
		/*
		 * case "STUDENT 2": student2SubmitAnswers(); break;
		 */
		}

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	private void student1SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "20%");

		sqp.clickNextQuestion();

		// Q2 - Correct in the second attempt
		// First Incorrect Attempt

		// Second Correct Attempt
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "40%");

		sqp.clickNextQuestion();

		// Q3 - Correct in the third attempt
		// First Incorrect Attempt

		// Third Correct Attempt
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "60%");

		sqp.clickNextQuestion();

		// Q4 - Correct in the fourth attempt
		// Fourth Correct Attempt
		customAssignment.question4Answers();
		submitCorrectAnswerAndAssert("4 OF 5 QUESTIONS COMPLETED", "80%");

		sqp.clickNextQuestion();

		// Q5 - Correct in the fifth attempt
		// Fifth Correct Attempt
		customAssignment.question5Answers();
		submitCorrectAnswerAndAssert("5 OF 5 QUESTIONS COMPLETED", "100%");
	}

	/*
	 * private void submitIncorrectAnswerAndClickTryAgain() throws Exception {
	 * sqp.clickSubmitAnswer();
	 * 
	 * sqp.clickFeedbackTryAgain();
	 * 
	 * Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
	 * "Submit Answer button is displayed in the footer?"); }
	 */

	private void submitCorrectAnswerAndAssert(String questionFooterLeftText,
			String score) throws Exception {
		sqp.clickSubmitAnswer();
		questionCloseLink();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(),
				questionFooterLeftText);
		Assert.assertEquals(sqp.getHeaderScore(), score);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(),
				"QUESTION COMPLETED");
	}

	private void questionCloseLink() throws Exception {
		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}
	}

	private void getClassActivityStudent(String studentName) throws Exception {
		classActivityReport.clickStudentSearchIcon();
		Thread.sleep(1000);
		classActivityReport.enterStudentName(studentName);
		Thread.sleep(1000);
		classActivityReport.clickStudentNamelink(studentName);

		Thread.sleep(1000);
		// classActivityReport.closeStudentDetailPage();
	}

	private void getStudentDetailScore(String OverallScore,
			String questioncompleted) throws Exception {
		
		driver = getDriver();
		sdo = new StudentDetailOverlayPage();
		classActivityReport.getStudentOverallScore();
		// Assert.assertEquals(value,
		// classActivityReport.getStudentOverallScore());
		Assert.assertEquals(OverallScore, sdo.getOverallScore());

		// Assert.assertTrue((sdo.getTimeSpent()!=0)||(sdo.TimeSpentData.getText().equalsIgnoreCase("-")));

		Assert.assertEquals(questioncompleted,
				sdo.getAssignedQuestionCompleted());

		System.out.println(sdo.getGradeText());
		System.out.println(sdo.getlateworkText());
		System.out.println(sdo.getStudentDetailTimeSpentData());
		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions column should show the 5 questions?");
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
