
package com.wwnorton.SW5Automation.UITests.CAR;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet5;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.QuestionDetailOverlayPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class ValidateOpenResponseQuestionType extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	ClassActivityReportPage classActivityReport;
	QuestionDetailOverlayPage questionDetailOverlay;
	CustomAssignmentQuestionSet5 customAssignment;
	AssignmentPage instructorAssignmentPage;
	TestHelper testHelper = new TestHelper();
	String assignmentTitle = "";

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-65 Custom Assignment - Open Response Question Type")
	@Test(priority = 0)
	public void validateOpenResponseQuestionType() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String studentName1 = testHelper.getStudentUserNameInUppercase(studentUsername);

		// Create new custom assignment & Publish
		InstructorPublishCustomAssignment instructorPublishCustomAssignment = new InstructorPublishCustomAssignment();
		assignmentTitle = instructorPublishCustomAssignment.updateSettingsAndPublishOpenResponseQuestion();

		// assignmentTitle = "Custom OpenResponse pqh5m";

		if (assignmentTitle == null || assignmentTitle.isEmpty())
			throw new Exception("CAR Custom assignment is not published in studentSetID => " + studentSetID);

		// STUDENT
		studentSubmitAnswers();

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();

		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		// Assert the instructor DLP page
		String averageTimeSpent = SW5DLPpage.getAverageTimeSpent(assignmentTitle);
		Assert.assertEquals(SW5DLPpage.getSubmittedGrades(assignmentTitle), "1",
				"Submitted grades for custom assignment " + assignmentTitle + " is 1?");
		Assert.assertTrue(averageTimeSpent.compareTo("—") != 0);
		Assert.assertEquals(SW5DLPpage.getAverageGradeText(assignmentTitle), "80%",
				"Average grade for custom assignment " + assignmentTitle + " is 80%?");

		// CAR STUDENTS TAB

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();

		Assert.assertEquals(classActivityReport.getCARAverageScoreOnAssignment(), "80%",
				"Average Score Assignment Graph should show 80%?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabStudentScore(studentName1), "80%",
				"First student should have score 80%?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabQuestionsAnswered(studentName1), "5/5",
				"First student should have 5/5 question answered?");

		// CAR QUESTIONS TAB

		classActivityReport.clickQuestionsTab();

		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions Tab should show the 5 questions?");

		List<String> questionsTabAverageScores = classActivityReport.getCARQuestionsTabAverageScoresList();

		if (questionsTabAverageScores.size() != 5) {
			throw new Exception("CAR Questions Tab is not having details for 5 questions.");
		}

		// Assert the questions average score is "Needs Grading" for 1st question and
		// 3/3pts for remaining all 4 questions
		Assert.assertEquals(questionsTabAverageScores.get(0), "Needs Grading",
				"Questions average score is \"Needs Grading\" for 1 question?");
		for (int i = 1; i < 5; i++) {
			Assert.assertEquals(questionsTabAverageScores.get(i), "3/3pts",
					"Questions average score is \"3/3pts\" for remaining all 4 questions?");
		}

		classActivityReport.clickCARQuestionsTabOpenQuestionDetailOverlayButton(1);
		Thread.sleep(5000);
		questionDetailOverlay = new QuestionDetailOverlayPage();
		Thread.sleep(5000);

		questionDetailOverlay.enterScoreQuestionDetailOverlayPopup(1, "3");

		questionDetailOverlay.clickQuestionDetailSaveButton();

		questionDetailOverlay.clickCloseQuestionDetailOverlay();

		classActivityReport = new ClassActivityReportPage();

		Assert.assertEquals(classActivityReport.getCARAverageScoreOnAssignment(), "100%",
				"Average Score Assignment Graph should show 100%?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabStudentScore(studentName1), "100%",
				"First student should have score 100%?");

		// CAR QUESTIONS TAB

		classActivityReport.clickQuestionsTab();

		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions Tab should show the 5 questions?");

		questionsTabAverageScores = classActivityReport.getCARQuestionsTabAverageScoresList();

		if (questionsTabAverageScores.size() != 5) {
			throw new Exception("CAR Questions Tab is not having details for 5 questions.");
		}

		// Assert the questions average score is 3/3pts for all 5 questions
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(questionsTabAverageScores.get(i), "3/3pts",
					"Questions average score is \"3/3pts\" for all 5 questions?");
		}

		driver.close();
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(1000);
		SW5DLPpage.logoutSW5();

		// STUDENT
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		Assert.assertTrue(sap.isReviewAssignmentButtonDisplayed(), "Review Assignment button is displayed?");
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "100%");
		Assert.assertEquals(sap.getQuestionStatus(1), "Completed");
		Assert.assertEquals(sap.getQuestionStatus(2), "Completed");

		sap.clickQuestion(1);
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		Assert.assertEquals(sqp.getQuestionFooterLeftText(), "5 OF 5 QUESTIONS COMPLETED");
		Assert.assertEquals(sqp.getHeaderScore(), "100%");
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	private void studentSubmitAnswers() throws Exception {
		new LoginAsStudent().LoginStudent();

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		customAssignment = new CustomAssignmentQuestionSet5();

		student1SubmitAnswers();

		// Navigate to Assignment Page
		sqp.clickHeaderTitle(sap);
		Thread.sleep(5000);

		Assert.assertEquals(sap.getHeaderScore(), "80%");
		Assert.assertEquals(sap.getQuestionStatus(1), "Submitted");
		Assert.assertEquals(sap.getQuestionStatus(2), "Completed");

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	private void student1SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("0%");

		sqp.clickNextQuestion();

		// Q2 - Correct answer
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("20%");

		sqp.clickNextQuestion();

		// Q3 - Correct answer
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("40%");

		sqp.clickNextQuestion();

		// Q4 - Correct answer
		customAssignment.question4Answers();
		submitCorrectAnswerAndAssert("60%");

		sqp.clickNextQuestion();

		// Q5 - Correct answer
		customAssignment.question5Answers();
		submitCorrectAnswerAndAssert("80%");
	}

	private void submitCorrectAnswerAndAssert(String score) throws Exception {
		sqp.clickSubmitAnswer();
		questionCloseLink();
		Assert.assertEquals(sqp.getHeaderScore(), score);
	}

	private void questionCloseLink() throws Exception {
		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
