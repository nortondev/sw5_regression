
package com.wwnorton.SW5Automation.UITests.CAR;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsStudent;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentCH13HW;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.ClassActivityReportPage;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet3;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.objectFactory.StudentAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.StudentQuestionPlayer;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorValidateClassActivityReportView extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	StudentAssignmentPage sap;
	StudentQuestionPlayer sqp;
	AssignmentCH13HW ch13HW;
	ClassActivityReportPage classActivityReport;
	CustomAssignmentQuestionSet3 customAssignment;
	AssignmentPage instructorAssignmentPage;
	TestHelper testHelper = new TestHelper();
	String assignmentTitle = "";

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("As a student work with an assignment - student assignment & question player validation scenarios")
	@Stories("AS-61 Custom Assignment - Class Activity Report Default View")
	@Test(priority = 0)
	public void instructorValidateClassActivityReportView() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet,
		// StudentUsernames)
		testHelper.checkPrerequisiteInGlobalContextMultipleStudents(3);

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);
		String studentUsername1 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_1);
		String studentUsername2 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_2);
		String studentUsername3 = (String) gContext.getAttribute(SW5Constants.STUDENT_USER_NAME_3);

		String studentName1 = testHelper.getStudentUserNameInUppercase(studentUsername1);
		String studentName2 = testHelper.getStudentUserNameInUppercase(studentUsername2);
		String studentName3 = testHelper.getStudentUserNameInUppercase(studentUsername3);

		// Create new custom assignment & Publish
		InstructorPublishCustomAssignment instructorPublishCustomAssignment = new InstructorPublishCustomAssignment();
		assignmentTitle = instructorPublishCustomAssignment
				.updateSettingsAndPublishCustomAssignmentClassActivityReportView();

		if (assignmentTitle == null || assignmentTitle.isEmpty())
			throw new Exception("CAR Custom assignment is not published in studentSetID => " + studentSetID);

		// STUDENT 1
		studentSubmitAnswers(studentUsername1, "STUDENT 1");

		// STUDENT 2
		studentSubmitAnswers(studentUsername2, "STUDENT 2");

		// STUDENT 3
		// As a student joined the class but did not started the assignment.

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();

		String winHandleBefore = driver.getWindowHandle();

		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		// Assert the instructor DLP page
		String averageTimeSpent = SW5DLPpage.getAverageTimeSpent(assignmentTitle);
		Assert.assertEquals(SW5DLPpage.getSubmittedGrades(assignmentTitle), "2",
				"Submitted grades for custom assignment " + assignmentTitle + " is 2?");
		Assert.assertTrue(averageTimeSpent.compareTo("—") != 0);
		Assert.assertEquals(SW5DLPpage.getAverageGradeText(assignmentTitle), "80%",
				"Average grade for custom assignment " + assignmentTitle + " is 80%?");

		// CAR STUDENTS TAB

		SW5DLPpage.clickReportsButton(assignmentTitle);

		// Instructor CAR Window
		testHelper.getInstructorClassActivityReportWindow(driver);

		classActivityReport = new ClassActivityReportPage();

		Assert.assertTrue(classActivityReport.StudentsTab.getAttribute("class").compareTo("tab-1 active") == 0,
				"Default student view is loaded in CAR");
		Assert.assertEquals(classActivityReport.getCARAverageScoreOnAssignment(), "80%",
				"Average Score Assignment Graph should show 80%?");
		Assert.assertEquals(classActivityReport.getCARStudentsCompletedTheAssignmentScore(), "33%",
				"Students completed the assignment percentage is 33%?");
		Assert.assertEquals(classActivityReport.getTotalStudentsCount(), 3,
				"Students Tab should show the names of 3 students?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabStudentScore(studentName1), "100%",
				"First student should have score 100%?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabStudentScore(studentName2), "60%",
				"Second student should have score 60%?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabStudentScore(studentName3), "-",
				"Third student should show \"-\"?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabQuestionsAnswered(studentName1), "5/5",
				"First student should have 5/5 question answered?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabQuestionsAnswered(studentName2), "3/5",
				"Second student should have 3/5 question answered?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabQuestionsAnswered(studentName3), "-",
				"Third student should show \"-\"?");

		Assert.assertEquals(classActivityReport.getCARStudentsTabIncorrectAttempts(studentName1), "10",
				"First student should have 10 incorrect attempt?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabIncorrectAttempts(studentName2), "3",
				"Second student should have 3 incorrect attempt?");
		Assert.assertEquals(classActivityReport.getCARStudentsTabIncorrectAttempts(studentName3), "-",
				"Third student should show \"-\"?");

		List<WebElement> pieChartSlices = classActivityReport.getPieChartSlices("33%");

		Assert.assertEquals(pieChartSlices.size(), 3,
				"The completed graph should show 33% as completed, 33% as Not Started, 33% as IN progress.?");

		// CAR QUESTIONS TAB

		classActivityReport.clickQuestionsTab();

		Assert.assertEquals(classActivityReport.getTotalQuestionsCount(), 5,
				"Questions Tab should show the 5 questions?");

		List<String> questionsTabAverageScores = classActivityReport.getCARQuestionsTabAverageScoresList();
		List<String> questionsTabStudentSubmissions = classActivityReport.getCARQuestionsTabStudentSubmissionsList();
		List<String> questionsTabAverageAttempts = classActivityReport.getCARQuestionsTabAverageAttemptsList();
		List<String> questionsTabAverageTimeSpentList = classActivityReport.getCARQuestionsTabAverageTimeSpentList();

		if (questionsTabAverageScores.size() != 5 || questionsTabStudentSubmissions.size() != 5
				|| questionsTabAverageAttempts.size() != 5 || questionsTabAverageTimeSpentList.size() != 5) {
			throw new Exception("CAR Questions Tab is not having details for 5 questions.");
		}

		// Assert the questions average score is 3/3pts for all 5 questions
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(questionsTabAverageScores.get(i), "3/3pts",
					"Questions average score is \"3/3pts\" for all 5 questions?");
		}

		// Assert the questions student submission is 2 for first 3 questions
		for (int i = 0; i < 3; i++) {
			Assert.assertEquals(questionsTabStudentSubmissions.get(i), "2",
					"Questions student submission is 2 for first 3 questions?");
		}

		// Assert the questions student submission is 1 for last 2 questions
		for (int i = 3; i < 5; i++) {
			Assert.assertEquals(questionsTabStudentSubmissions.get(i), "1",
					"Questions student submission is 1 for last 2 questions?");
		}

		// Assert the questions average attempts is 1/Unlimited to 5/Unlimited for all 5
		// questions
		for (int i = 0; i < 5; i++) {
			Assert.assertEquals(questionsTabAverageAttempts.get(i), (i + 1) + "/Unlimited");
		}

		// Assert the questions average attempts is 1/Unlimited to 5/Unlimited for all 5
		// questions

		for (int i = 0; i < 5; i++) {
			String timeSpent = questionsTabAverageTimeSpentList.get(i);
			Assert.assertTrue(timeSpent.compareTo("00:00:00") != 0);
			Assert.assertTrue(timeSpent.compareTo("-") != 0);
			Assert.assertTrue(timeSpent.compareTo("") != 0);
		}

		driver.close();
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(1000);
		SW5DLPpage.logoutSW5();
	}

	private void studentSubmitAnswers(String studentUsername, String student) throws Exception {
		new LoginAsStudent().LoginStudent(studentUsername);

		// Select mentioned Assignment
		SW5DLPstudent = new SW5DLPStudent();

		String parentWindow = driver.getWindowHandle();

		SW5DLPstudent.SelectAssignment(assignmentTitle);
		Thread.sleep(1000);

		// Student Assignment Window
		testHelper.getStudentAssignmentWindow(driver, parentWindow);

		sap = new StudentAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(sap.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertEquals(sap.getHeaderScore(), "-- %");

		sap.beginAssignmentClick();
		Thread.sleep(5000);

		sqp = new StudentQuestionPlayer();

		customAssignment = new CustomAssignmentQuestionSet3();

		switch (student) {
		case "STUDENT 1":
			student1SubmitAnswers();
			break;
		case "STUDENT 2":
			student2SubmitAnswers();
			break;
		}

		testHelper.logoutSmartwork5(driver, parentWindow, SW5DLPstudent);
	}

	private void student1SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "20%");

		sqp.clickNextQuestion();

		// Q2 - Correct in the second attempt
		// First Incorrect Attempt
		customAssignment.question2IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Correct Attempt
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "40%");

		sqp.clickNextQuestion();

		// Q3 - Correct in the third attempt
		// First Incorrect Attempt
		customAssignment.question3IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Correct Attempt
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "60%");

		sqp.clickNextQuestion();

		// Q4 - Correct in the fourth attempt
		// First Incorrect Attempt
		customAssignment.question4IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fourth Correct Attempt
		customAssignment.question4Answers();
		submitCorrectAnswerAndAssert("4 OF 5 QUESTIONS COMPLETED", "80%");

		sqp.clickNextQuestion();

		// Q5 - Correct in the fifth attempt
		// First Incorrect Attempt
		customAssignment.question5IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fourth Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Fifth Correct Attempt
		customAssignment.question5Answers();
		submitCorrectAnswerAndAssert("5 OF 5 QUESTIONS COMPLETED", "100%");
	}

	private void student2SubmitAnswers() throws Exception {
		// Q1 - Correct in first attempt
		// First Correct Attempt
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("1 OF 5 QUESTIONS COMPLETED", "20%");

		sqp.clickNextQuestion();

		// Q2 - Correct in the second attempt
		// First Incorrect Attempt
		customAssignment.question2IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Correct Attempt
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("2 OF 5 QUESTIONS COMPLETED", "40%");

		sqp.clickNextQuestion();

		// Q3 - Correct in the third attempt
		// First Incorrect Attempt
		customAssignment.question3IncorrectAnswers();
		submitIncorrectAnswerAndClickTryAgain();

		// Second Incorrect Attempt
		submitIncorrectAnswerAndClickTryAgain();

		// Third Correct Attempt
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("3 OF 5 QUESTIONS COMPLETED", "60%");

		// Q4 - No Submission

		// Q5 - No Submission

	}

	private void submitIncorrectAnswerAndClickTryAgain() throws Exception {
		sqp.clickSubmitAnswer();
		Thread.sleep(3000);
		sqp.clickFeedbackTryAgain();
		Thread.sleep(3000);
		Assert.assertEquals(sqp.isSubmitAnswerButtonDisplayed(), true,
				"Submit Answer button is displayed in the footer?");
	}

	private void submitCorrectAnswerAndAssert(String questionFooterLeftText, String score) throws Exception {
		sqp.clickSubmitAnswer();
		questionCloseLink();
		Assert.assertEquals(sqp.getQuestionFooterLeftText(), questionFooterLeftText);
		Assert.assertEquals(sqp.getHeaderScore(), score);
		Assert.assertEquals(sqp.getQuestionCompletedStatusText(), "QUESTION COMPLETED");
	}

	private void questionCloseLink() throws Exception {
		if (sqp.isFeedbackOverlayDisplayed()) {
			sqp.clickFeedbackClose();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
