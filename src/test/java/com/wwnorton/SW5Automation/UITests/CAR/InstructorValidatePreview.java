
package com.wwnorton.SW5Automation.UITests.CAR;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.SW5Automation.UITests.Loginfeatures.LoginAsInstructor;
import com.wwnorton.SW5Automation.UITests.PublishPremadeAssignments.InstructorPublishCustomAssignment;
import com.wwnorton.SW5Automation.UITests.Setup.TestHelper;
import com.wwnorton.SW5Automation.objectFactory.AssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.CustomAssignmentQuestionSet3;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewAssignmentPage;
import com.wwnorton.SW5Automation.objectFactory.InstructorPreviewQuestionPlayer;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPStudent;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadUIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.SW5Constants;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })
public class InstructorValidatePreview extends PropertiesFile {
	/*WebDriver driver = BaseDriver.getDriver();*/
	LoginPage SW5Login;
	SW5DLPPage SW5DLPpage;
	SW5DLPStudent SW5DLPstudent;
	InstructorPreviewAssignmentPage instPreviewAssignmentPage;
	InstructorPreviewQuestionPlayer instPreviewQuestionPlayer;
	CustomAssignmentQuestionSet3 customAssignment;
	AssignmentPage instructorAssignmentPage;
	TestHelper testHelper = new TestHelper();
	String assignmentTitle = "";

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJasonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.CRITICAL)
	@Description("Instructor Preview ")
	@Stories("AS-70 Instructor Preview")
	@Test
	public void validateInstructorPreview() throws Exception {

		driver = getDriver();
		
		// Checking global context for Prerequisite values (StudentSet, StudentUsername)
		testHelper.checkPrerequisiteInGlobalContext();

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String studentSetID = (String) gContext.getAttribute(SW5Constants.STUDENT_SET_ID);

		// Create new custom assignment & Publish
		InstructorPublishCustomAssignment instructorPublishCustomAssignment = new InstructorPublishCustomAssignment();
		assignmentTitle = instructorPublishCustomAssignment.updateSettingsAndPublishCustomAssignmentInstructorPreview();

//		assignmentTitle = "CustomAssignment InstPreview kj19s";

		if (assignmentTitle == null || assignmentTitle.isEmpty())
			throw new Exception(
					"InstructorPreview Custom assignment is not published in studentSetID => " + studentSetID);

		// INSTRUCTOR
		new LoginAsInstructor().loginInstructor();

		SW5DLPpage = new SW5DLPPage();
		SW5DLPpage.selectByPartOfVisibleText(studentSetID);

		SW5DLPpage.ClickAssignmentlink(assignmentTitle);

		String winHandleBefore = driver.getWindowHandle();
		testHelper.getInstructorAssignmentWindow(driver);

		instructorAssignmentPage = new AssignmentPage();
		instructorAssignmentPage.editAssignmentbutton();

		ReusableMethods.checkPageIsReady(driver);

		Assert.assertTrue(instructorAssignmentPage.isPreviewButtonDisplayed(), "Preview Logo is displayed?");

		String parentWindow = driver.getWindowHandle();

		instructorAssignmentPage.clickPreviewButton();

		// Instructor Preview Assignment Window
		testHelper.getInstructorPreviewAssignmentWindow(driver, winHandleBefore, parentWindow);

		instPreviewAssignmentPage = new InstructorPreviewAssignmentPage();

		// # Use case: Verify student assignment page - first time
		Assert.assertTrue(instPreviewAssignmentPage.isBeginAssignmentButtonDisplayed());
		Thread.sleep(5000);
		Assert.assertTrue(instPreviewAssignmentPage.isPreviewIconDisplayed(), "Preview Logo is displayed?");
		Assert.assertEquals(instPreviewAssignmentPage.getHeaderScore(), "-- %");

		instPreviewAssignmentPage.beginAssignmentClick();
		Thread.sleep(5000);

		instPreviewQuestionPlayer = new InstructorPreviewQuestionPlayer();
		customAssignment = new CustomAssignmentQuestionSet3();

		// Q1 - Correct answer
		customAssignment.question1Answers();
		submitCorrectAnswerAndAssert("20%");

		instPreviewQuestionPlayer.clickNextQuestion();

		// Q2 - Correct answer
		customAssignment.question2Answers();
		submitCorrectAnswerAndAssert("40%");

		instPreviewQuestionPlayer.clickNextQuestion();

		// Q3 - Correct answer
		customAssignment.question3Answers();
		submitCorrectAnswerAndAssert("60%");

		instPreviewQuestionPlayer.clickNextQuestion();

		// Q4 - Correct answer
		customAssignment.question4Answers();
		submitCorrectAnswerAndAssert("80%");

		instPreviewQuestionPlayer.clickNextQuestion();

		// Q5 - Correct answer
		customAssignment.question5Answers();
		submitCorrectAnswerAndAssert("100%");

		// Navigate to Assignment Page
		instPreviewQuestionPlayer.clickHeaderTitle(instPreviewAssignmentPage);
		Thread.sleep(5000);

		Assert.assertEquals(instPreviewAssignmentPage.getHeaderScore(), "100%");
		Assert.assertTrue(instPreviewAssignmentPage.isReviewAssignmentButtonDisplayed(),
				"Review Assignment button displayed");

		driver.close();
		driver.switchTo().window(parentWindow);
		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(1000);
		SW5DLPpage.logoutSW5();

	}

	private void submitCorrectAnswerAndAssert(String score) throws Exception {
		instPreviewQuestionPlayer.clickSubmitAnswer();
		questionCloseLink();
		Assert.assertEquals(instPreviewQuestionPlayer.getHeaderScore(), score);
	}

	private void questionCloseLink() throws Exception {
		if (instPreviewQuestionPlayer.isFeedbackOverlayDisplayed()) {
			instPreviewQuestionPlayer.clickFeedbackClose();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
