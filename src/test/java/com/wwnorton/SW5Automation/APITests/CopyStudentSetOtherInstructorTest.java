package com.wwnorton.SW5Automation.APITests;

import java.util.Map;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;
import com.wwnorton.SW5Automation.APIUtils.ApiUtils;
import com.wwnorton.SW5Automation.APIUtils.ReusableAPIMethods;
import com.wwnorton.SW5Automation.objectFactory.LoginPage;
import com.wwnorton.SW5Automation.objectFactory.SW5DLPPage;
import com.wwnorton.SW5Automation.utilities.BaseDriver;
import com.wwnorton.SW5Automation.utilities.PropertiesFile;
import com.wwnorton.SW5Automation.utilities.ReadAPIJsonFile;
import com.wwnorton.SW5Automation.utilities.ReusableMethods;
import com.wwnorton.SW5Automation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({TestListener.class })


public class CopyStudentSetOtherInstructorTest extends PropertiesFile {

private JsonPath jp = null; //JsonPath
    
    //Call to Read JSON file to get request data.
    
	ReadAPIJsonFile readJasonObject = new ReadAPIJsonFile();
	JsonObject jsonObj = readJasonObject.readAPIJason();
 
    /*
    	Do setup operations, get JSON response from the API and put it into JsonPath object
    	Then we will do query and manipulations with JsonPath class’s methods.
    */
    
	// TestNG Annotations
	@Parameters({ "browser" })
	@BeforeTest
	
	//Test Setup
    public void setUp (String browser) throws Exception 
	{
		//Call to Properties file to read base URI and base Path from Config file.
		PropertiesFile.readPropertiesFile();
		
		//Setup Base URI
		PropertiesFile.setBaseURI();
		
		//Setup Server Port
		PropertiesFile.setServerPort();
		
		 //Setup Base Path
		PropertiesFile.setBasePath("/api/instructor/studentSet/copy");
        
    }
 
	
	// Allure  Annotations

	@Severity(SeverityLevel.CRITICAL)
	@Description("Copy Student Set from Other Instructor validation")
	@Stories("Copy Student Set from Other Instructor")
	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
    public void ValidateCopyStudentSetOtherInstructor(String browser) throws Exception 
	{	
		
		try {
		//Create a Request pointing to the Service End point
		RequestSpecification request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 requestParams.put("copyStudentSetId", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("copyStudentSetId").getAsString());
		 requestParams.put("studentSetTitle", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("studentSetTitle").getAsString()); 
		 requestParams.put("schoolName", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("schoolName").getAsString()); 
		 requestParams.put("startDate", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("startDate").getAsString());
		 requestParams.put("endDate", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("endDate").getAsString());
		 requestParams.put("enrollDayslimit", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("enrollDayslimit").getAsInt());
		 requestParams.put("isSchoolIdRequired", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("isSchoolIdRequired").getAsString());
		 requestParams.put("schoolIdText", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("schoolIdText").getAsString());
		 requestParams.put("includeGAU", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("includeGAU").getAsString());
		 requestParams.put("testBaseUrl", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("testBaseUrl").getAsString());
		 requestParams.put("productCode", jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorRequest").get("productCode").getAsString());

		// Set a header stating the Request body is a JSON
		//ApiUtils.setContentType(ContentType.JSON);
		 request.header("Content-Type", "application/json");
		
		// Add the JSON to the body of the request
		request.body(requestParams.toJSONString());
		 
		
		// Post the request and check the response
		Response response = request.post("/otherInstructor");
		
		// Retrieve the body of the Response
		 ResponseBody repbody = response.getBody();
		 
		 // By using the ResponseBody.asString() method, we can convert the  body
		 // into the string representation.
		 System.out.println("Response Body is: " + repbody.asString());
		
		//Verify the http response status returned. Check Status Code is 200?
		ReusableAPIMethods.checkStatusIs200(response);
       
		//Set JsonPath
		jp = ApiUtils.getJsonPath(response);
		
		//Read the expected API Response values from JSON file.
		JsonObject jsonResObj = jsonObj.getAsJsonObject("CopyStudentSetOtherInstructorResponse");
		
		String strStudentSetTitle = jsonResObj.getAsJsonPrimitive("studentSetTitle").getAsString();
		int jsonCountOfAssignments = jsonResObj.getAsJsonPrimitive("numberOfAssignment").getAsInt();
		
		//Check the Response Results with JSON expected values.
		Assert.assertEquals(jp.get("studentSetTitle"), strStudentSetTitle);
		Assert.assertEquals(jp.get("numberOfAssignment").toString(), String.valueOf(jsonCountOfAssignments));
		
		JsonArray jsonArrayObj = jsonResObj.getAsJsonArray("assignment");
		
		JsonObject jsonTitleObj = new JsonObject();
		int matchCounter = 0;
	  
			 for (int i=0; i<jsonArrayObj.size(); i++) 
			 {
				 Map<Object, Object> resAssignment = jp.getMap("assignment[" + i + "]");
				 String resTitle = resAssignment.get("title").toString();
				 

				 for (int j=0; j<jsonArrayObj.size(); j++) 
				 { 
					 jsonTitleObj = jsonArrayObj.get(j).getAsJsonObject();
					 String jsonStrTitle = jsonTitleObj.get("title").getAsString();
			  
					 if (resTitle.equals(jsonStrTitle))
					 { 
						 
						 matchCounter++;
						 break;
						 
					 }
			  
				 }
			  
			 }
			 
			 
		Assert.assertEquals(matchCounter, jsonArrayObj.size(), "All Assignment Titles in API Response are matching correctly?");
		
		
		//Reset RestAssured
		RestAssured.reset();
		
		} catch (Exception e) {
			
			throw new Exception("Error: API Script Failed " + e.getMessage());
		}
		
		try {
			
		//Invoking Browser and setting Test URL via config file.
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
						
		//Calling SW5 application login method.
		
		ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
		
		String instructorUserName = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
		String instructorPassword = jsonObj.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();

		LoginPage login = PageFactory.initElements(driver, LoginPage.class);
		login.loginSW5(instructorUserName, instructorPassword);
		
		ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
		
		login.clickSW5Icon();
		
		ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
		
		login.clickOKButton();
		
		ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
		
		//Validate and Select newly created student set from Student Set drop down.
		SW5DLPPage dlp = PageFactory.initElements(driver, SW5DLPPage.class);
		dlp.selectSSByTitle(jp.get("studentSetDisplayTitle"));
		int count = dlp.assignmentRowCount();
		
		ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
				
		//Validating Number of Assignment with JSON Response.
		Assert.assertEquals(String.valueOf(count), jp.get("numberOfAssignment").toString());
				
		
		Thread.sleep(5000);
		
		
		//Verify that all assignment titles in UI are matching exactly with API Response - assignment titles.
		WebElement AssignmentTitle;
		int titleCounter = 0;
		
			for (int i=1; i<count; i++)
			{
				AssignmentTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
			
					for (int j=0; j<count; j++) 
					{
						//String apiResTitle = jp.get("assignmentTitles[" + j + "]");
						Map<Object, Object> apiResAssignment = jp.getMap("assignment[" + j + "]");
						String apiResTitle = apiResAssignment.get("title").toString();
					
						if(AssignmentTitle.getText().equals(apiResTitle))
						{
							titleCounter++;
							break;
						}
					
					}

			}
			
			
		//Assert.assertEquals(titleCounter, count, "All UI Assignment Titles are matching correctly?");

			
		//Verify that the Adaptive assignment in UI is matching with API Response - Adaptive value.
		WebElement adptiveTitle;
		
		 for (int i=1; i<count; i++) 
		 {
			adptiveTitle = driver.findElement(By.xpath("//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/a"));
			boolean isAdaptive = ReusableMethods.elementExist(BaseDriver.getDriver(), "//*[@id=\"activity_list_table\"]/tbody/tr[" + i + "]/td[1]/img");

			for (int j=0; j<count; j++)
			{	
				
				Map<Object, Object> resAssignment = jp.getMap("assignment[" + j + "]");
				String adaptiveStrValue = resAssignment.get("isAdaptive").toString();
				String apiResTitle = resAssignment.get("title").toString();
				 
				if(isAdaptive && adptiveTitle.getText().equals(apiResTitle))
					{		
						Assert.assertEquals(adaptiveStrValue, "ADAPTIVE");
						System.out.println("Adaptive Assignment in API matches in UI: " + adptiveTitle.getText());	
					}
				
				else if(!isAdaptive && adptiveTitle.getText().equals(apiResTitle)) 
					{
					
						Assert.assertEquals(adaptiveStrValue, "NON_ADPATIVE");
					
					}
					
			}
	
		}

	} catch (Exception e) {

				throw new Exception("Error: UI Script Failed " + e.getMessage());

			}
	}



		 
		@AfterTest
		    
		public void afterTest () throws Exception
			{
			
			try {
				
				PropertiesFile.tearDownTest();
				
				} catch (NullPointerException e) {
				
					throw new NullPointerException("Error: UI Script Failed " + e.getMessage());
				
				}
		    
			}

}